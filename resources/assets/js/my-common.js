$(document).ready(function() {
    //shop
    $(function () {
                   $('#datetimepicker1').datetimepicker(
                       {
                           format: 'DD/MM/YYYY'
                       }
                   );
                 
               });
   
    $(".btn-order-dropdown.dropdown-menu").on('click', 'li a', function(){
         $(this).parent().parent().siblings(".btn:first-child").html('<span class="mg-right-10" style="font-size:20px;">'+$(this).text()+'</span> <span class="caret"></span>');
         $(this).parent().parent().siblings(".btn:first-child").val($(this).text());
     });
     $(".btn-location-point.dropdown-menu").on('click', 'li a', function(){
         $(this).parent().parent().siblings(".btn:first-child").html('<span class="mg-right-10 text-location-point">'+$(this).text()+'</span><span style="font-size:5px;" class="glyphicon glyphicon-menu-down menu-down-location-point grey-font"></span>');
         $(this).parent().parent().siblings(".btn:first-child").val($(this).text());
     });

     //image-slider
     $('.venobox').venobox({
        titleattr: 'data-title',
        numeratio: true
    }); 


   });