<div class="mg-top-40"></div>
<div class="top-25">
    <div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mg-top-20" v-for="buy in buyers">
            <div class="rating-box">
                <div class="row no-margin rating-header cancel-header">
                    <div class="col-xs-12 col-sm-3 ">
                        <div class="like-image  buyer-profile">
                            <img class="buyer-image-rating " src="{{ Cookie::get('profile_img_profile') }}">
                        </div>
                        <div class="text-left" style="color:white;">
                            @{{ profile.display_name }}
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-9">
                        <h2 style="color:white;">สินค้า : @{{ buy.title }}</h2>
                    </div>

                    <div v-if="buy.status == 0" class="col-xs-9 col-sm-9" style="margin-top: -50px; position: relative;">
                        <span class="pull-right" style="color: #41ace8; font-size: 25px;">ยกเลิกแล้ว</span>
                    </div>
                    <div v-if="buy.status == 2" class="col-xs-9 col-sm-9" style="margin-top: -50px; position: relative;">
                        <img class="like-image pull-right" src="{{asset('image/app/confirm_icon.png')}}">
                    </div>
                </div>

                <div v-if="buy.status == 1">
                    <button class="btn-re-cancel pull-right re-model cancel-btn" @click="cancleOrder(buy)">
                        <span>ยกเลิกการซื้อ</span>
                    </button>

                </div>

                <div class="row no-margin">
                    <div class="col-xs-12" style="margin-top: -10px;">
                        <h2 >ยอดชำระ <span class="rating-price blue-font">@{{ buy.total_amount }}</span> บาท </h2>
                    </div>
                    <div class="col-xs-12">
                        <p >รายการสั่งซื้อ</p>
                        <p class="blue-font"> @{{ buy.orderdetail.length }} รายการ
                            <span v-for="(order,index) in buy.orderdetail">
                                @{{ order.name }} @{{ order.quantity }} <span v-if="index != buy.orderdetail.length-1">,</span>
                            </span>
                        </p>
                    </div>
                </div>
                <div class="light-grey-border" style="width:96%;"></div>
                <div class="row no-margin">
                    <div class="col-xs-12  mg-top-10">
                        <p>สถานที่รับสินค้า: <span class="blue-font"> @{{ buy.location_name }}</span></p>
                        <p>จุดนัดรับสินค้า: <span class="blue-font"> @{{ buy.location_point }}</span></p>
                        <p>วันที่: <span class="blue-font"> @{{ moment(buy.date_time).format('lll') }}</span> น.</p>
                    </div>
                </div>
            </div> <!-- rating-box-->
        </div>


        {{--@foreach($all_items as $key => $val)--}}
            {{--@if($val['action'] == 'buy')--}}
                {{--<div class="col-xs-12 col-sm -12 col-md-6 col-lg-6 mg-top-20">--}}
                    {{--<div class="rating-box">--}}
                        {{--<div class="row no-margin rating-header ">--}}
                            {{--<div class="col-xs-12 col-sm-3 blue-font">--}}
                                {{--<div class="like-image  buyer-profile">--}}
                                    {{--<img class="buyer-image-rating " src="{{$val['customer_image']}}">--}}
                                {{--</div>--}}
                                {{--<div class="text-left">--}}
                                    {{--{{$val['customer_name']}}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-9 col-sm-7 blue-font">--}}
                                {{--<h2>สินค้า : {{$val['goods_name']}}</h2>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-3 col-sm-2">--}}
                                {{--<img class="like-image pull-right" src="{{asset('image/app/like.png')}}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="test" id="rating-{{$val['id']}}">--}}
                            {{--<button class="yellow-capsule-blue-font pull-right rating-btn">RATING</button>--}}
                            {{--<!-- <button class="buyer-star-btn pull-right rating-btn">--}}
                                {{--<div class="rateYo1"></div>--}}
                            {{--</button> -->--}}

                        {{--</div>--}}
                        {{--<div class="row no-margin">--}}
                            {{--<div class="col-xs-12 grey-font" style="margin-top: -10px;">--}}
                                {{--<h2 >ยอดชำระ <span class="rating-price ">{{$val['price']}}</span> บาท </h2>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-12 grey-font">--}}
                                {{--<p >รายการสั่งซื้อ</p>--}}
                                {{--<p> {{$val['total']}} รายการ {{$val['description']}}</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="light-grey-border" style="width:96%;"></div>--}}
                        {{--<div class="row no-margin">--}}
                            {{--<div class="col-xs-12 grey-font mg-top-10">--}}
                                {{--<p>สถานที่รับสินค้า: {{$val['place']}}</p>--}}
                                {{--<p>จุดนัดรับสินค้า: {{$val['meeting_point']}}</p>--}}
                                {{--<p>วันที่: {{$val['date']}} เวลา: {{$val['time']}} น.</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div> <!-- rating-box-->--}}
                {{--</div>--}}
            {{--@elseif($val['action'] == 'cancel' || $val['action'] == 'confirm')--}}
            {{--@endif--}}

        {{--@endforeach--}}


            {{--@component('components/history/history_rating', ['all_items' =>--}}
            {{--[--}}
                {{--['id' => 1,--}}
                 {{--'goods_name' => 'MACARON',--}}
                 {{--'description' => 'รสส้ม 1, สตอเบอรรี่ 1, รสสับปะรด 1, บลูเบอรี่ 1',--}}
                 {{--'price' => '50',--}}
                 {{--'total' => 4,--}}
                 {{--'place' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค',--}}
                 {{--'meeting_point' => 'โต๊ะจ่ายตั๋ว',--}}
                 {{--'date' => '30 กรกฏาคม 2560',--}}
                 {{--'time' => '15.29',--}}
                 {{--'action' => 'buy',--}}
                 {{--'rating' => null,--}}
                 {{--'customer_image' => asset('image/shop/im_member4.png'),--}}
                 {{--'customer_name' => 'Arat09'--}}
                 {{--],--}}
                 {{--[--}}
                 {{--'id' => 2,--}}
                 {{--'goods_name' => 'ส้มโอ',--}}
                 {{--'description' => '',--}}
                 {{--'price' => '50',--}}
                 {{--'total' => 1,--}}
                 {{--'place' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค',--}}
                 {{--'meeting_point' => 'โต๊ะจ่ายตั๋ว',--}}
                 {{--'date' => '30 กรกฏาคม 2560',--}}
                 {{--'time' => '15.29',--}}
                 {{--'action' => 'confirm',--}}
                 {{--'customer_image' => asset('image/shop/im_member4.png'),--}}
                 {{--'customer_name' => 'Arat09'--}}
                 {{--],--}}
                 {{--['id' => 3,--}}
                 {{--'goods_name' => 'อมยิ้ม macaron',--}}
                 {{--'description' => 'รสส้ม 1, สตอเบอรรี่ 1, รสสับปะรด 1, บลูเบอรี่ 1',--}}
                 {{--'price' => '50',--}}
                 {{--'total' => 4,--}}
                 {{--'place' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค',--}}
                 {{--'meeting_point' => 'โต๊ะจ่ายตั๋ว',--}}
                 {{--'date' => '30 กรกฏาคม 2560',--}}
                 {{--'time' => '15.29',--}}
                 {{--'action' => 'buy',--}}
                 {{--'rating' => null,--}}
                 {{--'customer_image' => asset('image/shop/im_member4.png'),--}}
                 {{--'customer_name' => 'Arat09'--}}
                 {{--],--}}
                 {{--[--}}
                 {{--'id' => 4,--}}
                 {{--'goods_name' => 'ส้มโอ',--}}
                 {{--'description' => 'รสส้ม 1, สตอเบอรรี่ 1, รสสับปะรด 1, บลูเบอรี่ 1',--}}
                 {{--'price' => '50',--}}
                 {{--'total' => 1,--}}
                 {{--'place' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค',--}}
                 {{--'meeting_point' => 'โต๊ะจ่ายตั๋ว',--}}
                 {{--'date' => '30 กรกฏาคม 2560',--}}
                 {{--'time' => '15.29',--}}
                 {{--'action' => 'cancel',--}}
                 {{--'rating' => null,--}}
                 {{--'customer_image' => asset('image/shop/im_member4.png'),--}}
                 {{--'customer_name' => 'Arat09'--}}
                 {{--],--}}
                 {{--[--}}
                 {{--'id' => 44,--}}
                 {{--'goods_name' => 'ส้มโอ',--}}
                 {{--'description' => '',--}}
                 {{--'price' => '50',--}}
                 {{--'total' => 1,--}}
                 {{--'place' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค',--}}
                 {{--'meeting_point' => 'โต๊ะจ่ายตั๋ว',--}}
                 {{--'date' => '30 กรกฏาคม 2560',--}}
                 {{--'time' => '15.29',--}}
                 {{--'action' => 'sell',--}}
                 {{--'rating' => ['customer_image' => asset('image/shop/im_member4.png'),--}}
                               {{--'score' => 5--}}
                            {{--]--}}
                 {{--],--}}
                 {{--['id' => 5,--}}
                 {{--'goods_name' => 'อมยิ้ม macaron',--}}
                 {{--'description' => 'รสส้ม 1, สตอเบอรรี่ 1, รสสับปะรด 1, บลูเบอรี่ 1',--}}
                 {{--'price' => '50',--}}
                 {{--'total' => 4,--}}
                 {{--'place' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค',--}}
                 {{--'meeting_point' => 'โต๊ะจ่ายตั๋ว',--}}
                 {{--'date' => '30 กรกฏาคม 2560',--}}
                 {{--'time' => '15.29',--}}
                 {{--'action' => 'confirm',--}}
                 {{--'rating' => null,--}}
                 {{--'customer_image' => asset('image/shop/im_member4.png'),--}}
                 {{--'customer_name' => 'Arat09'--}}
                 {{--],--}}
                 {{--[--}}
                 {{--'id' => 6,--}}
                 {{--'goods_name' => 'ส้มโอ',--}}
                 {{--'description' => '',--}}
                 {{--'price' => '50',--}}
                 {{--'total' => 1,--}}
                 {{--'place' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค',--}}
                 {{--'meeting_point' => 'โต๊ะจ่ายตั๋ว',--}}
                 {{--'date' => '30 กรกฏาคม 2560',--}}
                 {{--'time' => '15.29',--}}
                 {{--'action' => 'cancel',--}}
                 {{--'rating' => null,--}}
                 {{--'customer_image' => asset('image/shop/im_member4.png'),--}}
                 {{--'customer_name' => 'Arat09'--}}
                 {{--]--}}
            {{--]])--}}
            {{--@endcomponent--}}
    </div>
</div>
