@extends('main')
@section('content')
<style>
.navbar.navbar-default.no-border {
    border:none;
}
</style>

<div class="top-25-container row content-sell">
    <div class="tabbable-panel">
        <div class="tabbable-line">
            <ul class="nav nav-tabs tabs-sell history-tab">
                <li class="active" style="margin-left:15%;" >
                    <a href="#tab_default_1" data-toggle="tab" id="history_buy_tab">
                    <img src="{{asset('image/app/white_buy_icon.png')}}" class="buy-icon">รายการสั่งซื้อ</a>
                </li>
                <li>
                    <a href="#tab_default_2" data-toggle="tab" id="history_sale_tab">
                    <img src="{{asset('image/app/grey_sale_icon.png')}}" class="sell-icon">รายการขาย</a>
                </li>
            </ul>

            <div class="top-25 text-center">
                @component('components/search')
                @endcomponent
            </div>

            <div class="tab-content tab-content-sell">
	        <div class="tab-pane active" id="tab_default_1">
                @include('pages/history/buy_history')
	        </div>

	        <div class="tab-pane" id="tab_default_2" style="margin-bottom:20%;">
            @include('pages/history/sale_history')            
	        </div>

	        <div class="tab-pane" id="tab_default_3">
	        	@include('pages/sell/sell_waiting_tab')              
	        </div>
      	</div>
            
        </div> <!--tabbable-line-->
    </div> <!-- tabbable-panel -->
</div>

</div>
   

@include('modal/history_rating')    
 <!-- reserv modal -->
@include('modal/reserv_modal')

@endsection

@section('script')
    <script src="/vue/directive/vue-select.js?v=3"></script>

    <script type="text/javascript">
        $().ready(function() {

            var $image = $(".image-crop > img")
            var cropper = $image.cropper({
                autoCrop: true,
                built: function () {
                    // Do something here
                    // ...

                    // And then
                    // $(this).cropper('clear');
                }
            });

            // var $inputImage = $("#inputImage");
            // Import image
            var inputImage = document.getElementById('inputImage');
            var uploadedImageType = 'image/jpeg';
            var uploadedImageURL;
            var image = $('#img-now')

            var options = {
                aspectRatio: NaN,
                crop: function (e) {
                    // $dataX.val(Math.round(e.x));
                    // $dataY.val(Math.round(e.y));
                }
            };

            if (URL) {
                inputImage.onchange = function () {
                    var files = this.files;
                    var file;

                    if (cropper && files && files.length) {
                        file = files[0];

                        if (/^image\/\w+/.test(file.type)) {
                            uploadedImageType = file.type;

                            if (uploadedImageURL) {
                                URL.revokeObjectURL(uploadedImageURL);
                            }

                            image.src = uploadedImageURL = URL.createObjectURL(file);
                            // cropper.destroy();
                            // $image.cropper("replace",image);



                            $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                            // cropper = new Cropper(image, options);
                            inputImage.value = null;
                        } else {
                            window.alert('Please choose an image file.');
                        }
                    }
                };
            } else {
                inputImage.disabled = true;
                inputImage.parentNode.className += ' disabled';
            }

            $("#download").click(function() {
                window.open($image.cropper("getDataURL"));
            });

            $("#reset").click(function() {
                $image.cropper("reset");
            });

            $("#cropped").click(function() {
                // var result = $image.cropper('crop');
                var result = $image.cropper("getCroppedCanvas", {fillColor: "#fff", maxHeight:4096, maxWidth:4096});
                var base64 = result.toDataURL("image/jpeg");
                vm.arrProducts.push(base64);
                vm.addImageProduct();

                // $image.cropper("replace",null);
                $image.cropper('destroy').attr('src', "").cropper(options);
                // $image.cropper('destroy')

                console.log(vm.arrProducts)
            });

            $("#zoomIn").click(function() {
                $image.cropper("clear");
                $image.cropper("zoom", 0.1);
            });

            $("#zoomOut").click(function() {
                $image.cropper("clear");
                $image.cropper("zoom", -0.1);
            });

            $("#rotateLeft").click(function() {
                $image.cropper("clear");
                $image.cropper("rotate", 45);
            });

            $("#rotateRight").click(function() {
                $image.cropper("clear");
                $image.cropper("rotate", -45);
            });

            $("#setDrag").click(function() {
                $image.cropper("clear");
                $image.cropper("setDragMode", "move");
            });

            $(".newImg").click(function() {
                $image.cropper("replace",this.src);

            });
        });

    </script>

    <script>
        Vue.config.devtools = true;
        Vue.component('v-select', VueSelect.VueSelect)

        Vue.http.options.xhr = {withCredentials: true}
        Vue.http.options.emulateJSON = true
        Vue.http.options.emulateHTTP = true
        Vue.http.options.crossOrigin = true
        Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'

        var Validator = SimpleVueValidator.Validator.create({
            templates: {
                greaterThan: 'จำเป็นต้องเลือก อย่างน้อย1ไฟล์',
                required: 'จำเป็นต้องใส่ข้อมูล'
            }
        });

        Vue.use(SimpleVueValidator);

        var vm = new Vue({
            el:'#app',
            data: {
                arrUrls:[],
                profile:{},
                buyers:[],
                count_waiting :0
            },
            components: {

            },
            validators: {

            },
            watch:{

            },
            mounted: function(){
                var url = window.location.href;
                this.arrUrls = url.split("/");


                this.getHistoryBuy();
            },
            methods: {
                getProfile : function () {
                    this.$http.get('/api/v1/profile').then(function(myresponse) {
                        this.profile = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                getHistoryBuy : function () {
                    this.$http.get('/api/v1/buyerorder').then(function(myresponse) {
                        this.buyers = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                getPronoun : function (id) {
                    this.$http.get('/api/v1/pronoun').then(function(myresponse) {
                        this.pronouns = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                cancleOrder : function (buy) {

                    swal({
                        title: 'ยกเลิกออเดอร์?',
                        text: "คุณต้องการยกเลิกออเดอร์นี้ใช่หรือไม่",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'ใช่',
                        cancelButtonText: 'ไม่',
                    }).then((result) => {
                        if (result.value) {

                            vm.$http.post('/api/v1/buyer/order/cancel',{order_id:buy.id}).then(function(myresponse) {
                                vm.getHistoryBuy();
                            }, function(myresponse) {
                                // error callback
                                console.log(myresponse.data);
                            });

                        }
                    })


                },
                addImageProduct : function () {
                    var formData = new FormData();
                    formData.append('images', vm.arrProducts[vm.arrProducts.length-1]);

                    vm.$http.post('/s3-base64-upload',{'image':vm.arrProducts[vm.arrProducts.length-1], 'folder':'product'}).then(function(myresponse) {
                        console.log(myresponse.data);

                        this.arrImages.push({image_cover: myresponse.data, status:1})


                    }, function(myresponse) {

                        console.log(myresponse.data);
                    });
                },
                addProduct : function () {
                    // this.$validate()
                    //     .then(function(success) {
                    //         if (success) {
                    //             if($('#image_market')[0].files.length == 0) {
                    //                 this.validators_image = 'กรุณาเลือกรูปตลาด';
                    //             }else{
                    swal({
                        title: 'กำลังบันทึกข้อมูล',
                        text: '(ปิดอัติโนมัติ)',
                        onOpen: () => {
                        swal.showLoading()
                }
                })

                    var product = {
                        image:vm.arrImages,
                        name:vm.name,
                        description:vm.description,
                        product: vm.arrSKUs
                    };

                    console.log(JSON.stringify(product));

                    vm.$http.post('/api/v1/post',product).then(function(myresponse) {
                        $('#addModal').modal('hide');
                        swal.close()

                        swal(
                            'Add Product Success',
                            '',
                            'success'
                        )

                        vm.name = ''
                        vm.description = ''

                    }, function(myresponse) {
                        // error callback
                        swal.close()

                        swal(
                            'เกิดข้อผิดพลาด',
                            'กรุณากดบันทึกใหม่',
                            'error'
                        )
                        console.log(myresponse.data);
                    });

                    //         }
                    //
                    //     }
                    // });

                },
                addSKU : function () {
                    vm.arrSKUs.push({name: this.sku.name, price:this.sku.price, amount:this.sku.amount });

                    this.sku={};
                },
                delSKU: function (index) {
                    vm.arrSKUs.splice(index, 1);

                },
                delImgProduct : function(image){
                    this.arrProducts.find(function(val,index){
                        if(val == image){
                            vm.arrProducts.splice(index, 1);
                            return;
                        }
                    })
                },
                openFile : function (event) {
                    var input = event.target;

                    var reader = new FileReader();
                    reader.onload = function(){
                        var dataURL = reader.result;
                        var output = document.getElementById('output');
                        output.src = dataURL;

                        vm.haveFile = true;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    </script>

@endsection