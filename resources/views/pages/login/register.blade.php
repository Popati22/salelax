<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Salelax</title> <!-- change this title for each page -->
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.js"></script>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{ asset('slick/slick.js') }}"></script>  
    <!-- scrollbar market-->
    <link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css" />
    <script src="https://unpkg.com/simplebar@latest/dist/simplebar.js"></script>

      <style>
          .help-block{
              text-align: left;
              margin-left: 51px;
              margin-bottom: 0px;
              margin-top: 0px;
          }
      </style>

      <script src="{{ asset('/js/moment.min.js?v=3') }}"></script>
      <script src="{{ asset('/js/moment-with-locales.min.js?v=2') }}"></script>
      <script src="{{ asset('/js/vue.min.js') }}"></script>
      <script src="{{ asset('/js/vue-resource.min.js') }}"></script>
      <script src="/vue/directive/vue-validator.js"></script>

</head>

<body class="bg">
<div class="login-left"></div>
<div class="login-center mg-top-40" >

<div class="login-bg">
    <div class="row text-center">
        <div class="">
            <div class="text-center" style="color:white;">
                <div class="login-container" >
                    <div class="invite-header join-us mg-top-40">Join Us</div>
                    <div class="login-box row register">
                        <div class="col-sm-12 text-center " :class="{ 'has-error': validation.hasError('email')}" style="margin-top: 1em;">
                            <span class="icon-left email-icon" ><img src="/image/app/email.png"/></span>
                            <input  class="input-right" v-model="email" type="email" placeholder="Email" name="email">
                            <span v-if="validation.hasError('email')" class="help-block">
                                <strong>@{{ validation.firstError('email') }}</strong>
                            </span>
                        </div>
                        
                        <div class="col-sm-12 grey-line"></div>
                    
                        <div class="col-sm-12 text-center" :class="{ 'has-error': validation.hasError('password')}">
                            <span class="icon-left pass-icon" ><img src="/image/app/pass.png"/></span>
                            <input  class="input-right" v-model="password" type="password" placeholder="Password" name="password" >
                            <span v-if="validation.hasError('password')" class="help-block">
                                <strong>@{{ validation.firstError('password') }}</strong>
                            </span>
                        </div>

                        <div class="col-sm-12 grey-line"></div>
                    
                        <div class="col-sm-12 text-center" :class="{ 'has-error': validation.hasError('repassword')}">
                            <span class="icon-left repass-icon"><img src="/image/app/repass.png"/></span>
                            <input  class="input-right" v-model="repassword" type="password" placeholder="Re - Password" name="re_password" >
                            <span v-if="validation.hasError('repassword')" class="help-block">
                                <strong>@{{ validation.firstError('repassword') }}</strong>
                            </span>
                        </div>
                        <div class="col-sm-12 grey-line"></div>
                    
                        <div class="col-sm-12 text-center">
                        <span class="icon-left telephone-icon" ><img src="/image/app/telephone.png"/></span>
                            <input  class="input-right" v-model="tel" type="text" placeholder="Telephone" name="telephone" >

                        </div>
                        <div class="col-sm-12 grey-line"></div>

                        <div class="col-sm-12 text-center">
                            <button class="login-btn" v-on:click="submit()">GET START</button>
                        </div>                       
                    </div>
                    </div> <!--login-box-->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
<div class="row no-margin">
    <div class="footer-bottom-register">
        <div class="text-center">
            <ul>
                <li>FAQ</li>
                <li>Term&Condition</li>
                <li class="border-left-white">SALELAX © 2017</li>
            </ul>
        </div>
    </div>
</div>
</body>


<script>
  Vue.config.devtools = true;
  var Validator = SimpleVueValidator.Validator.create({
      templates: {
          greaterThan: 'จำเป็นต้องเลือก อย่างน้อย1ไฟล์',
          required: 'จำเป็นต้องใส่ข้อมูล',
          match: "ยืนยันรหัสผ่านไม่ตรง",
          minLength: "กรุณาใส่อย่างน้อย 6 ตัวอักษร"
      }
  });

  Vue.use(SimpleVueValidator);

  var vm = new Vue({
      el:'.login-bg',
      data: {
          email: '',
          password : '',
          repassword : '',
          tel : "",
          submitted: false
      },
      validators: {
          'email': function (value) {
              return Validator.value(value).required().email('กรุณาใส่อีเมล์ให้ถูกต้อง');
          },
          'password': function (value) {
              return Validator.value(value).required().minLength(6);
          },
          'repassword, password': function (repeat, password) {
              if (this.submitted || this.validation.isTouched('repassword')) {
                  return Validator.value(repeat).required().match(password);
              }
          }
      },
      methods: {
          submit : function () {
              this.submitted = true;

              this.$validate()
                  .then(function(success) {
                      if (success) {

                          var profile = {
                              "email":vm.email,
                              "password":vm.password,
                              "telephone": vm.tel
                          }

                          vm.$http.post('/register',profile).then(function(myresponse) {

                              swal({
                                  title: 'ลงทะเบียนสำเร็จแล้ว',
                                  text: "คุณต้องการเข้าใช้ระบบเลยหรือไม่?",
                                  type: 'success',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: 'ตกลง เข้าระบบเลย',
                                  cancelButtonText: 'ยังไม่เข้าใช้'
                              }).then((result) => {
                                  if (result.value) {
                                      var profile = {
                                          "username":vm.email,
                                          "password":vm.password
                                      }

                                      vm.$http.post('/login',profile).then(function(myresponse) {
                                          if(myresponse.status == 200){
                                              window.location.href = '/market';
                                          }else{
    //                                    this.errors = myresponse.data.errors;
                                              $('html, body').animate({ scrollTop: 0 }, 'fast');
                                          }
                                      }, function(myresponse) {
                                          // error callback
                                          console.log(myresponse.data);
                                      });
                                  }
                              })
//                              if(myresponse.status == 200){
//                                  window.location.href = '/market';
//                              }else{
////                                    this.errors = myresponse.data.errors;
//                                  $('html, body').animate({ scrollTop: 0 }, 'fast');
//                              }
                          }, function(myresponse) {
                              swal('ไม่สามารถลงทะเบียนด้วยอีเมล์นี้ได้');
                              // error callback
                              console.log(myresponse.data);
                          });
                      }
                  });

          }
      }
  });
</script>

</html>