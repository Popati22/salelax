<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Salelax</title> <!-- change this title for each page -->
<!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/login.css') }}" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.js"></script>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ asset('slick/slick.js') }}"></script>
<!-- scrollbar market-->
<link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css" />
<script src="https://unpkg.com/simplebar@latest/dist/simplebar.js"></script>

    <script src="{{ asset('/js/moment.min.js?v=3') }}"></script>
    <script src="{{ asset('/js/moment-with-locales.min.js?v=2') }}"></script>
    <script src="{{ asset('/js/vue.min.js') }}"></script>
    <script src="{{ asset('/js/vue-resource.min.js') }}"></script>
    <script src="/vue/directive/vue-validator.js"></script>

</head>

<body class="bg">
  <div class="login-left"></div>
  <div class="login-center">
      <div class="login-bg">
            <div class="row text-center">
            <div class="">
            <div class="text-center" style="color:white;">
                <div class=" login-container" >
                    <div class="login-logo"><img src='/image/app/logo.png'></div>
                    <div class="invite-header">มาร่วมสนุกกับการขายด้วยกัน ที่นี้</div>
                    <div class="invite-text">Salelax จะเปลี่ยนจากสิ่งที่ทำในยามว่างของคุณ<br/>เป็นการสร้างรายได้จากงานที่ชอบ</div>
                    <div class="login-box row">
                        <div class="col-sm-12 text-center" style="margin-top: 1em;">
                        <span class="icon-left email-icon" ><img  src="/image/app/email.png"/></span>
                        <input  class="input-right" type="email" placeholder="Email" name="email" v-model="email">
                        </div>
                        
                        <div class="col-sm-12 grey-line"></div>
                        
                        <div class="col-sm-12 text-center" >
                        <span class="icon-left pass-icon" ><img src="/image/app/pass.png"/></span>
                        <input  class="input-right" type="password" placeholder="Password" name="password" v-model="password">
                        </div>

                        <div class="col-sm-12 text-center" >
                            <button class="login-btn" v-on:click="submit()">LOGIN</button>
                        </div>
                        <div class="col-sm-12 mg-top-10"><span style="color:black;font-size:20px;">OR</div>
                        <div class="col-sm-12 text-center" >
                            <button class="fb-login-btn" v-on:click="window.location.href='/facebook/login'">LOGIN WITH FACEBOOK</button>
                        </div>
                        <div class="col-sm-12 mg-top-15" >
                            <span class="text-left" style="float:left;font-size:20px"><a href="/register" class="font-blue">Join us</a></span>
                            <span class="text-right" style="color:black;float:right;font-size:15px;padding-top: 5px;"><a href="/reset/email" style="color:black;">Forget Password</a></span>
                        </div>
                    </div>
                    </div> <!--login-box-->
                </div>
            </div>
        </div>
      </div>
  </div>

    <div class="row no-margin">
        <div class="footer-bottom-login">
            <div class="text-center">
                <ul>
                    <li>FAQ</li>
                    <li>Term&Condition</li>
                    <li class="border-left-white">SALELAX © 2017</li>
                </ul>
            </div>
        </div>
    </div>
</body>

<script>
    Vue.config.devtools = true;
    var Validator = SimpleVueValidator.Validator.create({
        templates: {
            greaterThan: 'จำเป็นต้องเลือก อย่างน้อย1ไฟล์',
            required: 'จำเป็นต้องใส่ข้อมูล'
        }
    });

    Vue.use(SimpleVueValidator);

    var vm = new Vue({
        el:'.login-center',
        data: {
            email: '',
            password : '',
        },
        validators: {
            'email': function (value) {
                return Validator.value(value).email('กรุณาใส่อีเมล์ให้ถูกต้อง');
            },
            'password': function (value) {
                return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
            },

        },
        methods: {
            submit : function () {
                this.$validate()
                    .then(function(success) {
                        if (success) {

                            var profile = {
                                "username":vm.email,
                                "password":vm.password
                            }

                            vm.$http.post('/login',profile).then(function(myresponse) {
                                if(myresponse.status == 200){
                                    window.location.href = '/market';
                                }else{
                                    swal({
                                        type: 'error',
                                        title: 'เกิดข้อผิดพลาดในการเข้าสู่ระบบ',
                                        text: 'กรุณาตรวจสอบอีเมล์และรหัสผ่านอีกครั้ง'
                                    })
//                                    this.errors = myresponse.data.errors;
                                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                                }
                            }, function(myresponse) {
                                // error callback
                                swal({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาดในการเข้าสู่ระบบ',
                                    text: 'กรุณาตรวจสอบอีเมล์และรหัสผ่านอีกครั้ง'
                                })

                                console.log(myresponse.data);
                            });
                        }
                    });

            }
        }
    });
</script>
  
</html>