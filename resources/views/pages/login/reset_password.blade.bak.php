<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Salelax</title> <!-- change this title for each page -->
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{ asset('slick/slick.js') }}"></script>  
    <!-- scrollbar market-->
    <link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css" />
    <script src="https://unpkg.com/simplebar@latest/dist/simplebar.js"></script>
  
  </head>
  <style>
    body {
        background-color:#161616;
        background-image: url('/image/app/register_bg.png');
        
       
    }

    body, html {
    height: 100%;
    }

.bg { 
    /* The image used */
    background-image: url("/image/app/register_bg.png");

    /* Full height */
    height: 100%; 

    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
    .login-logo img {
        margin-top: 50px;
        height: 55px;
        width: auto;
    }
    .invite-header {
        font-size: 35px;
        font-weight: bold;
        margin-top: 30px;
    }
    .invite-text {
        font-size: 22px;
        margin-top: 10px;
    }
    .login-box {
        background-color: white;
        border-radius:15px;
    }
    .login-container {
        float: none;
        text-align: center;
        margin-left: auto;
        margin-right: auto;
    }
    .login-box{
        width: 70%;
        float: none;
        margin-left: auto;
        margin-right: auto;
        margin-top: 30px;
        padding:20px;
        height:auto;
    }
    .input-right {
        border: none;
        font-size: 20px;
        width: 85%;
        margin-left:15px;
        color:#616161;
        font-weight: thin;
        float:right;
    }
    .email-icon,.pass-icon , .repass-icon, .telephone-icon{
        float: left;
        width:35px;
        /* padding-top: 3.5%; */
    }
    .repass-icon img, .telephone-icon img {
        float: left;
        width:25px;
    }
    .grey-line
    {
        border-bottom: 1px solid #959595;
        margin-top:20px;
        margin-bottom:20px;
    }
    
 .login-btn {
  
    border-radius: 40px;
    font-size: 20px;
    padding: 5px 10px;
    background-color: #fff;
    width: 100%;
    border-color: transparent;
    background-color: #FEBA12;
    color: #005593;
    font-weight: bold;
    margin-top:40px;
    
}
.fb-login-btn {
    border-radius: 40px;
    font-size: 20px;
    padding: 5px 10px;
    background-color: #fff;
    width: 100%;
    border-color: transparent;
    background-color: #0095DE;
    margin-top:10px;
    font-weight:thin;
}
.login-center {
    margin-left: auto;
    margin-right: auto;

}
.input-right {
      float:left;
    }

@media(min-width:1101px)
{
   .login-center {
        width: 50%;
    }
}
@media(max-width:1100px)
{
   .login-center {
        width: 60%;
    }
}
@media(max-width:991px)
{
    .login-center {
        width: 70%;
    }
}
@media(max-width:810px)
{
    .login-center {
        width: 80%;
    }
}
@media(max-width:810px)
{
    .login-center {
        width: 85%;
    }
}
@media(max-width:768px)
{
    .login-center {
        width: 85%;
    }
    .grey-line {
        margin-top: 45px;
    }
}
@media(max-width:710px)
{
    .login-center {
        width: 90%;
    }
    .input-type {
        width:80%;
    }
}
@media(max-width:670px)
{
    .login-center {
        width: 85%;
        margin-left: auto;
        margin-right: auto;
    }
    .login-box {
        width:75%
    }
}
@media(max-width:627px)
{
    .login-center {
        width: 85%;
        margin-left: auto;
        margin-right: auto;
    }
    .login-box {
        width:85%
    }
   
}
@media(max-width:627px)
{
    .login-center {
        width: 80%;
        margin-left: auto;
        margin-right: auto;
    }
    .input-right {
        font-size:18px;
    }
    .email-icon > img {
        width:25px;
    }
    .pass-icon > img {
        height: 33px;
        margin-top: -7px;
    }
    .login-box {
        width:95%
    }
    .email-icon,.pass-icon, .telephone-icon, .repass-icon {
        float: left;
        width:15px;
        /* padding-top: 3.5%; */
    }
   
}
.invite-header.join-us {
    font-size: 55px;
    font-weight: normal;
    margin-top: 10px;

}
.login-box.register {
    margin-top: 20px !important;
}
.invite-header.reset{
    font-size: 55px;
    font-weight: normal;
    margin-top: 30px;
}

    
</style>
  <body class="bg">
  <div class="login-left"></div>
  <div class="login-center">

<div class="login-bg">
    <div class="row text-center">
        <div class="">
            <div class="text-center" style="color:white;">
                <div class="login-container" >
                    <div class="invite-header reset mg-top-40">Forget Password</div>
                    <div class="mg-top-40"></div>
                    <div class="login-box row register">
                        <div class="col-sm-12 text-center mg-top-40">
                            <span class="icon-left email-icon" ><img src="/image/app/email.png"/></span>
                            <input  class="input-right" type="email" placeholder="Email" name="email" >
                        </div>
                        
                        <div class="col-sm-12 grey-line"></div>
                    
                 

                        <div class="col-sm-12 text-center" style="margin-top: 120px;margin-bottom: 30px;" >
                            <button class="login-btn">GET PASSWORD</button>
                        </div>                       
                    </div>
                    </div> <!--login-box-->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
<div class="mg-top-40"></div>
<div class="row no-margin">
    <div class="footer-bottom-register">
        <div class="text-center">
            <ul>
                <li>FAQ</li>
                <li>Term&Condition</li>
                <li class="border-left-white">SALELAX © 2017</li>
            </ul>
        </div>
    </div>
</div>
</body>
  
</html>