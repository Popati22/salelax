<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Salelax</title> <!-- change this title for each page -->
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{ asset('slick/slick.js') }}"></script>  
    <!-- scrollbar market-->
    <link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css" />
    <script src="https://unpkg.com/simplebar@latest/dist/simplebar.js"></script>
  
  </head>
 
  <body class="bg">
  <div class="login-left"></div>
  <div class="login-center">

<div class="login-bg">
    <div class="row text-center">
        <div class="">
            <div class="text-center" style="color:white;">
                <div class="login-container" >
                    <div class="invite-header reset mg-top-40">Forget Password</div>
                    <div class="mg-top-40"></div>
                    <div class="login-box row register">
                        <div class="col-sm-12 text-center mg-top-40">
                            <span class="icon-left email-icon" ><img src="/image/app/email.png"/></span>
                            <input  class="input-right" type="email" placeholder="Email" name="email" >
                        </div>
                        
                        <div class="col-sm-12 grey-line"></div>
                    
                 

                        <div class="col-sm-12 text-center" style="margin-top: 120px;margin-bottom: 30px;" >
                            <button class="login-btn">GET PASSWORD</button>
                        </div>                       
                    </div>
                    </div> <!--login-box-->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
<div class="mg-top-40"></div>
<div class="row no-margin">
    <div class="footer-bottom-register">
        <div class="text-center">
            <ul>
                <li>FAQ</li>
                <li>Term&Condition</li>
                <li class="border-left-white">SALELAX © 2017</li>
            </ul>
        </div>
    </div>
</div>
</body>
  
</html>