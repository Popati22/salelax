<div v-if="summary.length > 0">

	<div class="top-25-container text-center ">
		@component('components/search')

		@endcomponent
	</div>

	<div class="top-25-container text-center ">
		<button class="btn-md-blue-w tx-center" style="width:20em;">
			<span class="mg-right-10">ส่งออกรายงาน</span>
		</button>
	</div>

	<div class="top-25-container text-center mg-bottom-40 no-padd">
	  <div class="col-sm-12 bt-box-shadow">&nbsp</div>
		<div class='col-md-12  no-padd bt-box-shadow mg-bottom-40' >
			@include('pages/sell/sell_slideshow_total')
		</div>
	</div>


	<div class="top-25-container text-center content-reserv">

		<sell_total></sell_total>

	</div>

</div>

<div v-else style="text-align: center;">

	<h4 style="margin-top: 20%;
font-size: 2em;
color: gray;
margin-bottom: 20%;">ไม่พบข้อมูล</h4>

</div>