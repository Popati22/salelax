<table class="table table-accept mg-top-20 mg-b-20" border="0">     
  <tbody>  
    @foreach($row as $key => $val)
    <tr class="h4">
      <td style="width:4%;" class="padd-l-5"></td>
      <td class="tx-black" style="width:30%;">
        <div class="col-xs-2 goods-avatar text-left no-padd">
           <center><img src="{{ $val['img'] }}" class="w-avatar"></center> 
        </div>
        <div class="col-xs-10" style="padding-top:10px;">
            {{$val['title']}}
        </div>
      </td>
      <td class="tx-blue tx-center">{{$val['detail']}}</td>
      <td class="tx-center tx-gray" style="width:20%;">รวมเงิน {{$val['price']}} บาท</td>
      <td class="tx-center">
        <div class="col-sm-6 pd-ac">
        <button class="btn-sm-yell sell-detail-modal b-ac1 pull-right">
          <span class="mg-right-10">รายละเอียด</span>
        </button>
        </div>
        <div class="col-sm-6 no-padd">
        <button class="btn-sm-blue pull-right b-ac2" >
          <span class="mg-right-10">ตอบรับ</span>
        </button>
        </div>
      </td>
    </tr>
     @endforeach
  </tbody> 
</table>

