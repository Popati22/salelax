<!-- .crousel slide -->
<section class="panel panel-default">
<div class="carousel slide  panel-body" id="c-slide" style="padding-top: 35px;padding-bottom: 35px;">
    <div class="carousel-inner">
      <div class="item active tx-center" >
        <h4 class="h4 tx-blue">TUESDAY</h4>
        <h4 class="h3 tx-blue tx-date-blue">24 MAY 2017</h4>
      </div>
      <div class="item tx-center">
        <h4 class="h4 tx-blue">TUESDAY</h4>
        <h4 class="h3 tx-blue tx-date-blue">25 MAY 2017</h4>
      </div>
      <div class="item tx-center">
        <h4 class="h4 tx-blue">TUESDAY</h4>
        <h4 class="h3 tx-blue tx-date-blue">26 MAY 2017</h4>
      </div>
    </div>
    <a class="left carousel-control" href="#c-slide" data-slide="prev">
      <span class="glyphicon glyphicon-menu-left tx-i-blue i-prev"></span>
    </a>
    <a class="right carousel-control" href="#c-slide" data-slide="next">
      <span class="glyphicon glyphicon-menu-right blue-font tx-i-blue i-next"></span>
    </a>
</div>
</section>
<!-- / .carousel slide -->
            
