<div v-if="count_waiting > 0">
	<div class="top-25-container text-center ">
		@component('components/search')

		@endcomponent
	</div>

	<div class="top-25-container text-center ">
		<button class="btn-md-blue-w tx-center" style="width:20em;">
			<span class="mg-right-10">ส่งออกรายงาน</span>
		</button>
	</div>

	<div class="top-25-container text-center mg-bottom-40 no-padd">
	  <div class="col-sm-12 bt-box-shadow">&nbsp</div>
		<div class='col-md-12  no-padd bt-box-shadow mg-bottom-40' >
			@include('pages/sell/sell_slideshow_wait')
		</div>
	</div>

	<div class="top-25-container text-center content-reserv">
		<sell_waiting></sell_waiting>
	 {{--@component('pages/sell/sell_waiting_list',['list' => [--}}
				{{--[--}}
				  {{--'title' => 'นกน้อยคัพเค้ก',--}}
				  {{--'custormer' => [--}}
					  {{--[--}}
						{{--'avartar' => asset('image/shop/im_member4.png'),--}}
						{{--'name' => 'Barr Lovery',--}}
						{{--'price' => '125',--}}
						{{--'detail' => '4 รายการ (รสส้ม 1, สตอเบอรี 1 ,รสสับปะรด 1,บลูเบอรี 1)',--}}
						{{--'site' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค...',--}}
						{{--'time' => '15.29'--}}
					  {{--],--}}
					  {{--[--}}
						{{--'avartar' => asset('image/shop/im_member4.png'),--}}
						{{--'name' => 'Barr Lovery',--}}
						{{--'price' => '125',--}}
						{{--'detail' => '4 รายการ (รสส้ม 1, สตอเบอรี 1 ,รสสับปะรด 1,บลูเบอรี 1)',--}}
						{{--'site' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค...',--}}
						{{--'time' => '15.29'--}}
					  {{--],--}}
				  {{--]--}}
				{{--],--}}
				{{--[--}}
				  {{--'title' => 'เกลือขัดผิว',--}}
				  {{--'custormer' => [--}}
					  {{--[--}}
						{{--'avartar' => asset('image/shop/im_member4.png'),--}}
						{{--'name' => 'Barr Lovery',--}}
						{{--'price' => '125',--}}
						{{--'detail' => '4 ชิ้น (รสส้ม 1,สตอเบอรี่ 1,รสสับปะรด 1,บลูเบอรี 1)',--}}
						{{--'site' => 'ท่ารถต้มธ.รังสิต - อนุเสาวรีย์/ฟิวเจอร์',--}}
						{{--'time' => '15.29'--}}
					  {{--]--}}
					{{--]--}}
				{{--]--}}


			{{--]--}}
		  {{--])--}}

	{{--@endcomponent  --}}
	</div>
</div>

<div v-else style="text-align: center;">

	<h4 style="margin-top: 20%;
font-size: 2em;
color: gray;
margin-bottom: 20%;">ไม่พบข้อมูล</h4>

</div>