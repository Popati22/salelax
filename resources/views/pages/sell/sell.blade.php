@extends('main')

@section('style')
    <link rel="stylesheet" href="/plugins/cropperjs/dist/cropper.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .btn {
            padding-left: .75rem;
            padding-right: .75rem;
        }

        .rtl{
            direction:RTL
        }

        label.btn {
            margin-bottom: 0;
        }

        .d-flex > .btn {
            flex: 1;
        }

        .no-border{
            border: 0px !important;
            box-shadow: 0px 0px 0px !important;
            padding: 0px;
        }
        .carbonads {
            border-radius: .25rem;
            border: 1px solid #ccc;
            font-size: .875rem;
            overflow: hidden;
            padding: 1rem;
        }

        .carbon-wrap {
            overflow: hidden;
        }

        .carbon-img {
            clear: left;
            display: block;
            float: left;
        }

        .carbon-text,
        .carbon-poweredby {
            display: block;
            margin-left: 140px;
        }

        .carbon-text,
        .carbon-text:hover,
        .carbon-text:focus {
            color: #fff;
            text-decoration: none;
        }

        .carbon-poweredby,
        .carbon-poweredby:hover,
        .carbon-poweredby:focus {
            color: #ddd;
            text-decoration: none;
        }

        @media (min-width: 768px) {
            .carbonads {
                float: right;
                margin-bottom: -1rem;
                margin-top: -1rem;
                max-width: 360px;
            }
        }

        .footer {
            font-size: .875rem;
        }

        .heart {
            color: #ddd;
            display: block;
            height: 2rem;
            line-height: 2rem;
            margin-bottom: 0;
            margin-top: 1rem;
            position: relative;
            text-align: center;
            width: 100%;
        }

        .heart:hover {
            color: #ff4136;
        }

        .heart::before {
            border-top: 1px solid #eee;
            content: " ";
            display: block;
            height: 0;
            left: 0;
            position: absolute;
            right: 0;
            top: 50%;
        }

        .heart::after {
            background-color: #fff;
            content: "♥";
            padding-left: .5rem;
            padding-right: .5rem;
            position: relative;
            z-index: 1;
        }

        .img-container,
        .img-preview {
            background-color: #f7f7f7;
            text-align: center;
            width: 100%;
        }

        .img-container {
            margin-bottom: 1rem;
            max-height: 497px;
            min-height: 497px;
        }

        @media (min-width: 768px) {
            .img-container {
                min-height: 497px;
            }
        }

        .img-container > img {
            max-width: 100%;
        }

        .docs-preview {
            margin-right: -1rem;
        }

        .img-preview {
            float: left;
            margin-bottom: .5rem;
            margin-right: .5rem;
            overflow: hidden;
        }

        .img-preview > img {
            max-width: 100%;
        }

        .preview-lg {
            height: 9rem;
            width: 16rem;
        }

        .preview-md {
            height: 4.5rem;
            width: 8rem;
        }

        .preview-sm {
            height: 2.25rem;
            width: 4rem;
        }

        .preview-xs {
            height: 1.125rem;
            margin-right: 0;
            width: 2rem;
        }
        .vbox-overlay{
            z-index: 1500;
        }

        .docs-data > .input-group {
            margin-bottom: .5rem;
        }

        .docs-data .input-group-prepend .input-group-text {
            min-width: 4rem;
        }

        .docs-data .input-group-append .input-group-text {
            min-width: 3rem;
        }

        .docs-buttons > .btn,
        .docs-buttons > .btn-group,
        .docs-buttons > .form-control {
            margin-bottom: .5rem;
            margin-right: .25rem;
        }

        .docs-toggles > .btn,
        .docs-toggles > .btn-group,
        .docs-toggles > .dropdown {
            margin-bottom: .5rem;
        }

        .docs-tooltip {
            display: block;
            margin: -.5rem -.75rem;
            padding: .5rem .75rem;
        }

        .docs-tooltip > .icon {
            margin: 0 -.25rem;
            vertical-align: top;
        }

        .tooltip-inner {
            white-space: normal;
        }

        .btn-upload .tooltip-inner,
        .btn-toggle .tooltip-inner {
            white-space: nowrap;
        }

        .btn-toggle {
            padding: .5rem;
        }

        .btn-toggle > .docs-tooltip {
            margin: -.5rem;
            padding: .5rem;
        }

        @media (max-width: 400px) {
            .btn-group-crop {
                margin-right: -1rem!important;
            }

            .btn-group-crop > .btn {
                padding-left: .5rem;
                padding-right: .5rem;
            }

            .btn-group-crop .docs-tooltip {
                margin-left: -.5rem;
                margin-right: -.5rem;
                padding-left: .5rem;
                padding-right: .5rem;
            }
        }

        .docs-options .dropdown-menu {
            width: 100%;
        }

        .docs-options .dropdown-menu > li {
            font-size: .875rem;
            padding: .125rem 1rem;
        }

        .docs-options .dropdown-menu .form-check-label {
            display: block;
        }

        .docs-cropped .modal-body {
            text-align: center;
        }

        .docs-cropped .modal-body > img,
        .docs-cropped .modal-body > canvas {
            max-width: 100%;
        }
        .tabs-sell li {
            width: 25%;
        }
        .ui-timepicker-standard{
            z-index: 2;
        }
    </style>

    <style>
        .v-select .dropdown-toggle{
            border: none !important;
        }
        .v-select .selected-tag {
            background-color: transparent !important;
            border: none !important;
            color: #0195db !important;
            font-size: 16px !important;
        }

        .b-radius-8{
            border-radius: 8px;
        }
    </style>

@endsection


@section('content')

<div class="top-25-container row content-sell" style="margin-top:-20px;">

  	<div class="tabbable-panel">

        <div class="tabbable-line">

            <ul class="nav nav-tabs tabs-sell">
                <!-- <li >
                  <a href="#tab_default_1" data-toggle="tab">ขาย</a>
                </li>
                <li>
                  <a href="#tab_default_2" data-toggle="tab">ตอบรับ
                    <b class="badge badge-sell pull-right">8</b>
                  </a>
                </li>
                <li>
                  <a href="#tab_default_3" data-toggle="tab">รายการรอส่ง
                    <b class="badge badge-sell pull-right">5</b>
                  </a>
                </li>
                <li class="active">
                  <a href="#tab_default_4" data-toggle="tab">สรุปยอด
                    <b class="badge badge-sell pull-right">5</b>
                  </a>
                </li> -->
                <li class="active tabs-border-right" @click="tab_active = 1">
                  <a href="#tab_default_1" data-toggle="tab" aria-expanded="true">

                      <img v-if="tab_active == 1" src="{{ asset('image/sell/menu1_white.png') }}" width="40px">
                      <img v-else src="{{ asset('image/sell/menu1.png') }}" width="40px">
                      <label>ขาย</label>

                       {{--<label class="col-md-3 col-lg-3 control-label tx-left no-padd">--}}
                           {{--<img v-if="tab_active == 1" src="{{ asset('image/sell/menu1_white.png') }}" class="mini-icon images-thumbnail img-responsive" >--}}
                           {{--<img v-else src="{{ asset('image/sell/menu1.png') }}" class="mini-icon images-thumbnail img-responsive" >--}}
                       {{--</label>--}}
                       {{--<label class="col-md-8 col-lg-8 control-label tx-left">ขาย</label>--}}
                       <div class="clearfix"></div>
                  </a>
                </li>
                <li class="tabs-border-right" @click="tab_active = 2">
                  <a href="#tab_default_2" data-toggle="tab">

                      <img v-if="tab_active == 2" src="{{ asset('image/sell/menu2_white.png') }}" width="40px">
                      <img v-else src="{{ asset('image/sell/menu2.png') }}" width="40px">
                      <label>ตอบรับ <b class="badge badge-sell pull-right" style="position: relative; right: -13px; top: -4px;">@{{ sellers.length }}</b></label>

                      {{--<label class="col-md-4 col-lg-4 control-label tx-left no-padd"><center><img src="{{ asset('image/sell/menu2.png') }}" class="mini-icon images-thumbnail img-responsive" ></center></label>--}}
                      {{--<label class="col-md-6 col-lg-6 control-label tx-left">ตอบรับ</label>--}}
                      {{--<label class="col-md-2 col-lg-2 control-label tx-left"><b class="badge badge-sell pull-right">@{{ sellers.length }}</b></label>--}}
                      {{--<div class="clearfix"></div>--}}
                  </a>
                </li>
                <li class="tabs-border-right" @click="tab_active = 3">
                  <a href="#tab_default_3" data-toggle="tab">

                      <img v-if="tab_active == 3" src="{{ asset('image/sell/menu3_white.png') }}" width="40px">
                      <img v-else src="{{ asset('image/sell/menu3.png') }}" width="40px">
                      <label>รายการรอส่ง <b class="badge badge-sell pull-right" style="position: relative; right: -13px; top: -4px;">@{{ count_waiting }}</b></label>

                      {{--<label class="col-md-4 col-lg-4 control-label tx-left no-padd"><center><img src="{{ asset('image/sell/menu3.png') }}" class="mini-icon images-thumbnail img-responsive" ></center></label>--}}
                      {{--<label class="col-md-6 col-lg-6 control-label tx-left">รายการรอส่ง</label>--}}
                      {{--<label class="col-md-2 col-lg-2 control-label tx-left"><b class="badge badge-sell pull-right">@{{ count_waiting }}</b></label>--}}
                      <div class="clearfix"></div>
                  </a>
                </li>
                <li class="" @click="tab_active = 4">
                  <a href="#tab_default_4" data-toggle="tab" aria-expanded="false">

                      <img v-if="tab_active == 4" src="{{ asset('image/sell/menu4_white.png') }}" width="40px">
                      <img v-else src="{{ asset('image/sell/menu4.png') }}" width="40px">
                      <label>สรุปยอด <b class="badge badge-sell pull-right" style="position: relative; right: -13px; top: -4px;">@{{ summary.length }}</b></label>


                      {{--<label class="col-md-4 col-lg-4 control-label tx-left no-padd"><center><img src="{{ asset('image/sell/menu4.png') }}" class="mini-icon images-thumbnail img-responsive" ></center></label>--}}
                   {{--<label class="col-md-6 col-lg-6 control-label tx-left">สรุปยอด</label>--}}
                   {{--<label class="col-md-2 col-lg-2 control-label tx-left"><b class="badge badge-sell pull-right">@{{ summary.length }}</b></label>--}}
                   <div class="clearfix"></div>
                  </a>
                </li>
            </ul>

            <div class="tab-content tab-content-sell">
                <div class="tab-pane active" id="tab_default_1">
                    @include('pages/sell/sell_sell_tab')
                </div>

                <div class="tab-pane" id="tab_default_2" style="margin-bottom:20%;">
                    @include('pages/sell/sell_accept_tab')
                </div>

                <div class="tab-pane" id="tab_default_3">
                    @include('pages/sell/sell_waiting_tab')
                </div>

                <div class="tab-pane" id="tab_default_4">
                    @include('pages/sell/sell_total_tab')

                </div>
            </div>

        </div>
    </div>
</div>
</div>

<div>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.10&appId=283301778844117";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
</div>


<!-- sell modal -->
@include('pages/sell/sell_accept_detail_modal')  


@endsection

@section('script')
    <script src="/plugins/cropperjs/dist/cropper.js?v=2"></script>
    <script src="/vue/directive/select2.js"></script>
    <script src="/vue/directive/vue-select.js?v=3"></script>

    <script src="/vue/directive/flexslider.js?v=9"></script>
    <script src="/vue/directive/sell_waiting.js?v=1"></script>
    <script src="/vue/directive/sell_waiting_list.js?v=1"></script>
    <script src="/vue/directive/datepicker.js?v=4"></script>
    <script src="/vue/directive/timepicker.js?v=2"></script>
    <script src="/vue/directive/shop_goods_time.js?v=2"></script>
    <script src="/vue/directive/sell_total.js?v=3"></script>
    {{--<script type="text/javascript" src="https://connect.facebook.net/en_US/sdk.js"></script>--}}

    <script type="text/javascript">
        $().ready(function() {

            var $image = $(".image-crop > img")
            var cropper = $image.cropper({
                autoCrop: true,
                built: function () {
                    // Do something here
                    // ...

                    // And then
                    // $(this).cropper('clear');
                }
            });

            // var $inputImage = $("#inputImage");
            // Import image
            var inputImage = document.getElementById('inputImage');
            var uploadedImageType = 'image/jpeg';
            var uploadedImageURL;
            var image = $('#img-now')

            var options = {
                aspectRatio: NaN,
                crop: function (e) {
                    // $dataX.val(Math.round(e.x));
                    // $dataY.val(Math.round(e.y));
                }
            };

            if (URL) {
                inputImage.onchange = function () {
                    var files = this.files;
                    var file;

                    if (cropper && files && files.length) {
                        file = files[0];

                        if (/^image\/\w+/.test(file.type)) {
                            uploadedImageType = file.type;

                            if (uploadedImageURL) {
                                URL.revokeObjectURL(uploadedImageURL);
                            }

                            image.src = uploadedImageURL = URL.createObjectURL(file);
                            vm.haveFile = true;

                            // cropper.destroy();
                            // $image.cropper("replace",image);

                            $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                            // cropper = new Cropper(image, options);
                            inputImage.value = null;
                        } else {
                            window.alert('Please choose an image file.');
                        }
                    }
                };
            } else {
                inputImage.disabled = true;
                inputImage.parentNode.className += ' disabled';
            }

            $("#download").click(function() {
                window.open($image.cropper("getDataURL"));
            });

            $("#reset").click(function() {
                $image.cropper("reset");
            });

            $("#cropped").click(function() {
                // var result = $image.cropper('crop');
                var result = $image.cropper("getCroppedCanvas", {fillColor: "#fff", maxHeight:4096, maxWidth:4096});
                var base64 = result.toDataURL("image/jpeg");
                vm.arrProducts.push(base64);
                vm.addImageProduct();
                vm.haveFile = false;

                // $image.cropper("replace",null);
                $image.cropper('destroy').attr('src', "").cropper(options);
                // $image.cropper('destroy')

                console.log(vm.arrProducts)
            });

            $("#zoomIn").click(function() {
                $image.cropper("clear");
                $image.cropper("zoom", 0.1);
            });

            $("#zoomOut").click(function() {
                $image.cropper("clear");
                $image.cropper("zoom", -0.1);
            });

            $("#rotateLeft").click(function() {
                $image.cropper("clear");
                $image.cropper("rotate", 45);
            });

            $("#rotateRight").click(function() {
                $image.cropper("clear");
                $image.cropper("rotate", -45);
            });

            $("#setDrag").click(function() {
                $image.cropper("clear");
                $image.cropper("setDragMode", "move");
            });

            $(".newImg").click(function() {
                $image.cropper("replace",this.src);

            });
        });

    </script>

    <script>
        Vue.config.devtools = true;
        Vue.component('v-select', VueSelect.VueSelect)

        Vue.http.options.xhr = {withCredentials: true}
        Vue.http.options.emulateJSON = true
        Vue.http.options.emulateHTTP = true
        Vue.http.options.crossOrigin = true
        Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'

        var Validator = SimpleVueValidator.Validator.create({
            templates: {
                greaterThan: 'จำเป็นต้องเลือก อย่างน้อย1ไฟล์',
                required: 'จำเป็นต้องใส่ข้อมูล'
            }
        });

        Vue.use(SimpleVueValidator);

        var vm = new Vue({
            el:'#app',
            data: {
                tab_active: 1,
                haveFile: false,
                category:'',
                pronoun:'',
                item_condition:{
                    id:0,
                    name:"สินค้าใหม่"
                },
                specify_type:1,
                date_start:{
                    show:'',
                    value:''
                },
                date_end:{
                    show:'',
                    value:''
                },
                time_start:'',
                time_end:'',
                location_type: 1,
                post_id:'',
                profile:'',
                name: '',
                title:'',
                quantity: '',
                description : '',
                sku:{},
                validators_image: '',
                groupSelect:[],
                fb:{
                    groups:{
                        data:[],
                        paging:{}
                    }
                },
                location:{
                    address:'',
                    point:''
                },

                count_waiting:0,
                summary:[],
                arrProducts : [],
                arrImages: [],
                arrSKUs:[
                    {
                        amount:"",
                        name:"",
                        price:""

                    }
                ],
                products : [],
                categorys: [],
                item_conditions:[{id:0, name:"สินค้าใหม่"},{id:1, name:"มือสอง"}],
                locations: [],
                pronouns: [],
                sellers: [],//รายการรอตอบรับ
            },
            validators: {
                'name': function (value) {
                    return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
                },
                'quantity': function (value) {
                    return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
                },
                'description': function (value) {
                    return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
                },

            },
            watch:{
                'arrProducts' : function () {
                    // this.addImageProduct()
                },
                'date_start.show' : function(val){
                    if(val.length == 10){
                        this.date_start.value = val;
                        this.date_start.show = moment(val).format('ll');
                    }
                },
                'date_end.show' : function(val){
                    if(val.length == 10){
                        this.date_end.value = val;
                        this.date_end.show = moment(val).format('ll');
                    }
                }
            },
            computed: {
                loopBackImgProducts: function() {

                    var arrBackProducts = [];
                    for(i=this.arrImages.length-1 ; i>=0 ; i--){
                        arrBackProducts.push(this.arrImages[i]);
                    }
                    return arrBackProducts;
                }
            },
            mounted: function(){

                this.getCategory();
                this.getPronoun();
                // this.getFBGroups();
                this.getProfile();

                this.getSellerOrder();
                this.getOrderWaiting();
                this.getSellerOrderSummery();
            },
            methods: {
                btn_share : function(){
                    FB.ui({
                        method: 'share',
                        mobile_iframe: true,
                        href: '{{ config('app.url') }}shop/order/'+vm.post_id,
                    }, function(response){});
                },
                getOrderWaiting(){
                    this.$http.get('/api/v1/seller/order/waiting').then((myresponse) => {
                        this.count_waiting = myresponse.data.length;
                    }, (myresponse) => {
                        // error callback
                        console.log(myresponse.data);
                    });
                },
                getProfile : function () {
                    this.$http.get('/api/v1/profile').then(function(myresponse) {
                        this.profile = myresponse.data;

                        if(this.profile.img_profile == null){
                            this.profile.img_profile = '/image/default/user.png'
                        }

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                transferDateType: function(val){// 10/12/2018 -> 2018-12-10
                    if(val == ""){
                        return "";
                    }else{
                        var date_start = val;
                        var arr_date_start = date_start.split('/');

                        return arr_date_start[2]+'-'+arr_date_start[1]+'-'+arr_date_start[0];

                    }

                },
                addLocation: function(){

                    this.locations.push({displayname: this.location.displayname, name : this.location.name});

                    this.location.displayname = '';
                    this.location.name = '';
                },
                delLocation: function (index) {
                    vm.locations.splice(index, 1);

                },
                setAsCover : function(img){
                    for(i=0;i<this.arrImages.length;i++){
                        if(this.arrImages[i].image_cover != img.image_cover){
                            this.arrImages[i].status = 0;

                        }else{
                            this.arrImages[i].status = 1;

                        }
                    }
                },
                getCategory : function (id) {
                    this.$http.get('/api/v1/category').then(function(myresponse) {
                        this.categorys = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                getPronoun : function (id) {
                    this.$http.get('/api/v1/pronoun').then(function(myresponse) {
                        this.pronouns = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                // getFBGroups : function (id) {
                //     this.$http.get('/api/fb/groups').then(function(myresponse) {
                //         this.fb.groups = myresponse.data;
                //
                //     }, function(myresponse) {
                //         // error callback
                //         console.log(myresponse.data);
                //     });
                //
                // },

                getSellerOrder : function () {
                    this.$http.get('/api/v1/sellerorder').then(function(myresponse) {
                        this.sellers = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                getSellerOrderSummery : function () {
                    this.$http.get('/api/v1/seller/order/summary').then(function(myresponse) {
                        this.summary = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                confirmOrder : function (order) {

                    if(status == 'one'){
                        var param = {order_id:order.id}
                    }else{
                        var param = {post_id:order.id}

                    }

                    vm.$http.post('/api/v1/seller/order/confirm',{order_id:order.id}).then(function(myresponse) {
                        vm.getSellerOrder();
                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                confirmOrderAll : function (post) {

                    vm.$http.post('/api/v1/seller/order/confirmall',{order_id:post.id}).then(function(myresponse) {
                        vm.getSellerOrder();
                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                addImageProduct : function () {
                    var formData = new FormData();
                    formData.append('images', vm.arrProducts[vm.arrProducts.length-1]);

                    vm.$http.post('/s3-base64-upload',{'image':vm.arrProducts[vm.arrProducts.length-1], 'folder':'product'}).then(function(myresponse) {
                        console.log(myresponse.data);

                        if(this.arrImages.length == 0){
                            this.arrImages.push({image_cover: myresponse.data, status:1})

                        }else{
                            this.arrImages.push({image_cover: myresponse.data, status:0})

                        }


                    }, function(myresponse) {

                        console.log(myresponse.data);
                    });
                },
                addProduct : function () {
                    // this.$validate()
                    //     .then(function(success) {
                    //         if (success) {
                    //             if($('#image_market')[0].files.length == 0) {
                    //                 this.validators_image = 'กรุณาเลือกรูปตลาด';
                    //             }else{
                    swal({
                        title: 'กำลังบันทึกข้อมูล',
                        text: '(ปิดอัติโนมัติ)',
                        onOpen: () => {
                            swal.showLoading()
                        }
                    })

                    var product = {
                        category_id: vm.category.id,
                        pronoun_id: vm.pronoun.id,
                        image:vm.arrImages,
                        title:vm.title,
                        name:vm.name,
                        description:vm.description,
                        item_condition:vm.item_condition.id,
                        location_type: (vm.location_type==true)?1:0,
                        location: vm.locations,
                        specify_type: vm.specify_type,
                        date_start :vm.date_start.value+' '+vm.time_start,
                        date_end :vm.date_end.value+' '+vm.time_end,
                        product: vm.arrSKUs,
                        groups: vm.groupSelect
                    };

                    console.log(JSON.stringify(product));

                    vm.$http.post('/api/v1/post',product).then(function(myresponse) {
                        // $('#addModal').modal('hide');
                        swal.close()

                        swal(
                            'เพิ่มสินค้าสำเร็จ',
                            '',
                            'success'
                        )

                        console.log('after : '+ JSON.stringify(myresponse.data.after));

                        this.post_id = myresponse.data.after.id;
                        $('#share_fb').modal('show');

                        // setTimeout(function(){
                        //     location.reload();
                        // })

                    }, function(myresponse) {
                        // error callback
                        swal.close()

                        swal(
                            'เกิดข้อผิดพลาด',
                            'กรุณากดบันทึกใหม่',
                            'error'
                        )
                        console.log(myresponse.data);
                    });

                        //         }
                        //
                        //     }
                        // });

                },

                addSKU : function () {
                    // vm.arrSKUs.push({name: this.sku.name, price:this.sku.price, amount:this.sku.amount });
                    vm.arrSKUs.push({name: "", price:"", amount:"" });

                    this.sku={};
                },
                delSKU: function (index) {
                    vm.arrSKUs.splice(index, 1);

                },
                delImgProduct : function(image){

                    swal({
                        title: 'ต้องการลบใช่หรือไม่?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'ยกเลิก',
                        confirmButtonText: 'ใช่ลบเลย!'
                    }).then((result) => {
                        if (result.value) {
                            vm.arrImages.find(function(val,index){
                                if(val.image_cover == image.image_cover){

                                    vm.arrImages.splice(index, 1);
                                    if(image.status == 1){
                                        vm.arrImages[0].status = 1;
                                    }

                                    return;
                                }
                            })
                        }
                    })


                },
                openFile : function (event) {
                    var input = event.target;

                    var reader = new FileReader();
                    reader.onload = function(){
                        var dataURL = reader.result;
                        var output = document.getElementById('output');
                        output.src = dataURL;

                        vm.haveFile = true;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
	</script>

@endsection

