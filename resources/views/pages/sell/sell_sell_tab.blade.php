<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding">
  @include('pages/sell/sell_images_cropper')

  {{--@include('pages/sell/sell_images_add')--}}
</div>
<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">

     <div class="col-xs-12">
        <input type="text" name="product_name" class="form-control bd-gray input-lg" v-model="name" placeholder="ชื่อสินค้า"/>
    </div>

    <div class="col-xs-12 mg-top-20 ">
      <div class="bordor-sty-gray">
        <div class="box-input">
            <input type="text" name="short_msg" class="input-sty1" v-model="title" placeholder="คำโปรย"/>
        </div>
        <div class="box-textarea">
            <textarea class="textarea-sty1" name="product_detail" rows="8" cols="5" v-model="description" placeholder="รายละเอียดสินค้า" style="width:100%;"></textarea>
        </div>
      </div>
    </div>

    <div class="col-xs-12 mg-top-20">

        <div class="col-xs-6 blue-capsule-sort" style="margin-left: 2%; width: 48%;">
            {{--<div class="col-xs-12 blue-capsule-sort" style="padding-left: 8px">--}}
                {{----}}
            {{--</div>--}}

            <div class="col-xs-4" style="    padding-top: 8px;">
                <span class="mg-right-10">หมวดหมู่</span> |
            </div>
            <div class="col-xs-8">
                <v-select label="name" :options="categorys" v-model="category"></v-select>
            </div>
        </div>

        <div class="col-xs-6 blue-capsule-sort">

           <div class="col-xs-4" style="    padding-top: 8px;">
               <span class="mg-right-10">หน่วยนับ</span> |
           </div>
           <div class="col-xs-8">
               <v-select label="name" :options="pronouns" v-model="pronoun"></v-select>
           </div>

        </div>
    </div>
    <div class="col-xs-12 mg-top-20" v-if="category.id != 2 && category != ''">

      <div class="col-xs-6 blue-capsule-sort pull-left">

           <div class="col-xs-5" style="    padding-top: 8px;">
               <span class="mg-right-10">สภาพสินค้า</span> |
           </div>
           <div class="col-xs-7">
               <v-select label="name" :options="item_conditions" v-model="item_condition"></v-select>
           </div>

       </div>
       <div class="col-xs-6"></div>

    </div>


    <div class="col-xs-12 mg-top-30">
        @include('pages.sell.sell_add_sku')
    </div>


    <div class="col-xs-12 mg-top-20">
        @include('pages/sell/sell_time_add')
    </div>

    <div class="col-xs-12 mg-top-20">
        <div class="col-xs-12 mg-top-20 share-social bordor-sty-gray">
            <div class="panel-group  black-bd-bt" id="accordion1">
                <div class="panel panel-default" style="border:0px;border-color:#fff;">
                  <div class="panel-heading bg-none black-bd-bt">
                    <h4 class="panel-title">
                      <a id="dp1" data-toggle="collapse" data-parent="#accordion1" href="#collapse1">
                      <span class="pul-left h4 tx-black">แชร์ไปยังตลาด</span>
                      <span class="pull-right glyphicon glyphicon-menu-down dp-down"></span>
                      <span class="pull-right glyphicon glyphicon-menu-up dp-up"></span>
                      </a>
                    </h4>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse in" >
                    <div class="panel-body no-padd" >
                        <div class="checkbox checkbox-primary ">
                          <input id="checkboxs1" type="checkbox" checked >
                          <label for="checkboxs1" class="h4 tx-gray">
                              Nectec Markets 1
                          </label>
                        </div>
                        <div class="checkbox checkbox-primary ">
                          <input id="checkboxs2" type="checkbox"  >
                          <label for="checkboxs2" class="h4 tx-gray">
                              Nectec Markets 2
                          </label>
                        </div>
                        <div class="checkbox checkbox-primary ">
                          <input id="checkboxs3" type="checkbox"  >
                          <label for="checkboxs3" class="h4 tx-gray">
                              Nectec Markets 3
                          </label>
                        </div>
                    </div>
                  </div>
                </div>
            </div>

            {{--<div class="panel-group  black-bd-bt" id="accordion2">--}}
                {{--<div class="panel panel-default" style="border:0px;border-color:#fff;">--}}
              {{--<div class="panel-heading bg-none black-bd-bt" style="background: #4266b2 !important;">--}}
                {{--<h4 class="panel-title">--}}
                  {{--<a id="dp2" data-toggle="collapse" data-parent="#accordion2" href="#collapse2">--}}
                  {{--<span class="pul-left h4 tx-black" style="color:white !important;">--}}
                      {{--<i class="fa fa-facebook-square"></i> แชร์ไปยังเฟสบุ๊ค--}}

                  {{--</span>--}}
                  {{--<span class="pull-right glyphicon glyphicon-menu-down dp-down"></span>--}}
                  {{--<span class="pull-right glyphicon glyphicon-menu-up dp-up"></span>--}}
                  {{--</a>--}}
                {{--</h4>--}}
              {{--</div>--}}
              {{--<div id="collapse2" class="panel-collapse collapse in" >--}}
                {{--<div class="panel-body no-padd" >--}}
                    {{--<div class="checkbox checkbox-primary " v-for="group in fb.groups.data">--}}
                        {{--<input :id="group.id" type="checkbox" :value="group" v-model="groupSelect">--}}
                        {{--<label :for="group.id" class="h4 tx-gray">--}}

                          {{--@{{ group.name }}--}}
                      {{--</label>--}}
                    {{--</div>--}}
                {{--</div>--}}
              {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

        </div>
    </div>

    <div class="col-xs-12 col-sm-12  mg-top-30">
     <!--  <center>
        <button class="btn-preview confirm-modal">
        <span>PREVIEW</span>
        </button>
      </center> -->
      <div class="col-md-12 col-xs-12 box-wait mg-top-30 mg-b-20">
        <center>
          <button class="btn-preview confirm-modal" @click="$('#sell_confirmModal').modal('show')" v-bind:class="{'disabled': name == '' && description == '' }">
            <span>ดูตัวอย่าง</span>
          </button>

          <button class="btn-md-blue-w-p" >
            <span class="mg-right-10">ยกเลิก</span>
          </button>
        </center>
      </div>
    </div>

</div>

<!-- reserv modal -->
@include('modal/sell_modal_confirm')
