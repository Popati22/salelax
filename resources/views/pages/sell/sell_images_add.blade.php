 <style>
      .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 2px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 100%;
        height: 350px;
      }

      .cropit-preview-image-container {
        cursor: move;
      }

      .cropit-preview-background {
        opacity: .2;
        cursor: auto;
      }

      .image-size-label {
        margin-top: 10px;
      }

      input, .export {
        /* Use relative position to prevent from being covered by image background */
        position: relative;
        z-index: 10;
      }

      button {
        margin-top: 10px;
      }
</style>

<div class="col-md-12 mg-bottom-20 border-all-gray m-img">
    <div class="col-md-12">
      <a href="javascript:void(0);" class="img_modal">
       <center> <img src="{{ asset('image/sell/bag1.png') }}" class="images-thumbnail img-responsive"></center>
       <input type="hidden" value="{{ asset('image/sell/bag1.png') }}" class="path_img">
      </a>       
    </div>
    <div class="col-md-12 img-mg-t-m">
    	<button type="button" class="btn btn-cover pull-left">COVER</button>
    	<button type="button" class="btn btn-minus pull-right">
    		<span class="glyphicon glyphicon-minus"></span>
    	</button>
    </div>
</div>

<div class="col-md-12 mg-bottom-20 border-all-gray m-img">
    <div class="col-md-12">
      <!-- <a href="{{ asset('image/sell/bag2.png') }}" target= "_blank"> -->
      <a href="javascript:void(0);" class="img_modal">
        <center><img src="{{ asset('image/sell/bag2.png') }}" class="images-thumbnail img-responsive"></center>
        <input type="hidden" value="{{ asset('image/sell/bag2.png') }}" class="path_img">
      </a>
    </div>
    <div class="col-md-12 img-mg-t-m">
    	<button type="button" class="btn btn-set-cover pull-left">SET AS COVER</button>
    	<button type="button" class="btn btn-minus pull-right">
    		<span class="glyphicon glyphicon-minus"></span>
    	</button>
    </div>
</div>




<div class="col-md-12 no-padd mg-top-20 editor-img">
	<div class="image-editor">
		<div class="col-md-12 no-padd mg-top-10 mg-bottom-20">
		  <div class="cropit-preview mg-bottom-10"></div>
		  <center><input type="file" id="newProduct" class="cropit-image-input mg-top-20"><br></center>
		  <div class="image-size-label mg-top-20 tx-center tx-blue">
			Resize image
		  </div>
		  <input type="range" class="cropit-image-zoom-input">
			<center>
			  <button class="rotate-ccw btn-blue-img">
				<!-- <span class="glyphicon glyphicon-minus"></span> -->
				Rotate right
			  </button>
			  <button class="rotate-cw btn-blue-img">
				Rotate left
			  </button>
			  <button class="btn-blue-img">OK</button>
			</center>
		</div>
	</div>
</div>













