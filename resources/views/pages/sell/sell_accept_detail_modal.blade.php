<div class="modal fade" id="accept_detail_modal" role="dialog">
<div class="modal-dialog modal-md">
    <div class="modal-content modal-redius30 no-padd">   
    <form class="bs-example form-horizontal"> 
      <div class="modal-body no-padd">
        <header class="panel-heading bg-head-re lt no-border ">
          <div class="clearfix mg-l-r1">
            <div class="clear">
              <div class="h2 m-t-xs m-b-xs">
                <span>อมยิ้ม MACARON</span>              
              </div>
              <span class="h3 tx-weight1 pull-left">ยอดชำระ</span>  
              <span class="h1 tx-yellow pull-right">
                100
                <span class="h3 tx-weight1 tx-white">บาท</span>
              </span>
            </div>                
          </div>
        </header>
        <div class="col-sm-12 mg-l-r1">

        <div class='col-md-12 border-bt-gray mg-top-20 mg-bottom-20 '>                              
            <!-- <input type='text' class="input-lg input-sty1  form-control no-padd" 
             placeholder="รายการสั่งซื้อ"/>  -->
             <h4 class="h4 tx-gray">รายการสั่งซื้อ</h4>                                               
        </div>

        @component('components/table_normal',['list' => [
            [  
                'header' => [
                  'col1' => 'รสชาติ',
                  'col2' => 'ราคา / ชื้น',
                  'col3' => 'จำนวนสั่งซื้อ',
                ],
                'row' => [
                    [
                      'title' => 'ส้ม',
                      'price' => 'Barr Lovery',
                      'amount' => '125 ชิ้น'
                    ],
                    [
                      'title' => 'ส้ม',
                      'price' => 'Barr Lovery',
                      'amount' => '125 ชิ้น'
                    ],
                    [
                      'title' => 'ส้ม',
                      'price' => 'Barr Lovery',
                      'amount' => '125 ชิ้น'
                    ],
                    [
                      'title' => 'ส้ม',
                      'price' => 'Barr Lovery',
                      'amount' => '125 ชิ้น'
                    ],            
                ]
              ]
          ]
        ])

        @endcomponent  
        </div>

        <div class="col-sm-12 border-t-gray ">
          <form class="bs-example form-horizontal">   
            <div class="form-group h4 mg-l-r1">
              <label class="col-lg-4 control-label tx-left tx-weight3">สถานที่รับสินค้า : </label>
              <div class="col-lg-8">
                <span class="help-block m-b-none tx-blue tx-weight3">ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค</span>
              </div>
            </div>
            <div class="form-group h4 mg-l-r1">
              <label class="col-lg-4 control-label tx-left tx-weight3">จุดนัดรับสินค้า : </label>
              <div class="col-lg-8">
                <span class="help-block m-b-none tx-blue tx-weight3">โต๊ะจ่ายตั๋ว</span>
              </div>
            </div>   
            <div class="form-group h4 mg-l-r1">
              <label class="col-lg-2 col-xs-3 control-label tx-left tx-weight3">วันที่ : </label>
              <div class="col-lg-5 col-xs-9 ">
                <span class="help-block m-b-none tx-blue tx-weight3">30 กรกฏาคม 2560</span>
              </div>
              <label class="col-lg-2 col-xs-3 control-label tx-left tx-weight3">เวลา : </label>
              <div class="col-lg-3 col-xs-9">
                <span class="help-block m-b-none tx-blue tx-weight3">15.29 น.</span>
              </div>
            </div>                     
          </form>
        </div>

        <div class='col-md-12  mg-top-20 mg-bottom-20 '>                              
            <input type='text' class="input-lg input-sty1 border-bt-gray form-control" 
             placeholder="ข้อความจากผู้ซื้อ"/>                                                
        </div>

        <div class="col-sm-12 mg-top-20">  
            @component('components/data_user',[
                'title' => 'ข้อมูลผู้ซื้อ',
                'seller_name' => 'Arat09',
                'seller_link' => '/shop/seller',
                'rating' => 155,
                'facebook' => 'facebook.com',
                'twitter' => 'twitter.com']) 
                @slot('avatar')
                    {{asset('image/shop/im_member4.png')}}
                @endslot               
            @endcomponent    
        </div>     

      </div>            
      <!--end modal-body -->
      <div class="modal-footer" style="text-align:center;">
          <div class="col-sm-12 padd-t-20 mg-top-30">
              <button class="btn-modal-confirm pull-left" style="padding: 8px;">      
              <span class="h4">ตอบรับการซื้อสินค้า</span>
              </button>                
            </div> 
          <div class="col-xs-12 padd-t-20">
              <button class="btn-modal-cancel pull-left sell-cancle" >  <!-- data-dismiss="modal" -->
              <span class="h4">ปฏิเสธการสั่งซื้อ</span>
              </button>
          </div>            
      </div>    
      </form>
  </div>
</div>
</div>



<div class="modal fade" id="sell_cancle_modal" role="dialog">
<div class="modal-dialog modal-w400">
    <div class="modal-content modal-redius30">
        <form class="bs-example form-horizontal"> 
            <div class="modal-body">
            <div class="panel-body panel-reserv">                    
                <div class="form-group form-group-head" style="padding-bottom:0px !important;">
                  <div class="col-lg-12">
                    <div class="h3 tx-center">คุณกำลังปฏิเสธการสั่งซื้อ</div>
                  </div>   
                  <div class='col-md-12 mg-top-20'>                              
                    <input type='text' class="input-lg input-sty1  form-control no-padd"  
                    placeholder="โปรดระบุเหตุผลในการปฏิเสธ"/>                                                
                  </div>

                </div>  
            </div>
            </div>            
            <!--end modal-body -->
            <div class="modal-footer mg-top-20" style="text-align:center;">
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <button class="btn-modal-cancel pull-left" data-dismiss="modal">      
                    <span>ยกเลิก</span>
                    </button>
                </div>
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <button class="btn-modal-confirm pull-left" >      
                    <span>ยืนยัน</span>
                    </button>                
                </div>   
            </div>    
        </form>
  </div>
</div>
</div>





<!-- //=========== Modal total tab ================== -->

<div class="modal fade" id="detail_total" role="dialog">
<div class="modal-dialog modal-md">
   <div class="modal-content modal-redius30 no-padd">
   <form class="bs-example form-horizontal">
   <div class="modal-body no-padd">
       <header class="panel-heading bg-head-re2 lt no-border ">
         <div class="clearfix mg-l-r1">
           <div class="clear">
             <div class="h2 m-t-xs m-b-xs tx-center">
               <span>นกน้อยคัพเค้ก</span>
             </div>
           </div>
         </div>
       </header>
       <!-- <div class="col-sm-12 mg-l-r1"> -->

       <div class="col-md-12 mg-top-20 mg-bottom-20 tx-center">
            <h3 class="h3 tx-blue">
                <b>ยอดรวม </b>
                รายการสั่งซื้อ ณ ปัจจุบัน
            </h3>
            <h4 class="h4 tx-gray"> (Sun Nov 19 , 28:52 am)</h4>
       </div>

       <div class="col-md-12">
       <table class="table table-normal mg-top-20">

           <tbody class="table-body-sell">
               <tr class="h4 tx-gray">
               <td class="tx-left" >รสสตอเบอรี่</td>
               <td class="tx-right">10 ชิ้น</td>
             </tr>
             <tr class="h4 tx-gray">
               <td class="tx-left" >รสสัปะรด</td>
               <td class="tx-right">10 ชิ้น</td>
             </tr>
             <tr class="h4 tx-gray">
               <td class="tx-left" >รสช้อกโกแลต</td>
               <td class="tx-right">10 ชิ้น</td>
             </tr>
             <tr class="h4 tx-gray border-bt-gray" >
               <td class="tx-left">รถบลูเบอรี่</td>
               <td class="tx-right">20 ชิ้น</td>
            </tr>
            <tr class="h2 tx-black border-bt-gray">
              <td class="tx-right " colspan="2">50 ชิ้น</td>
            </tr>
            <tr class="border-bt-gray">
              <td class="tx-left h4 tx-gray" >รวมเงิน</td>
              <td class="tx-right h2 tx-black" colspan="2">12,500 บาท</td>
            </tr>
           </tbody>
       </table>
       </div>
 <!-- </div> -->
 </div>
 </form>
     <!--end modal-body -->
     <div class="modal-footer" style="text-align:center;">
         <div class="col-xs-12 padd-t-20">
             <div class="arrow-up2"></div>
             <div class="arrow-up"></div>
             <div class="">
                 <button class="btn-list-accept pull-left">  <!-- data-dismiss="modal" --> 
                  <div class="col-xs-2">
                      <img src="{{ asset('image/sell/icon-sell-blue.png') }}" 
                      class="images-thumbnail img-responsive" style="width:100%;">
                  </div>
                  <div class="col-xs-10" style="padding-top:5px;">
                    <span class="h4">คุณมีรายการ</span>
                    <span class="h4"><b>"รอการตอบรับ 3 รายการ"</b></span>
                  </div>
                 </button>
             </div>
         </div>
         <div class="col-sm-12 padd-t-20 mg-top-10">
              <button class="btn-modal-confirm pull-left" style="padding: 8px;">  
                  <div class="col-xs-10 no-padd" style="padding-top:5px;">
                    <span class="h4 tx-white">EXPORT TO EXCEL</span>
                  </div>
                   <div class="col-xs-2 no-padd">
                      <img src="{{ asset('image/sell/icon-excel.png') }}" 
                      class="images-thumbnail img-responsive" style="width:60%;">
                  </div>
              </button>                
        </div> 
     </div>

 </div>
</div>
</div>





<div class="modal fade" id="cancle_deliver_modal" role="dialog">
<div class="modal-dialog modal-w400">
    <div class="modal-content modal-redius30">
        <form class="bs-example form-horizontal" action="#">
            <div class="modal-body">
            <div class="panel-body panel-reserv">
                <div class="form-group form-group-head" style="padding-bottom:0px !important;">
                  <div class="col-lg-12">
                    <div class="h3 tx-center">คุณกำลังยกเลิกการส่งสินค้า</div>
                  </div>
                  <div class="col-md-12 mg-top-20">
                    <input type="text" class="input-lg input-sty1  form-control no-padd" placeholder="โปรดระบุเหตุผลในการยกเลิก">
                  </div>
                </div>
            </div>
            </div>
            <!--end modal-body -->
            <div class="modal-footer mg-top-20" style="text-align:center;">
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <button class="btn-modal-cancel pull-left" data-dismiss="modal">
                    <span>ยกเลิก</span>
                    </button>
                </div>
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <button class="btn-modal-confirm pull-left">
                    <span>ยืนยัน</span>
                    </button>
                </div>
            </div>
        </form>
  </div>
</div>
</div>


<div class="modal fade" id="change_deliver_modal" role="dialog">
<div class="modal-dialog modal-w400">
    <div class="modal-content modal-redius30">
        <form class="bs-example form-horizontal" action="#">
            <div class="modal-body">
            <div class="panel-body panel-reserv">
                <div class="form-group form-group-head" style="padding-bottom:0px !important;">
                  <div class="col-lg-12">
                    <div class="h3 tx-center">คุณกำลังเลื่อนการส่งสินค้า</div>
                  </div>
                  <div class="col-md-12 mg-top-20">
                    <input type="text" class="input-lg input-sty1  form-control no-padd" placeholder="โปรดระบุรายละเอียดการเปลี่ยนแปลง">
                  </div>
                </div>
            </div>
            </div>
            <!--end modal-body -->
            <div class="modal-footer mg-top-20" style="text-align:center;">
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <button class="btn-modal-cancel pull-left" data-dismiss="modal">
                    <span>ยกเลิก</span>
                    </button>
                </div>
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <button class="btn-modal-confirm pull-left">
                    <span>ยืนยัน</span>
                    </button>
                </div>
            </div>
        </form>
  </div>
</div>
</div>

<div class="modal fade" id="warnning_deliver_modal" role="dialog">
<div class="modal-dialog modal-w400">
    <div class="modal-content modal-redius30">
        <form class="bs-example form-horizontal" action="#">
            <div class="modal-body">
            <div class="panel-body panel-reserv">
                <div class="form-group" style="padding-bottom:0px !important;">
                  <div class="col-lg-12">
                    <div class="h4 tx-center tx-black">กรุณาระบุ</div>
                    <div class="h3 tx-center tx-black">
                      <b>การเปลี่ยนแปลงการส่งสินค้า</b>
                    </div><br>
                    <center>
                      <img src="{{ asset('image/sell/icon-wanning.png') }}" 
                      class="images-thumbnail img-responsive" style="width:40%;">
                    </center>
                  </div>
                </div>
            </div>
            </div>
            <!--end modal-body -->
            <div class="modal-footer" style="text-align:center;">
                <div class="col-xs-12">
                    <button class="btn-modal-confirm btn-change-total1 pull-left">
                    <span>เลื่อนการส่งสินค้า</span>
                    </button>
                </div>
                <div class="col-xs-12 padd-t-20">
                    <button class="btn-modal-cancel btn-change-total2 pull-left" >
                    <span>ยกเลิกการขาย</span>
                    </button>
                </div>
            </div>
        </form>
  </div>
</div>
</div>




<!--read : detail sell -->
<div class="modal fade" id="sell_detail" role="dialog">
<div class="modal-dialog modal-w1200">
    <div class="modal-content modal-redius-close" >   
        <div class="modal-header no-padd">
          <button type="button" class="close h3" data-dismiss="modal">&times;</button>
        </div>
        <form class="bs-example form-horizontal"> 
        <div class="modal-body">
            <div class="panel-body panel-reserv">                    
              <div class="col-lg-4">
                <img src="{{asset('image/shop/im_sell4.png')}}" class=" img-responsive">
             </div>   
             <div class="col-lg-8">
                @component('components/shop/shop_goods_detail')
                    @slot('name')
                    อมยิ้ม MACARON
                    @endslot
                    @slot('desc')
                        ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า
                    @endslot
                    @slot('ingredients')
                        มีส่วนประกอบหลัก คือ ไข่ขาว อัลมอนต์บดละเอียดและน้ำตาลปน 
                        มีลักษณะคล้ายคุกกี้ชิ้นเล็กสองอันประกบกัน มีไส้ตรงกลาง มีสีสันสดใส
                        กรอบนอกนุ่มใน สอดไส้ตรงกลางด้วยครีมกานาซ (Ganache) มีหลากหลายรสชาติ
                        เช่น ช็อกโกแลต สตรอเบอร์รี่ วานิลลา กาแฟ อัลมอนต์ หรือรสผลไม้ตามฤดูกาล
                    @endslot
                @endcomponent

                <div class="col-xs-12 mg-top-20 no-padd">                  
                  @component('components/table_normal',['list' => [
                    [  
                        'header' => [
                          'col1' => 'รสชาติ',
                          'col2' => 'ราคา / ชื้น',
                          'col3' => 'จำนวนสั่งซื้อ',
                        ],
                        'row' => [
                            [
                              'title' => 'ส้ม',
                              'price' => 'Barr Lovery',
                              'amount' => '125 ชิ้น'
                            ],
                            [
                              'title' => 'ส้ม',
                              'price' => 'Barr Lovery',
                              'amount' => '125 ชิ้น'
                            ],
                            [
                              'title' => 'ส้ม',
                              'price' => 'Barr Lovery',
                              'amount' => '125 ชิ้น'
                            ],
                            [
                              'title' => 'ส้ม',
                              'price' => 'Barr Lovery',
                              'amount' => '125 ชิ้น'
                            ],            
                        ]
                      ]
                  ]
                ])
                @endcomponent
                </div>


                <div class="col-xs-12 mg-top-20 border-bottom-price" ></div>
              
                @component('components/shop/shop_goods_time', ['time' => '2 วัน', 'condition' => 'สินค้าใหม่'])  
                @endcomponent


             </div>              

            </div>
        </div>            
        <!--end modal-body -->

        <div class="modal-footer" style="text-align:center;">          
            <div class="col-xs-12 border-t-gray padd-t-30">
                <button class="btn-modal-confirm" data-dismiss="modal" style="width:30%; padding: 8px;" v-on:click="addProduct()">
                    <span>ปิด</span>
                </button>                
            </div>            
        </div>    
        </form>

    </div>
</div>
</div>