<div v-if="sellers.length > 0">

    <div class="panel-group" id="accordion">

    <div class="panel panel-default panel-sell" v-for="post in sellers">
        <div class="panel-heading panel-head-sell">
                <table class="table table-accept" border="0">
                    <thead>
                        <tr class="h4">

                            <th style="width:4%;" class="padd-l-5">
                                <a class="a-sell-panel " data-toggle="collapse" data-parent="#accordion" :href="'#collapse'+post.id">

                                <div class="tx-center">
                                    <b class="badge badge-tg" style="margin-top:-3px;">
                                        <span class="glyphicon glyphicon-menu-down tx-i-white"></span>
                                        <span class="glyphicon glyphicon-menu-up tx-i-white hide"></span>

                                    </b>
                                </div>
                                </a>
                            </th>
                            <th class="tx-black" style="width:28%;">
                                <a class="a-sell-panel " data-toggle="collapse" data-parent="#accordion" :href="'#collapse'+post.id">
                                    @{{ post.title }}
                                </a></th>
                            <th class="tx-gray tx-center">@{{ post.order.length }} รายการ</th>

                            <th class="tx-center" style="width:20%;"></th>
                            <th class="tx-center">
                                <button class="btn-sm-blue pull-right mg-t-1 w-b-acp" @click="confirmOrderAll(post)">
                                    <span class="mg-right-10 tx-center">ตอบรับทั้งหมด</span>
                                </button>
                            </th>
                        </tr>
                    </thead>
                </table>

        </div>
        <div :id="'collapse'+post.id" class="panel-collapse collapse">
            <div class="panel-body panel-body-sell no-padd">

                <table class="table table-accept mg-top-20 mg-b-20" border="0" v-for="order in post.order" v-if="order.status == 1">
                    <tbody>
                        <tr class="h4" v-for="product in order.orderdetail">
                            <td style="width:4%;" class="padd-l-5"></td>
                            <td class="tx-black" style="width:30%;">
                                <div class="col-xs-2 goods-avatar text-left no-padd">
                                    <center>
                                        <img v-if="order.buyer_img_profile!=null" :src="order.buyer_img_profile" class="w-avatar">
                                        <img v-else src="/image/default/user.png" class="w-avatar">

                                    </center>
                                </div>
                                <div class="col-xs-10" style="padding-top:10px;">
                                    @{{ product.name }}
                                </div>
                            </td>
                            <td class="tx-blue tx-center">จำนวน @{{ product.quantity }} ชิ้น</td>
                            <td class="tx-center tx-gray" style="width:20%;">รวมเงิน @{{ product.sub_total }} บาท</td>
                            <td class="tx-center">
                                <div class="col-sm-6 pd-ac">
                                    <button class="btn-sm-yell sell-detail-modal b-ac1 pull-right">
                                        <span class="mg-right-10">รายละเอียด</span>
                                    </button>
                                </div>
                                <div class="col-sm-6 no-padd">
                                    <button class="btn-sm-blue pull-right b-ac2" @click="confirmOrder(order)">
                                        <span class="mg-right-10">ตอบรับ</span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    {{--@component('pages/sell/sell_accept_list_panel',['list' => [--}}
            {{--[  --}}
              {{--'no'  => '1',--}}
              {{--'title' => 'อมยิ้ม MACARON',--}}
              {{--'detail' => 'คงเหลือ 5 ชิ้น',--}}
              {{--'toggle' => 'in',--}}
              {{--'data' =>[--}}
                  {{--[--}}
                    {{--'img' => asset('image/shop/im_member4.png'),--}}
                    {{--'title' => 'นกน้อยคัพเค้ก',--}}
                    {{--'detail' => 'คงเหลือ 5 ชิ้น',--}}
                    {{--'price' => '125'--}}
                  {{--],--}}
                  {{--[--}}
                    {{--'img' => asset('image/shop/im_member4.png'),--}}
                    {{--'title' => 'นกน้อยคัพเค้ก',--}}
                    {{--'detail' => 'คงเหลือ 5 ชิ้น',--}}
                    {{--'price' => '125'--}}
                  {{--],--}}
              {{--]--}}
            {{--],--}}
            {{--[  --}}
              {{--'no'  => '2',--}}
              {{--'title' => 'อมยิ้ม MACARON',--}}
              {{--'detail' => 'คงเหลือ 5 ชิ้น',--}}
              {{--'toggle' => '',--}}
              {{--'data' =>[--}}
                  {{--[--}}
                    {{--'img' => asset('image/shop/im_member4.png'),--}}
                    {{--'title' => 'นกน้อยคัพเค้ก',--}}
                    {{--'detail' => 'คงเหลือ 5 ชิ้น',--}}
                    {{--'price' => '125'--}}
                  {{--],--}}
                  {{--[--}}
                    {{--'img' => asset('image/shop/im_member4.png'),--}}
                    {{--'title' => 'นกน้อยคัพเค้ก',--}}
                    {{--'detail' => 'คงเหลือ 5 ชิ้น',--}}
                    {{--'price' => '125'--}}
                  {{--],--}}
              {{--]--}}
            {{--],--}}
            {{--[  --}}
              {{--'no'  => '3',--}}
              {{--'title' => 'อมยิ้ม MACARON',--}}
              {{--'detail' => 'คงเหลือ 5 ชิ้น',--}}
              {{--'toggle' => '',--}}
              {{--'data' =>[--}}
                  {{--[--}}
                    {{--'img' => asset('image/shop/im_member4.png'),--}}
                    {{--'title' => 'นกน้อยคัพเค้ก',--}}
                    {{--'detail' => 'คงเหลือ 5 ชิ้น',--}}
                    {{--'price' => '125'--}}
                  {{--],--}}
                  {{--[--}}
                    {{--'img' => asset('image/shop/im_member4.png'),--}}
                    {{--'title' => 'นกน้อยคัพเค้ก',--}}
                    {{--'detail' => 'คงเหลือ 5 ชิ้น',--}}
                    {{--'price' => '125'--}}
                  {{--],--}}
              {{--]--}}
            {{--],--}}
        {{--]--}}
      {{--])--}}
    {{--@endcomponent  --}}
</div>

</div>

<div v-else style="text-align: center;">

    <h4 style="margin-top: 20%;
font-size: 2em;
color: gray;
margin-bottom: 20%;">ไม่พบข้อมูล</h4>

</div>