@foreach($list as $key => $val)

<div style="margin-top:40px;">

<div class="col-xs-1">
    <img src="{{ asset('image/sell/icon-list.png') }}" 
      class="images-thumbnail img-responsive btn-total" style="width:100%; position: relative; z-index: 999;">
</div>

<div class="col-xs-11">
    <button class="btn-reserv  btn-sell-detail padding-10" style="width:80%;">      
        <span class="mg-left-15 h3 tx-weight2" 
         style="position: relative; z-index: 999;" >
         {{ $val['title'] }}</span>
        <span class="pull-right glyphicon glyphicon-menu-right"></span>
    </button>
</div>

</div>

<div class="col-xs-12 mg-bottom-20 border-blue" style="margin-top:-40px;">

   
    <div class="row box-wait-body">
    	<div class="col-xs-12 col-xs-12 box-wait " style="margin-bottom:50px;">
            <table class="table table-normal mg-top-20">
            <tbody class="table-body-sell">
            @foreach($val['row'] as $key => $data)
              <tr class="h4 tx-gray">
                <td class="tx-left" >{{ $data['name'] }}</td>
                <td class="tx-right">{{ $data['amount'] }} ชิ้น</td>
              </tr>
            @endforeach
             
             <tr class="h2 tx-black border-bt-gray">
               <td class="tx-right " colspan="2">{{ $val['count'] }} ชิ้น</td>
             </tr>
             <tr class="border-bt-gray">
               <td class="tx-left h4 tx-gray" >รวมเงิน</td>
               <td class="tx-right h2 tx-black" colspan="2">{{ $val['total'] }} บาท</td>
             </tr>           
            </tbody>
            </table>
        </div>
    </div>
</div>
@endforeach










