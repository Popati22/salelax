 <div class="col-xs-12 bordor-sty-gray padd-15">                  
    <table class="table table-sell">
      <thead>
        <tr class="h4">
          <th>ชื่อสินค้า</th>
          <th class="">ราคา</th>
          <th class="">จำนวน</th>
          <th class="tx-center" style="width:10%;"></th>
        </tr>
      </thead>
      <tbody class="table-body-sell">
        <tr class="h4" v-for="(sku,index) in arrSKUs">
          <td>
            <input type="text" class="f-price form-control input-lg no-border" v-model="sku.name" placeholder="ชื่อสินค้า"/>
          </td>
          <td class="" >
            <input type="number" class="f-price form-control input-lg no-border" v-model="sku.price" placeholder="ราคา"/>
          </td>
          <td class="" >
            <input type="number" class="f-price form-control input-lg no-border" v-model="sku.amount" placeholder="จำนวน"/>
          </td>
          <td class="tx-center">
            <a v-if="sku.name != '' && sku.price != '' && sku.amount!='' && index > 0" @click="delSKU(index)" class="remove-bin">
              <i class="fa fa-trash" style="    padding: 12px 0px; color: #ee8496;"></i>
{{--              <img src="{{asset('image/sell/icon-bin.png')}}" class="img-responsive icon-bin">--}}
            </a>
          </td>
        </tr>

      </tbody>
      {{--<tfoot>--}}
        {{--<tr class="h4" style="color:#a1a1a1;">--}}
          {{--<td class="no-padd">--}}
            {{--<input type="text" name="name" class="f-name form-control input-lg input-sty1 " v-model="sku.name" placeholder="ชื่อสินค้า"/>--}}
          {{--</td>--}}
          {{--<td class="no-padd">--}}
            {{--<input type="number" name="price" class="f-price form-control input-lg input-sty1" v-model="sku.price" placeholder="ราคา"/>--}}
          {{--</td>--}}
          {{--<td class="no-padd">--}}
            {{--<input type="number" name="amount" class="f-amount form-control input-lg input-sty1" v-model="sku.amount" placeholder="จำนวน"/>--}}
          {{--</td>--}}
          {{--<td></td>--}}
        {{--</tr>--}}
      {{--</tfoot>--}}
    </table>                  
    <div class="padd-30">
      <button class="btn-sell-add" id="add-data" @click="addSKU()">
          <span class="mg-right-10">เพิ่มสินค้า</span>
      </button>
    </div>
</div>

