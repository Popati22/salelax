 <div class="col-xs-12 bordor-sty-gray no-padd">  
    <div class="col-xs-12 mg-top-30 bd-box-shadow padd-15">
      <label class="col-lg-2 control-label tx-left">วันเวลา</label>
      <div class="reg col-lg-5">
        <bdo>
            <input type="radio" v-model="specify_type" value="2" onfocus="javascript:checkNullProf()" name="memberSunSign">
            <span></span>
            <abbr style="padding-left: 20px;"> ระบุวันพร้อมส่ง </abbr>
        </bdo>
      </div>
      <div class="reg col-lg-5">
          <bdo>
              <input type="radio" v-model="specify_type" value="1" onfocus="javascript:checkNullProf()" name="memberSunSign">
              <span></span>
              <abbr style="padding-left: 20px;"> พร้อมส่งทันที </abbr>
          </bdo>
      </div>
    </div>

    <div class="col-xs-12 mg-top-20 pad-l-r5">                 
        <div class='col-md-12 col-lg-6'>
            <div class="form-group">
                <div class='input-group date input-bt-gr'>
                    <datepicker class="input-sty1 input-lg bg-none" v-model="date_start.show" placeholder="วันเริ่ม"></datepicker>

                    {{--<input type='text' id="date_start" class="input-sty1 datetimepicker_from bg-none" v-model="date_start" placeholder="วันเริ่ม" readonly/>--}}
                    <span class="input-group-addon calender-addon-btn ca-addon1">
                        <span class="glyphicon glyphicon-calendar tx-i-blue"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class='col-md-12 col-lg-6'>
            <div class="form-group">
                <div class="input-group date input-bt-gr">
                    <datepicker class="input-sty1 input-lg bg-none" v-model="date_end.show" placeholder="วันสุดท้าย"></datepicker>

                    {{--<input type='text' id="date_end" class="input-sty1 datetimepicker_todate bg-none"  placeholder="วันสุดท้าย" readonly/>--}}
                    <span class="input-group-addon calender-addon-btn ca-addon2">
                        <span class="glyphicon glyphicon-calendar tx-i-blue"></span>
                    </span>
                </div>
            </div>
        </div>

        <div class='col-sm-6' >
            <div class="form-group">
                <div class='input-group date input-bt-gr input-sty-gray'>
                    <timepicker class="input-sty1 input-lg bg-none" v-model="time_start" placeholder="เริ่มส่งเวลา" ></timepicker>
                    {{--<input type='text' id="time_start" class="t1 form-control datetimepicker1 input-sty1 input-lg bg-none" v-model="date_start" placeholder="เริ่มส่งเวลา" readonly />--}}
                    <span class="input-group-addon calender-addon-btn add-t1">
                        <span class="glyphicon glyphicon-time tx-i-blue"></span>
                    </span>
                </div>
            </div>
        </div>       
        <!-- <div class='col-sm-1 tx-center no-padd' >
          -
        </div> -->
        <div class='col-sm-6' >
          <div class="form-group">
            <div class='input-group date input-bt-gr input-sty-gray'>
                <timepicker class="input-sty1 input-lg bg-none" v-model="time_end" placeholder="ถึงเวลา" ></timepicker>
                {{--<input type='text' id="time_end" class="t2 datetimepicker2 form-control input-sty1 input-lg bg-none" placeholder="ถึงเวลา" readonly/>--}}
                <span class="input-group-addon calender-addon-btn add-t2">
                    <span class="glyphicon glyphicon-time tx-i-blue"></span>
                </span>
            </div>

            </div>
        </div>
    </div>

   <!--  <div class="col-xs-12 mg-top-20 pad-l-r5">   
        <div class="checkbox checkbox-primary ">
          <input id="checkbox2" type="checkbox" checked >
          <label for="checkbox2" class="h4 tx-gray">
              ให้ผู้ซื้อเป็นผู้ระบุสถานที่นัดรับสินค้าและช่วงเวลานัดหมาย
          </label>
        </div>
    </div> -->


    <div class="col-xs-12 mg-top-20 no-padd">

     <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading head-sell-blue">
            <h4 class="panel-title">
              <div class="" style="margin:0px;">
                <input id="checkbox_place2" type="checkbox" v-model="location_type">

                <label for="checkbox_place2" class="h4 tx-white" style="padding-left: 1em">
                    ให้ผู้ซื้อเป็นผู้ระบุสถานที่นัดรับสินค้าและช่วงเวลานัดหมาย
                </label>

                  <a id="dp0" data-toggle="collapse" data-parent="#accordion" href="#collapse0">
                    <span class="tx-white pull-right glyphicon glyphicon-menu-down dp-down"></span>
                    <span class="tx-white pull-right glyphicon glyphicon-menu-up dp-up"></span>
                  </a>
              </div>
              <!-- <span class="pul-left h4">TIME & PLACE</span>
              <span class="pull-right glyphicon glyphicon-menu-down dp-down"></span>
              <span class="pull-right glyphicon glyphicon-menu-up dp-up"></span> -->
            </h4>
          </div>
          <div id="collapse0" class="panel-collapse collapse" :class="{'in': location_type == false }">
            <div class="panel-body no-padd" >
              {{--<div class='col-md-12 bd-box-shadow no-padd'>  --}}
                 {{--@include('pages/sell/sell_slideshow')--}}
              {{--</div>               --}}
              <div class="col-md-12 padd-l-r1 div-place mg-b-20" >

                <div class='col-md-12 input-bt-gr mg-top-20 no-padd'>
                    <div class="input-group">
                      <span class="input-group-btn">
                        <button class="btn bg-none" type="button">
                          <span class="glyphicon glyphicon-map-marker tx-i-blue"></span>
                        </button>
                      </span>
                      <input type="text" class="input-lg input-sty1 form-control no-padd input-place" v-model="location.name" placeholder="สถานที่">
                    </div>
                </div>
                <div class='col-md-12 input-bt-gr mg-top-20 input-sty-gray'>
                    <input type='text' class="input-lg input-sty1  form-control no-padd input-meet" v-model="location.displayname" placeholder="จุดนัดพบ"/>
                </div>
              </div>

              <div class="col-md-12 mg-b-20 mg-t-20">
                  <button class="btn-sell-add" id="add-place" @click="addLocation()">
                      <span class="mg-right-10">เพิ่มจุดนัดส่งสินค้า</span>
                  </button>
              </div>

                <div class="col-md-12 mg-b-20 mg-t-20">

                    <div v-for="(loc,index) in locations">
                        <div class="col-sm-6">
                            <span class="glyphicon glyphicon-map-marker tx-i-blue"></span>
                            <input type="text" style="    display: inline-block; width: 90%;" class="f-price form-control input-lg input-sty-table" v-model="loc.name" />

                        </div>
                        <div class="col-sm-6">
                            <input type="text" style="    display: inline-block; width: 90%;" class="f-price form-control input-lg input-sty-table" v-model="loc.displayname" />

                            <img style="display: inline; width: 20px; cursor: pointer;" @click="delLocation(index)" src="{{asset('image/sell/icon-bin.png')}}" class="img-responsive icon-bin pull-right">

                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
      </div>

        
    </div>


                 
</div>  


