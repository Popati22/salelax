@foreach($list as $key => $val)
 <div class="panel panel-default panel-sell">
    <div class="panel-heading panel-head-sell">     
      <a class="a-sell-panel {{$val['toggle']}}" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$val['no']}}">
        <table class="table table-accept" border="0">     
            <thead>
            <tr class="h4">
              <th style="width:4%;" class="padd-l-5">
                <div class="tx-center">
                  <b class="badge badge-tg" style="margin-top:-3px;">
                    <span class="glyphicon glyphicon-menu-down tx-i-white @if($val['toggle'] == 'in') hide @endif"></span>
                    <span class="glyphicon glyphicon-menu-up tx-i-white @if($val['toggle'] != 'in') hide @endif"></span> 
                  </b>
                </div>
              </th>
              <th class="tx-black" style="width:28%;">{{$val['title']}}</th>
              <th class="tx-gray tx-center">{{$val['detail']}}</th>
              <th class="tx-center" style="width:20%;"></th>
              <th class="tx-center">
                <button class="btn-sm-blue pull-right mg-t-1 w-b-acp">
                  <span class="mg-right-10 tx-center">ตอบรับทั้งหมด</span>
                </button>
              </th>
            </tr>
          </thead> 
        </table>
      </a>
    </div>
    <div id="collapse{{$val['no']}}" class="panel-collapse collapse {{$val['toggle']}}">
      <div class="panel-body panel-body-sell no-padd">
        @component('pages/sell/sell_accept_row_table',['row' =>  $val['data'] 
          ])
        @endcomponent  
      </div>
    </div>
  </div>
@endforeach


