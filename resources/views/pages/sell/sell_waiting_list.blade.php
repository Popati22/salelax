@foreach($list as $key => $val)

<div style="margin-top:40px;">
    <div class="col-xs-1">
        <img src="{{ asset('image/sell/icon-list.png') }}" 
          class="images-thumbnail img-responsive btn-total" style="width:100%; position: relative; z-index: 999;">
    </div>

    <div class="col-xs-6">
    <button class="btn-reserv btn-sell-detail padding-10" style="width:100%;">      
        <span class="mg-left-15 h3 tx-weight2" 
        style="position: relative; z-index: 999;">{{ $val['title'] }}</span>
        <span class="pull-right glyphicon glyphicon-menu-right"></span>
    </button>
    </div>

    <div class="col-xs-5">
    <button class="btn-md-blue-w tx-center " style="width:100%; position: relative; z-index: 999;">
        <span class="mg-right-10">ส่งสินค้าแล้วทั้งหมด</span>
    </button>
    </div>
</div>


<div class="col-xs-12 mg-bottom-20 border-blue" style="margin-top:-40px;">

    @foreach($val['custormer'] as $key => $data)
    <div class="row box-wait-body">
    	<div class="col-xs-12 box-wait border-bt-gray">
		    <div class="col-md-1 col-xs-12 goods-avatar no-padd tx-center" >               
                <img class="pull-left img-responsive img-i-w" src="{{ $data['avartar'] }}" >                
            </div>
            <div class="col-md-2 col-xs-12 no-padd ">
                <h4 class="h4 tx-black tx-center" >{{ $data['name'] }}</h4> 
            </div>
            <div class="col-md-9 col-xs-12">
            	<span class="h4 tx-black pull-right">รวมเงิน {{ $data['price'] }} บาท</span>
            </div>
    	</div>
    	<div class="col-xs-12 col-xs-12 box-wait border-bt-gray">		
    		<label class="h4 tx-gray pull-right">{{ $data['detail'] }}</label>
    	</div>
    	<div class="col-xs-12 col-xs-12 box-wait border-bt-gray">
    		<div class="col-md-8">			
            	<span class="h4 tx-blue pull-left">
            		<span class="glyphicon glyphicon-map-marker tx-i-blue"></span>
            		ส่งที่ : {{ $data['site'] }}
            	</span>
            </div>
            <div class="col-md-4">
            	<label class="h4 tx-gray pull-right">เวลา {{ $data['time'] }} น.</label>
            </div>
    	</div>
    	<div class="col-xs-6 col-xs-12 box-wait mg-top-30 mg-b-20">
            <button class="btn-modal-cancel tx-center btn-change-total"
             style="width:80%;">      
                <span>เปลี่ยนแปลงการส่ง</span>
            </button>
            
    	</div>
        <div class="col-xs-6 col-xs-12 box-wait mg-top-30 mg-b-20">
            <button class="btn-md-blue-w tx-center" style="width:80%;">
                <span class="mg-right-10">ส่งสินค้าแล้ว</span>
            </button>
        </div>
    </div>
    @endforeach

</div>
@endforeach











