{{--<div class="img-container">--}}
    {{--<img src="/image/market/im_group1.png" alt="Picture">--}}
{{--</div>--}}
<div class="img-container image-crop" style="background: url('/image/sell/addImage.png');    background-position: center;background-repeat: no-repeat;    background-size: 45px;  border: 1px solid #7d7d7d; border-radius: 15px;">
    <img id="img-now" >
</div>

<div class="row" id="actions">
    <div class="col-md-12 docs-buttons">
        <!-- <h3>Toolbar:</h3> -->

        <div class="row" style="    margin-bottom: 6px;">

            <div class="col-sm-6">
                <label class="btn btn-primary btn-upload btn-block" for="inputImage" title="Upload image file">
                    <input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                    <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                        <i class="fa fa-upload"></i> เลือกไฟล์อัพโหลด
                    </span>
                </label>
            </div>

            <div class="col-sm-6">
                <button v-show="haveFile==true" type="button" id="reset" class="btn btn-info btn-block" data-method="reset" title="Reset">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.reset()">
                         รีเซ็ตการครอป
                    </span>
                </button>
            </div>

        </div>

        <div class="row" v-show="haveFile==true">
            <div class="col-sm-6">
                <button type="button" id="rotateLeft" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(-45)">
                      <span class="fa fa-rotate-left"></span>
                    </span>
                </button>
                <button type="button" id="rotateRight" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(45)">
                      <span class="fa fa-rotate-right"></span>
                    </span>
                </button>
            </div>

            <div class="col-sm-6">

                <button type="button" id="cropped" class="btn btn-success pull-right" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getCroppedCanvas({ maxWidth: 4096, maxHeight: 4096 })">
                      <span class="fa fa-check"></span> ยืนยัน
                    </span>
                </button>
            </div>
        </div>

        <!-- Show the cropped image in modal -->
        <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="getCroppedCanvasTitle">รูปสินค้าที่ครอบแล้ว</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                        <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">ยืนยัน</a>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->

        <div class="col-md-3 docs-toggles">

        </div>

    </div><!-- /.docs-toggles -->
</div>


<div class="row" style="margin-top: 2em">

    <div class="col-md-12 mg-bottom-20 border-all-gray m-img b-radius-8" v-for="image in loopBackImgProducts">

        <div class="col-md-12">
            <a href="javascript:void(0);" class="img_modal">
                <center>
                    <img :src="image.image_cover" class="images-thumbnail img-responsive" style="width: 100%">
                </center>
                {{--<input type="hidden" value="{{ asset('image/sell/bag2.png') }}" class="path_img">--}}
            </a>
        </div>

        <div class="col-md-12 img-mg-t-m">
            <button type="button" class="btn btn-set-cover pull-left" v-if="image.status == 0" @click="setAsCover(image)">SET AS COVER</button>
            <button type="button" class="btn btn-set-cover pull-left" style="background-color: #01a132 !important;" v-if="image.status == 1">COVER</button>
            <button type="button" class="btn btn-minus pull-right" @click="delImgProduct(image)">
                <span class="glyphicon glyphicon-minus"></span>
            </button>
        </div>

    </div>


</div>