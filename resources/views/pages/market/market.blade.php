@extends('main')

@section('style')
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        /*html, body {*/
            /*height: 100%;*/
            /*margin: 0;*/
            /*padding: 0;*/
        /*}*/
        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;

            background-color: #FFF;
            z-index: 20;
            position: fixed;
            display: inline-block;
            float: left;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 25px;
            font-weight: 500;
            padding: 6px 12px;
        }
        #target {
            width: 345px;
        }

        .modal{
            z-index: 20;
        }
        .modal-backdrop{
            z-index: 10;
        }​

    </style>

    {{--<style>--}}
        {{--/* Always set the map height explicitly to define the size of the div--}}
 {{--* element that contains the map. */--}}
        {{--#map {--}}
            {{--height: 100%;--}}
        {{--}--}}
        {{--/* Optional: Makes the sample page fill the window. */--}}
        {{--html, body {--}}
            {{--height: 100%;--}}
            {{--margin: 0;--}}
            {{--padding: 0;--}}
        {{--}--}}
        {{--#description {--}}
            {{--font-family: Roboto;--}}
            {{--font-size: 15px;--}}
            {{--font-weight: 300;--}}
        {{--}--}}

        {{--#infowindow-content .title {--}}
            {{--font-weight: bold;--}}
        {{--}--}}

        {{--#infowindow-content {--}}
            {{--display: none;--}}
        {{--}--}}

        {{--#map #infowindow-content {--}}
            {{--display: inline;--}}
        {{--}--}}

        {{--.pac-card {--}}
            {{--margin: 10px 10px 0 0;--}}
            {{--border-radius: 2px 0 0 2px;--}}
            {{--box-sizing: border-box;--}}
            {{---moz-box-sizing: border-box;--}}
            {{--outline: none;--}}
            {{--box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);--}}
            {{--background-color: #fff;--}}
            {{--font-family: Roboto;--}}
        {{--}--}}

        {{--#pac-container {--}}
            {{--padding-bottom: 12px;--}}
            {{--margin-right: 12px;--}}
        {{--}--}}

        {{--.pac-controls {--}}
            {{--display: inline-block;--}}
            {{--padding: 5px 11px;--}}
        {{--}--}}

        {{--.pac-controls label {--}}
            {{--font-family: Roboto;--}}
            {{--font-size: 13px;--}}
            {{--font-weight: 300;--}}
        {{--}--}}

        {{--#pac-input {--}}
            {{--background-color: #fff;--}}
            {{--font-family: Roboto;--}}
            {{--font-size: 15px;--}}
            {{--font-weight: 300;--}}
            {{--margin-left: 12px;--}}
            {{--padding: 0 11px 0 13px;--}}
            {{--text-overflow: ellipsis;--}}
            {{--width: 400px;--}}
        {{--}--}}

        {{--#pac-input:focus {--}}
            {{--border-color: #4d90fe;--}}
        {{--}--}}

        {{--#title {--}}
            {{--color: #fff;--}}
            {{--background-color: #4d90fe;--}}
            {{--font-size: 25px;--}}
            {{--font-weight: 500;--}}
            {{--padding: 6px 12px;--}}
        {{--}--}}
        {{--#target {--}}
            {{--width: 345px;--}}
        {{--}--}}
    {{--</style>--}}
@endsection

@section('content')

    <div class="large-device">
        <div class="col-xs-1 col-sm-1 col-md-3">  </div>
        <div class="col-xs-4 col-sm-3 col-md-2 text-right">
           <span class="recommend" >แนะนำ </span>
        </div>
        <div class="col-xs-7 col-sm-8 col-md-7">
            <ul class="suggest pull-right">
                <li><a class="blue-capsule blue-font" href="/recommend" >แสดงทั้งหมด</a></li>
                <li><a class="blue-capsule blue-font" href="/scanAround" > ใกล้ตัว</a></li>
            </ul>
        </div>
    
    </div>

    <div class="small-device" style="display:none;">
        <div class="col-xs-12  text-center">
           <span class="recommend" >แนะนำ </span>
        </div>
        <div class="col-xs-12 text-center mg-top-10">
            <ul class="suggest">
                <li><a class="blue-capsule blue-font" href="/recommend" >แสดงทั้งหมด</a></li>
                <li><a class="blue-capsule blue-font" href="/scanAround" > ใกล้ตัว</a></li>
                
            </ul>
        </div>
    </div>

    @include('modal/market_add')

    <div class="col-xs-12" style="    padding: 0px 45px;">
        @component('components/recommend', ['all_items' => [
                ['id' => 1,
                 'image_src' => asset('image/market/im_recommend1.png'),
                 'count_view' => 599,
                 'market_name' => 'NECTEC MARKET 1',
                 'location' => 'ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...',
                 'market_desc' => '',
                 'create' => 'เมื่อ 2 อาทิตย์ ที่ผ่านมา   ',
                ],
                ['id' => 2,
                 'image_src' => asset('image/market/im_recommend2.png'),
                 'count_view' => 599,
                 'market_name' => 'NECTEC MARKET 1',
                 'location' => 'ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...',
                 'market_desc' => '',
                 'create' => 'เมื่อ 2 อาทิตย์ ที่ผ่านมา   '
                 ],
                 ['id' => 3,
                 'image_src' => asset('image/market/im_group6.png'),
                 'count_view' => 599,
                 'market_name' => 'NECTEC MARKET 1',
                 'location' => 'ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...',
                 'market_desc' => '',
                 'create' => 'เมื่อ 2 อาทิตย์ ที่ผ่านมา   '
                ],
                ['id' => 4,
                 'image_src' => asset('image/market/im_group7.png'),
                 'count_view' => 599,
                 'market_name' => 'NECTEC MARKET 1',
                 'location' => 'ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...',
                 'market_desc' => '',
                 'create' => 'เมื่อ 2 อาทิตย์ ที่ผ่านมา   '
                 ],
                 ['id' => 5,
                 'image_src' => asset('image/market/im_group2.png'),
                 'count_view' => 599,
                 'market_name' => 'NECTEC MARKET 1',
                 'location' => 'ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...',
                 'market_desc' => '',
                 'create' => 'เมื่อ 2 อาทิตย์ ที่ผ่านมา   '
                ],
                ['id' => 6,
                 'image_src' => asset('image/market/im_group3.png'),
                 'count_view' => 599,
                 'market_name' => 'NECTEC MARKET 1',
                 'location' => 'ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...',
                 'market_desc' => '',
                 'create' => 'เมื่อ 2 อาทิตย์ ที่ผ่านมา   '
                 ]
            
            ]])
              
        @endcomponent
       
    </div>

    <div class="row border-recommend">
        <p ></p>
    </div>

    <div class="top-15-container text-center">
        <span class="attend-group"> กลุ่มที่คุณเข้าร่วม</span>
        <span style="float:right;">
           <button class="blue-capsule blue-font" id="add-market-btn"> เพิ่มตลาด</button>
        </span>
    </div>

    <div class="top-25-container text-center">

        @component('components/search')
        @endcomponent
       
    </div>

    
    <div class="top-25-container text-center">
        <div class="row text-left">
            <market v-for="data in markets" :data="data"></market>

        </div>    
    </div>


    <!-- market-join -->
    @include('modal/market_join')

@endsection

@section('script')
    <script src="/vue/directive/market.js?v=4"></script>

    <script>
        Vue.config.devtools = true;

        Vue.http.options.xhr = {withCredentials: true}
        Vue.http.options.emulateJSON = true
        Vue.http.options.emulateHTTP = true
        Vue.http.options.crossOrigin = true
        Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'

        var Validator = SimpleVueValidator.Validator.create({
            templates: {
                greaterThan: 'จำเป็นต้องเลือก อย่างน้อย1ไฟล์',
                required: 'จำเป็นต้องใส่ข้อมูล'
            }
        });

        Vue.use(SimpleVueValidator);

        var vm = new Vue({
            el:'#app',
            data: {
                haveFile: false,
                name: '',
                address: '',
                description : '',
                validators_image: '',

                markets : []
            },
            validators: {
                'name': function (value) {
                    return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
                },
                'address': function (value) {
                    return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
                },
                'description': function (value) {
                    return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
                },

            },
            mounted: function(){
                this.getMarket();
            },
            methods: {
                getMarket : function(){
                    this.$http.get('/api/v1/marketlist').then(function(myresponse) {
                        vm.markets = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });
                },
                addMarket : function () {
                    this.$validate()
                        .then(function(success) {
                            if (success) {
                                if($('#image_market')[0].files.length == 0) {
                                    this.validators_image = 'กรุณาเลือกรูปตลาด';
                                }else{

                                    swal({
                                        title: 'กำลังบันทึกข้อมูล',
                                        text: '(ปิดอัติโนมัติ)',
                                        onOpen: () => {
                                            swal.showLoading()
                                        }
                                    })

                                    var formData = new FormData();
                                    formData.append('image', $('#image_market')[0].files[0]);
                                    formData.append('name', vm.name);
                                    formData.append('address', places[0].formatted_address);
                                    formData.append('place_id', places[0].place_id);
                                    formData.append('location', JSON.stringify( place_detail.results[0].geometry.location) );
                                    formData.append('description', vm.description);

                                    // console.log('vue:'+places[0])

                                    vm.$http.post('/api/v1/market',formData).then(function(myresponse) {
                                        $('#addModal').modal('hide');
                                        swal.close()

                                        swal(
                                            'Add Market Success',
                                            '',
                                            'success'
                                        )

                                        vm.name = ''
                                        vm.address = ''
                                        vm.description = ''
                                        $('#image_market').val('')
                                        $('#output')[0].src = '';
                                        vm.haveFile = false;
                                        this.getMarket();

                                    }, function(myresponse) {
                                        // error callback
                                        swal.close()

                                        swal(
                                            'เกิดข้อผิดพลาด',
                                            'กรุณากดบันทึกใหม่',
                                            'error'
                                        )
                                        console.log(myresponse.data);
                                    });
                                }

                            }
                        });

                },
                openFile : function (event) {
                    var input = event.target;

                    var reader = new FileReader();
                    reader.onload = function(){
                        var dataURL = reader.result;
                        var output = document.getElementById('output');
                        output.src = dataURL;

                        vm.haveFile = true;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    </script>
    <script>
        // This example adds a search box to a map, using the Google Place Autocomplete
        // feature. People can enter geographical searches. The search box will return a
        // pick list containing a mix of places and predicted search terms.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
        var places;
        var place_detail;
        function initAutocomplete() {
            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                console.log(places)
                $.get("https://maps.googleapis.com/maps/api/geocode/json?place_id="+places[0].place_id+"&key=AIzaSyDFRY2KjaFj6i0yn1rHXNs0ebKNDdWfTaE", function(data, status){
                    place_detail = data;
                    console.log(JSON.stringify(place_detail))
                    vm.address = $('#pac-input').val()

                });
            });
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-oKKUErHXY7C6f543UynhqpW_79pFuRk&libraries=places&callback=initAutocomplete"
            async defer></script>
@endsection