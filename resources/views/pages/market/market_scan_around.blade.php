@extends('main')
@section('content')
<div class="top-15-container">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="/market">Market</a></li>
        <li class="active">Scan Around</li>
    </ol>
</div>
<div class="top-15-container">
   
    @component('components/search')

    @endcomponent
    
</div>

<div class="top-15-container text-center">
    <div class="header-font">SCAN AROUND</div> 
</div>

<div class="top-15-container text-center">
    <div class="map no-padding">
        For google map api
    </div>
</div>

<div class="top-15-container text-center">
    @component('components/scan_map_item', ['all_items' => [
            ['number' => 1, 'name' => 'ศูนย์เทคโนโลยีอีเล็กทรอนิกส์และคอมพิวเตอร์...', 'destination' => 2.5 , 'type' => 'your_market'],
            ['number' => 2, 'name' => 'ท่ารถตู้ ม.ธรรมศาสตร์ รังสิต - ฟิวเจอร์พาร์ค', 'destination' => 2.5, 'type' => 'your_market'],
            ['number' => 3, 'name' => 'โรงอาหารอินเตอร์โซน', 'destination' => 2.5, 'active' => 'blue-font' ,'type' => 'your_market'],
            ['number' => 4, 'name' => 'หอสมุดป๋วย', 'destination' => 2.5, 'type' => 'recommend'],
            ['number' => 5, 'name' => 'ท่ารถตู้ อนุสวรีย์ชัย - ม.ธรรมศาสตร์ รังสิต', 'destination' => 2.5, 'type' => 'recommend']
        ]])
    @endcomponent
</div> 


@endsection