@extends('main')
@section('content')

<!-- cover -->
    <div class="row text-center no-margin">
        <div class="container">
        <div class="mg-top-20"></div>
            <div>
           <span class="title-market-group" >COOKIE MARKET </span>
            </div>
        <div>
           <span class="blue-20 glyphicon glyphicon-map-marker"></span><span class="blue-20 mg-left-10">ศูนย์เทคโนโลยีอิเล็กทรอนิกส์และคอมพิวเตอร์แห่งชาติ (NECTEC) เนคเทค</span></span>
        </div>
        </div>
    </div>
    <div class="row no-margin">
        <div class="top-25-container group-cover">
            <div class="select-text" style="width:93%;padding-right:5px;">
                <div class="col-xs-2 col-sm-2">   
                    <div class="text-center">
                        <img class="admin-icon-cover" src="{{asset('image/app/admin_icon.png')}}" >
                    </div>
                </div>
                <div class="col-xs-10 pull-right">
                <div class="col-xs-10 col-sm-11 text-right">
                    <div class="btn-group seller-contact-group  pull-right" role="group">
                            <button class="button-tran"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <span class="glyphicon glyphicon-info-sign" style="font-size:20px;"></span>
                                <span class="mg-left-10" style="font-size:15px;"></span>
                                    Group Information
                            </button>
                            <div class="dropdown-menu information-dropdown-admin">
                                <div class="description">
                                    <div class="grey-20 mg-left-10">DESCRIPTION</div>
                                    <div class="light-grey-border"  style="width:95%;"></div>
                                    <div class="desc-text">
                                        ใส่อุปกรณ์ของคุณไปยิมด้วยกระเป๋าสะพายหลังใบนี้ ช่องใส่ของหลักสำหรับเก็บอุปกรณ์กีฬา มีช่องแล็อปท็อปที่บุวัสดุ
                                        ด้านหน้าช่องซิบซ่อนสำหรับใส่ของชิ้นเล็กสำคัญ มีสายรัดยางยืด สำหรับปรับความจุ พร้อมสายสะพายบุนุ่มเพื่อปกป้องไหล่ของคุณ
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="grey-20 mg-left-10">CREATED</div>
                                    <div class="light-grey-border" style="width:95%;"></div>
                                    <div class="desc-text">
                                        เมื่อ 2 สัปดาห์ที่ผ่านมา
                                    </div>
                                </div>
                            </div><!--information dropdown-->
                    </div>
                </div>
                <div class="col-xs-2 col-sm-1 text-right">
                    <div class="btn-group seller-contact-group" style="position: relative;" role="group">
                        <a class="menu-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="background-color:white;cursor:pointer">
                            <div class="text-center" style="text-align: right;width: 50px;margin-top: 10px;">
                                <img class="admin-icon-cover" style="width:30px !important;" src="{{asset('image/app/admin_setting_icon.png')}}" >
                            </div>   
                        </a>
                        <ul class="dropdown-menu contact-dropdown  menu-user-dropdown" style="margin-left:-260px;">
                            <li class="text-center"><a href="javascript:void(0);" id="administrator"><div><span>ADMINISTRATOR</span></div></a></li>
                            <li class="text-center"><a href="#"><div><span>EDIT INFORMATION</span></div></a></li>
                            <li class="text-center"><a href="#"><div><span>REMOVE PRODUCT</span></div></a></li>
                            <li class="text-center"><a href="#"><div><span>LEAVE GROUP</span></div></a></li>
                        </ul>
                    </div>
                </div>

            </div>
               
            </div>
            <div>
                <img src="{{asset('image/market/im_recommend2.png')}}">
            </div>
            <div class="invite-btn">
                <button class="yellow-capsule-blue-font invite-friend">INVITE FRIENDS</button>
            </div>
        </div>
    </div>
<!--end cover -->
    <div class="row no-margin">
        <div class="top-15-container">
            <div style="float:left;" class="col-xs-12 col-sm-4 col-md-3 col-lg-2 no-padding">
                <button class="blue-capsule-member mg-top-10"  data-toggle="modal" href="#group_member">599 Members</button>
            </div>
            @component('components/market_group_member',['members' => [
                ['profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member4.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member5.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member6.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member4.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member5.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member6.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member4.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member5.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member6.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member4.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member5.png'), 'link' => 'shop/profile' ],
                ['profile' =>  asset('image/shop/im_member6.png'), 'link' => 'shop/profile' ],

            ]
                ])
            @endcomponent

            @component('components/market_group_members_list',['count' => 559,
                'members' => [
                ['profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile', 'name' => 'Member 1' ],
                ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile', 'name' => 'Member 2' ],
                ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile', 'name' => 'Member 3' ],
                ['profile' =>  asset('image/shop/im_member4.png'), 'link' => 'shop/profile', 'name' => 'Member 4' ],
                ['profile' =>  asset('image/shop/im_member5.png'), 'link' => 'shop/profile', 'name' => 'Member 5'  ],
                ['profile' =>  asset('image/shop/im_member6.png'), 'link' => 'shop/profile', 'name' => 'Member 6'  ],
                ['profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile', 'name' => 'Member 1' ],
                ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile', 'name' => 'Member 2' ],
                ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile', 'name' => 'Member 3' ],
                ['profile' =>  asset('image/shop/im_member4.png'), 'link' => 'shop/profile', 'name' => 'Member 4' ],
                ['profile' =>  asset('image/shop/im_member5.png'), 'link' => 'shop/profile', 'name' => 'Member 5'  ],
                ['profile' =>  asset('image/shop/im_member6.png'), 'link' => 'shop/profile', 'name' => 'Member 6'  ],
                ['profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile', 'name' => 'Member 1' ],
                ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile', 'name' => 'Member 2' ],
                ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile', 'name' => 'Member 3' ],
                ['profile' =>  asset('image/shop/im_member4.png'), 'link' => 'shop/profile', 'name' => 'Member 4' ],
                ['profile' =>  asset('image/shop/im_member5.png'), 'link' => 'shop/profile', 'name' => 'Member 5'  ],
                ['profile' =>  asset('image/shop/im_member6.png'), 'link' => 'shop/profile', 'name' => 'Member 6'  ],
                ['profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile', 'name' => 'Member 1' ],
                ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile', 'name' => 'Member 2' ],
                ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile', 'name' => 'Member 3' ],
                ['profile' =>  asset('image/shop/im_member4.png'), 'link' => 'shop/profile', 'name' => 'Member 4' ],
                ['profile' =>  asset('image/shop/im_member5.png'), 'link' => 'shop/profile', 'name' => 'Member 5'  ],
                ['profile' =>  asset('image/shop/im_member6.png'), 'link' => 'shop/profile', 'name' => 'Member 6'  ]
            ]
                ])
            @endcomponent
        </div>
    </div>
    <div class="mg-top-20"></div>
    <div class="top-15-container">
        @component('components/search')
        @endcomponent
    </div>
    <div class="mg-top-40"></div>
    
    <div class="top-15-container text-center " id ="wrapper">
        <div id="list" >
            @component('components/shop/shop_item',['all_goods' =>
            [
            [ 'seller_image' => asset('image/shop/im_member1.png'),
                'goods_image' => asset('image/shop/im_sell1.png'),
                'member_name' =>  'Nissaajaa',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,
                'name' => 'นกน้อยคัพเค้ก',
                'price' => '25 - 50',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า',
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'

            ],
            [ 'seller_image' => asset('image/shop/im_member2.png'),
                'goods_image' => asset('image/shop/im_sell2.png'),
                'member_name' =>  'Nuntar',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,
                'name' => 'เค้กกล้วยหอม',
                'price' => '30 - 50',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member3.png'),
                'goods_image' => asset('image/shop/im_sell3.png'),
                'member_name' =>  'Mathakoon',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,
                'name' => 'ผักดองเพื่อสุขภาพ',
                'price' => '150',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member4.png'),
                'goods_image' => asset('image/shop/im_sell4.png'),
                'member_name' =>  'Arat09',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,
                'name' => 'อมยิ้ม macaron',
                'price' => '15 - 30',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member5.png'),
                'goods_image' => asset('image/shop/im_sell5.png'),
                'member_name' =>  'Barr',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,
                'name' => 'เกลือชาเขียว',
                'price' => '40 - 80',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member6.png'),
                'goods_image' => asset('image/shop/im_sell6.png'),
                'member_name' =>  'Datta',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,
                'name' => 'เสื้อถักแขนยาว',
                'price' => '550',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            
            [ 'seller_image' => asset('image/shop/im_member6.png'),
                'goods_image' => asset('image/shop/im_sell7.png'),
                'member_name' =>  'Datta',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'น้ำผึ้งแท้จากราชบุรี',
                'price' => 250,
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member5.png'),
                'goods_image' => asset('image/shop/im_sell8.png'),
                'member_name' =>  'Barr',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'macaron',
                'price' => '50 - 80',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member2.png'),
                'goods_image' => asset('image/shop/im_sell9.png'),
                'member_name' =>  'Nuntar',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'คุกกี้ช็อก',
                'price' => '80 - 150',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],

            [ 'seller_image' => asset('image/shop/im_member4.png'),
                'goods_image' => asset('image/shop/im_sell10.png'),
                'member_name' =>  'Arat09',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'สายยางม้วน',
                'price' => '300',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],

            [ 'seller_image' => asset('image/shop/im_member2.png'),
                'goods_image' => asset('image/shop/im_sell11.png'),
                'member_name' =>  'Nuntar',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'พรมปูพื้นทำเอง',
                'price' => '350',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member3.png'),
                'goods_image' => asset('image/shop/im_sell3.png'),
                'member_name' =>  'Mathakoon',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'ตุ๊กตาคั่นหนังสือ',
                'price' => '90',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member1.png'),
                'goods_image' => asset('image/shop/im_sell13.png'),
                'member_name' =>  'Nissajana',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'เสื้อกล้ามออกแบบเอง',
                'price' => '150',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member6.png'),
                'goods_image' => asset('image/shop/im_sell14.png'),
                'member_name' =>  'Datta',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'ไก่แจ้บ้านแท้',
                'price' => '1550',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller',
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member5.png'),
                'goods_image' => asset('image/shop/im_sell15.png'),
                'member_name' =>  'Barr',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'เค้กกล้วยหอม',
                'price' => '15-30',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member3.png'),
            'goods_image' => asset('image/shop/im_sell16.png'),
            'member_name' =>  'Mathakoon',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'มะม่ววงสุกจากสวน',
                'price' => '50',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 
                'seller_link' => 'shop/seller', 
                'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member5.png'),
            'goods_image' => asset('image/shop/im_sell17.png'),
            'member_name' =>  'Barr',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'ต่างหูไข่มุก',
                'price' => '15-50',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member4.png'),
            'goods_image' => asset('image/shop/im_sell18.png'),
            'member_name' =>  'Arat09',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'จักรยานญี่ปุ่น',
                'price' => '15-50',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'
            ],
            [ 'seller_image' => asset('image/shop/im_member2.png'),
            'goods_image' => asset('image/shop/im_sell19.png'),
            'member_name' =>  'Nuntar',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'มินิคุ๊กกี้',
                'price' => '15-50',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'
            ],
            [  'seller_image' => asset('image/shop/im_member3.png'),
            'goods_image' => asset('image/shop/im_sell20.png'),
            'member_name' =>  'Mathakoon',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'เกลือเรนโบว',
                'price' => '250',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'
            ],
            [  'seller_image' => asset('image/shop/im_member1.png'),
            'goods_image' => asset('image/shop/im_sell21.png'),
            'member_name' =>  'Nissajana',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'คุกกี้น้องหมา',
                'price' => '25 - 50',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'
            ],
            [   'seller_image' => asset('image/shop/im_member2.png'),
            'goods_image' => asset('image/shop/im_sell22.png'),
            'member_name' =>  'Nuntar',
                'location' => 'หอสมุดป๋วย',
                'total' => 10,
                'left' => 3,         
                'name' => 'พรมปูพื้นทำเอง',
                'price' => '550',
                'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', 
                'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'
            ]
            ]
            ])
            @endcomponent
        </div>
    </div>



<!-- Group modal -->
@include('modal/market_group')
<!-- @include('modal/market_group_admin') -->

@component('components/market_group_admin_list', ['members' => [
                ['main' => true, 'profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile', 'name' => 'Barr Lovery'],
                ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile', 'name' => 'Forda ryatana' ],
                ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile', 'name' => 'Loveryata' ]
            ]])
@endcomponent
@component('components/market_group_add_admin', [
                'admin' => [
                    ['main' => true, 'profile' =>  asset('image/shop/im_member1.png'), 'link' => 'shop/profile', 'name' => 'Barr Lovery'],
                    ['profile' =>  asset('image/shop/im_member2.png'), 'link' => 'shop/profile', 'name' => 'Forda ryatana' ],
                    ['profile' =>  asset('image/shop/im_member3.png'), 'link' => 'shop/profile', 'name' => 'Loveryata' ],
                ],
                'search_members' => [   
                    ['id'=> 1, 'profile' =>  asset('image/shop/im_member4.png'), 'link' => 'shop/profile', 'name' => 'Rya', ],
                    ['id'=> 2, 'profile' =>  asset('image/shop/im_member5.png'), 'link' => 'shop/profile', 'name' => 'Loveryata' ],
                    ['id'=> 3, 'profile' =>  asset('image/shop/im_member6.png'), 'link' => 'shop/profile', 'name' => 'ryata' ]
                ]
            ])
@endcomponent



@endsection



