@extends('main')
@section('content')
<div class="top-15-container">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="/market">Market</a></li>
        <li class="active">Show All</li>
    </ol>
</div>
<div class="top-15-container">
   
    @component('components/search')

    @endcomponent
    
</div>

<div class="top-15-container text-center">
    <div class="header-font">Recommend</div> 
</div>

<div class="top-15-container text-center">
    <div class="row no-margin">
            @component('components/recommend_suggest')
                    @slot('id')
                        3
                    @endslot
                    @slot('image_src')
                        {{asset('image/market/im_group1.png')}}
                    @endslot
                    @slot('count_view')
                        599
                    @endslot
                    @slot('market_name')
                        NECTEC Market
                    @endslot
                    @slot('location')
                        ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...
                    @endslot
                    @slot('market_desc')  
                    @endslot
                    @slot('create')
                    เมื่อ 2 อาทิตย์ ที่ผ่านมา   
                    @endslot
                    @slot('admin')
                        /group/admin
                    @endslot
                @endcomponent
                @component('components/recommend_suggest')
                    @slot('id')
                        4
                    @endslot
                    @slot('image_src')
                        {{asset('image/market/im_group2.png')}}
                    @endslot
                    @slot('count_view')
                        599
                    @endslot
                    @slot('market_name')
                        NECTEC MARKET 
                    @endslot
                    @slot('location')
                        ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...
                    @endslot
                    @slot('market_desc')
                        
                    @endslot
                    @slot('create')
                    เมื่อ 2 อาทิตย์ ที่ผ่านมา    
                    @endslot
                @endcomponent
                @component('components/recommend_suggest')
                    @slot('id')
                        5
                    @endslot
                    @slot('image_src')
                        {{asset('image/market/im_group3.png')}}
                    @endslot
                    @slot('count_view')
                        599
                    @endslot
                    @slot('market_name')
                        NECTEC MARKET 
                    @endslot
                    @slot('location')
                        ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...
                    @endslot
                    @slot('market_desc')
                        
                    @endslot
                    @slot('create')
                    เมื่อ 2 อาทิตย์ ที่ผ่านมา   
                    @endslot
                @endcomponent
                @component('components/recommend_suggest')
                    @slot('id')
                        6
                    @endslot
                    @slot('image_src')
                        {{asset('image/market/im_group4.png')}}
                    @endslot
                    @slot('count_view')
                        599
                    @endslot
                    @slot('market_name')
                        NECTEC MARKET 
                    @endslot
                    @slot('location')
                        ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...
                    @endslot
                    @slot('market_desc')
                        
                    @endslot
                    @slot('create')
                    เมื่อ 2 อาทิตย์ ที่ผ่านมา   
                    @endslot
                @endcomponent
                @component('components/recommend_suggest')
                    @slot('id')
                        7
                    @endslot
                    @slot('image_src')
                        {{asset('image/market/im_group5.png')}}
                    @endslot
                    @slot('count_view')
                        599
                    @endslot
                    @slot('market_name')
                        NECTEC MARKET 
                    @endslot
                    @slot('location')
                        ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...
                    @endslot
                    @slot('market_desc')
                        
                    @endslot
                    @slot('create')
                    เมื่อ 2 อาทิตย์ ที่ผ่านมา   
                    @endslot
                @endcomponent
                @component('components/recommend_suggest')
                    @slot('id')
                        8
                    @endslot
                    @slot('image_src')
                        {{asset('image/market/im_group6.png')}}
                    @endslot
                    @slot('count_view')
                        599
                    @endslot
                    @slot('market_name')
                        NECTEC MARKET 
                    @endslot
                    @slot('location')
                        ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...
                    @endslot
                    @slot('market_desc')
                        
                    @endslot
                    @slot('create')
                    เมื่อ 2 อาทิตย์ ที่ผ่านมา   
                    @endslot
                @endcomponent
                @component('components/recommend_suggest')
                    @slot('id')
                        9
                    @endslot
                    @slot('image_src')
                        {{asset('image/market/im_group7.png')}}
                    @endslot
                    @slot('count_view')
                        599
                    @endslot
                    @slot('market_name')
                        NECTEC MARKET 
                    @endslot
                    @slot('location')
                        ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...
                    @endslot
                    @slot('market_desc')
                        
                    @endslot
                    @slot('create')
                    เมื่อ 2 อาทิตย์ ที่ผ่านมา   
                    @endslot
                @endcomponent
                @component('components/recommend_suggest')
                     @slot('id')
                        10
                    @endslot
                    @slot('image_src')
                        {{asset('image/market/im_group8.png')}}
                    @endslot
                    @slot('count_view')
                        599
                    @endslot
                    @slot('market_name')
                        NECTEC MARKET 
                    @endslot
                    @slot('location')
                        ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...
                    @endslot
                    @slot('market_desc')
                        
                    @endslot
                    @slot('create')
                    เมื่อ 2 อาทิตย์ ที่ผ่านมา   
                    @endslot
                @endcomponent
                @component('components/recommend_suggest')
                     @slot('id')
                        11
                    @endslot
                    @slot('image_src')
                        {{asset('image/market/im_group9.png')}}
                    @endslot
                    @slot('count_view')
                        599
                    @endslot
                    @slot('market_name')
                        NECTEC MARKET 
                    @endslot
                    @slot('location')
                        ศูนย์เทคโนโลยีอิเลกทรอนิกส์และคอมพิวเตอร์...
                    @endslot
                    @slot('market_desc')
                        
                    @endslot
                    @slot('create')
                    เมื่อ 2 อาทิตย์ ที่ผ่านมา   
                    @endslot
                @endcomponent


</div>
</div>

@include('modal/market_join')
@endsection