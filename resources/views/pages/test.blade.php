
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Salelax</title> <!-- change this title for each page -->

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
    <link href="http://localhost:8000/css/style.css" rel="stylesheet">
    <link href="http://localhost:8000/css/slick.css" rel="stylesheet">
    <link href="http://localhost:8000/css/slick-theme.css" rel="stylesheet">
    <link rel="stylesheet" href="http://localhost:8000/css/venobox.css"/>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="http://localhost:8000/slick/slick.js"></script>  
    <!-- scrollbar market-->
    <link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css" />
    <script src="https://unpkg.com/simplebar@latest/dist/simplebar.js"></script>
    <!-- Datepicker -->
    <link href="http://localhost:8000/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script type="text/javascript" src="http://localhost:8000/js/moment.js"></script>
    <script type="text/javascript" src="http://localhost:8000/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="http://localhost:8000/js/venobox.js"></script>

    <script type="text/javascript" src="http://localhost:8000/js/my-common.js"></script>

  </head>
  <body>

 


  <nav class="navbar navbar-default">
<div class="container-fluid">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">
      <img alt="sale lax" class="header-logo" src="http://localhost:8000/image/app/logo.png">
    </a>
  </div>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <div class="col-xs-8" style="float:right;">
      <ul class="nav navbar-nav text-center">
        <li><a  class="menu-list"  href="/market"><span class="icon-zag-ic-markets"></span><span class="menu-desc">Markets</span></a></li>
        <li><a class="menu-list" href="/basket"><span class="badge1" data-badge="5"></span><span class="icon-zag-ic-basket"></span><span class="menu-desc">Basket</span></a></li> 
        <li><a class="menu-list active" href="/shop"><span class="icon-zag-ic-shop"></span><span class="menu-desc">Shop</span></a></li>   
        <li><a class="menu-list" href="/sell"><span class="icon-zag-ic-sell"></span><span class="menu-desc">Sell</span></a></li>   
        <li><a class="menu-list" href="/history"><span class="badge1" data-badge="5"></span><span class="icon-zag-ic-history"></span><span class="menu-desc">History<span></a></li>        
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a class="menu-user" href="/"><span class="icon-zag-ic-user"></span></a></li>
        <li><a class="menu-more" href="/"><span class="icon-zag-ic-more"></span></a></li>
      </ul>
</div>

</div>
</nav>  <div>
      
<div class="top-15-container">
   
    <div class="row no-margin">
    <div class="col-xs-12 col-md-6 no-padding">
        <button class="grey-capsule">
            <input type="text" placeholder="Search" class="search-input">
            <span class="glyphicon glyphicon-search search-icon" ></span>
        </button>
    </div>
    <div class="col-xs-12 col-md-3 no-padding">
        <button class="blue-capsule-sort" >
            <span class="mg-right-10">SORT</span> | 
            <span class="blue-font mg-left-15">LOWER PRICE </span>
            <span class="glyphicon glyphicon-menu-down blue-font mg-left-15"></span>
        </button>
    </div>
    <div class="col-xs-12 col-md-3 no-padding">
        <button class="blue-capsule-sort">
            <span class="mg-right-10">FILTER</span> | 
            <span class="blue-font mg-left-15">CATEGORY </span>
            <span class="glyphicon glyphicon-menu-down blue-font mg-left-15"></span>
        </button>
    </div>
</div>

    
</div>

<div class="top-15-container text-left">
       <button type="button"  class="btn  suggest-tag-plus mg-top-10" ><span class="glyphicon glyphicon-plus"></span></button>
            <button type="button" class="btn suggest-tag dark-brown mg-top-10" >หอสมุดป๋วย</button>
            <button type="button" class="btn suggest-tag dark-brown mg-top-10" >สถาบันเอเชียตะวันออกศึกษา</button>
            <button type="button" class="btn suggest-tag dark-grey mg-top-10" >โรงอาหารกลาง</button>
            <button type="button" class="btn suggest-tag light-grey mg-top-10" >Inter Zone</button>
            <button type="button" class="btn suggest-tag grey mg-top-10" >สถานีรถตู้ อนุสาวรีย์-ธรรมศาสตร์</button>
            <button type="button" class="btn suggest-tag dark-grey mg-top-10" >สถานีรถตู้ จตุจักร-ธรรมศาสตร์</button>
            <button type="button" class="btn suggest-tag light-grey mg-top-10" >บีทีเอชสะพานควาย</button>
    </div>

<div class="mg-top-40"></div>
<div class="top-15-container text-center">
  <!-- <div class="row no-padding" id="layout-variable-" > -->
    <!-- first-colume -->
    <div id="#layout-variable" style="display:container">
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member1.png" >
            <span >Nissaajaa</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell1.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>นกน้อยคัพเค้ก</span>
            <span style="float:right;">25 - 50</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member6.png" >
            <span >Datta</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell7.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>น้ำผึ้งแท้จากราชบุรี</span>
            <span style="float:right;">250</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member2.png" >
            <span >Nuntar</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell2.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>เค้กกล้วยหอม</span>
            <span style="float:right;">30 - 50</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member5.png" >
            <span >Barr</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell8.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>macaron</span>
            <span style="float:right;">50 - 80</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member3.png" >
            <span >Mathakoon</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell3.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>ผักดองเพื่อสุขภาพ</span>
            <span style="float:right;">150</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member5.png" >
            <span >Barr</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell8.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>macaron</span>
            <span style="float:right;">50 - 80</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member4.png" >
            <span >Arat09</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell4.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>อมยิ้ม macaron</span>
            <span style="float:right;">15 - 30</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member2.png" >
            <span >Nuntar</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell9.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>คุกกี้ช็อก</span>
            <span style="float:right;">80 - 150</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member5.png" >
            <span >Barr</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell5.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>เกลือชาเขียว</span>
            <span style="float:right;">40 - 80</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member4.png" >
            <span >Arat09</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell10.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>สายยางม้วน</span>
            <span style="float:right;">300</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member6.png" >
            <span >Datta</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell6.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>เสื้อถักแขนยาว</span>
            <span style="float:right;">550</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>
<div class="col-xs-4 col-md-3 col-lg-2 blog-wh" >
    <div class="mg-top-40">
        <div class="seller text-left">
            <img src="http://localhost:8000/image/shop/im_member2.png" >
            <span >Nuntar</span>
        </div>
        <div class="goods-image">
            <img src="http://localhost:8000/image/shop/im_sell11.png">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">3</span>/10 
        </div>
        
        <div class="goods-name">
            <span>พรมปูพื้นทำเอง</span>
            <span style="float:right;">350</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า</span>
        </div>
    </div>
</div>



         </div>
        <!-- </div> -->
   
</div>
</div>


  </div>

<div class="mg-top-40"></div>
  <div class="padding-top-25">
    <div class="footer-top">
    <div class="text-center">
        <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-5 footer-border-top"></div>
                <div class="col-xs-3 col-sm-2 col-md-2 footer-logo-top"><img class="footer-logo img-responsive" src="http://localhost:8000/image/app/logo_footer.png"></div>
                <div class="col-xs-4 col-sm-5 col-md-5 footer-border-top"></div>

        </div>
    </div>
    </div>
    <div class="footer-bottom">
        <div class="text-center">
            <ul>
                <li>FAQ</li>
                <li>Term&Condition</li>
                <li class="border-left-white">SALELAX © 2017</li>
            </ul>
        <div>
</div>
</div>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    
  </body>
  

</html>