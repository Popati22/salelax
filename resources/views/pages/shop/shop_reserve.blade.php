@extends('main')
@section('content')
<div class="top-15-container">
 
@component('components/shop/shop_order_bar')
@slot('first_step')
    active
@endslot
@slot('button_name')
    SELECT
@endslot

@endcomponent

</div>


<div class="row no-margin">
<div class="mg-top-25 text-center">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding">
        @component('components/shop/shop_images_slider', [ 'gallery' =>  [
                ['src' => asset('image/shop/im_sell4.png'), 'caption' => 'อมยิ้ม MACAROON', 'cover' => true],
                ['src' => asset('image/shop/im_sell2.png'), 'caption' => 'อมยิ้ม MACAROON'],
                ['src' => asset('image/shop/im_sell3.png'), 'caption' => 'อมยิ้ม MACAROON'],
                ['src' => asset('image/shop/im_sell1.png'), 'caption' => 'อมยิ้ม MACAROON'] 
            ]])
        @endcomponent
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
        <div class="top-15-container">
            @component('components/shop/shop_goods_detail')
                @slot('name')
                อมยิ้ม MACARON
                @endslot
                @slot('desc')
                    ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า
                @endslot
                @slot('ingredients')
                    มีส่วนประกอบหลัก คือ ไข่ขาว อัลมอนต์บดละเอียดและน้ำตาลปน 
                    มีลักษณะคล้ายคุกกี้ชิ้นเล็กสองอันประกบกัน มีไส้ตรงกลาง มีสีสันสดใส
                    กรอบนอกนุ่มใน สอดไส้ตรงกลางด้วยครีมกานาซ (Ganache) มีหลากหลายรสชาติ
                    เช่น ช็อกโกแลต สตรอเบอร์รี่ วานิลลา กาแฟ อัลมอนต์ หรือรสผลไม้ตามฤดูกาล
                @endslot
            @endcomponent
        </div>
        <div class="top-25-container border-bottom-price">
            @component('components/shop/shop_goods_price', ['all_price' => [
                    ['type' => 'รสสตอเบอรี่', 'price' => '30', 'left' => '5'],
                    ['type' => 'รสสัปปะรด', 'price' => '30', 'left' => '5'],
                    ['type' => 'รสช็อคโกแลต', 'price' => '40', 'left' => '5'],
                    ['type' => 'รสบลูเบอรี่', 'price' => '50', 'left' => '5']

                ] ])  
            @endcomponent
        </div>
        <div class="top-25-container">
            @component('components/shop/shop_goods_time', ['time' => '2 วัน', 'condition' => 'สินค้าใหม่'])  
            @endcomponent
        </div>
        <div class="top-25-container">
            @component('components/shop/shop_goods_seller',[
                'seller_name' => 'Arat09',
                'rating' => 155,
                'facebook' => 'facebook.com',
                'twitter' => 'twitter.com'])  

                @slot('avatar')
                    {{asset('image/shop/im_member4.png')}}
                @endslot
            @endcomponent
        </div>
    </div>
</div>
@endsection