@extends('main')
@section('content')


<div class="row no-margin">
<div class="mg-top-25 text-center">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding">
            @component('components/shop/shop_images_slider', [ 'gallery' =>  [
                ['src' => asset('image/shop/im_sell4.png'), 'caption' => 'อมยิ้ม MACAROON', 'cover' => true],
                ['src' => asset('image/shop/im_sell2.png'), 'caption' => 'อมยิ้ม MACAROON'],
                ['src' => asset('image/shop/im_sell3.png'), 'caption' => 'อมยิ้ม MACAROON'],
                ['src' => asset('image/shop/im_sell1.png'), 'caption' => 'อมยิ้ม MACAROON'] 
            ]])
            @endcomponent
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 no-padding">
    <div style="margin-left:50px;">
        @component('components/shop/shop_order_bar')
            @slot('thrid_step')
                active
            @endslot
            @slot('button_name')
                CONFIRM
            @endslot
        @endcomponent
    </div>
    <div class="top-15-container"></div>
    @component('components/shop/shop_goods_name', ['name' => 'อมยิ้ม MACAROON', 'price' => 100 ])  
    @endcomponent

    <div class="top-25-container"></div>
       
    <div class="top-15-container ">
        
        @component('components/shop/shop_goods_confirm', 
            ['total' => 4,
                'order' => [
                            [ 'type' => 'รสส้ม', 'pieces' => 1],
                            [ 'type' => 'สตอเบอรี่', 'pieces' => 1],
                            [ 'type' => 'สัปปะรด', 'pieces' => 1],
                            [ 'type' => 'บลูเบอรี่', 'pieces' => 1]
                        ],
                'location' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค',
                'place' => 'โต๊ะจ่ายตั๋ว',
                'date' => '30 กรกฏาคม 2560',
                'time' => '15.29'
            ])  
        @endcomponent
    </div>
        
    <div class="top-15-container text-left mg-top-40">
        <textarea class="customer-message" rows="4" cols="50" placeholder="เขียนข้อความ ถึงผู้ขาย"></textarea>
        </div>
        <a  style="color: #003C6E;" class="confirm-order-btn">
        <div class="top-15-container text-center mg-top-40 yellow-capsule-blue-font" >
          CONFIRM
        </div>
        </a>

    </div>
</div>

<div class="modal fade" id="order-sent" role="dialog">
    <div class="modal-dialog" style="width:350px;height:300px;">
      <div class="modal-content" style="height:260px;    background: rgba(0, 0, 0, 0.6);">
            <div class="text-center" style="margin-top:50px;">
                <img src="{{asset('image/app/correct_icon.png')}}" style="width:100px;">
            </div>
            <div class="text-center">
                <h3 style="color:white;">YOUR ORDER SENT</h3>
            </div>
      </div>
    </div>
  </div>

@endsection

