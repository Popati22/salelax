@extends('main')

@section('content')


<div class="top-15-container">
   
    @component('components/search')

    @endcomponent
    
</div>

<div class="top-15-container text-left">
   <?php $color = ['light-grey','grey', 'dark-grey', 'dark-brown','brown'] ;?>
    @component('components/shop/suggest_tag',['suggest' => [
      ['color' => $color[array_rand($color,1)],  'location' => 'หอสมุดป๋วย' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'สถาบันเอเชียตะวันออกศึกษา' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'โรงอาหารกลาง' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'Inter Zone' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'สถานีรถตู้ อนุสาวรีย์-ธรรมศาสตร์' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'สถานีรถตู้ จตุจักร-ธรรมศาสตร์' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'บีทีเอชสะพานควาย' ]
          ]
    ])

    @endcomponent
</div>


<div class="mg-top-40"></div>
<div class="top-15-container text-center " id ="wrapper">
    <div id="list">

        <shop_item v-for="data in model" :data="data"></shop_item>

        {{--@component('components/shop/shop_item',['all_goods' =>--}}
        {{--[--}}
          {{--[ 'seller_image' => asset('image/shop/im_member1.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell1.png'),--}}
            {{--'member_name' =>  'Nissaajaa',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,--}}
            {{--'name' => 'นกน้อยคัพเค้ก',--}}
            {{--'price' => '25 - 50',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า',--}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}

          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member2.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell2.png'),--}}
            {{--'member_name' =>  'Nuntar',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,--}}
            {{--'name' => 'เค้กกล้วยหอม',--}}
            {{--'price' => '30 - 50',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member3.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell3.png'),--}}
            {{--'member_name' =>  'Mathakoon',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,--}}
            {{--'name' => 'ผักดองเพื่อสุขภาพ',--}}
            {{--'price' => '150',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member4.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell4.png'),--}}
            {{--'member_name' =>  'Arat09',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,--}}
            {{--'name' => 'อมยิ้ม macaron',--}}
            {{--'price' => '15 - 30',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member5.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell5.png'),--}}
            {{--'member_name' =>  'Barr',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,--}}
            {{--'name' => 'เกลือชาเขียว',--}}
            {{--'price' => '40 - 80',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member6.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell6.png'),--}}
            {{--'member_name' =>  'Datta',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,--}}
            {{--'name' => 'เสื้อถักแขนยาว',--}}
            {{--'price' => '550',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
         {{----}}
          {{--[ 'seller_image' => asset('image/shop/im_member6.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell7.png'),--}}
            {{--'member_name' =>  'Datta',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'น้ำผึ้งแท้จากราชบุรี',--}}
            {{--'price' => 250,--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member5.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell8.png'),--}}
            {{--'member_name' =>  'Barr',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'macaron',--}}
            {{--'price' => '50 - 80',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member2.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell9.png'),--}}
            {{--'member_name' =>  'Nuntar',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'คุกกี้ช็อก',--}}
            {{--'price' => '80 - 150',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}

          {{--[ 'seller_image' => asset('image/shop/im_member4.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell10.png'),--}}
            {{--'member_name' =>  'Arat09',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'สายยางม้วน',--}}
            {{--'price' => '300',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}

          {{--[ 'seller_image' => asset('image/shop/im_member2.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell11.png'),--}}
            {{--'member_name' =>  'Nuntar',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'พรมปูพื้นทำเอง',--}}
            {{--'price' => '350',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member3.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell3.png'),--}}
            {{--'member_name' =>  'Mathakoon',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'ตุ๊กตาคั่นหนังสือ',--}}
            {{--'price' => '90',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member1.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell13.png'),--}}
            {{--'member_name' =>  'Nissajana',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'เสื้อกล้ามออกแบบเอง',--}}
            {{--'price' => '150',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member6.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell14.png'),--}}
            {{--'member_name' =>  'Datta',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'ไก่แจ้บ้านแท้',--}}
            {{--'price' => '1550',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller',--}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member5.png'),--}}
            {{--'goods_image' => asset('image/shop/im_sell15.png'),--}}
            {{--'member_name' =>  'Barr',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'เค้กกล้วยหอม',--}}
            {{--'price' => '15-30',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member3.png'),--}}
          {{--'goods_image' => asset('image/shop/im_sell16.png'),--}}
          {{--'member_name' =>  'Mathakoon',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'มะม่ววงสุกจากสวน',--}}
            {{--'price' => '50',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', --}}
            {{--'seller_link' => 'shop/seller', --}}
            {{--'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member5.png'),--}}
          {{--'goods_image' => asset('image/shop/im_sell17.png'),--}}
          {{--'member_name' =>  'Barr',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'ต่างหูไข่มุก',--}}
            {{--'price' => '15-50',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member4.png'),--}}
          {{--'goods_image' => asset('image/shop/im_sell18.png'),--}}
          {{--'member_name' =>  'Arat09',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'จักรยานญี่ปุ่น',--}}
            {{--'price' => '15-50',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'--}}
          {{--],--}}
          {{--[ 'seller_image' => asset('image/shop/im_member2.png'),--}}
          {{--'goods_image' => asset('image/shop/im_sell19.png'),--}}
          {{--'member_name' =>  'Nuntar',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'มินิคุ๊กกี้',--}}
            {{--'price' => '15-50',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'--}}
          {{--],--}}
          {{--[  'seller_image' => asset('image/shop/im_member3.png'),--}}
          {{--'goods_image' => asset('image/shop/im_sell20.png'),--}}
          {{--'member_name' =>  'Mathakoon',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'เกลือเรนโบว',--}}
            {{--'price' => '250',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'--}}
          {{--],--}}
          {{--[  'seller_image' => asset('image/shop/im_member1.png'),--}}
          {{--'goods_image' => asset('image/shop/im_sell21.png'),--}}
          {{--'member_name' =>  'Nissajana',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'คุกกี้น้องหมา',--}}
            {{--'price' => '25 - 50',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'--}}
          {{--],--}}
          {{--[   'seller_image' => asset('image/shop/im_member2.png'),--}}
          {{--'goods_image' => asset('image/shop/im_sell22.png'),--}}
          {{--'member_name' =>  'Nuntar',--}}
            {{--'location' => 'หอสมุดป๋วย',--}}
            {{--'total' => 10,--}}
            {{--'left' => 3,         --}}
            {{--'name' => 'พรมปูพื้นทำเอง',--}}
            {{--'price' => '550',--}}
            {{--'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า', --}}
            {{--'link' => '/shop/order', 'seller_link' => 'shop/seller', 'count' => '18k'--}}
          {{--]--}}
          {{--]--}}
        {{--])--}}
        {{--@endcomponent--}}
    </div>
</div>
@endsection

@section('script')

    <script src="/vue/directive/shop_item.js?v=2"></script>

   <script>
       var access_token =  '@php echo Cookie::get('access_token') @endphp';


       Vue.config.devtools = true;

       new Vue({
           el:'#app',
           data: {
               model: {},
               columns: {},
               show_col: {},
               query: {
                   page: 1,
                   column: 'id',
                   direction: 'desc',
                   per_page: 15,
                   search_column: 'id',
                   search_operator: 'equal',
                   search_input: '',
               },
               operators: {
                   equal: '=',
                   not_equal: '<>',
                   less_than: '<',
                   greater_than: '>',
                   less_than_or_equal_to: '<=',
                   greater_than_or_equal_to: '>=',
                   in: 'IN',
                   like: 'LIKE'
               },

               errors: []
           },
           mounted: function () {
               this.getData();
           },
           methods: {
               getData(){
                   this.$http.get('/api/v1/products').then((myresponse) => {
                       this.model = myresponse.data.data;
               }, (myresponse) => {
                       // error callback
                       console.log(myresponse.data);
                   });
               },
           }

       });

   </script>

@endsection

