@extends('main')

@section('content')
<style>
.item {
 /** order: <integer>; /* default is 0 */

  width: 200px;  /* Or whatever */
  height: auto; /* Or whatever */
  margin-top: 0px;  /* Magic! */
  margin-left:auto;
  margin-bottom:0px;
  margin-right:auto;
  -webkit-column-break-inside: avoid;

}
.my-layout {
  display:flex;
  flex-direction: row;
  flex-wrap:wrap;
  justify-content: flex-start;
  align-content: flex-start ;
 
}
</style>
<div class="top-15-container">
   
    @component('components/search')

    @endcomponent
    
</div>

<div class="top-15-container text-left">
   <?php $color = ['light-grey','grey', 'dark-grey', 'dark-brown','brown'] ;?>
    @component('components/shop/suggest_tag',['suggest' => [
      ['color' => $color[array_rand($color,1)],  'location' => 'หอสมุดป๋วย' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'สถาบันเอเชียตะวันออกศึกษา' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'โรงอาหารกลาง' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'Inter Zone' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'สถานีรถตู้ อนุสาวรีย์-ธรรมศาสตร์' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'สถานีรถตู้ จตุจักร-ธรรมศาสตร์' ],
      ['color' => $color[array_rand($color,1)],  'location' => 'บีทีเอชสะพานควาย' ]
          ]
    ])

    @endcomponent
</div>

<div class="mg-top-40"></div>
<div class="top-15-container text-center">
  <!-- <div class="row no-padding" id="layout-variable-" > -->
    <!-- first-colume -->
    <div id="#layout-variable" class="my-layout">
        @component('components/shop/shop_item',['all_goods' =>
        [
          [ 'seller_image' => asset('image/shop/im_member1.png'),
            'goods_image' => asset('image/shop/im_sell1.png'),
            'member_name' =>  'Nissaajaa',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,
            'name' => 'นกน้อยคัพเค้ก',
            'price' => '25 - 50',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
          ],
          [ 'seller_image' => asset('image/shop/im_member6.png'),
            'goods_image' => asset('image/shop/im_sell7.png'),
            'member_name' =>  'Datta',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,         
            'name' => 'น้ำผึ้งแท้จากราชบุรี',
            'price' => 250,
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
          ],
          [ 'seller_image' => asset('image/shop/im_member2.png'),
            'goods_image' => asset('image/shop/im_sell2.png'),
            'member_name' =>  'Nuntar',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,
            'name' => 'เค้กกล้วยหอม',
            'price' => '30 - 50',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
          ],
          [ 'seller_image' => asset('image/shop/im_member5.png'),
            'goods_image' => asset('image/shop/im_sell8.png'),
            'member_name' =>  'Barr',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,         
            'name' => 'macaron',
            'price' => '50 - 80',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
          ],
          [ 'seller_image' => asset('image/shop/im_member3.png'),
            'goods_image' => asset('image/shop/im_sell3.png'),
            'member_name' =>  'Mathakoon',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,
            'name' => 'ผักดองเพื่อสุขภาพ',
            'price' => '150',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
          ],
          [ 'seller_image' => asset('image/shop/im_member5.png'),
            'goods_image' => asset('image/shop/im_sell8.png'),
            'member_name' =>  'Barr',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,         
            'name' => 'macaron',
            'price' => '50 - 80',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
          ],
          [ 'seller_image' => asset('image/shop/im_member4.png'),
            'goods_image' => asset('image/shop/im_sell4.png'),
            'member_name' =>  'Arat09',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,
            'name' => 'อมยิ้ม macaron',
            'price' => '15 - 30',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า',
            'action' => 'buy'
          ],
          [ 'seller_image' => asset('image/shop/im_member2.png'),
            'goods_image' => asset('image/shop/im_sell9.png'),
            'member_name' =>  'Nuntar',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,         
            'name' => 'คุกกี้ช็อก',
            'price' => '80 - 150',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
          ],
          [ 'seller_image' => asset('image/shop/im_member5.png'),
            'goods_image' => asset('image/shop/im_sell5.png'),
            'member_name' =>  'Barr',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,
            'name' => 'เกลือชาเขียว',
            'price' => '40 - 80',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า',
          ],
          [ 'seller_image' => asset('image/shop/im_member4.png'),
            'goods_image' => asset('image/shop/im_sell10.png'),
            'member_name' =>  'Arat09',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,         
            'name' => 'สายยางม้วน',
            'price' => '300',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
          ],
          [ 'seller_image' => asset('image/shop/im_member6.png'),
            'goods_image' => asset('image/shop/im_sell6.png'),
            'member_name' =>  'Datta',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,
            'name' => 'เสื้อถักแขนยาว',
            'price' => '550',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า',
          ],
          [ 'seller_image' => asset('image/shop/im_member2.png'),
            'goods_image' => asset('image/shop/im_sell11.png'),
            'member_name' =>  'Nuntar',
            'location' => 'หอสมุดป๋วย',
            'total' => 10,
            'left' => 3,         
            'name' => 'พรมปูพื้นทำเอง',
            'price' => '350',
            'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
          ]
          ]
        ])
        @endcomponent
       </ul>
        <!-- </div> -->
   
</div>
</div>

<script type="text/javascript">

// $(function () {
// $(document).ready(function () {
//   var all_items = array();
//   $('div .blog-wh').each(function(i,d){
    
//       var item = { width: d.offsetWidth,
// 					        height: d.offsetHeight};
//       all_items.push(item );
//     )
//     var items = 
//     $('#layout-variable').igLayoutManager({
//                     layoutMode: "flow",
//                     items:  all_items
//     });
// });
// })
</script>
@endsection