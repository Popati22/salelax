@extends('main')

@section('style')

    <meta property="og:url"                content="{{ config('app.url') }}/shop/order/{{ $id }}/" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="{{ $title }}" />
    <meta property="og:description"        content="{{ $description }}" />
    <meta property="og:image"              content="{{ $image }}" />
    <meta property="og:image:secure_url"   content="{{ $image }}" />
    <meta property="og:image:width"        content="600" />
    <meta property="og:image:height"        content="315" />

    <meta property="fb:app_id"             content="283301778844117" />



    <style>
        .select2-container{
            width: 100px !important;

        }
        .select2-container--default .select2-selection--single{
            height: 40px;
            border-radius: 11px;
        }
        .select2-container .select2-selection--single .select2-selection__rendered {
            padding: 5px 20px 5px 10px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 40px;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #0082cc;
            font-weight: 300;
        }


        .form-control {
            font-size: 16px;
            height: 53px;
        }

        #my-breadcrumbs ul li:last-child a {
            border-top-right-radius: 20px;
            border-bottom-right-radius: 20px;
            border-bottom-left-radius: 0px;
            border: none;
            border-top-left-radius: 0px;
        }

    </style>

@endsection

@section('content')

<div class="row no-margin">
<div class="mg-top-25 text-center"></div>
    <div class="col-xs-12 col-sm-4 col-md-4 ">

        {{--image post--}}
        {{--<div class="flexslider order-image">--}}
            {{--<ul class="slides col-xs-12">--}}
                {{--<li v-for="img in post.image_cover">--}}
                    {{--<a href="javascript:void(0);" class="click-venobox" ><img :src="img.image_cover" /></a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}

        {{--<div v-for="img in post.image_cover" style="display:none;" class="modal-venobox">--}}
            {{--<a class="venobox"  data-gall="myGallery" :href="img.image_cover">--}}
                {{--<img class="img-responsive goods-image-order" :src="img.image_cover">--}}
            {{--</a>--}}
        {{--</div>--}}

        <flexslider :lists="post.image_cover"></flexslider>

        {{--image post--}}

            {{--@component('components/shop/shop_goods_seller',[--}}
                {{--'seller_name' => 'Arat09',--}}
                {{--'seller_link' => '/shop/profile',--}}
                {{--'rating' => 155,--}}
                {{--'facebook' => 'facebook.com',--}}
                {{--'twitter' => 'twitter.com'])  --}}

                {{--@slot('avatar')--}}
                    {{--{{asset('image/shop/im_member4.png')}}--}}
                {{--@endslot--}}
            {{--@endcomponent--}}

            <button type="button" class="btn btn-primary" @click="btn_share()"> <i class="fa fa-facebook-square"></i> Share</button>

            <div class="top-25-container"></div>

            <shop_goods_seller :seller="post"></shop_goods_seller>

            <div>
                <!-- Load Facebook SDK for JavaScript -->
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.10&appId=283301778844117";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
            </div>

        <div class="top-25-container"></div>
    </div>

    <div class="col-xs-12 col-sm-8 col-md-8 " style="padding-right:50px;">

            @component('components/shop/shop_order_bar')
                @slot('first_step')
                    active
                @endslot
                {{--@slot('button_name')--}}
                    {{--ORDER--}}
                {{--@endslot--}}
            @endcomponent

            <div class="top-25-container "></div>

            <div class="tab-content">
                <div id="order" class="tab-pane fade in active">

                    @component('components/shop/shop_goods_detail')
                        @slot('name')
                            @{{ post.title }}
                        @endslot
                        @slot('desc')
                            @{{ post.description }}
                        @endslot
                    @endcomponent

                    <div class="top-25-container "> </div>
                    {{--shop_goods_order--}}
                    <div class="row goods-price-header">
                        <div class="col-xs-3 col-md-3"><p class="text-left">รสชาติ</p></div>
                        <div class="col-xs-3 col-md-3"><p class="text-center">ราคา/ชิ้น</p></div>
                        <div class="col-xs-3 col-md-3"><p class="text-center">คงเหลือ</p></div>
                        <div class="col-xs-3 col-md-3"><p class="text-center">จำนวน</p></div>

                    </div>

                    <div class="row grey-20-bold" v-for="product in post.products">
                        <div class="col-xs-3 col-md-3 mg-top-10"><p class="text-left">@{{ product.name }}</p></div>
                        <div class="col-xs-3 col-md-3 mg-top-10"><p class="text-center">@{{ product.price }}</p></div>
                        <div class="col-xs-3 col-md-3 mg-top-10"><p class="text-center">@{{ product.amount }}</p></div>
                        <div class="col-xs-3 col-md-3 text-center">
                            <p class="text-center">
                                <select2 :options="product.arrAmount" v-model="product.quantity"></select2>
                            </p>
                        </div>
                    </div>
                    {{--shop_goods_order--}}

                    <div class="top-25-container"> </div>
                    <div v-if="post.location_type == 0">

                        <div class="col-xs-12 mg-top-10 no-padding ">
                            <div class="dropdown">
                                {{--<button class="btn btn-location-point dropdown-toggle" type="button" data-toggle="dropdown">--}}
                                    {{--<span class="mg-right-10 text-location-point">เลือกจุดนัดรับสินค้า</span>--}}
                                    {{--<span style="font-size:5px;" class="glyphicon glyphicon-menu-down menu-down-location-point grey-font"></span>--}}
                                {{--</button>--}}
                                {{--<ul class="btn-location-point dropdown-menu" v-model="location_select">--}}
                                    {{--<li v-for="loc in post.location">--}}
                                        {{--<a href="javascript:void(0);" :value="loc.id">@{{ loc.name }} - @{{ loc.displayname }} </a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                                <select class="form-control" v-model="location_select" placeholder="เลือกจุดนัดรับสินค้า">
                                    <option v-for="loc in post.location" :value="loc">
                                        <a href="javascript:void(0);">@{{ loc.name }} - @{{ loc.displayname }} </a>
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div v-else>

                        <div class="col-xs-12 mg-top-10 no-padding ">
                            <div class="dropdown">

                                <select class="form-control" v-model="location_select" placeholder="เลือกจุดนัดรับสินค้า">
                                    <option v-for="loc in locations" :value="loc">
                                        <a href="javascript:void(0);">@{{ loc.name }} - @{{ loc.displayname }} </a>
                                    </option>
                                </select>

                            </div>
                        </div>

                    </div>
                    <!--START date & time -->

                    <div class="col-xs-6 mg-top-10" style="padding-left:0px;">
                        <button class="grey-capsule">
                            <div class="input-group date input-sty-gray">
                                <datepicker class="input-sty1 input-lg bg-none" placeholder="วันที่ต้องการรับสินค้า" v-model="date_start.show"></datepicker>

                                {{--<input v-model="date" type="date" placeholder="วัน" readonly="readonly" class="input-sty1 datetimepicker_from bg-none  grey-font" style="width: 100%;margin-top: 7px;">--}}
                                <span class="input-group-addon calender-addon-btn ca-addon1">
                                    <span class="glyphicon glyphicon-calendar tx-i-blue"></span>
                                </span>
                            </div>
                        </button>
                    </div>
                    <div class="col-xs-6 mg-top-10 no-padding ">
                        <button class="grey-capsule">
                            <div class="input-group date  input-sty-gray">
                                {{--<input class="form-control" type="time" v-model="time">--}}
                                <timepicker class="input-sty1 input-lg bg-none" placeholder="เวลาที่สะดวกรับ" v-model="time"></timepicker>

                                {{--<input type="text" placeholder="เวลา" readonly="readonly" class="t2 datetimepicker2 form-control input-sty1 input-lg bg-none grey-font" style="height: 0px; margin-top:7px; font-size:15px;">--}}
                                <span class="input-group-addon calender-addon-btn add-t2">
                                    <span class="glyphicon glyphicon-time tx-i-blue"></span>
                                </span>
                            </div>
                        </button>
                    </div>
                    <div class="col-xs-12 mg-top-30 border-bottom-price" ></div>
                    <div>

                        {{--@component('components/shop/shop_goods_time', ['time' => '2 วัน', 'condition' => 'สินค้าใหม่'])--}}
                        {{--@endcomponent--}}
                    </div>
                    <!-- END date & time -->

                </div>
                <div id="confirm" class="tab-pane fade">

                    <div class="top-15-container"></div>
                    {{--@component('components/shop/shop_goods_name', ['name' => 'อมยิ้ม MACAROON', 'price' => 100 ])--}}
                    {{--@endcomponent--}}

                    <div class="goods-detail-order">
                        <div class="row no-margin">
                            <div class="col-xs-6 goods-name-order" style="padding-top:20px;padding-left:0px;">@{{ post.title }}</div>
                            <div class="col-xs-6"> <span class="grey-20">ยอดชำระ</span> <span class="yellow-price"> @{{ sumTotal }} </span><span class="black-20" style="font-size: xx-large;">บาท</span></div>
                        </div>
                    </div>

                    <div class="top-25-container"></div>


                    <div class="">
                        <div class="mg-top-40 text-left mg-bottom-40">
                            <div> รายการสั่งซื้อ </div>
                            <div class="blue-font">@{{ orders.length }} รายการ (
                                <span v-for="(order,index) in orders">
                                    <span>@{{ order.name }} @{{ order.quantity }} </span>
                                    <span v-if="index < orders.length-1">, </span>
                                </span>)

                            </div>
                            <div class="mg-top-15">
                                <div> สถานที่รับสินค้า<span class="mg-left-10"> :</span> <span class="blue-font mg-left-10">@{{ location_select.name }} - @{{ location_select.displayname }}</span></div>
                            </div>
                            {{--<div class="mg-top-15">--}}
                                {{--<div> จุดนัดรับสินค้า<span class="mg-left-10"> :</span> <span class="blue-font mg-left-10">-</span></div>--}}
                            {{--</div>--}}
                            <div class="mg-top-15">
                                <div class="row no-margin">
                                    <div class="col-xs-12 col-sm-5 col-md-6 col-lg-4 no-padding" v-if="date_start.show != ''">
                                        วันที่<span class="mg-left-10"> :</span>
                                        <span class="blue-font mg-left-10">@{{ date_start.show }}</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-6 col-lg-8 no-padding" v-if="time != ''">
                                        เวลา<span class="mg-left-10"> :</span>
                                        <span class="blue-font mg-left-10">@{{ time }}</span> น.
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="text-left mg-top-40">
                        <textarea class="customer-message" rows="4" cols="50" placeholder="เขียนข้อความ ถึงผู้ขาย"></textarea>
                    </div>
                    <a  style="color: #003C6E;" class="confirm-order-btn" @click="postOrder()">
                        <div class="text-center mg-top-40 yellow-capsule-blue-font" >
                            ยืนยัน
                        </div>
                    </a>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('script')
    <script src="/vue/directive/flexslider.js?v=9"></script>
    <script src="{{ asset('/vue/directive/datepicker.js?v=2') }}"></script>

    <script src="/vue/directive/select2.js"></script>
    <script src="/vue/directive/shop_goods_seller.js?v=4"></script>
    <script src="/vue/directive/datepicker.js?v=4"></script>
    <script src="/vue/directive/timepicker.js?v=2"></script>

    <script>
        Vue.config.devtools = true;

        Vue.http.options.xhr = {withCredentials: true}
        Vue.http.options.emulateJSON = true
        Vue.http.options.emulateHTTP = true
        Vue.http.options.crossOrigin = true
        Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'

        var Validator = SimpleVueValidator.Validator.create({
            templates: {
                greaterThan: 'จำเป็นต้องเลือก อย่างน้อย1ไฟล์',
                required: 'จำเป็นต้องใส่ข้อมูล'
            }
        });

        Vue.use(SimpleVueValidator);

        var vm = new Vue({
            el:'#app',
            data: {
                haveFile: false,
                post:{},
                profile:'',
                name: '',
                quantity: '',
                date:'',
                time:'',
                description : '',
                sku:{},
                validators_image: '',
                date_start:{
                    show:'',
                    value:''
                },
                location_select:'',
                locations:[],

                orders: [],
                sumTotal:0,

                arrUrls : [],
                arrImages: [],
                arrSKUs:[],
                posts : []
            },
            validators: {
                'name': function (value) {
                    return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
                },
                'quantity': function (value) {
                    return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
                },
                'description': function (value) {
                    return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
                },

            },
            watch:{
                'arrProducts' : function () {
                    // this.addImageProduct()
                },
                'date_start.show' : function(val){
                    if(val.length == 10){
                        this.date_start.value = val;
                        this.date_start.show = moment(val).format('ll');
                    }
                }
            },
            computed: {

            },
            mounted: function(){
                var url = window.location.href;
                this.arrUrls = url.split("/");

                this.getProfile();

                this.getPost(this.arrUrls[this.arrUrls.length-1]);

            },
            methods: {
                btn_share : function(){
                    FB.ui({
                        method: 'share',
                        mobile_iframe: true,
                        href: '{{ config('app.url') }}shop/order/'+vm.post.id,
                    }, function(response){});
                },
                calOrder : function(){
                    this.orders = [];

                    for(i=0;i<this.post.products.length;i++){
                        var product = this.post.products[i];
                        if(typeof product.quantity !== 'undefined' && product.quantity != ''){
                            this.orders.push({ product_id:product.id,name:product.name, quantity:product.quantity, price:product.price, sub_total:(product.quantity*product.price) });
                        }
                    }
                    this.calSumTotal();
                },
                calSumTotal:function () {
                    this.sumTotal = 0;
                    for(i=0;i<this.orders.length;i++){
                        this.orders[i].sub_total = Number(this.orders[i].quantity)*this.orders[i].price
                        this.sumTotal += this.orders[i].sub_total;
                    }

                },

                addImageProduct : function () {
                    var formData = new FormData();
                    formData.append('images', vm.arrProducts[0]);

                    vm.$http.post('/s3-base64-upload',{'image':vm.arrProducts[0], 'folder':'post'}).then(function(myresponse) {
                        console.log(myresponse.data);

                        this.arrImages.push({image_cover: myresponse.data, status:1})


                    }, function(myresponse) {

                        console.log(myresponse.data);
                    });
                },
                postOrder: function () {
                    var order = {
                        "post_id": this.post.id,
                        "total_amount": this.sumTotal,
                        "orderdetail": this.orders,
                        "location_id": this.location_select
                    }
                    //ซื้อแล้ว
                    this.$http.post('/api/v1/order', order).then(function(myresponse) {
                        console.log(myresponse.data);
                        swal(
                            'ทำรายการซื้อสำเร็จ',
                            '',
                            'success'
                        )

                        setTimeout(function(){
                            window.location.href='/shop';
                        },1500)

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                getPost : function (id) {
                    this.$http.get('/api/v1/post/'+id).then(function(myresponse) {
                        this.post = myresponse.data;

                        if(this.post.location_type == 1){
                            this.getLocation();
                        }

                        for(i=0 ; i<this.post.products.length ; i++){
                            this.post.products[i].arrAmount = [];

                            for(j=0 ; j<=this.post.products[i].amount ; j++){
                                this.post.products[i].arrAmount.push(j);
                            }

                            $('meta[property="og:title"]').attr('content',this.post.title)
                            $('meta[property="og:description"]').attr('content',this.post.description)
                            $('meta[property="og:image"]').attr('content',this.post.image_cover[0].image_cover)

                        }

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                getProfile : function () {
                    this.$http.get('/api/v1/profile').then(function(myresponse) {
                        this.profile = myresponse.data;

                        if(this.profile.img_profile == null){
                            this.profile.img_profile = '/image/default/user.png'
                        }

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                getLocation : function () {
                    this.$http.get('/api/v1/location').then(function(myresponse) {
                        this.locations = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                addSKU : function () {
                    vm.arrSKUs.push({name: this.sku.name, price:this.sku.price, amount:this.sku.amount });

                    this.sku={};
                },
                delSKU: function (index) {
                    vm.arrSKUs.splice(index, 1);

                },
                delImgProduct : function(image){
                    this.arrProducts.find(function(val,index){
                        if(val == image){
                            vm.arrProducts.splice(index, 1);
                            return;
                        }
                    })
                },
                openFile : function (event) {
                    var input = event.target;

                    var reader = new FileReader();
                    reader.onload = function(){
                        var dataURL = reader.result;
                        var output = document.getElementById('output');
                        output.src = dataURL;

                        vm.haveFile = true;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    </script>

@endsection
