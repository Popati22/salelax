@extends('main')
@section('content')
<style>
.navbar-default {
    border:0px;
    padding-left: 50px;
    padding-right: 50px;
    margin-left:0px;
    margin-right:0px;
} 
body {
    background-color:#ECEDED;
}
.fw-contact-btn {
    position: relative;
    display: inline;
}
@media(max-width:992px)
{
   
    .fw-contact-btn {
        width:100%;
        position:relative;
        text-align:center;
    }
    .follow-btn {
        /* width: 55% !important; */
        position: absolute;
        text-align: right;
        margin-right: 10px;
        right:50%;
    }
    .seller-contact-group {
        position: absolute;
        display: flex;
        float: right;
        left: 50%;
    }
    .show-item {
        margin-top:40px;
    }
}
@media(max-width:600px)
{
    .profile {
        margin-left:38%;
    }
    .seller-profile-container div:first-child, .seller-profile-container div:nth-child(2){
        width:100%;
        text-align:center;   
    }
    .fw-contact-btn {
        width:100%;
        position:relative;
        text-align:center;
    }
    .follow-btn {
        position: absolute;
        text-align: right;
        margin-right: 10px;
        right:21%;
    }
    .seller-contact-group {
        position: absolute;
        display: flex;
        float: right;
        left: 50%;
    }
    .show-item {
        margin-top:40px;
    }
}
</style>
<div class="profile-banner">
    <img src={{asset('image/profile/cover.png')}}>
</div>
<div class="container">
<img src={{asset('image/profile/im_profile.png')}}  class="profile">
</div>
<div class="row no-margin">
    <div class="container seller-profile-container">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3 text-left" >
            <div class="seller-name">LIKE NAME</div>
            <div class="seller-desc">...The Best Seller</div>
            <div class="mg-top-10"></div>
            <div class="seller-desc blue-20"><img src={{asset('image/app/speaker.png')}}  class="speaker"><span class="sell-text">SELL</span> <span class="sell-count">85</span> </div>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-4 col-lg-5 text-left mg-top-20">
            <div class="blue-font"><span class="glyphicon glyphicon-map-marker"></span><span class="mg-left-5">ศูนย์เทคโนโลยีอีเล็กทรอนิกส์และคอมพิวเตอร์แห่งชาติ</span></div>
            <ul class="none-list">
                <li class="text-center"><div class="blue-20-bold">185</div><div class="blue-font">RATING</div></li>
                <li class="border-left-blue text-center"><div class="black-20-bold">850</div><div >FOLLOWING</div></li>
                <li class="border-left-blue text-center"><div class="black-20-bold">235</div><div >FOLLOWERS</div></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-right mg-top-10 fw-contact-btn">
                <div class="col-xs-12 no-padding" style="position: relative;display: flex;">
                    <div class="follow-btn">
                        <button class="follow-button">FOLLOW</button>
                    </div>
                    @component('components/shop/shop_contact',['email' => 'SaleLaxLover@hotmail.com',
                                                                'telephone' => '080-5559989',
                                                                'facebook' => 'SaleLaxLover',
                                                                'line'=> 'SaleLaxLover' ])
                    @endcomponent

                </div>
                <div class="col-xs-12 show-item" style="padding-right: 50px;">
                    <div>
                        <span class="number-item">85</span><span class="mg-left-10"> ITEMS</span>
                    </div>
                </div>
     
        </div> 
        </div>
       
    </div>
    <div class="container join-container"><span class="mg-left-20">JOINED ON 09 JUN 2017</span></div>
</div>


<div class="seller-border-bar"></div>
<div class="row no-margin">
    <div class="container like-selling-container" style="padding-bottom:40px;">
        <div >
            @component('components/search')
            @endcomponent
        </div>

        <div class="mg-top-40"></div>
            <div class=" text-center" style="text-align: -webkit-center;">
                <div class="grey-capsule-sale-item"> 85 ITEMS </div>
            </div>
        
        <div class="mg-top-40"></div>
        <div class="text-center" id ="wrapper">
            <div id="list" >
                @component('components/shop/shop_goods_profile_item',['all_goods' =>
                [
                    [ 
                    'goods_image' => asset('image/shop/im_sell1.png'),
                    'member_name' =>  'Nissaajaa',
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,
                    'name' => 'นกน้อยคัพเค้ก',
                    'price' => '25 - 50',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
                ],
                [ 
                    'goods_image' => asset('image/shop/im_sell2.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,
                    'name' => 'เค้กกล้วยหอม',
                    'price' => '30 - 50',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
                ],
                [
                    'goods_image' => asset('image/shop/im_sell3.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,
                    'name' => 'ผักดองเพื่อสุขภาพ',
                    'price' => '150',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
                ],
                [ 
                    'goods_image' => asset('image/shop/im_sell4.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,
                    'name' => 'อมยิ้ม macaron',
                    'price' => '15 - 30',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า',
                ],
                [
                    'goods_image' => asset('image/shop/im_sell5.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,
                    'name' => 'เกลือชาเขียว',
                    'price' => '40 - 80',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า',
                ],
                [ 
                    'goods_image' => asset('image/shop/im_sell6.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,
                    'name' => 'เสื้อถักแขนยาว',
                    'price' => '550',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า',
                ],
                
                [ 
                    'goods_image' => asset('image/shop/im_sell7.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,         
                    'name' => 'น้ำผึ้งแท้จากราชบุรี',
                    'price' => 250,
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
                ],
                
                [ 
                    'goods_image' => asset('image/shop/im_sell8.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,         
                    'name' => 'macaron',
                    'price' => '50 - 80',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
                ],
                [ 
                    'goods_image' => asset('image/shop/im_sell8.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,         
                    'name' => 'macaron',
                    'price' => '50 - 80',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
                ],
                [ 
                    'goods_image' => asset('image/shop/im_sell9.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,         
                    'name' => 'คุกกี้ช็อก',
                    'price' => '80 - 150',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
                ],
                [ 
                    'goods_image' => asset('image/shop/im_sell10.png'),
                    'location' => 'หอสมุดป๋วย',
                    'total' => 10,
                    'left' => 3,         
                    'name' => 'สายยางม้วน',
                    'price' => '300',
                    'desc' => 'ทำจากแป้งคัดสรรค์พิเศษ หอมนุ่ม หวานน้อย ทำใม่ทุกวันหอมกรุ่นจากเตาทุกเช้า'
                ]
                ]
            ])
                @endcomponent
            </div>
        </div>
    </div>
</div>

@endsection

