@extends('main')

@section('style')

<style>
    .v-select > .dropdown-toggle > span:nth-child(1n+0){
        background-color: #6D8496;
    }
    .v-select > .dropdown-toggle > span:nth-child(2n+0){
        background-color: #546F88;
    }
    .v-select > .dropdown-toggle > span:nth-child(3n+0){
        background-color: #9FAEBA;
    }

    .v-select .selected-tag{
        color:white !important;
    }

    .img-thumbnail{
         border: none;
        border-radius: 12px !important;
    }
</style>
@endsection

@section('content')

<div class="top-15-container">
   
    @include('components/search')
    
</div>

<div class="top-15-container text-left">
    <list_tags></list_tags>
</div>

<div class="mg-top-40"></div>

<div class="top-15-container text-center " id ="wrapper">
    <div id="list">
        <shop_item v-for="data in model" :data="data"></shop_item>

    </div>
</div>

@endsection

@section('script')

    <script src="/vue/directive/tags.js?v=3"></script>
    <script src="/vue/directive/shop_item.js?v=9"></script>

    <script>

        Vue.config.devtools = true;
        Vue.component('v-select', VueSelect.VueSelect)

        new Vue({
            el:'#app',
            data: {
                model: {},
                columns: {},
                show_col: {},
                query: {
                    page: 1,
                    column: 'id',
                    direction: 'desc',
                    per_page: 15,
                    search_column: 'id',
                    search_operator: 'equal',
                    search_input: '',
                },
                operators: {
                    equal: '=',
                    not_equal: '<>',
                    less_than: '<',
                    greater_than: '>',
                    less_than_or_equal_to: '<=',
                    greater_than_or_equal_to: '>=',
                    in: 'IN',
                    like: 'LIKE'
                },

                errors: []
            },
            mounted: function () {
                this.getData();
            },
            methods: {
                getData(){
                    this.$http.get('/api/v1/post').then((myresponse) => {
                        this.model = myresponse.data;


                }, (myresponse) => {
                        // error callback
                        console.log(myresponse.data);
                    });
                },
            }

        });

    </script>

@endsection
