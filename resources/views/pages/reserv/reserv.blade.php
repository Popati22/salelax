@extends('main')
@section('content')

<div class="top-25-container text-center">

    @component('components/search')
    
    @endcomponent
   
</div>

<div class="top-25-container text-center content-reserv">

<div class="row mg-bottom-40">      
    <button class="btn-reserv" >      
        <span class="badge badg-bg-yl"><i class="glyphicon glyphicon-menu-right"></i></span>
        <span class="mg-left-15 h3 tx-weight2" id="select_category">สินค้าพร้อมส่งทั้งหมด 3 รายการ </span>
    </button>
</div>

<div class="row text-left">
	    @component('components/reserv_item',['reserv' => [
		      [	 
		      	'title' => 'อมยิ้ม MACARON',
		      	'price' => '100',
		      	'type_button' => 'btn-re-cancle',
		      	'detail' => '4 รายการ (รสส้ม 1, สตอเบอรี 1 ,รสสับปะรด 1,บลูเบอรี 1)',
		      	'site' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค...',
		      	'appointment' => 'โต๊ะจ่ายตั๋ว',		      	
		      	'date' => '30 กรกฏาคม 2560',
		      	'time' => '15.29'
		      ],
		       [	 
		      	'title' => 'อมยิ้ม MACARON',
		      	'price' => '100',
		      	'type_button' => 'btn-re-cancle',
		      	'detail' => '4 รายการ (รสส้ม 1, สตอเบอรี 1 ,รสสับปะรด 1,บลูเบอรี 1)',
		      	'site' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค...',
		      	'appointment' => 'โต๊ะจ่ายตั๋ว',		      	
		      	'date' => '30 กรกฏาคม 2560',
		      	'time' => '15.29'
		      ],
		       [	 
		      	'title' => 'อมยิ้ม MACARON',
		      	'price' => '100',
		      	'type_button' => 'btn-re-cancle',
		      	'detail' => '4 รายการ (รสส้ม 1, สตอเบอรี 1 ,รสสับปะรด 1,บลูเบอรี 1)',
		      	'site' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค...',
		      	'appointment' => 'โต๊ะจ่ายตั๋ว',		      	
		      	'date' => '30 กรกฏาคม 2560',
		      	'time' => '15.29'
		      ],
		       [	 
		      	'title' => 'อมยิ้ม MACARON',
		      	'price' => '100',
		      	'type_button' => 'btn-re-confirm',
		      	'detail' => '4 รายการ (รสส้ม 1, สตอเบอรี 1 ,รสสับปะรด 1,บลูเบอรี 1)',
		      	'site' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค...',
		      	'appointment' => 'โต๊ะจ่ายตั๋ว',		      	
		      	'date' => '30 กรกฏาคม 2560',
		      	'time' => '15.29'
		      ],
		       [	 
		      	'title' => 'อมยิ้ม MACARON',
		      	'price' => '100',
		      	'type_button' => 'btn-re-confirm',
		      	'detail' => '4 รายการ (รสส้ม 1, สตอเบอรี 1 ,รสสับปะรด 1,บลูเบอรี 1)',
		      	'site' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค...',
		      	'appointment' => 'โต๊ะจ่ายตั๋ว',		      	
		      	'date' => '30 กรกฏาคม 2560',
		      	'time' => '15.29'
		      ],
		       [	 
		      	'title' => 'อมยิ้ม MACARON',
		      	'price' => '100',
		      	'type_button' => 'btn-re-confirm',
		      	'detail' => '4 รายการ (รสส้ม 1, สตอเบอรี 1 ,รสสับปะรด 1,บลูเบอรี 1)',
		      	'site' => 'ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค...',
		      	'appointment' => 'โต๊ะจ่ายตั๋ว',		      	
		      	'date' => '30 กรกฏาคม 2560',
		      	'time' => '15.29'
		      ],
          ]
	    ])

    @endcomponent  
   
</div>    
</div>


 <!-- reserv modal -->
@include('modal/reserv_modal')


@endsection



