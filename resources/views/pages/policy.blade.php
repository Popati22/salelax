<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Salelax</title> <!-- change this title for each page -->
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{ asset('slick/slick.js') }}"></script>
    <!-- scrollbar market-->
    <link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css" />
    <script src="https://unpkg.com/simplebar@latest/dist/simplebar.js"></script>

    <script src="{{ asset('/js/moment.min.js?v=3') }}"></script>
    <script src="{{ asset('/js/moment-with-locales.min.js?v=2') }}"></script>
    <script src="{{ asset('/js/vue.min.js') }}"></script>
    <script src="{{ asset('/js/vue-resource.min.js') }}"></script>
    <script src="/vue/directive/vue-validator.js"></script>

    <style>
        p{
            color:black;
            text-align: justify;
            text-indent: 2em;
        }
        h4{
            color:black;
            text-align: left;
            margin-top: 2em;
        }
    </style>
</head>

<body class="bg">
<div class="login-left"></div>
<div class="login-center">
    <div class="login-bg">
        <div class="row text-center">
            <div class="">
                <div class="text-center" style="color:white;">
                    <div class=" login-container" >
                        <div class="panel panel-default" style="margin-top: 15%">
                            <div class="panel-heading">Salelax Digital Privacy Policy</div>
                            <div class="panel-body">
                                <p>This Privacy Policy outlines how we collect and use your data through our application. Personal information is information about you that is personally identifiable to you such as name, email addresses and phone number. “Anonymized Information” means information that is not associated with or linked to your Personal Information. Anonymized Information does not permit identification of you. Anonymized Information, such as your location, your products and transactions through the use of our services, does not by itself identify you.</p>

                                <h4>Collecting Information</h4>
                                <p>What you give us
                                    We collect information you give us or permit us to access. Information may include, but is not limited to, your name, email and telephone number, location (GPS) information, activities and transaction information.
                                </p>

                                <h4>Using Information</h4>
                                <p>We may use your information to analyze buying and selling behavior data and transactions trends on how you buy or sell from or to another account.</p>

                                <h4>Sharing of Information</h4>
                                <p>We will not share any of your information that we collect from you to any third party.</p>

                            </div>
                        </div>
                    </div> <!--login-box-->
                </div>
            </div>
        </div>
    </div>
</div>

</body>

<script>
    Vue.config.devtools = true;
    var Validator = SimpleVueValidator.Validator.create({
        templates: {
            greaterThan: 'จำเป็นต้องเลือก อย่างน้อย1ไฟล์',
            required: 'จำเป็นต้องใส่ข้อมูล'
        }
    });

    Vue.use(SimpleVueValidator);

    var vm = new Vue({
        el:'.login-center',
        data: {
            email: '',
            password : '',
        },
        validators: {
            'email': function (value) {
                return Validator.value(value).email('กรุณาใส่อีเมล์ให้ถูกต้อง');
            },
            'password': function (value) {
                return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
            },

        },
        methods: {
            submit : function () {
                this.$validate()
                    .then(function(success) {
                        if (success) {

                            var profile = {
                                "username":vm.email,
                                "password":vm.password
                            }

                            vm.$http.post('/login',profile).then(function(myresponse) {
                                if(myresponse.status == 200){
                                    window.location.href = '/market';
                                }else{
//                                    this.errors = myresponse.data.errors;
                                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                                }
                            }, function(myresponse) {
                                // error callback
                                console.log(myresponse.data);
                            });
                        }
                    });

            }
        }
    });
</script>

</html>