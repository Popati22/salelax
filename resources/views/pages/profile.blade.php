@extends('main')

@section('content')
<style>
    .navbar-default {
        border:0px;
        padding-left: 50px;
        padding-right: 50px;
        margin-left:0px;
        margin-right:0px;
    }
    body {
        background-color:#ECEDED;
    }
</style>

<div class="profile-banner text-right">
    <div class="top-25-container">
        <!-- <button class="cover-btn">Cover</button> -->
        <span class="btn cover-btn btn-file"><span style="color:white;"> Cover</span>
            <input id="select_cover" type="file" accept='image/*' onchange='openFile(event)'>
        </span>
    </div>
    <img v-if="profile.img_cover != null" id="img_cover" :src="profile.img_cover">
    <img v-else id="img_cover" src="/image/profile/cover.png">

</div>

<div class="container">
    <span class="btn profile-btn btn-file">
        <i class="fa fa-camera"></i>
        <input type="file" accept='image/*' onchange='openImgProfile(event)'>
    </span>
    <img v-if="profile.img_profile != null" id="img_profile" :src="profile.img_profile"  class="profile my-image">
    <img v-else id="img_profile" src="/image/default/user.png"  class="profile my-image">
</div>

<div class="row no-margin">
    <div class="container profile-container">
        <div class="col-lg-3"></div>
        <div class="col-xs-12 col-md-12 col-lg-5 text-center profile-connection">
            <ul class="none-list">
                <li class="text-center">
                    <div class="blue-20-bold" v-if="profile.ratting != null">@{{ profile.ratting }}</div>
                    <div class="blue-20-bold" v-else>-</div>
                    <div class="mg-top-5">RATING</div>
                </li>
                <li class="border-left-blue text-center" ><div class="blue-20-bold">@{{ following.length }}</div><div >FOLLOWING</div></li>
                <li class="border-left-blue text-center" ><div class="blue-20-bold">@{{ follwers.length }}</div><div >FOLLOWERS</div></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 text-center">
            <button class="blue-capsule-feedback" >FEEDBACK TO SALELAX</button>
        </div>
    </div>
    <div class="container join-container"><span class="mg-left-20">JOINED ON 09 JUN 2017</span></div>
</div>



<div class="row no-margin">
    <div class="container account-container grey-font">
        <div class="row">
            <div class="container profile-border-bottom" style="padding-bottom:0px;" >
                <div class="col-xs-2 text-center"><span class="icon-zag-ic-user"></span></div>
                {{--<div class="col-xs-10">Username</div>--}}
                <div class="col-xs-10"><input type="text" v-model="profile.display_name" class="input-none-border" name="displayname" placeholder="Username"></div>
            </div>
        </div>
        <div class="row">
            <div class="container profile-border-bottom ">
                <div class="col-xs-2 text-center"><img src="{{asset('image/app/email.png')}}"></div>
                <div class="col-xs-10"><lable style="    color: black;">@{{ profile.email }}</lable></div>
                <!-- <div class="col-xs-10"><input type="text"  v-model="profile.email" class="input-none-border" name="email" placeholder="test.email@hotmail.com"></div> -->

            </div>
        </div>
        {{--<div class="row">--}}
            {{--<div class="container profile-border-bottom ">--}}
                {{--<div class="col-xs-2 text-center"><img src="{{asset('image/app/pass.png')}}"></div>--}}
                {{--<div class="col-xs-10 pd-top-10">**********</div>--}}
                 {{--<!-- <div class="col-xs-10"><input type="password" value="" name="password" class="input-none-border" placeholder="password"></div> -->--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="container profile-border-bottom ">
                <div class="col-xs-2 text-center"><img style="height:30px;" src="{{asset('image/app/telephone.png')}}"></div>
                {{--<div class="col-xs-10 pd-top-10">080-5555989</div>--}}
                <div class="col-xs-10">
                    <input type="text" placeholder="080-0000000" v-model="profile.telephone" class="input-none-border" name="telephone">
                </div>
            </div>
        </div>
        {{--<div class="row" style="padding-bottom: 20px;">--}}
            {{--<div class="container profile-border-bottom " style="border:0px;">--}}
                {{--<div class="col-xs-2 text-center"><span class="blue-30 glyphicon glyphicon-map-marker"></span></div>--}}
                {{--<div class="col-xs-10 blue-font">YOU LOCATION</div>--}}
                {{--<div class="col-xs-10"><input type="text" class="input-none-border"name="location" placeholder="YOU LOCATION"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>

<div class="row no-margin">
    <div class="container account-container ">
        <div class="row">
            <div class="container profile-border-bottom" style="padding-bottom:0px;" >
                <div class="col-xs-2 text-center"><img style="height:45px;margin-bottom:15px;" src="{{asset('image/app/fb_icon.png')}}"></div>
                <div class="col-xs-10">
                    @if(Cookie::has('fb_id'))
                        <a class="btn btn-primary " style="background: #4267b2; color:white;    padding: 3px 10px;">
                            <img src="{{ Cookie::get('avatar') }}" class="img-circle" style="width: 32px;"> {{ Cookie::get('name') }}</a>
                    @else
                        <a class="btn btn-primary " style=" background: #4267b2; color:white;    padding: 10px;" v-on:click="window.location.href='/facebook/login'">
                            <i class="fa fa-facebook-square"></i> เชื่อมต่อ facebook</a>
                    @endif
                </div>

                <!-- <div class="col-xs-10 pd-top-10"><input type="text" v-model="profile.email" name="facebook" class="input-none-border" placeholder="Account Name"></div> -->
            </div>
        </div>
        <div class="row" style="padding-bottom: 20px;">
            <div class="container profile-border-bottom" style="border:0px;">
                <div class="col-xs-2 text-center"><img style="height:45px;"src="{{asset('image/app/line_icon.png')}}"></div>
                {{--<div class="col-xs-10 pd-top-10">LineID</div>--}}
                 <div class="col-xs-10 pd-top-10"><input type="text" v-model="profile.line_id" name="line" class="input-none-border" placeholder="LineID"></div>
            </div>
        </div>
    </div>
</div>

<div class="mg-top-40"></div>
<div class="row no-margin">
    <div class="container">
    <div class="col-xs-12 text-center">
        <button class="yellow-capsule-blue-font save-profile-btn" @click="updateProfile()">SAVE</button>
</div>
</div>
</div>
 
</div>
<!-- market-join -->
@include('modal/salelax_feedback');

@endsection

@section('script')
    <script>
        var openFile = function(event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                var output = document.getElementById('img_cover');
                output.src = dataURL;
                vm.profile.img_cover = dataURL;
            };
            reader.readAsDataURL(input.files[0]);
        };

        var openImgProfile = function(event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                var output = document.getElementById('img_profile');
                output.src = dataURL;
                vm.profile.img_profile = dataURL;
            };
            reader.readAsDataURL(input.files[0]);
        };
    </script>


    <script>
        Vue.config.devtools = true;

        var vm = new Vue({
            el:'#app',
            data: {
                profile:{
                    img_cover_base64:'',
                    img_cover:'',
                    img_profile_base64:'',
                    img_profile:'',
                    display_name:'',
                    telephone:'',
                    email:'',
                    fb_id:'',
                    line_id:''
                },
                follwers:[],
                following:[],

                errors: []
            },
            watch:{
                'profile.img_cover' : function () {
                    if(this.profile.img_cover.indexOf(';base64') > -1)
                        this.uploadImage(this.profile.img_cover, 'img_cover')
                },
                'profile.img_profile' : function () {
                    if(this.profile.img_profile.indexOf(';base64') > -1)
                        this.uploadImage(this.profile.img_profile, 'img_profile')
                }
            },
            mounted:function(){
                this.getProfile()
                this.getFollwers()
                this.getFollowing()
            },
            methods:{
                getProfile : function () {
                    this.$http.get('/api/v1/profile').then(function(myresponse) {
                        this.profile = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                getFollwers : function () {
                    this.$http.get('/api/v1/follwers').then(function(myresponse) {
                        this.follwers = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                getFollowing : function () {
                    this.$http.get('/api/v1/following').then(function(myresponse) {
                        this.following = myresponse.data;

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                updateProfile : function () {
                    this.$http.put('/api/v1/profile/1',this.profile).then(function(myresponse) {
                        swal(
                            'อัพเดทข้อมูลสำเร็จ',
                            '',
                            'success'
                        )

                        this.getProfile()

                    }, function(myresponse) {
                        // error callback
                        console.log(myresponse.data);
                    });

                },
                uploadImage : function (base64, name) {//base64, name

                    vm.$http.post('/s3-base64-upload',{'image': base64, 'folder':'profile'}).then(function(myresponse) {
                        console.log(myresponse.data);

                        if(name == 'img_cover')
                            this.profile.img_cover = myresponse.data;
                        if(name == 'img_profile')
                            this.profile.img_profile = myresponse.data;

                    }, function(myresponse) {

                        console.log(myresponse.data);
                    });
                },
            }
        });
    </script>
@endsection