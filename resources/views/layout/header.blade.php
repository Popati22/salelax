
<nav class="navbar navbar-default">
<div class="container-fluid">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/" style="padding-top: 35px;">
      <img alt="sale lax" class="header-logo" src="{{asset('image/app/logo.png')}}">
    </a>
  </div>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <div class="col-xs-8" style="float:right;">
      <ul class="nav navbar-nav text-center">
        <li>
          <a @if(Route::currentRouteName()=='market')class="menu-list active"@else class="menu-list" @endif href="/market">
            <span class="icon-zag-ic-markets "></span><span class="menu-desc">ตลาด</span>
          </a>
        </li>
        <li><a @if(Route::currentRouteName()=='history')class="menu-list active"@else class="menu-list" @endif href="/history">
          <span class="badge1 history-badge" style="display:none;" data-badge="5"></span>
          <img  @if(Route::currentRouteName()=='history') src="{{asset('image/app/blue_history_icon.png')}}" @else
          src="{{asset('image/app/grey_history_icon.png')}}" @endif style="height: 46px;padding-bottom:  10px;padding-left: 10px;"><span class="menu-desc">ประวัติ<span></a>
        </li>        
        <li>
          <a @if(Route::currentRouteName()=='shop')class="menu-list active"@else class="menu-list" @endif href="/shop">
            <span class="icon-zag-ic-shop" style="    height: 46px;"></span>
            <span class="menu-desc">ร้านค้า</span>
          </a>
        </li>
        <li>
          <a @if(Route::currentRouteName()=='sell') class="menu-list active"@else class="menu-list" @endif href="/sell">
            <span class="icon-zag-ic-sell" style="    height: 46px;"></span>
            <span class="menu-desc">ขาย</span>
          </a>
        </li>
        <li><a @if(Route::currentRouteName()=='notification')class="menu-list active"@else class="menu-list" @endif href="/notification">
          <div style="margin-left:30px;"><span class="badge1 noti-badge" style="display:none;" data-badge="5"></span></div>
           <img  @if(Route::currentRouteName()=='notification') src="{{asset('image/app/blue_noti_icon.png')}}" @else  
            src="{{asset('image/app/grey_noti_icon.png')}}" @endif 
            class="noti-icon-menu" style="height: 46px;">
          <span class="menu-desc">แจ้งเตือน</span></a>
        </li>   
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li>
          {{--@if(Cookie::has('fb_id'))--}}
            {{--<a class="btn btn-primary " style="margin-top: 30px; background: #4267b2; color:white;    padding: 3px 10px;">--}}
              {{--<img src="{{ Cookie::get('avatar') }}" class="img-circle" style="width: 32px;"> {{ Cookie::get('name') }}</a>--}}
          {{--@else--}}
            {{--<a class="btn btn-primary " style="margin-top: 30px; background: #4267b2; color:white;    padding: 10px;" href="/facebook/login">--}}
              {{--<i class="fa fa-facebook-square"></i> เข้าสู่ระบบ</a>--}}
          {{--@endif--}}
        </li>
        <li><a @if(Route::currentRouteName()=='profile')class="menu-user active"@else class="menu-user"@endif href="/profile"><span class="icon-zag-ic-user"></span></a></li>
        <li>
        <div class="btn-group seller-contact-group" style="position: relative; top:39px;" role="group">
          <a class="menu-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="background-color:white;cursor:pointer">
            <span class="icon-zag-ic-more"></span>
          </a>
          <ul class="dropdown-menu contact-dropdown  menu-user-dropdown">
            @if(Cookie::get('access_token'))
            <li class="text-center"><a href="#"><div><span>SALELAX</span></div></a></li>
            <li class="text-center"><a href="#"><div><span>SETTING</span></div></a></li>
            <li class="text-center"><a href="#"><div><span>FAQ</span></div></a></li>
            <li class="text-center"><a href="#"><div><span>TERM & CONDITIONS</span></div></a></li>
            <li class="text-center"><a href="/logout"><div><span>LOGOUT</span></div></a></li>
              @else
                <li class="text-center"><a href="/login"><div style="padding:5px;"><span>LOGIN</span></div></a></li>

              @endif
          </ul>
      </div>

         
        </li>

      </ul>
</div>

</div>
</nav>