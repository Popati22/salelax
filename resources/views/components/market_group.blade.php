<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-6 text-center mg-top-15"  >
    <a href="javascript:void(0);"  class="marget-join-group" id="market-group-{{$id}}">
        <div class="image">
            <img  class="recommend-image-group" :src="{{$image_src}}">
            <div class="recommend-market-group">
                <span class="market-group-name">{{$market_name}}</span>
                <span class="market-group-count-view">{{$count_view}} Members</span>
                <input type="hidden" name="location" value="{{$location}}">
                <input type="hidden" name="create" value="{{$create}}">
                <input type="hidden" name="description" value="{{$market_desc}}">
            </div>
        </div>
    </a>
</div>