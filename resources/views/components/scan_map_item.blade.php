<div>
    @foreach($all_items as $key => $val)

        <div class="scan-large row no-margin  grey-border-bottom scan-item "  @isset($val['active']) style="color:#0DB0FB !important;background-color:#ECEDED;" @endisset>
            <div class="col-xs-2 no-padding text-center">
                <span class="badge1" style=" position: relative; top: -10px;">{{$val['number']}}</span>

                <span class="glyphicon glyphicon-map-marker @if($val['type'] == 'recommend') yellow-font @else blue-font @endif" style="font-size: 46px;@isset($val['active']) color:#0DB0FB !important; @endisset"></span>
            </div>
            <div class="col-xs-6 no-padding text-left">
               {{$val['name']}}
            </div>
            <div class="col-xs-3 text-center">
                ระยะทาง {{$val['destination']}} Km
            </div>
            <div class="col-xs-1 no-padding text-center">
                <span class="glyphicon glyphicon-menu-right blue-font"></span>
            </div>
        </div>

    @endforeach
</div>
@foreach($all_items as $key => $val)
    <div class="scan-small row no-margin scan-item {{$val['active'] or ''}}">
        <div class="row no-margin">
            <div class="col-xs-2 no-padding text-center">
                <span class="badge1" style=" position: relative; top: -10px;">{{$val['number']}}</span>
                <span class="glyphicon glyphicon-map-marker @if($val['type'] == 'recommend') yellow-font @else blue-font @endif" style="font-size: 30px;"></span>
            </div>
            <div class="col-xs-10 no-padding text-left">
            {{$val['name']}}
            </div>
        </div>
        <div class="row no-margin grey-border-bottom">
            <div class="col-xs-2 no-padding"></div>
            <div class="col-xs-6 text-left no-padding">
                ระยะทาง {{$val['destination']}} Km
            </div>
            <div class="col-xs-4 no-padding text-center">
                <span class="glyphicon glyphicon-menu-right blue-font"></span>
            </div>
        </div>
    </div>
@endforeach