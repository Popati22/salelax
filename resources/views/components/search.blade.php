<!--  large device  -->
<div class="row no-margin text-center large-sort">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6  no-padding text-left" >
        <button class="grey-capsule" style="    border-radius: 8px;width:96%;float:left;">
            <input type="text" placeholder="ค้นหาตลาด" class="search-input">
            <span class="glyphicon glyphicon-search search-icon" ></span>
        </button>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 no-padding">
        <div class="btn-group" role="group" style="width:96%;float:left;">
            <button class="blue-capsule-sort btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mg-right-10">SORT</span> | 
                <span class="blue-font mg-left-15" id="select_price">LOWER PRICE </span>
                <span class="glyphicon glyphicon-menu-down blue-font mg-left-15"></span>
            </button>
            <ul class="dropdown-menu price-dropdown">
                <li><a href="javascript:void(0);" id="low_price"><div class="margin-left-10">ราคาถูก</div></a></li>
                <li><a href="javascript:void(0);" id="height_price"><div class="margin-left-10">ราคาสูง</div></a></li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 " style="padding-right: 0px">
        <div class="btn-group" role="group" style="width:100%;">
            <button class="blue-capsule-sort btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mg-right-10">FILTER</span> | 
                <span class="blue-font mg-left-15" id="select_category">CATEGORY </span>
                <span class="glyphicon glyphicon-menu-down blue-font mg-left-15"></span>
            </button>
            <ul class="dropdown-menu category-dropdown">
                <li><a href="javascript:void(0);" id="A"><div class="margin-left-10">CATAGORY A</div></a></li>
                <li><a href="javascript:void(0);" id="B"><div class="margin-left-10">CATAGORY B</div></a></li>
            </ul>
        </div>
    </div>
</div>

<!--  medium device  -->
<div class="row no-margin text-center medium-sort">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6 no-padding text-center" >
        <button class="grey-capsule" style="width:100%;">
            <input type="text" placeholder="Search" class="search-input">
            <span class="glyphicon glyphicon-search search-icon" ></span>
        </button>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 no-padding">
        <div class="btn-group" role="group" style="width:100%;">
            <button class="blue-capsule-sort" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mg-right-10">SORT</span> | 
                <span class="blue-font mg-left-15" id="select_price">LOWER PRICE </span>
                <span class="glyphicon glyphicon-menu-down blue-font mg-left-15"></span>
            </button>
            <ul class="dropdown-menu price-dropdown">
                <li><a href="javascript:void(0);" id="low_price"><div class="margin-left-10">ราคาถูก</div></a></li>
                <li><a href="javascript:void(0);" id="height_price"><div class="margin-left-10">ราคาสูง</div></a></li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="padding-right:0px;" >
        <div class="btn-group" role="group" style="width:100%;">
            <button class="blue-capsule-sort" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mg-right-10">FILTER</span> | 
                <span class="blue-font mg-left-15" id="select_category">CATEGORY </span>
                <span class="glyphicon glyphicon-menu-down blue-font mg-left-15"></span>
            </button>
            <ul class="dropdown-menu category-dropdown">
                <li><a href="javascript:void(0);" id="A"><div class="margin-left-10">CATAGORY A</div></a></li>
                <li><a href="javascript:void(0);" id="B"><div class="margin-left-10">CATAGORY B</div></a></li>
            </ul>
        </div>
    </div>  
</div>

<!-- small device -->
<div class="row no-margin text-center small-sort">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6 no-padding text-center" >
        <button class="grey-capsule" style="width:100%;">
            <input type="text" placeholder="Search" class="search-input">
            <span class="glyphicon glyphicon-search search-icon" ></span>
        </button>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 no-padding">
        <div class="btn-group" role="group" style="width:100%;">
            <button class="blue-capsule-sort" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mg-right-10">SORT</span> | 
                <span class="blue-font mg-left-15" id="select_price">LOWER PRICE </span>
                <span class="glyphicon glyphicon-menu-down blue-font mg-left-15"></span>
            </button>
            <ul class="dropdown-menu price-dropdown">
                <li><a href="javascript:void(0);" id="low_price"><div class="margin-left-10">ราคาถูก</div></a></li>
                <li><a href="javascript:void(0);" id="height_price"><div class="margin-left-10">ราคาสูง</div></a></li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 no-padding" >
        <div class="btn-group" role="group" style="width:100%;">
            <button class="blue-capsule-sort" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mg-right-10">FILTER</span> | 
                <span class="blue-font mg-left-15" id="select_category">CATEGORY </span>
                <span class="glyphicon glyphicon-menu-down blue-font mg-left-15"></span>
            </button>
            <ul class="dropdown-menu category-dropdown">
                <li><a href="javascript:void(0);" id="A"><div class="margin-left-10">CATAGORY A</div></a></li>
                <li><a href="javascript:void(0);" id="B"><div class="margin-left-10">CATAGORY B</div></a></li>
            </ul>
        </div>
    </div>  
</div>