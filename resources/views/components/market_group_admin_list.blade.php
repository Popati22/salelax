<div class="modal fade" id="admin-modal" role="dialog">
    <div class="modal-dialog" style="max-width:400px;">
    <div class="modal-content" style="background-color:#0095DE;">
            <div class="modal-header text-center ">
                <h3 class="modal-title bold" style="color:white;">ADMINISTRATOR</h3>
            </div>
            <div class="modal-body" style="background-color:white;">
                <div class="row no-margin">
                    <div >
                    @foreach($members as $key => $val)
                        <div class="member-list light-grey-border mg-top-10">
                            <div class="col-xs-3"><a href="#" class="text-center"><img class="img-responsive" style="height:40px;width:40px;border-radius:50%;" src="{{$val['profile']}}" /></a></div>
                            <div style="padding-top:10px;" class="col-xs-6 no-padding grey-font">{{$val['name']}}</div>  
                            <div class="col-xs-3">
                                @if(isset($val['main']))<img class="admin-icon-cover" style="width:50px !important;" src="{{asset('image/app/admin_icon.png')}}" >
                                @else
                                <img class="admin-icon-cover" style="width: 35px !important;margin-top: 0px;" src="{{asset('image/app/minus_icon.png')}}" >
                                @endif
                            </div>                       
                        </div>
                        @endforeach
                    </div>
                </div>
                </div>
            <!--end modal-body -->
            <div class="modal-footer leave-group-footer" style="text-align:center;background-color:white;border-bottom-left-radius: 15px;;border-bottom-right-radius: 15px;">
                <button class="green-capsule " style="margin-top:20px;" id="add-admin-btn">
                    <span class="col-xs-3"><img class="admin-icon-cover" style="width: 35px !important;margin-top: 0px;" src="{{asset('image/app/plus_icon.png')}}" ></span>
                    <span class="col-xs-8  green-font" style="margin-top: 5px;" >Add administrator</span> 
                    <span class="col-xs-1  green-font" style="margin-top: -8px;font-size: 32px;">></span>
                </button>
            </div>

        </div>
    </div>
</div>
