<div class="row goods-price-header">
    <div class="col-xs-6 col-md-4"><p class="text-left">รสชาติ</p></div>
    <div class="col-xs-3 col-md-4"><p class="text-center">ราคา/ชิ้น</p></div>
    <div class="col-xs-3 col-md-4"><p class="text-center">คงเหลือ</p></div>
</div>
@foreach($all_price as $item => $val)
<div class="row grey-20-bold">
    <div class="col-xs-6 col-md-4"><p class="text-left">{{$val['type']}}</p></div>
    <div class="col-xs-3 col-md-4"><p class="text-center">{{$val['price']}}</p></div>
    <div class="col-xs-3 col-md-4"><p class="text-center">{{$val['left']}}</p></div>
</div>
@endforeach
