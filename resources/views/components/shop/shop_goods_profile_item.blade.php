
@foreach($all_goods as $key => $val)
<div class="item">
    <div>
        <div class="goods-image" style="margin-top:0px;">
            <img src="{{$val['goods_image']}}">
        </div>
        <div class="goods-total grey-border-bottom">
            เหลือจำนวน <span class="orange-font margin-left-15">{{$val['left']}}</span>/{{$val['total']}} 
        </div>
        
        <div class="goods-name">
            <span>{{$val['name']}}</span>
            <span style="float:right;">{{$val['price']}}</span> 
        
        </div>
        <div class="goods-desc text-left">
            <span>{{$val['desc']}}</span>
        </div>
    </div>
</div>
@endforeach