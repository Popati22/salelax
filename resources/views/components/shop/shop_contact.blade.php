<div class="btn-group seller-contact-group" role="group">
    <button class="contact-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><img src="{{asset('image/app/blue_email.png')}}"><span class="blue-font mg-left-10">CONTACT</span></button>
    <ul class="dropdown-menu contact-dropdown">
        <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/email.png"/></span><span class="mg-left-10">{{$email}}</span></div></a></li>
        <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/telephone.png" style="width:26px;"/></span><span class="mg-left-20">{{$telephone}}</div></a></li>
        <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/fb_icon.png"/></span><span class="mg-left-10">{{$facebook}}</span></div></a></li>
        <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/line_icon.png"/></span><span class="mg-left-10">{{$line}}</span></div></a></li>
    </ul>
</div>  