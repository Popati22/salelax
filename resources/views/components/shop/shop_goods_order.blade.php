<div class="row goods-price-header">
    <div class="col-xs-3 col-md-3"><p class="text-left">รสชาติ</p></div>
    <div class="col-xs-3 col-md-3"><p class="text-center">ราคา/ชิ้น</p></div>
    <div class="col-xs-3 col-md-3"><p class="text-center">คงเหลือ</p></div>
    <div class="col-xs-3 col-md-3"><p class="text-center">จำนวน</p></div>
   
</div>
@foreach($all_price as $item => $val)
<div class="row grey-20-bold">
    <div class="col-xs-3 col-md-3 mg-top-10"><p class="text-left">{{$val['type']}}</p></div>
    <div class="col-xs-3 col-md-3 mg-top-10"><p class="text-center">{{$val['price']}}</p></div>
    <div class="col-xs-3 col-md-3 mg-top-10"><p class="text-center">2</p></div>
    <div class="col-xs-3 col-md-3 text-center">
        <p class="text-center">
            <div class="dropdown">
                <button class="btn btn-order-dropdown dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class="mg-right-10" style="font-size:20px;">{{$val['left']}}</span>
                <span style="font-size:5px;" class="glyphicon glyphicon-menu-down"></span></button>
                <ul class="btn-order-dropdown dropdown-menu">
                    @if($val['left'] > 0)
                        @for($i=1; $i <= $val['left'] ;$i++)
                            <li><a href="javascript:void(0);"> {{$i}} </a></li>
                        @endfor
                    @else
                        <li><a href="javascript:void(0);">0</a></li>
                    @endif

                </ul>
            </div>
        </p>
    </div>
</div>
@endforeach

