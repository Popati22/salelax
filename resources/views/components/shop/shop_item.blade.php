@foreach($all_goods as $key => $val)
<div class="item" >
    <div class="item-hover">
        <div class="seller text-left">
            <div class="row no-margin">

                {{--<a href="/shop/seller" target="_blank">--}}
                    <img src="{{$val['seller_image']}}" class="mg-top-20">
                    <h5 style="display: inline-block; position: relative; top: 6px;">{{$val['member_name']}}</h5>
                {{--</a>--}}

                {{--<div class="col-xs-4" style="padding-left:0px;">--}}
                    {{--<a href="{{$val['seller_link']}}"  target="_blank"><img class="mg-top-20" src="{{$val['seller_image']}}" ></a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-8" style="padding-top:5px;">--}}
                    {{--<span >{{$val['member_name']}}</span>--}}
                {{--</div>--}}
             </div>
        </div>
        <a style="color:unset;" href="{{$val['link']}}" target="_blank">
            <div class="goods-image">
                @isset($val['count'])
                <div class="buy-count-hover">
                    <div class="buy-count">{{$val['count']}}</div>
                    <img class="buy-heart" src="{{asset('image/app/heart.png')}}" >
                </div>
                @endisset
                    <img src="{{$val['goods_image']}}">

                    <button class="buy-count-hover buy-button">ซื้อ</button>
            </div>
            <div class="goods-total grey-border-bottom">
                เหลือจำนวน <span class="orange-font margin-left-15">{{$val['left']}}</span>/{{$val['total']}}
            </div>

            <div class="goods-name">
                <span>{{$val['name']}}</span>
                <span style="float:right;">{{$val['price']}}</span>

            </div>
            <div class="goods-desc text-left">
                <span>{{$val['desc']}}</span>
            </div>
        </a>
    </div>

</div>
@endforeach



  