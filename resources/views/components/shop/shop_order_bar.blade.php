
<div class="bar-large" >
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 text-left  bar-large-process">
            <div id="my-breadcrumbs" >
                <ul class="nav nav-tabs">
                    <li><a data-toggle="tab" href="#order" class="active">สั่งซื้อ</a></li>
                    <li><a data-toggle="tab" @click="calOrder()" href="#confirm" @isset($thrid_step)) class="active" @endisset>ยืนยัน</a></li>
                </ul>
            </div>
            {{--<div class="text-right mg-top-15" @isset($thrid_step)) style="display:none;" @endisset >--}}
                {{--<a  class="yellow-capsule-blue-font">{{$button_name}}</a>--}}
            {{--</div>--}}
        </div>
    </div>
</div>


<div class="bar-small">
    <div class="row">
        <div class="container text-center">
            <div class="col-xs-10 col-sm-8 bar-small-process no-padding" style="margin-left:-40px;">
                <div id="my-breadcrumbs">
                    <ul>
                        <li><a data-toggle="tab" href="#order" class="active">สั่งซื้อ</a></li>
                        <li><a data-toggle="tab" @click="calOrder()" href="#confirm" @isset($thrid_step)) class="active" @endisset>ยืนยัน</a></li>
                    </ul>
                </div>  
            </div>
           
            {{--<div class="col-xs-2 col-sm-4 mg-top-10 bar-small-button no-padding" @isset($thrid_step)) style="display:none;" @endisset>    --}}
                {{--<div class="text-right" >--}}
                    {{--<a  class="yellow-capsule-blue-font">{{$button_name}}</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
    </div>

    
    
