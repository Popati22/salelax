<div class="mg-top-40 text-left mg-bottom-40">
    <div> รายการสั่งซื้อ </div>
    <div class="blue-font">{{$total}} รายการ (
        @foreach($order as $key => $val ) {{$val['type']}} {{$val['pieces']}} @if( ($key+1) < count($order)) , @endif @endforeach )

    </div>
    <div class="mg-top-15">
        <div> สถานที่รับสินค้า<span class="mg-left-10"> :</span> <span class="blue-font mg-left-10">{{$location}}</span></div>   
    </div> 
    <div class="mg-top-15">
        <div> จุดนัดรับสินค้า<span class="mg-left-10"> :</span> <span class="blue-font mg-left-10">{{$place}}</span></div>   
    </div>       
    <div class="mg-top-15">
        <div class="row no-margin">
        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-4 no-padding"> วันที่<span class="mg-left-10"> :</span> <span class="blue-font mg-left-10">{{$date}}</span> </div>   
        <div class="col-xs-12 col-sm-7 col-md-6 col-lg-8 no-padding"> เวลา<span class="mg-left-10"> :</span> <span class="blue-font mg-left-10">{{$time}}</span> น. </div>   
    </div>
    </div>   
 </div>