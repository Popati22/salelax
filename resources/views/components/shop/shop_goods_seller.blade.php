<div class="row goods-seller no-margin">
    <div class="col-xs-9 text-left"> 
        ข้อมูลผู้ขาย
    </div>
    <div class="col-xs-3 text-right" role="group"> 
        <span class="icon-zag-ic-more" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer"></span>
            <ul class="dropdown-menu contact-dropdown grey-font contact-shop-order" >
                <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/email.png"/></span><span class="mg-left-10">SaleLaxLover@hotmail.com</span></div></a></li>
                <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/telephone.png" style="width:26px;"/></span><span class="mg-left-20">080-5559989</div></a></li>
                <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/fb_icon.png"/></span><span class="mg-left-10">SaleLaxLover</span></div></a></li>
                <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/line_icon.png"/></span><span class="mg-left-10">SaleLaxLover</span></div></a></li>
            </ul>
    </div>
</div>

<div class="mg-top-10"></div>
<div class="row no-margin">
        <div class="col-xs-12 text-center"> 
            <div class="row black-20">
                <div class="col-xs-4 goods-avatar text-left">
                   <a href="{{$seller_link}}" target="_blank"> <img src="{{$avatar}}"></a>
                </div>
                <div class="col-xs-8 text-right">
                    <div class="row">
                        <span>NAME</span><span class="mg-left-25">: </span><span class="mg-left-25">{{$seller_name}}</span>
                    </div>
                    <div class="row mg-top-15">
                        <img style="width:125px;" src="{{asset('image/app/rating.png')}}" ><span class="mg-left-25">{{$rating}}</span>
                    </div>
                </div>
            </div>

        </div>
  
        <div class="col-xs-12 mg-top-30 text-center " > 
            @isset($facebook)
            <a href={{$facebook}}> <img style="width:50px;" src="{{asset('image/app/facebook.png')}}"></a>
            @endif
            @isset($twitter)
                <a href={{@twitter}}><img style="width:50px;" src="{{asset('image/app/twitter.png')}}"> </a>
            @endif
        </div>

</div>


