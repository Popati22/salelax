<div class="row no-margin text-left">
    <div class="goods-name-order">
        {{$name}}
    </div>
    <div class="goods-detail-order">
        <p class="grey-font"> รายละเอียด</p>
        <p style="font-size:18px;"> {{$desc}}</p>
    </div>
</div>