<div class="flexslider profile-selling-slider">
  <ul class="slides">
    @foreach($all_selling as $key => $val)
    <li class="col-xs-3">
      <img class="img-responsive" style="height:350px;" src="{{$val['goods_image']}}" />
    </li>
    @endforeach
  </ul>
</div>