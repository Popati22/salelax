<!-- media width <  -->
<div class="flexslider recommend-market-slider  recommend-large" style="margin-top: 1em;">
  <ul class="slides">
    @foreach($all_items as $key => $val)
    <li class="" >
        <a href="javascript:void(0);" id="show-market-{{$val['id']}}" class="market-join">
            <div class="text-center" >
                <div class="image">
                    <img  class="recommend-image" src="{{$val['image_src']}}">
                    <div>
                        <div class="recommend-view center-block"> View</div>
                        <div class="recommend-count-view center-block"> {{$val['count_view']}} Members</div>
                    </div>

                    <p class="recommend-market">{{$val['market_name']}}</p>
                    <input type="hidden" name="location" value="{{$val['location']}}">
                    <input type="hidden" name="create" value="{{$val['create']}}">
                    <input type="hidden" name="description" value="{{$val['market_desc']}}">
                </div>
            </div>
        </a>
    </li>
    @endforeach
  </ul>
</div>



