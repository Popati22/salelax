<div>
    @isset($admin)
    <a href="{{$admin}}">
       <img src="/image/app/admin_icon.png" style="position: absolute;  width: 60px; z-index: 20; right: 5em;">
    </a>
    @endisset
    <a href="javascript:void(0);" id="show-market-{{$id}}" class="market-join">
        <div class="col-xs-12 col-sm-6  text-center suggest-container">
            <div class="image">
                <img  class="recommend-image suggest-image-responsive" src="{{$image_src}}" >
                <div class="recommend-view center-block"> View</div>
                <div class="recommend-count-view center-block"> {{$count_view}} Members</div>
                    <p class="recommend-market">{{$market_name}}</p>
                    <input type="hidden" name="location" value="{{$location}}">
                    <input type="hidden" name="create" value="{{$create}}">
                    <input type="hidden" name="description" value="{{$market_desc}}">
            </div>
        </div>
    </a>
</div>
