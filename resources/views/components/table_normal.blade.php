<table class="table table-normal mg-top-20">
  @foreach($list as $key => $val)
    <thead>
      <tr class="h4">
        <th class="tx-black">{{ $val['header']['col1'] }}</th>
        <th class="tx-black tx-center">{{ $val['header']['col2'] }}</th>
        <th class="tx-black tx-center">{{ $val['header']['col3'] }}</th>
      </tr>
    </thead>
    <tbody class="table-body-sell">
      @foreach($val['row'] as $key => $row)
      <tr class="h4 tx-gray">
        <td>{{ $row['title'] }}</td>
        <td class="tx-center" >{{ $row['price'] }}</td>
        <td class="tx-center" >{{ $row['amount'] }}</td>                          
      </tr>
      @endforeach 
    </tbody>   
  @endforeach                  
</table> 