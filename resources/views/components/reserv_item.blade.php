@foreach($reserv as $key => $val)
<div class="col-md-6 col-xs-12">        
      <section class="panel panel-default re-h-redius">
         <header class="panel-heading bg-head-re lt no-border">
          <div class="clearfix">
            <div class="clear">
              <div class="h3 m-t-xs m-b-xs">
                <span>{{$val['title']}}</span>
                @if($val['type_button'] == 'btn-re-confirm')
                  <button class="btn-re-confirm pull-right re-model" > 
                    <span><img src="{{asset('image/app/icon-ok.png')}}" class="icon-ok"></span> 
                    <span class="margin-left-10">ยืนยันแล้ว</span>
                  </button>
                @else
                  <button class="btn-re-cancel pull-right re-model" >      
                    <span>ยกเลิกการซื้อ</span>
                  </button>
                @endif
              </div>
              <span class="h4 tx-weight1">ยอดชำระ</span>
              <span class="h2 tx-yellow">{{$val['price']}}</span>
              <span class="h3 tx-weight1">บาท</span>
            </div>                
          </div>
        </header>
        <div class="panel-body panel-reserv">
          <form class="bs-example form-horizontal">           
            <div class="form-group h4">
              <label class="col-lg-12 control-label tx-left">รายการสั่งซื้อ</label>              
            </div>
            <div class="form-group h4">              
             <div class="col-lg-12">
                <span class="help-block m-b-none tx-blue">{{$val['detail']}}</span>
              </div>           
            </div>
            <div class="form-group h4">
              <label class="col-lg-4 control-label tx-left">สถานที่รับสินค้า : </label>
              <div class="col-lg-8">
                <span class="help-block m-b-none tx-blue">{{$val['site']}}</span>
              </div>
            </div>
            <div class="form-group h4">
              <label class="col-lg-4 control-label tx-left">จุดนัดรับสินค้า : </label>
              <div class="col-lg-8">
                <span class="help-block m-b-none tx-blue">{{$val['appointment']}}</span>
              </div>
            </div>   
            <div class="form-group h4">
              <label class="col-lg-2 col-xs-3 control-label tx-left">วันที่ : </label>
              <div class="col-lg-5 col-xs-9 ">
                <span class="help-block m-b-none tx-blue">{{$val['date']}}</span>
              </div>
              <label class="col-lg-2 col-xs-3 control-label tx-left">เวลา : </label>
              <div class="col-lg-3 col-xs-9">
                <span class="help-block m-b-none tx-blue">{{$val['time']}} น.</span>
              </div>
            </div>                     
          </form>
        </div>
      </section>
    </div>
 @endforeach