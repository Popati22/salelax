<div class="row goods-seller" style="padding-top:5px;">
    <div class="col-xs-10 text-left"> 
        <span class="h4">{{ $title }}</span>
    </div>
    <div class="col-xs-2 text-right"> 
        <span class="icon-zag-ic-more"></span>
    </div>
</div>
<div class="row mg-top-30" >
    <div class="col-xs-12 text-center"> 
        <div class="row black-20">
            <div class="col-xs-2 goods-avatar text-left">
               <a href="{{ $seller_link }}" target="_blank"> 
                <img src="{{$avatar}}" style="width:100%;height:100%;">
                </a>
            </div>
            <div class="col-xs-6 text-left">
                <div class="row h4">
                    <span>NAME</span><span class="mg-left-20">:</span><span class="mg-left-20">{{ $seller_name }}</span>
                </div>
                <div class="row mg-top-10">
                    <img style="width:40%;height:40%;" src="{{asset('image/app/rating.png')}}" >
                    <span class="tx-blue mg-left-25 tx-weight2">{{$rating}}</span>
                </div>
            </div>
            <div class="col-xs-4 goods-avatar text-right">
            @isset($facebook)
                <a href={{$facebook}}> <img style="width:35%;height:35%;" src="{{asset('image/app/facebook.png')}}"></a>
            @endif
            @isset($twitter)
                <a href={{$twitter}}><img style="width:35%;height:35%;" src="{{asset('image/app/twitter.png')}}"> </a>
            @endif
            </div>
        </div>
    </div>                  
</div>