

@foreach($all_items as $key => $val)
@if($val['action'] == 'buy')
<div class="col-xs-12 col-sm -12 col-md-6 col-lg-6 mg-top-20">
    <div class="rating-box">
        <div class="row no-margin rating-header">
            <div class="col-xs-3 blue-font">
                <h3>{{$val['goods_name']}}</h3>
                <p class="light-blue">ยอดชำระ <span class="rating-price ">{{$val['price']}}</span><span class="light-blue-20"> บาท</span> </p>
            </div>
            <div class="col-xs-3 blue-font">
                <h3>{{$val['goods_name']}}</h3>
                <p class="light-blue">ยอดชำระ <span class="rating-price ">{{$val['price']}}</span><span class="light-blue-20"> บาท</span> </p>
            </div>
            <div class="col-xs-3">
                <img class="like-image pull-right" src="{{asset('image/app/like.png')}}">
            </div>
        </div>
        <div>
            <button class="yellow-capsule-blue-font pull-right rating-btn">RATING</button>
        </div>
        <div class="row no-margin">
            <div class="col-xs-12 grey-font">
                <p รายการสั่งซื้อ</p>
                <p> {{$val['total']}} รายการ {{$val['description']}}</p>
            </div>
        </div>
        <div class="light-grey-border" style="width:96%;"></div>
        <div class="row no-margin">
            <div class="col-xs-12 grey-font mg-top-10">
                <p>สถานที่รับสินค้า: {{$val['place']}}</p>
                <p>จุดนัดรับสินค้า: {{$val['meeting_point']}}</p>
                <p>วันที่: {{$val['date']}} เวลา: {{$val['time']}} น.</p>
            </div>
        </div> 
    </div> <!-- rating-box-->
</div>
@else
<div class="col-xs-12 col-sm -12 col-md-6 col-lg-6 mg-top-20">
    <div class="rating-box rating-box-buy">
        <div class="row no-margin rating-header ">
            <div class="col-xs-6 grey-font">
                <h3>{{$val['goods_name']}}</h3>
                <p >ยอดชำระ <span class="rating-price ">{{$val['price']}}</span><span class="grey-20"> บาท</span> </p>
            </div>
            <div class="col-xs-6 pull-right">
                <div class="like-image pull-right buyer-profile">
                @if(isset($val['rating']['buyer_image']) && $val['rating']['buyer_image'] !== null)
                  <div class="grey-20-bold buyer-rating-text"> BUYER </div><img class="buyer-image-rating pull-right" src="{{$val['rating']['buyer_image']}}">
                @else 
                 <img class="like-image pull-right" src="{{asset('image/app/like.png')}}">   
                @endif
                </div>

            </div>
        </div>
        <div>
           @if(isset($val['rating']['score']) && $val['rating']['score'] > 0)
            <button class="buyer-star-btn pull-right rating-btn">
            <div class="rateYo"></div>
                <!-- <div class="starrr" >
                    @for($i = 1; $i <  6; $i++ )
                        @if( (int)$val['rating']['score']  > $i)
                        <a href="#" class="fa-star fa"></a>
                        @else
                        <a href="#" class="fa fa-star-o"></a>
                        @endif
                    @endfor
                </div> -->
                <!-- <div class="starrr" id="star{{$val['id']}}">  </div>
                <input type="hidden" name="rating" value="{{(int)$val['rating']['score']}}" id="star{{$val['id']}}_input"> -->
            </button>
           @else
            <button class="yellow-capsule-blue-font pull-right rating-btn">RATING</button>
            @endif
        </div>
        <div class="row no-margin">
            <div class="col-xs-12 grey-font">
                <p>รายการสั่งซื้อ</p>
                <p> {{$val['total']}} รายการ {{$val['description']}}</p>
            </div>
        </div>
        <div class="light-grey-border" style="width:96%;"></div>
        <div class="row no-margin">
            <div class="col-xs-12 grey-font mg-top-10">
                <p>สถานที่รับสินค้า: {{$val['place']}}</p>
                <p>จุดนัดรับสินค้า: {{$val['meeting_point']}}</p>
                <p>วันที่: {{$val['date']}} เวลา: {{$val['time']}} น.</p>
            </div>
        </div> 
    </div> <!-- rating-box-->
</div>

@endif
@endforeach