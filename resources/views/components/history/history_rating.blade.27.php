

@foreach($all_items as $key => $val)
@if($val['action'] == 'buy')
<div class="col-xs-12 col-sm -12 col-md-6 col-lg-6 mg-top-20">
    <div class="rating-box">
        <div class="row no-margin rating-header ">
            <div class="col-xs-6 col-sm-3 blue-font">
                <div class="like-image pull-right buyer-profile">
                  <img class="buyer-image-rating pull-right" src="{{$val['customer_image']}}">
                </div>
                <div class="text-center">
                {{$val['customer_name']}}
                </div>
            </div>
            <div class="col-xs-6 col-sm-7 blue-font">
                <h2>สินค้า : {{$val['goods_name']}}</h2>              
            </div>
            <div class="col-xs-6 col-sm-2">
                <img class="like-image pull-right" src="{{asset('image/app/like.png')}}">
            </div>
        </div>
        <div class="test" id="rating-{{$val['id']}}">
            <button class="yellow-capsule-blue-font pull-right rating-btn">RATING</button>
            <!-- <button class="buyer-star-btn pull-right rating-btn">
                <div class="rateYo1"></div>
            </button> -->
            
        </div>
        <div class="row no-margin">
            <div class="col-xs-12 grey-font" style="margin-top: -10px;">
                <h2 >ยอดชำระ <span class="rating-price ">{{$val['price']}}</span> บาท </h2>
            </div>
            <div class="col-xs-12 grey-font">
                <p >รายการสั่งซื้อ</p>
                <p> {{$val['total']}} รายการ {{$val['description']}}</p>
            </div>
        </div>
        <div class="light-grey-border" style="width:96%;"></div>
        <div class="row no-margin">
            <div class="col-xs-12 grey-font mg-top-10">
                <p>สถานที่รับสินค้า: {{$val['place']}}</p>
                <p>จุดนัดรับสินค้า: {{$val['meeting_point']}}</p>
                <p>วันที่: {{$val['date']}} เวลา: {{$val['time']}} น.</p>
            </div>
        </div> 
    </div> <!-- rating-box-->
</div>
@endif




@endforeach