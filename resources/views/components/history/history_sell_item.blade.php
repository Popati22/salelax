@foreach($all_goods as $key => $val)
<div class="item" >
    <div @if(isset($val['soldout'])) style="opacity:0.5;" @else class="item-hover" @endif>

        <a style="color:unset;" href="{{$val['link']}}" target="_blank">
            <div class="goods-image">
                @isset($val['count'])
                <div class="buy-count-hover">
                    <div class="buy-count">{{$val['count']}}</div>
                    <img class="buy-heart" src="{{asset('image/app/heart.png')}}" >
                </div>
                @endisset
                    <img src="{{$val['goods_image']}}">
                    <button class="buy-count-hover buy-button">ซื้อ</button>
            </div>
            <div class="goods-total grey-border-bottom">
                เหลือจำนวน 
                @if(isset($val['soldout']))<span class="orange-font margin-left-15"> {{$val['soldout']}}</span>
                @else<span class="orange-font margin-left-15">{{$val['left']}}</span>/{{$val['total']}}
                @endif
            </div>
            <div class="goods-name">
                <span>{{$val['name']}}</span>
                <span style="float:right;">{{$val['price']}}</span>
            </div>
            <div class="goods-desc text-left">
                <span>{{$val['desc']}}</span>
            </div>
        </a>
    </div>

</div>
@endforeach



  