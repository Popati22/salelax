
<div class="col-xs-12 col-sm-8 col-md-9 col-lg-10 no-padding">
<div class="flexslider flex-market-group">
  <ul class="slides">
    @foreach($members as $key => $val)
    <li class="col-xs-3">
      <a href="{{$val['link']}}"><img class="img-responsive" style="height:50px;width:50px;border-radius:50%;" src="{{$val['profile']}}" /></a>
    </li>
    @endforeach
  </ul>
</div>
</div>
