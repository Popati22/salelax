<div class="modal" id="addAdmin" role="dialog">
    <div class="modal-dialog" style="max-width:800px;overflow-y: initial !important">
    <div class="modal-content" style="background-color:#0095DE;">
            <div class="modal-header">
                <div class="row">
                    <div class="text-center" style="width:50%;float:left;">
                        <h3 class="modal-title bold" style="color:white;">ADMINISTRATOR</h3>
                    </div>
                    <div class="text-center" style="width:50%;float:right;">
                        <button class="pink-capsule-white-font">Cancel</button>
                        <button class="yellow-capsule-blue-font-admin">Save</button>
                    </div>
                </div>
            </div>
            <div class="modal-body" style="background-color:white; height: 300px;overflow-y: auto;">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 no-padding current-admin">
                        @foreach($admin as $key => $val)
                        <div class="member-list light-grey-border w-100 mg-top-10">
                            <div class="col-xs-3"><a href="#" class="text-center">
                                <img class="img-responsive" style="height:40px;width:40px;border-radius:50%;" src="{{$val['profile']}}" /></a></div>
                            <div style="padding-top:10px;" class="col-xs-6 no-padding grey-font">{{$val['name']}}</div>  
                            <div class="col-xs-3">
                                @if(isset($val['main']))<img class="admin-icon-cover" style="width:50px !important;" src="{{asset('image/app/admin_icon.png')}}" >
                                @else
                                <img class="admin-icon-cover" style="width: 35px !important;margin-top: 0px;" src="{{asset('image/app/minus_icon.png')}}" >
                                @endif
                            </div>                       
                        </div>
                        @endforeach
                        <!--select -->
                                       

                    </div>
                    <div class="col-xs-12 col-sm-6 no-padding">
                        <div class="col-xs-12  text-left mg-top-10" >
                            <button class="grey-capsule" style="float:left;">
                                <input type="text" placeholder="Search" class="search-input" value="Rya">
                                <span class="glyphicon glyphicon-search search-icon" ></span>
                            </button>
                        </div>
                        <div class="col-xs-12  text-left mg-top-10" >
                        @foreach($search_members as $key => $val)
                            <div  class="grey-capsule mg-top-10" id="{{$val['id']}}">
                                <div class="col-xs-3 no-padding"><a href="#" class="text-center">
                                    <img class="img-responsive user-profile" style="height:40px;width:40px;border-radius:50%;" src="{{$val['profile']}}" /></a>
                                </div>
                                <div style="padding-top:10px;" class="col-xs-6 grey-font"><span class="blue-font my-name">{{$val['name']}}</span></div>  
                                <div class="col-xs-3 no-padding">
                                    <a href="javascript:void(0);"  @if(isset($val['select_member'])) class="pull-right select-admin" @else  class="pull-right not-select-admin" @endif>
                                    @if(isset($val['select_member']))  
                                        <img style="width: 35px !important;margin-top: 0px;"  src="{{asset('image/app/green_circle.png')}}" >
                                    @else
                                        <img style="width: 35px !important;margin-top: 0px;" src="{{asset('image/app/grey_circle.png')}}" >
                                    @endif
                                    </a>   
                                </div>  
                            </div>
                        @endforeach   
                        </div>
                                            
                    </div>
                </div>
                
            </div>
            <!--end modal-body -->
            <div class="modal-footer leave-group-footer" style="text-align:center;background-color:white;border-bottom-left-radius: 15px;;border-bottom-right-radius: 15px;">
                
            </div>
           
        </div>
    </div>
</div>
















