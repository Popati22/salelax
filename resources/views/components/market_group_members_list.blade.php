<div class="modal" id="group_member">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="border-bottom: 1px solid #F1F1F1 !important;">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">{{$count}} Members</h4>
        </div>
        <div class="modal-body">
            @foreach($members as $key => $val)
            <div class="row no-margin">
                <div class="container">
                    <div class="member-list">
                        <div class="col-xs-4 col-sm-2"><a href="{{$val['link']}}" class="text-center"><img class="img-responsive" style="height:50px;width:50px;border-radius:50%;" src="{{$val['profile']}}" /></a></div>
                        <div class="col-xs-8 col-sm-10" style="padding-top:10px;">{{$val['name']}}</div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
</div>