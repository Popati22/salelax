<div class="modal fade" id="addModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
            <div class="modal-header text-center">
            <h4 class="modal-title blue-font bold">ADD MARKET</h4>

            </div>
            <div class="modal-body">
                <div class="grey-capsule add-market-image">

                    <input type="file" id="image_market" accept='image/*' @change="openFile(event)" class="input-file button-hover" style="width: 100%; position: absolute; height: 250px;">
                    <img id='output' class="img-responsive" style="object-fit: contain; z-index: 5;margin: 1em auto;">

                    {{--<i class="fa fa-times"></i>--}}
                    <div class="plus-image">
                        <img class="button-hover" style="width: 50px;"src="{{asset('image/app/plus_image.png')}}">
                        <div class="blue-font" style="margin-top: 5px;">เพิ่มรูปตลาด</div>
                    </div>

                    <div class="plus-image" style="bottom: -2px;cursor:  pointer;" v-show="haveFile">
                        <a>เลือกภาพใหม่</a>
                        <input type="file" id="image_market" accept="image/*" @change="openFile(event)" class="input-file button-hover" style="width: 100%; position: absolute; height: 250px;">
                    </div>

                </div>
                <div class="grey-capsule mg-top-10 grey-font">

                    <input type="text" id="name_market" class="input-none-border" name="add_market_name" v-model="name" placeholder="ชื่อตลาด">
                </div>
                <div class="grey-capsule mg-top-10">
                    <span class="glyphicon glyphicon-map-marker blue-font" style="width: 18%;">สถานที่ตั้ง</span>
                    {{--<input type="text" class="input-none-border" style="width:82%" name="add_market_location" placeholer="">--}}
                    <input id="pac-input" class="controls input-none-border" type="text" v-model="address" placeholder="ค้นหาสถานที่">
                </div>
                <div class="grey-capsule mg-top-10 grey-font">
                    <textarea rows="4" cols="100%" name="add_market_desc" class="input-none-border" v-model="description" placeholder="คำบรรยาย"></textarea>
                </div>
            </div>

            <!--end modal-body -->
            <div class="modal-footer" style="text-align:center;">
                <button class="modal-join-btn" style="margin-top:20px;" @click="addMarket()">
                    <span class="mg-right-10">SENT</span>
                </button>
            </div>
        
      </div>
    </div>
</div>



<script>
    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initAutocomplete() {

        // Create the search box and link it to the UI element.
        var input = document.getElementById('address_market');
        var searchBox = new google.maps.places.SearchBox(input);

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            console.log(places)
        });
    }
</script>

<!-- Replace the value of the key parameter with your own API key. -->

{{--<script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_api') }}&libraries=places&callback=initAutocomplete"--}}
        {{--async defer></script>--}}
