<div class="modal fade" id="reservModal" role="dialog">
<div class="modal-dialog modal-w400">
    <div class="modal-content modal-redius30">
        <form class="bs-example form-horizontal"> 
            <div class="modal-body">
            <div class="panel-body panel-reserv">                    
                <div class="form-group form-group-head">
                  <div class="col-lg-12">
                    <div class="h3 tx-center">คุณกำลังยกเลิกการซื้อสินค้า</div>
                    <div class="h4 tx-center tx-blue">ยกเลิกการซื้อ "นกน้อยคัพเค้ก" หรือไม่ ? </div>
                 </div>              
                </div>  
            </div>
            </div>            
            <!--end modal-body -->
            <div class="modal-footer mg-top-20" style="text-align:center;">
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <button class="btn-modal-cancel pull-left" data-dismiss="modal">      
                    <span>ไม่ยกเลิก</span>
                    </button>
                </div>
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <button class="btn-modal-confirm pull-left" >      
                    <span>ยืนยันยกเลิก</span>
                    </button>                
                </div>   
            </div>    
        </form>
  </div>
</div>
</div>

