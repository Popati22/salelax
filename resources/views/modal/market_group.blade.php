<style>
    /* //fix auto add padding-right */
    .modal-open {
        overflow: scroll;
    }
    
    #group_member .modal-content
    {
        height:350px;
        overflow:auto;
        overflow-x:hidden;
        border-radius: 0px;
    }
 
 </style>
<div class="modal fade" id="leaveGroup" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
            <div class="modal-header text-center mg-top-40">
            <h3 class="modal-title bold">คุณกำลังออกจากสถานีตลาด</h3>
                <h4 class="modal-title blue-font bold">ออกจาก "NETTEC Market" หรือไม่?</h4>
            </div>
            <div class="light-grey-border mg-top-40"></div>
            <!--end modal-body -->
            <div class="modal-footer leave-group-footer" style="text-align:center;">
                <button class="modal-join-btn cancel-red-btn" style="margin-top:20px;" >
                    <span class="mg-right-10">ยกเลิก</span>
                </button>
                <button class="modal-join-btn confirm-leave leave-group" style="margin-top:20px;" >
                    <span class="mg-right-10">ยืนยัน</span>
                </button>
            </div>
        
    </div>
    </div>
</div>
<div class="modal fade" id="inviteGroup" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
            <div class="modal-header text-center mg-top-40">
                <h3 class="modal-title blue-font bold">INVITE FRIENDS</h3>
            </div>
            <div class="modal-body">
                <div class="grey-capsule input-invite-friend mg-top-10 grey-font">
                    <input type="text" class="input-none-border" name="find_friend" placeholder="Friends in Salelax">
                    <span class="glyphicon glyphicon-search" style="font-size:23px" ></span>
                </div>
                <div class="mg-top-20 text-center">
                    <span class="grey-20-bold"> OR</span>
               </div>
       
                <div class="grey-capsule input-invite-friend mg-top-20 grey-font">               
                    <input  class="input-right" type="email" placeholder="Email" name="email" v-model="email">
                    <span class="icon-left email-icon" ><img  src="/image/app/email.png"/></span>
                </div>
            </div>
            <!--end modal-body -->
            <div class="modal-footer leave-group-footer" style="text-align:center;">
                <button class="modal-join-btn invite-sent" style="margin-top:20px;" >
                    <span class="mg-right-10">SENT</span>
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="invite-already" role="dialog">
    <div class="modal-dialog" style="width:350px;height:300px;">
      <div class="modal-content" style="height:260px;    background: rgba(0, 0, 0, 0.6);">
            <div class="text-center" style="margin-top:50px;">
                <img src="{{asset('image/app/correct_icon.png')}}" style="width:100px;">
            </div>
            <div class="text-center">
                <h3 style="color:white;">INVITED ALREADY</h3>
            </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="leave-already" role="dialog">
    <div class="modal-dialog" style="width:350px;height:300px;">
      <div class="modal-content" style="height:260px;    background: rgba(0, 0, 0, 0.6);">
            <div class="text-center" style="margin-top:50px;">
                <img src="{{asset('image/app/correct_icon.png')}}" style="width:100px;">
            </div>
            <div class="text-center">
                <h3 style="color:white;">LEAVE GROUP ALREADY</h3>
            </div>
      </div>
    </div>
  </div>


