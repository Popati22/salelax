
<div class="modal fade" id="joinModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <div class="modal-location blue-font"><span class="glyphicon glyphicon-map-marker"></sapn><span class="m-location mg-left-5"></span></div>
                <div class="modal-desc-view">
                    <span class="grey-font-l">DESCRIPTION</span>
                    <span class="grey-font-r" id="count-view-market"></span>
                </div>
                <div class="description-wrap" data-simplebar="init">
                    <div class="simplebar-track vertical" style="visibility: visible;">
                        <div class="simplebar-scrollbar" style="top: 46px; height: 68px;"></div>
                    </div>
                    <div class="simplebar-track horizontal" style="visibility: visible;">
                        <div class="simplebar-scrollbar"></div>
                    </div>
                    <div class="simplebar-scroll-content" style="margin-right: -20px; padding-right: 20px; margin-bottom: -34px;">
                        <!--start description -->
                        <div class="simplebar-content" id="market_description" style="padding-bottom: 20px; margin-right: -17px;">

                                <h2>Scroll me!</h2>
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Scroll me!</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Scroll me!</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--<p class="odd">Some content</p>--}}
                                {{--<p>Some more content</p>--}}
                                {{--hey!--}}
                    </div>
                </div> 
            </div>
             <!--end wrap -->
            <div class="modal-create">
                <span class="grey-font-l">CREATED</span>
            </div>
            <div class="modal-create-date">
            </div>
        </div>
         <!--end modal-body -->
        <div class="modal-footer" style="text-align:center;">
            @if(Auth::guest())
                <a href="/login" class="modal-join-btn join-market-btn" >
                    <span class="mg-right-10">JOIN</span>
                </a>
            @else
                <button class="modal-join-btn join-market-btn" >
                    <span class="mg-right-10">JOIN</span>
                </button>
            @endif
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="join-successed" role="dialog">
    <div class="modal-dialog" style="width:350px;height:300px;">
      <div class="modal-content" style="height:260px;    background: rgba(0, 0, 0, 0.6);">
            <div class="text-center" style="margin-top:50px;">
                <img src="{{asset('image/app/correct_icon.png')}}" style="width:100px;">
            </div>
            <div class="text-center">
                <h3 style="color:white;">JOIN SUCCESSED</h3>
            </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="not-proceed" role="dialog">
    <div class="modal-dialog" style="width:350px;height:300px;">
      <div class="modal-content" style="height:260px;    background: rgba(0, 0, 0, 0.6);">
            <div class="text-center" style="margin-top:50px;">
                <img src="{{asset('image/app/cannot_proceed_icon.png')}}" style="width:100px;">
            </div>
            <div class="text-center">
                <h3 style="color:white;">CAN NOT PROCEED</h3>
            </div>
      </div>
    </div>
  </div>


