
<div class="modal fade col-xs-12 text-center" id="feedback" role="dialog">
    <div class="modal-dialog ">
      <div class="modal-content"  style="min-height:600px;">
            <div class="modal-header text-center">
                <h4 class="modal-title blue-font bold">FEEDBACK TO SALELAX</h4>
            </div>
            <div class="modal-body">
                <div class="grey-capsule mg-top-10 grey-font"><input type="text" class="input-none-border" name="topic" placeholder="Topic"></div>
                <div class="grey-capsule mg-top-10 grey-font">
                    <textarea rows="14" cols="100%" name="message" class="input-none-border" placeholder="Message"></textarea>
                </div>
            </div>
            <!--end modal-body -->
            <div class="modal-footer" style="text-align:center;">
                <button class="modal-join-btn send-feedback-btn" style="margin-top:20px;" >
                    <span class="mg-right-10">SENT</span>
                </button>
            </div>
        
      </div>
    </div>
  </div>


