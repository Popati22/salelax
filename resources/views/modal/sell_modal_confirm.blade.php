<div class="modal fade" id="sell_confirmModal" role="dialog">
    <div class="modal-dialog modal-w1200">
    <div class="modal-content modal-redius-close" >   
        {{--<div class="modal-header no-padd">--}}
            {{--<button type="button" class="close h3" data-dismiss="modal">&times;</button>--}}
        {{--</div>--}}
        <form class="bs-example form-horizontal" action="#"> 
        <div class="modal-body">

            <button type="button" class="close h3" data-dismiss="modal">&times;</button>

            <div class="panel-body panel-reserv">                    
              <div class="col-lg-4">

                  <div class="mg-top-20 no-padd" v-if="arrImages.length>0" style="margin-right: -15px; margin-left: -15px;">

                      <img v-for="img in arrImages" v-if="img.status==1" class="img-responsive" :src="img.image_cover">

                  </div>

                  <div class="col-xs-12 mg-top-20 no-padd">
                      {{--<flexslider :lists="arrImages"></flexslider>--}}

                      {{--<div class="flexslider order-image">--}}
                          {{--<ul class="slides col-xs-12">--}}
                              {{--<li v-for="img in arrImages">--}}
                                  {{--<a href="javascript:void(0);" class="click-venobox" ><img :src="img.image_cover" /></a>--}}
                              {{--</li>--}}
                          {{--</ul>--}}
                      {{--</div>--}}

                      {{--<div v-for="img in arrImages" style="display:none;" class="modal-venobox">--}}
                          {{--<a class="venobox"  data-gall="myGallery" :href="img.image_cover">--}}
                              {{--<img class="img-responsive goods-image-order" :src="img.image_cover">--}}
                          {{--</a>--}}
                      {{--</div>--}}


                  </div>

                  <div class="col-xs-12 mg-top-20 no-padd">

                     <div class="row goods-seller" style="padding-top:5px;">
                         <div class="col-xs-10 text-left">
                             <span class="h4">@{{  profile.display_name }}</span>
                         </div>
                         <div class="col-xs-2 text-right">
                             <span class="icon-zag-ic-more"></span>
                         </div>
                     </div>
                     <div class="row mg-top-30" >
                         <div class="col-xs-12 text-center">
                             <div class="row black-20">
                                 <div class="col-xs-2 goods-avatar text-left">
                                     <a :href="profile.img_profile" target="_blank">
                                         <img :src="profile.img_profile" style="object-fit:cover">
                                     </a>
                                 </div>
                                 <div class="col-xs-6 text-left">
                                     <div class="h4">
                                         <span>NAME</span><span class="mg-left-20">:</span><span class="mg-left-20">@{{ profile.display_name }}</span>
                                     </div>
                                     <div class="mg-top-10">
                                         <img style="width:40%;height:40%;" src="{{asset('image/app/rating.png')}}" >
                                         <span class="tx-blue mg-left-25 tx-weight2">@{{ profile.ratting }}</span>
                                     </div>
                                 </div>
                                 <div class="col-xs-4 goods-avatar text-right">
                                     {{--@isset($facebook)--}}
                                         {{--<a href={{$facebook}}> <img style="width:35%;height:35%;" src="{{asset('image/app/facebook.png')}}"></a>--}}
                                     {{--@endif--}}
                                     {{--@isset($twitter)--}}
                                         {{--<a href={{$twitter}}><img style="width:35%;height:35%;" src="{{asset('image/app/twitter.png')}}"> </a>--}}
                                     {{--@endif--}}
                                 </div>
                             </div>
                         </div>
                     </div>

                   {{--@component('components/data_user',[--}}
                      {{--'title' => 'ข้อมูลผู้ขาย',--}}
                      {{--'seller_name' => @{{ profile.display_name }},--}}
                      {{--'seller_link' => @{{ profile.img_profile }},--}}
                      {{--'rating' => @{{ profile.following }},--}}
                      {{--'facebook' => 'facebook.com',--}}
                      {{--'twitter' => 'twitter.com'])--}}
                      {{--@slot('avatar')--}}
                          {{--{{asset('image/shop/im_member4.png')}}--}}
                      {{--@endslot--}}
                  {{--@endcomponent--}}
                </div>
             </div>   
             <div class="col-lg-8">                
                @component('components/shop/shop_goods_detail')
                    @slot('name')
                        @{{ name }}
                    @endslot
                    @slot('desc')
                        @{{ description }}
                    @endslot

                @endcomponent

                <div class="col-xs-12 mg-top-20 no-padd">

                    <table class="table table-normal mg-top-20">
                            <thead>
                                <tr class="h4">
                                    <th class="tx-black">ชื่อ</th>
                                    <th class="tx-black tx-center">ราคา</th>
                                    <th class="tx-black tx-center">จำนวนสั่ง</th>
                                </tr>
                            </thead>
                            <tbody class="table-body-sell">
                                <tr class="h4 tx-gray" v-for="sku in arrSKUs">
                                    <td>@{{ sku.name }}</td>
                                    <td class="tx-center" >@{{ sku.price }}</td>
                                    <td class="tx-center" >@{{ sku.amount }}</td>
                                </tr>
                            </tbody>
                    </table>

                </div>


                <div class="col-xs-12 mg-top-20 border-bottom-price" ></div>

                    <shop_goods_time :location_type="location_type" :item_condition="item_condition"></shop_goods_time>
                {{--@component('components/shop/shop_goods_time', ['time' => '2 วัน', 'condition' => 'สินค้าใหม่'])--}}
                {{--@endcomponent--}}

             

             </div>              

            </div>
        </div>            
        <!--end modal-body -->

        <div class="modal-footer" style="text-align:center;">    
            <div class="col-xs-3 border-t-gray padd-t-30">
                {{--<button class="btn-md-blue-w-p"  style="width:100%;">--}}
                  {{--<span class="mg-right-10">Edit</span>--}}
                {{--</button>  --}}
            </div>      
            <div class="col-xs-6 border-t-gray padd-t-30">
                <button class="btn-modal-confirm" style="padding: 8px;" @click="addProduct()">
                    <span>ยืนยัน</span>
                </button>                
            </div>
            <div class="col-xs-3 border-t-gray padd-t-30">
                {{--<button class="btn-modal-cancel pull-left" style="padding: 8px;" data-toggle="modal" data-target="#cancle_sell">      --}}
                {{--<span>ยกเลิก</span>--}}
                {{--</button>--}}
            </div> 
        </div>    
        </form>

    </div>
</div>
</div>


<div class="modal fade" id="share_fb" role="dialog">
    <div class="modal-dialog modal-w400">
        <div class="modal-content modal-redius30">
            <form class="bs-example form-horizontal">
                <div class="modal-body">
                    <div class="panel-body panel-reserv">
                        <div class="form-group" style="padding-bottom:0px !important;">
                            <div class="col-lg-12">
                                <div class="h3 tx-center">แชร์เฟสบุ๊ค</div>
                            </div>
                            <div class='col-md-12 mg-top-20 text-center'>

                                <!-- Your share button code -->
                                <button type="button" class="btn btn-primary" @click="btn_share()">Share Facebook</button>
                                {{--<div class="fb-share-button"--}}
                                     {{--:data-href="'{{ config('app.url') }}shop/order/'+"--}}
                                     {{--data-layout="button_count">--}}
                                {{--</div>--}}

                            </div>

                        </div>
                    </div>
                </div>
                <!--end modal-body -->
                {{--<div class="modal-footer mg-top-20" style="text-align:center;">--}}
                    {{--<div class="col-sm-6 col-xs-12 padd-t-20">--}}
                        {{--<button class="btn-modal-cancel pull-left" data-dismiss="modal">--}}
                            {{--<span>ไม่ยกเลิก</span>--}}
                        {{--</button>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-6 col-xs-12 padd-t-20">--}}
                        {{--<button class="btn-modal-confirm pull-left" >--}}
                            {{--<span>ยืนยันยกเลิก</span>--}}
                        {{--</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="cancle_sell" role="dialog">
    <div class="modal-dialog modal-w400">
        <div class="modal-content modal-redius30">
            <form class="bs-example form-horizontal">
                <div class="modal-body">
                <div class="panel-body panel-reserv">
                    <div class="form-group form-group-head" style="padding-bottom:0px !important;">
                      <div class="col-lg-12">
                        <div class="h3 tx-center">คุณกำลังยกเลิกการขายสินค้า</div>
                      </div>
                      <div class='col-md-12 mg-top-20'>
                        <div class="h4 tx-center tx-blue">ต้องการยกเลิกการขายหรือไม่ ?</div>
                      </div>

                    </div>
                </div>
                </div>
                <!--end modal-body -->
                <div class="modal-footer mg-top-20" style="text-align:center;">
                    <div class="col-sm-6 col-xs-12 padd-t-20">
                        <button class="btn-modal-cancel pull-left" data-dismiss="modal">
                        <span>ไม่ยกเลิก</span>
                        </button>
                    </div>
                    <div class="col-sm-6 col-xs-12 padd-t-20">
                        <button class="btn-modal-confirm pull-left" >
                        <span>ยืนยันยกเลิก</span>
                        </button>
                    </div>
                </div>
            </form>
      </div>
    </div>
</div>



<div class="modal fade" id="modal_images" role="dialog">
<div class="modal-dialog modal-w400">
    <div class="modal-content">
        <form class="bs-example form-horizontal"> 
            <div class="modal-body">
            <div class="panel-body panel-reserv">        
                <center><img src="" class="images-thumbnail img-responsive" id="tag-img"></center>
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <a class="btn btn-delete pull-left"style="width:100%;">      
                    <span>DELETE</span>
                    </a>
                </div>
                <div class="col-sm-6 col-xs-12 padd-t-20">
                    <a class="btn-cover2 btn pull-left" style="width:100%;">      
                    <span>SET AS COVER</span>
                    </a>                
                </div>  
            </div>
            </div>            
           
        </form>
  </div>
</div>
</div>