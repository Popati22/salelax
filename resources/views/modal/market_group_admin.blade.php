<div class="modal fade" id="addAdmin" role="dialog">
    <div class="modal-dialog" style="width:800px;">
    <div class="modal-content" style="background-color:#0095DE;">
            <div class="modal-header">
                <div class="row">
                    <div class="text-center" style="width:50%;float:left;">
                        <h3 class="modal-title bold" style="color:white;">ADMINISTRATOR</h3>
                    </div>
                    <div class="text-center" style="width:50%;float:right;">
                        <button class="pink-capsule-white-font">Cancel</button>
                        <button class="yellow-capsule-blue-font-admin ">Save</button>
                    </div>
                </div>
            </div>
            <div class="modal-body" style="background-color:white;">
                <div class="row no-margin" >
                    <div style="width:50%;float:left;"  >
                        <div class="member-list  mg-top-10">
                            <div style="width:100px;"><a href="#" class="text-center"><img class="img-responsive" style="height:40px;width:40px;border-radius:50%;margin-left: 10px;" src="{{asset('image/shop/im_member1.png')}}" /></a></div>
                            <div style="padding-top:10px;width:150px;" class="grey-font">Barr Lovery</div>  
                            <div style="width:200px;">
                                <img class="admin-icon-cover" style="width:50px !important; margin-left:95px;" src="{{asset('image/app/admin_icon.png')}}" >
                            </div>                       
                        </div>
                        <div class="member-list  mg-top-10">
                            <div style="width:100px;"><a href="#" class="text-center"><img class="img-responsive" style="height:40px;width:40px;border-radius:50%;margin-left: 10px;" src="{{asset('image/shop/im_member2.png')}}" /></a></div>
                            <div style="padding-top:10px;width:155px;" class="grey-font">Forda ryatana</div>  
                            <div style="width:200px;">
                                <img class="admin-icon-cover" style="width: 35px !important;margin-left: 100px;margin-top: 0px;" src="{{asset('image/app/minus_icon.png')}}" >
                            </div>                       
                        </div>
                        <div class="member-list  mg-top-10">
                            <div style="width:100px;"><a href="#" class="text-center"><img class="img-responsive" style="height:40px;width:40px;border-radius:50%;margin-left: 10px;" src="{{asset('image/shop/im_member3.png')}}" /></a></div>
                            <div style="padding-top:10px;width:155px;" class="grey-font">Loveryata</div>  
                            <div style="width:200px;">
                                <img class="admin-icon-cover" style="width: 35px !important;margin-left: 100px;margin-top: 0px;" src="{{asset('image/app/minus_icon.png')}}" >
                            </div>                       
                        </div>
                        <!--select -->
                        <div style="background-color:#DEDDDD;">
                            <div class="member-list  mg-top-10" style="padding-top:10px;">
                                <div style="width:100px;"><a href="#" class="text-center"><img class="img-responsive" style="height:40px;width:40px;border-radius:50%;margin-left: 10px;" src="{{asset('image/shop/im_member4.png')}}" /></a></div>
                                <div style="padding-top:10px;width:155px;" class="blue-font">Rya</div>  
                                <div style="width:200px;">
                                    
                                </div>                       
                            </div>
                        </div>

                    </div>
                    <div style="width:50%;float:right">
                        <div class="col-xs-12  text-left" >
                            <button class="grey-capsule" style="float:left;">
                                <input type="text" placeholder="Search" class="search-input" value="Rya">
                                <span class="glyphicon glyphicon-search search-icon" ></span>
                            </button>
                        </div>
                        <div class="col-xs-12  text-left mg-top-40" >
                            <div  class="grey-capsule">
                                <div style="width:100px;"><a href="#" class="text-center"><img class="img-responsive" style="height:40px;width:40px;border-radius:50%;" src="{{asset('image/shop/im_member4.png')}}" /></a></div>
                                <div style="padding-top:10px;width:155px;" class="grey-font"><span class="blue-font">Rya</span></div>  
                                <div style="width:200px;">
                                    <a href="javascript:void(0);">
                                        <img class="admin-icon-cover" style="width: 35px !important;margin-left: 100px;margin-top: 0px;" src="{{asset('image/app/green_circle.png')}}" >
                                    </a>   
                                </div>  
                            </div>

                            <div  class="grey-capsule mg-top-10">
                                <div style="width:100px;"><a href="#" class="text-center"><img class="img-responsive" style="height:40px;width:40px;border-radius:50%;" src="{{asset('image/shop/im_member5.png')}}" /></a></div>
                                <div style="padding-top:10px;width:155px;" class="grey-font">Love<span class="blue-font">rya</span>ta</div>  
                                <div style="width:200px;">
                                    <a href="javascript:void(0);">
                                        <img class="admin-icon-cover" style="width: 35px !important;margin-left: 100px;margin-top: 0px;" src="{{asset('image/app/grey_circle.png')}}" >
                                    </a>   
                                </div>  
                            </div>

                            <div  class="grey-capsule mg-top-10">
                                <div style="width:100px;"><a href="#" class="text-center"><img class="img-responsive" style="height:40px;width:40px;border-radius:50%;" src="{{asset('image/shop/im_member6.png')}}" /></a></div>
                                <div style="padding-top:10px;width:155px;" class="grey-font">Love<span class="blue-font">rya</span>ta</div>  
                                <div style="width:200px;">
                                    <a href="javascript:void(0);">
                                        <img class="admin-icon-cover" style="width: 35px !important;margin-left: 100px;margin-top: 0px;" src="{{asset('image/app/grey_circle.png')}}" >
                                    </a>   
                                </div>  
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
            <!--end modal-body -->
            <div class="modal-footer leave-group-footer" style="text-align:center;background-color:white;border-bottom-left-radius: 15px;;border-bottom-right-radius: 15px;">
                
            </div>
           
        </div>
    </div>
</div>
















