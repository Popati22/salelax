
<div class="modal fade" id="ratingModal" role="dialog">
    <div class="modal-dialog" style="width:350px;">
      <div class="modal-content">
            <div class="modal-header text-center grey-border">
                <h4 class="modal-title blue-font bold">RATING GIVING</h4>
            </div>
            <div class="modal-body mg-top-20">
                <div class="row text-center">
                    <div class="col-xs-2"></div>
                        <div class="col-xs-7">
                            <div class="rateYo1"  ></div>
                        </div>
                        <div class="col-xs-2"></div>
                </div>
                <div class="row text-center mg-top-20">
                    <div class="col-xs-6 pull-left grey-font">ไม่ประทับใจ</div>
                    <div class="col-xs-6 pull-right blue-font">ประทับใจ</div>
                </div>
            </div>
            <!--end modal-body -->
            <div class="modal-footer" style="text-align:center;">
                <button class="modal-join-btn" style="margin-top:20px;" >
                    <span class="rating-next">NEXT</span>
                </button>
            </div>
        
      </div>
    </div>
  </div>

  <div class="modal fade" id="experienceModal" role="dialog">
    <div class="modal-dialog" style="width:350px;">
      <div class="modal-content">
            <div class="modal-header text-center grey-border">
                <h4 class="modal-title blue-font bold">RATE YOUR EXPERIENCE</h4>
            </div>
            <div class="modal-body mg-top-20">
                <div class="row text-center">
                    <div class="col-xs-2"></div>
                        <div class="col-xs-7">
                            <div class="rateYoExp"></div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                <div class="row text-center mg-top-20">
                    <div class="col-xs-6 pull-left grey-font">ไม่ชอบ</div>
                    <div class="col-xs-6 pull-right blue-font">ชอบ</div>
                </div>
            </div>
            <!--end modal-body -->
            <div class="modal-footer" style="text-align:center;">
                <button class="modal-join-btn" style="margin-top:20px;" >
                    <span class="rating-done">DONE</span>
                </button>
            </div>
        
      </div>
    </div>
  </div>
