<!DOCTYPE html>
<html lang="en" xmlns:og='http://ogp.me/ns#'>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">



  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Salelax</title> <!-- change this title for each page -->

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
    
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/venobox.css')}}"/>
    <link rel="stylesheet" href="{{ asset('css/flexslider.css')}}"/>
    <link href="{{ asset('css/style.css?v=24') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css" />

    {{--<script src="{{ asset('/js/jquery.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/js/jquery-ui-1.11.4.custom.js?v=5') }}"></script>--}}

    {{--<link rel="stylesheet" href="{{ asset('/css/jquery-ui.css') }}">--}}
    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <!-- scrollbar market-->
     <link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css" />
    <link href="{{ asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css"> -->
    <link rel="stylesheet"  href="{{ asset('css/jquery.rateyo.css') }}" >

    <!-- css images -->
    <link href="{{ asset('css/bootstrap-imageupload.css') }}" rel="stylesheet">

    {{--select2--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

    {{--shop item--}}
    <style>
      @media(max-width:764px){
        .large-sort {
          display:none;
        }
        .medium-sort {
          display:none;
        }
        .small-sort {
          display:inline;
        }
      }
      @media(min-width:764px){
        .large-sort {
          display:none;
        }
        .medium-sort {
          display:inline;
        }
        .small-sort {
          display:none;
        }
      }
      @media(min-width:992px){
        .large-sort {
          display:inline;
        }
        .medium-sort {
          display:none;
        }
        .small-sort {
          display:none;
        }
      }

      .v-select .selected-tag{
        height: 34px !important;
        margin: 4px 1px 0 3px !important;
        padding: 4px 4px 4px 1em !important;
      }
    </style>

    @yield('style')


  </head>

  <body>

      @include('layout/header')
      <div id="app">
          @yield('content')
      </div>

      <div class="mg-top-40"></div>

      @include('layout/footer')
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->

      {{--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>--}}

      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

      <script src="{{ asset('slick/slick.js') }}"></script>

      <!-- rating -->
      <script type="text/javascript" src="{{ asset('/js/starrr.js') }}"></script>

      <!-- gallery slider -->
      <script src="{{ asset('js/jquery.flexslider.js') }}"></script>

      <script src="https://unpkg.com/simplebar@latest/dist/simplebar.js"></script>
      <!-- Datepicker -->
      <script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
      {{--<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>--}}
      <script type="text/javascript" src="{{ asset('js/venobox.js') }}"></script>


      <!-- grid layout -->
      <!-- <script type="text/javascript" src="{{ asset('/js/masonry.rap-datetimepicker.js') }}"></script> -->
      <script type="text/javascript" src="{{ asset('js/venobox.js') }}"></script>


      <!-- grid layout -->
      <script type="text/javascript" src="{{ asset('/js/masonry.pkgd.js') }}"></script>

      {{--select2--}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/i18n/th.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.js"></script>
      <script src="{{ asset('/js/moment.min.js?v=3') }}"></script>
      <script src="{{ asset('/js/moment-with-locales.min.js?v=2') }}"></script>
      <script src="{{ asset('/js/vue.min.js') }}"></script>
      <script src="{{ asset('/js/vue-resource.min.js') }}"></script>
      <script src="/vue/directive/vue-select.js?v=3"></script>
      <script src="/vue/directive/vue-validator.js"></script>

      <script type="text/x-template" id="select2-template">
        <select>
          <slot></slot>
        </select>
      </script>

      @yield('script')

      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script> -->
      <script src="{{ asset('/js/jquery.rateyo.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('/js/my-common.js?v=2') }}"></script>

      <!-- images  --> 
      <script src="{{ asset('/js/sell.js?v=2') }}"></script>
      <script src="{{ asset('/js/jquery.cropit.js') }}"></script>
      <script src="{{ asset('/js/bootstrap-imageupload.js') }}"></script>

  </body>



</html>