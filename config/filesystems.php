<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

//        popati04@gmail.com Popsoo30
        's3' => [
            'driver' => 's3',
            'key' => 'AKIAJTZHGH25VUPOZAKA',
            'secret' => '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',
            'region' => 'ap-southeast-1',
            'bucket' => 'salelax-web',
        ],

//        's3' => [
//            'driver' => 's3',
//            'key' => 'AKIAJWQKT77ANOR627WQ',
//            'secret' => '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',
//            'region' => 'ap-southeast-1',
//            'bucket' => 'salelax',
//        ],

//        S3
//Bucket : arn:aws:s3:::salelax
//User :  for-android
//           Access key ID :  AKIAJWQKT77ANOR627WQ
//Secret access key : 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


    ],

];
