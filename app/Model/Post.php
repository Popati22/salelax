<?php
/**
 * Copyright (c) 2561. By Nisachon.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table="post";

    public $timestamps = false;

}
