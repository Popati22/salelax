<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Cookie\Factory;
use GuzzleHttp\Client;

class LoginController extends Controller
{
    public function postLogin(Request $request){

        $dataRecive = $request->json()->all();

        $json['client_id']="2";
        $json['client_secret']="cTLrlzGZ3bM5EeXaCejj8K28Rp6mPA6NVF0wspwy";
        $json['password']=$dataRecive['password'];
        $json['username']=$dataRecive['username'];
        $json['grant_type']="password";

        $param = json_encode($json);

        $client = new Client(['defaults' => ['verify' => false]]);
        $headers = ['Content-Type' => 'application/json'];
        print_r($param);
        $res = $client->request('POST', 'http://128.199.205.186/oauth/token', ['headers'=>$headers,'body' => $param]);

        if($res->getStatusCode() == 200) {
            $userdata = $res->getBody();
            $userdata = json_decode($userdata);

            return redirect('/')->cookie('access_token', $userdata->token_type.' '.$userdata->access_token, $userdata->expires_in)
                ->cookie('refresh_token', $userdata->refresh_token, $userdata->expires_in);

        }
    }

    public function postRegister(Request $request){

        $dataRecive = $request->json()->all();

        $json['client_id']="2";
        $json['client_secret']="cTLrlzGZ3bM5EeXaCejj8K28Rp6mPA6NVF0wspwy";
        $json['email']=$dataRecive['email'];
        $json['password']=$dataRecive['password'];
        $json['c_password']=$dataRecive['password'];
        $json['telephone']=$dataRecive['telephone'];

        $param = json_encode($json);

        $client = new Client(['defaults' => ['verify' => false]]);
        $headers = ['Content-Type' => 'application/json'];
//        print_r($param);
        $res = $client->request('POST', 'http://128.199.205.186/api/v1/register', ['headers'=>$headers,'body' => $param]);

        if($res->getStatusCode() == 200) {
//            $userdata = $res->getBody();
//            $userdata = json_decode($userdata);
//            return $userdata;


//            $res2 = $client->request('POST', 'http://128.199.205.186/oauth/token', ['headers'=>$headers,'body' => $param]);
//
//            if($res2->getStatusCode() == 200) {
//                $userdata = $res->getBody();
//                $userdata = json_decode($userdata);
//
//                return redirect('/')->cookie('access_token', $userdata->token_type.' '.$userdata->access_token, $userdata->expires_in)
//                    ->cookie('refresh_token', $userdata->refresh_token, $userdata->expires_in);
//
//            }
        }
    }

    public function postLogout(){

        $cookie1 = \Cookie::forget('access_token');
        $cookie2 = \Cookie::forget('refresh_token');
        $cookie3 = \Cookie::forget('email');
        $cookie4 = \Cookie::forget('name');
        $cookie5 = \Cookie::forget('avatar');
        $cookie6 = \Cookie::forget('fb_id');
        $cookie7 = \Cookie::forget('token');

        return redirect('/login')->withCookie($cookie1)->withCookie($cookie2)->withCookie($cookie3)
            ->withCookie($cookie4)->withCookie($cookie5)->withCookie($cookie6)->withCookie($cookie7);
    }


}
