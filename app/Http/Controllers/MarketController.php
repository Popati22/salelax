<?php

namespace App\Http\Controllers;

use App\Model\Market;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Illuminate\Contracts\Cookie\Factory;
use Ixudra\Curl\Facades\Curl;
use App\Http\Controllers\MainController;

class MarketController extends Controller
{

    public function index(){
        $main = new MainController;
        $markets = $main->index('market');

//        return json_encode($markets->getData())->data;
        return view('pages/market/market',['markets'=>$markets]);

        return response()->json($userdata);
    }


}
