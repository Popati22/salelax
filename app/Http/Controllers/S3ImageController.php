<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use Storage;
use Carbon\Carbon;

class S3ImageController extends Controller
{


    /**
     * Create view file
     *
     * @return void
     */
    public function imageUpload()
    {
        return view('image-upload');
    }


    /**
     * Manage Post Request File
     *
     * @return void
     */
    public function imageUploadPost(Request $request,$folder)
    {
        $this->validate($request, [
            'image' => 'required|file',
//            'image' => 'required|file|max:2048',
        ]);

        $now = Carbon::now();
        $imageName = $now->timestamp.'.'.$request->image->getClientOriginalExtension();
        $image = $request->file('image');
        Storage::disk('s3')->put($folder.'/'.$imageName, file_get_contents($image), 'public');
        $urlImage = Storage::disk('s3')->url($folder.'/'.$imageName);

        return $urlImage;

        return back()
            ->with('success','Image Uploaded successfully.')
            ->with('path',$imageName);
    }

    /**
     * Manage Post Request Image Base64
     *
     * @return void
     */
    public function base64UploadPost(Request $request)
    {

        $image = $request->image;
//            $imageName = time().'.'.$image->getClientOriginalExtension();
        $now = Carbon::now();
        $imageName = $now->timestamp.'.png';

        Storage::disk('s3')->put($request->folder.'/'.$imageName, file_get_contents($image), 'public');
        $urlImage = Storage::disk('s3')->url($request->folder.'/'.$imageName);


        return $urlImage;

    }
}
