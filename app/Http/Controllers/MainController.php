<?php

namespace App\Http\Controllers;

use App\Model\Market;
use App\Model\Products;
use App\Model\Post;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Illuminate\Contracts\Cookie\Factory;
use Ixudra\Curl\Facades\Curl;

class MainController extends Controller
{
    public function token(){
        return \Cookie::get('access_token');
    }

    public function index($name){
        $headers = [
            'Content-Type' => 'application/json',
            "Access-Control-Allow-Origin"=>"*",
            "authorization"=> \Cookie::get('access_token'),
        ];
        $client = new Client(['defaults' => ['verify' => false]]);

        $res = $client->request('GET', 'http://128.199.205.186/api/v1/'.$name, ['headers'=>$headers]);
        $userdata = $res->getBody();
        $userdata = json_decode($userdata);

        if($name == "profile"){
            if(isset($userdata->img_profile)){
                return response()->json($userdata)->cookie('profile.img_profile',$userdata->img_profile);

            }
        }


        return response()->json($userdata);
    }

    public function index2($name,$name2,$name3){
        $headers = [
            'Content-Type' => 'application/json',
            "Access-Control-Allow-Origin"=>"*",
            "authorization"=> \Cookie::get('access_token'),
        ];
        $client = new Client(['defaults' => ['verify' => false]]);

        $res = $client->request('GET', 'http://128.199.205.186/api/v1/'.$name.'/'.$name2.'/'.$name3, ['headers'=>$headers]);
        $userdata = $res->getBody();
        $userdata = json_decode($userdata);

        return response()->json($userdata);
    }


    public function call(Request $request){
//        $json = $request->json()->all();

        return response()->json([
            'headers' => $request->header(),
            'output' => $request->all()
        ]);
    }

    public function postMethod($name, Request $request){

        $s3Image = new S3ImageController;
//        return $name;
        $client = new Client(['base_uri' => 'http://128.199.205.186/api/v1/']);
//        $client = new Client(['base_uri' => '{{ config('app.url') }}api/call/']);

        if($name == "market"){
            $data = new Market;
            $data->image = $s3Image->imageUploadPost($request, 'market');
            $data->name = $request->name;
            $data->address = $request->address;
            $data->description = $request->description;
            $data->save();

//            $image = config('app.url').'/storage/'.substr($data->image,7);
            $data['image_link'] = $data->image;

        }

        if($name == "products"){
            $data = new Products;
            $data->name = $request->name;
            $data->quantity = $request->quantity;
            $data->description = $request->description;
            $data->save();

        }else if($name == "order"){
            $data['post_id'] = $request->post_id;
            $data['total_amount'] = $request->total_amount;
            $data['orderdetail'] = $request->orderdetail;
        }

        if($name == "post") {
            $data['category_id'] = $request->category_id;
            $data['pronoun_id'] = $request->pronoun_id;

            $data['name'] = $request->name;
            $data['title'] = $request->title;
            $data['description'] = $request->description;

            $data['item_condition'] = $request->item_condition;
            $data['location_type'] = $request->location_type;

            if($request->date_start != "" && $request->time_start != ""){
                $data['date_start'] = $request->date_start." ".$request->time_start.":00";
            }

            if($request->date_end != "" && $request->time_end != ""){
                $data['date_end'] = $request->date_end." ".$request->time_end.":00";
            }

            $data['image'] = $request->image;

            $data['product'] = array();
            if(is_array($request->product)){
                foreach ($request->product as $value){
                    array_push($data['product'], $value);

                }
            }

            $data['location'] = array();
            if (is_array($request->location) || is_object($request->location)) {
                foreach ($request->location as $value) {
                    array_push($data['location'], $value);
                }
            }
        }

        
        $res = $client->request('POST', $name,['headers'=> [
//            'content-type' => 'multipart/form-data',
//                "Access-Control-Allow-Origin"=>"*",
            "Authorization"=> \Cookie::get('access_token'),
            'Accept' => 'application/json',

        ], 'json'=> $data ]);

        $dataCallback = json_decode($res->getBody(),true);

        if($name == "post"){

//            $fb = new FacebookController;
//            $fb->postToGroup($request,$dataCallback);

            $data = new Post;
            $data->id = $dataCallback['id'];
            $data->title = $dataCallback['title'];
            $data->description = $dataCallback['description'];

            foreach ($request->image as $img){
                if($img['status'] == 1){
                    $data->image = $img['image_cover'];
                }
            }
            $data->save();
        }

        return response()->json([
            'before'=>$data,
            'after'=>$dataCallback
        ]);
    }

    public function postMethod2($name, $name2, $name3, Request $request){

        $urlApi = $name.'/'.$name2.'/'.$name3;
        $client = new Client(['base_uri' => 'http://128.199.205.186/api/v1/']);

        if($name == "buyer" && $name2 == "order" && $name3 == "cancel" ||
            $name == "seller" && $name2 == "order" && $name3 == "confirm" ||
            $name == "seller" && $name2 == "order" && $name3 == "confirmall"){

            if(isset($request->order_id)){
                $data['order_id'] = $request->order_id;
            }

            if(isset($request->post_id)){
                $data['post_id'] = $request->post_id;
            }
        }


        $res = $client->request('POST', $urlApi,['headers'=> [
            "Authorization"=> \Cookie::get('access_token'),
            'Accept' => 'application/json',

        ], 'json'=> $data ]);

        $dataCallback = json_decode($res->getBody(),true);


        return response()->json([
            'before'=>$data,
            'after'=>$dataCallback
        ]);
    }

    public function putMethod($name,$id, Request $request){

        $client = new Client(['base_uri' => 'http://128.199.205.186/api/v1/']);
//        $client = new Client(['base_uri' => '{{ config('app.url') }}api/call/']);


        if($name == "profile"){
            $data['display_name'] = $request->display_name;
            $data['telephone'] = $request->telephone;
            $data['line_id'] = $request->line_id;
            $data['img_cover'] = $request->img_cover;
            $data['img_profile'] = $request->img_profile;

        }

        $res = $client->request('PUT', $name.'/'.$id,['headers'=> [
//            'content-type' => 'multipart/form-data',
//                "Access-Control-Allow-Origin"=>"*",
            "Authorization"=> \Cookie::get('access_token'),
            'Accept' => 'application/json',

        ], 'json'=> $data ]);

        $dataCallback = json_decode($res->getBody(),true);


        return response()->json([
            'before'=>$data,
            'after'=>$dataCallback
        ]);
    }


    public function show($name,$id){
        $headers = [
            'Content-Type' => 'application/json',
            "Access-Control-Allow-Origin"=>"*",
            "authorization"=> \Cookie::get('access_token'),
        ];
        $client = new Client(['defaults' => ['verify' => false]]);

        $res = $client->request('GET', 'http://128.199.205.186/api/v1/'.$name.'/'.$id, ['headers'=>$headers]);
        $userdata = $res->getBody();
        $userdata = json_decode($userdata);

        if($name == 'post'){
            $data = Post::where('id',$userdata->id)->first();

            if($data == null){
                $data = new Post;
            }

            $data->id = $userdata->id;
            $data->title = $userdata->title;
            $data->description = $userdata->description;
            $data->image = $userdata->image_cover[0]->image_cover;
            $data->save();
        }

        return response()->json($userdata);
    }

    public function show_order($id){
        $data = Post::find($id);

        if($data != null){
            return view('pages/shop/shop_order')
                ->with('id',$id)
                ->with('title',$data->title)
                ->with('description',$data->description)
                ->with('image',$data->image);
        }else{
            $this->show('post',$id);

            $data = Post::find($id);
            return view('pages/shop/shop_order')
                ->with('id',$id)
                ->with('title',$data->title)
                ->with('description',$data->description)
                ->with('image',$data->image);

        }

    }

    public function deleteMethod($name){

        $client = new Client(['base_uri' => 'http://128.199.205.186/api/v1/']);

        for ($i=70 ; $i<80 ; $i++){
            $res = $client->request('DELETE', $name.'/'.$i ,['headers'=> [
                'Content-Type' => 'application/json',
                "Access-Control-Allow-Origin"=>"*",
                "authorization"=> 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU0NGY1MDEyYjFjYjgxZjM3Njg3OGU2MzgyOWY2NGI0MTYyMTM4YzYzMDA3ZjUwOWYxODg5NzcyNmY1ZDY5MGY3ODZhYjY0NDZjYjUxOWU0In0.eyJhdWQiOiIyIiwianRpIjoiNTQ0ZjUwMTJiMWNiODFmMzc2ODc4ZTYzODI5ZjY0YjQxNjIxMzhjNjMwMDdmNTA5ZjE4ODk3NzI2ZjVkNjkwZjc4NmFiNjQ0NmNiNTE5ZTQiLCJpYXQiOjE1MTA4MTA5NDYsIm5iZiI6MTUxMDgxMDk0NiwiZXhwIjoxNTQyMzQ2OTQ2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.fASUWrzugluoEzgLy7YLKuEHHPgM8-9bOa9Rrr1KCNZfFWixQ_9x2rtLyZmExMsCDyIF51IlT6HP_4-N7eW4XONiUKmbeJCtc0rEOlMlvw1_hqo-D8ECk-g0qhe4iBLzSPtGnKiyHtXekGVIjx0VT5zUhrHSV_pRzehWdI3KOhaEZl9uYfW16bZYGnX3PZUYHxvM1ooPxmyTwuGqMT0DKGZfUCIS4bLbFe1e_93JvT1Ki2zLUlGmKGCSjJ2EffPJjetszJsvKL6RhvkZZ0hZg6yqDtUh0lJ-T8V1vEiEmWVWF9mKZwd6yENvgm3bCgVgJ6rV-mHZROxomSB20IzvpD9GSuUACtvG32W8mZe08IeTNgthuiN_Qj6oO9g6pwu0wWArixTzWRlTxhmqbG86Mt1kzMofUxw4F84tZ7FZ5dKdt9wC4Pqb5-fu4VhXMZEWfMyPjZ0FBelNYqpTgoOi8fQWFET_QBqARQdIim9v6XBOJSO7QiHU5IsHiwNOPDkVrCGc2UlyW-woUx5ELZIVh4W5kqv5KTvCXP2no7c-6ZHi67BOapU143qg-ydzPJM2qj9RKoJVW411xJChiO7sB6YbNs7Rj3CEmhnqEH6nF-jAPSKA16lgZ9lLfbSBvBqxmLTgzHnrwg56cb2YAw27U1dz9pReNk2jh0UUo-b-58o',

            ] ]);
        }


    }
}
