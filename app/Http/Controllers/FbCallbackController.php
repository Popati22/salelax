<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;

class FbCallbackController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
//        print_r($user);
//        return true;

        //login
        //update profile

        return redirect('/')->cookie('name',$user->name)
            ->cookie('fb_id',$user->id)
            ->cookie('email',$user->email)
            ->cookie('avatar',$user->avatar)
            ->cookie('token',$user->token);
//            ->cookie('data',$user);

    }
}
