<?php
/**
 * Copyright (c) 2561. By Nisachon.
 */

namespace App\Http\Controllers;

use App\Model\GalleryAlbum;
use App\Model\GalleryEvent;
use App\Model\GalleryPhotos;
use App\Model\GalleryPhotosComments;
use App\Model\GalleryPhotosReaction;
use App\Model\MemberUploadFile;
use App\Model\Topic;
use App\Model\TopicComments;
use App\Model\TopicLikes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Facebook\FacebookSession;
use Session;
use Facebook\FacebookRequest;
use Facebook\GraphObject;
use Facebook\FacebookRequestException;
use DB;
use Illuminate\Support\Facades\Log;

class FacebookController extends Controller
{

    public function me(){
        $fb = new \Facebook\Facebook([
            'app_id' => '283301778844117',
            'app_secret' => 'd2f06cfb56d42807a1e857e4904ffd93',
            'default_graph_version' => 'v2.10',
            'default_access_token' => 'EAAEBqVXkadUBACU4sWNZAwZAOfK9zaJ8eSb6lNV4K7dncPdlPNF8W8RmfiQ0NTxOMXPhhEtOOTyJO5Arld0ZBI1jiPkN9iPLYPQh4dAAuNCY408KUNhPM0tyahnIlX9at1aL7nbOCb83HhnVawZADT4zxnT75ZAQZD', // optional
        ]);

// Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
//   $helper = $fb->getRedirectLoginHelper();
//   $helper = $fb->getJavaScriptHelper();
//   $helper = $fb->getCanvasHelper();
//   $helper = $fb->getPageTabHelper();

        try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $fb->get('/me', 'EAAEBqVXkadUBACU4sWNZAwZAOfK9zaJ8eSb6lNV4K7dncPdlPNF8W8RmfiQ0NTxOMXPhhEtOOTyJO5Arld0ZBI1jiPkN9iPLYPQh4dAAuNCY408KUNhPM0tyahnIlX9at1aL7nbOCb83HhnVawZADT4zxnT75ZAQZD ');
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $me = $response->getGraphUser();
        echo 'Logged in as ' . $me->getName();
    }


    public function getGroups(Request $request)
    {
//        return $request->cookie('fb_id');

        $fb = new \Facebook\Facebook([
            'app_id' => '283301778844117',
            'app_secret' => 'd2f06cfb56d42807a1e857e4904ffd93',
            'default_graph_version' => 'v2.10',
        ]);

        try {
            $response = $fb->get('/'.$request->cookie('fb_id').'/groups', $request->cookie('token') );
            $body = json_decode($response->getBody(), true);//groups

            return $body;
//            var_dump($body);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }

    public function postToGroup(Request $request, $fromApi){
        try {
            // Returns a `Facebook\FacebookResponse` object
            $access_token = $request->cookie('token');

            $fb = new \Facebook\Facebook([
                'app_id' => '283301778844117',
                'app_secret' => 'd2f06cfb56d42807a1e857e4904ffd93',
                'default_graph_version' => 'v2.12',
            ]);

            foreach($request->groups as $group){

                $linkData = [
                    'link' => config('app.url')."/shop/order/".$fromApi['id'],
                    'message' => $request->name." ".$request->description,
                ];

//                Log::info('postToGroup $linkData=',$linkData);

                $response = $fb->post(
                    $group['id'].'/feed',$linkData,$access_token
                );
            }

        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            $response = 'Graph returned an error: ' . $e->getMessage();
            Log::info('FacebookResponseException : ',$response);

            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            $response = 'Facebook SDK returned an error: ' . $e->getMessage();
            Log::info('FacebookSDKException : ',$response);

            exit;
        }
        $graphNode = $response->getGraphNode();
//        Log::info('postToGroup $graphNode=',$graphNode);

    }


//    public function postToPage(Request $request, $header, $url, $topic_id)
//    {//1501488333235808
//
//        $fb = new \Facebook\Facebook([
//            'app_id' => '283301778844117',
//            'app_secret' => 'd2f06cfb56d42807a1e857e4904ffd93',
//            'default_graph_version' => 'v2.12',
//        ]);
//
//        $linkData = [
//            'link' => $url,
//            'message' => $header . ' ' . $url,
//        ];
//        try {
//            $topic = Topic::find($topic_id);
//            if($topic->fb_post_id == null){
//                if ($request->session()->has('fb_access_token')) {
//                    $fb->setDefaultAccessToken(Session::get('fb_access_token'));
//
//                    $access_token = $request->session()->get('fb_access_token');
//                    $response_fb = $fb->post('/1501488333235808/feed', $linkData, $access_token);
//
//                    $graphNode = $response_fb->getGraphNode();
//
//                    Topic::find($topic_id)->update(['fb_post_id' => $graphNode['id']]);
//                    echo 'Posted with id: ' . $graphNode['id'];
//                    $response['post_id'] = $graphNode['id'];
//                    $response['code'] = 200;
//
//                } else {
//                    $response['code'] = 501;
//                }
//            }else{
//                $response['code'] = 200;
//            }
//
//        }
//        catch (Facebook\Exceptions\FacebookResponseException $e) {
//            echo 'Graph returned an error: ' . $e->getMessage();
//            exit;
//        }
//        catch (Facebook\Exceptions\FacebookSDKException $e) {
//            echo 'Facebook SDK returned an error: ' . $e->getMessage();
//            exit;
//        }
//
//
//        return $response;
//    }

}