<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\MainController;

Route::post('/test/storage', function (\Illuminate\Http\Request $request) {
    $fileContents = file_get_contents($request->image);
    Storage::disk('s3')->put('avatars/1', $fileContents);
});

Route::get('s3-image-upload','S3ImageController@imageUpload');
Route::post('s3-image-upload','S3ImageController@imageUploadPost');
Route::post('s3-base64-upload','S3ImageController@base64UploadPost');

Route::get('login', function () {  return view('pages/login/login'); });
Route::post('login','LoginController@postLogin');

Route::get('logout','LoginController@postLogout');

Route::get('/register', function () { return view('pages/login/register'); });
Route::post('/register', 'LoginController@postRegister');

Route::get('/reset/email', function () {
    return view('pages/login/reset_password');
});

Route::get('/', function () {
    return view('pages/market/market');
})->name('market');

Route::get('market', 'MarketController@index')->name('market');


Route::group(['middleware' => ['web','auth.api']], function () {
    //
    Route::get('scanAround', function () {
        return view('pages/market/market_scan_around');
    });

    Route::get('/group', function () {
        return view('pages/market/market_group_inside');
    })->name('market');

    Route::get('/group/admin', function () {
        return view('pages/market/market_group_inside_admin');
    })->name('market');

    Route::get('recommend', function () {
        return view('pages/market/market_suggest');
    });

    Route::get('/basket', function () {
        return view('pages/reserv/reserv');
    })->name('basket');


    Route::prefix('shop')->group(function (){
        Route::get('/', function () {  return view('pages/shop/shop'); })->name('shop');
        Route::get('select', function () {  return view('pages/shop/shop_reserve'); })->name('shop');
        Route::get('order', function () {  return view('pages/shop/shop_order'); })->name('shop');
        Route::get('order/{id}', 'MainController@show_order')->name('shop');

        Route::get('confirm', function () {  return view('pages/shop/shop_confirm'); })->name('shop');
        Route::get('seller', function () {  return view('pages/shop/shop_seller'); })->name('shop');
        Route::get('profile', function () {  return view('pages/shop/shop_seller_profile'); })->name('shop');
    });

    Route::get('/profile', function () {
        return view('pages/profile');
    })->name('profile');

    Route::get('/history', function () {
        return view('pages/history/history');
    })->name('history');

    Route::get('/notification', function () {
        return view('pages/reserv/reserv');
    })->name('notification');

    Route::get('/sell', function () {
        return view('pages/sell/sell');
    })->name('sell');

    Route::get('/test', function () {
        return view('pages/test');
    })->name('shop');

    Route::get('/policy', function () {
        return view('pages/policy');
    });

    Route::get('/token', 'MainController@token');

    Route::get('/test', 'LoginController@test')->name('shop');

    Route::prefix('facebook')->group(function () {
        Route::get('login', 'FbCallbackController@redirectToProvider');
        Route::get('callback', 'FbCallbackController@handleProviderCallback');

    });

    Route::get('/storage/{folder}/{name}',function($folder, $name){
        try{
            $path = storage_path().'/app/'.$folder.'/'.$name;
            if (file_exists($path)) {
    //            return ($path);
    //            $url = Storage::url($folder.'/'.$name);
    //            return $url;
                return file_get_contents($path);
            }else{
                return 'no found file';
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    });

    Route::get('/map', function () {
        return view('pages/map');
    })->name('map');
});

Route::prefix('api')->group(function (){

    Route::get('/v1/{name}', 'MainController@index');
    Route::get('/v1/{name}/{name2}/{name3}', 'MainController@index2');

    Route::post('/v1/{name}', 'MainController@postMethod');
    Route::post('/v1/{name}/{name2}/{name3}', 'MainController@postMethod2');

    Route::put('/v1/{name}/{id}', 'MainController@putMethod');
    //    Route::delete('/v1/{name}', 'MainController@deleteMethod');

    Route::get('/v1/{name}/{id}', 'MainController@show');

    Route::post('/call/{name}', 'MainController@call');

    Route::prefix('fb')->group(function () {
        Route::get('groups', 'FacebookController@getGroups');
    });

});


// Route::get('about', function () {
//     return view('pages/about');
// });

// Route::get('contact', function () {
//     return view('pages/contact');
// });

// Route::get('footer', function () {
//     return view('layout/footer');
// });


