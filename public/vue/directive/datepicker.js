Vue.component('datepicker', {
props: ['value'],
template: '<input type="text" \
        ref="input" \
        v-bind:value="value" \
        v-on:input="$emit(\'input\', $event.target.value)"/>',

mounted: function () {
    // activate the plugin when the component is mounted.
    $(this.$el).datepicker({
        changeMonth: true,
        changeYear: true,
        yearOffSet: 543,
        buttonImageOnly: false,

        dateFormat: 'yy-mm-dd',
        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
        constrainInput: true,

        prevText: 'ก่อนหน้า',
        nextText: 'ถัดไป',
        yearRange: '-100:+1',
        onClose: this.onClose
    });

    // $(this.$el).datepicker("setDate", new Date() ); //Set ค่าวันปัจจุบัน
    // this.$emit('input', moment().add(543, 'years').format('YYYY-MM-DD'));

},
methods: {
    // callback for when the selector popup is closed.
    onClose : function(date) {
        // var year1 = date.substr(0,4);
        // var year2 = Number(year1)+543;
        // date = date.replace(year1, year2.toString());
        //
        // var obj = {
        //     'humen':moment(date).format('lll'),
        //     'com': date
        // }
        this.$emit('input', moment(date).format('YYYY-MM-DD') );
    }
},
watch: {
    // when the value fo the input is changed from the parent,
    // the value prop will update, and we pass that updated value to the plugin.
    value : function(newVal) { $(this.el).datepicker('setDate', newVal); }
}
});