Vue.component('showunit', {
    props:['unit_id'],
    template: '<span>{{ unit }}</span>',

    data: function () {
        return {
            units : [],
            unit:''
        }
    },
    mounted: function(){
        this.getunit();
    },
    methods: {
        getunit: function () {
            this.$http.get('/api/get_unit').then(function(myresponse) {
                this.units = myresponse.data;
                for(i=0; i<this.units.length ; i++){
                    if(this.units[i].id == this.unit_id){
                        this.unit = this.units[i].name;
                    }
                }
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        }
    }
});