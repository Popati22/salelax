Vue.component('flexslider', {
    props: ['lists'],
    data: function () {
        return {
            lists: this.lists,
        }
    },
    template: '' +
    '<div>' +
    '   <div class="flexslider order-image">\n' +
'            <ul class="slides col-xs-12">\n' +
'                <li v-for="img in lists">\n' +
'                    <a href="javascript:void(0);" class="click-venobox" ><img :src="img.image_cover" /></a>\n' +
'                </li>\n' +
'            </ul>\n' +
'        </div>\n' +
'\n' +
'        <div v-for="img in lists" style="display:none;" class="modal-venobox">\n' +
'            <a class="venobox"  data-gall="myGallery" :href="img.image_cover">\n' +
'                <img class="img-responsive goods-image-order" :src="img.image_cover">\n' +
'            </a>\n' +
'        </div>' +
    '</div>',
    watch: {
        'lists': function () {
            this.render();

        }
    },

    mounted: function () {
        this.render();

    },
    methods: {
        // callback for when the selector popup is closed.
        render: function(){
            $(this.$el).flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 370,
                itemMargin: 5,
                controlNav: false,
                directionNav: true,
                minItems: 1,
                maxItems: 3
            });
        }


    }
});