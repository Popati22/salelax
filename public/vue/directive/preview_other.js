Vue.component('preview-other', {
props: ['other'],
template:`
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <label v-if="other.questionnaire_id != false">{{ other.name }}</label>
                <label v-else>
                    <input class="form-control" v-model="other.name" placeholder="กรุณาใส่ชื่อผลไม้อื่นๆที่ไม่มีในระบบ">
                </label>
                <span style="position: absolute; right: 35px; margin-top: 0px;">
                    <button class="btn btn-warning btn-xs" v-if="other.questionnaire_id != false" v-on:click="other.questionnaire_id = false">แก้ไข</button>
                    <button class="btn btn-info btn-xs" v-if="other.questionnaire_id == false" v-on:click="edit_other(other)">บันทึก</button>
                    <button class="btn btn-danger btn-xs" v-if="other.questionnaire_id == false" v-on:click="ther.questionnaire_id = true">ยกเลิก</button>
                    <button class="btn btn-danger btn-xs" v-show="other.id > 0 && other.questionnaire_id != false" v-on:click="del_method(other)">ลบ</button>
                </span>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="form-group">
                    <div class="col-sm-3" v-if="other.questionnaire_id != false">
                        <h4 class="blue-color">ความถี่ในการบริโภคอาหาร</h4>

                        <div>
                            <label v-if="other.value1 == 2"> น้อยกว่า 1 ครั้ง/เดือน</label>
                        </div>
                        <div>
                            <label v-if="other.value1 == 3"> 1-3 ครั้ง/เดือน</label>
                        </div>
                        <div>
                            <label v-if="other.value1 == 4"> 1-3 ครั้ง/สัปดาห์</label>
                        </div>
                        <div>
                            <label v-if="other.value1 == 5"> 4-6 ครั้ง/สัปดาห์</label>
                        </div>
                        <div>
                            <label v-if="other.value1 == 6"> ทุกวัน</label>
                        </div>

                    </div>
                    <div class="col-sm-3" v-else>
                            <h4 class="blue-color">ความถี่ในการบริโภคอาหาร</h4>
                            <div>
                                <label>
                                    <input type="radio" checked="" value="2" v-model="other.value1"> น้อยกว่า 1 ครั้ง/เดือน
                                </label>
                            </div>
                            <div>
                                <label>
                                    <input type="radio" checked="" value="3" v-model="other.value1"> 1-3 ครั้ง/เดือน
                                </label>
                            </div>
                            <div>
                                <label>
                                    <input type="radio" checked="" value="4" v-model="other.value1"> 1-3 ครั้ง/สัปดาห์
                                </label>
                            </div>
                            <div>
                                <label>
                                    <input type="radio" checked="" value="5" v-model="other.value1"> 4-6 ครั้ง/สัปดาห์
                                </label>
                            </div>
                            <div>
                                <label>
                                    <input type="radio" checked="" value="6" v-model="other.value1"> ทุกวัน
                                </label>
                            </div>

                        </div>


                    <div class="col-sm-5">
                        <h4 class="blue-color" style="text-align: center;">สัดส่วนการบริโภค / ครั้ง</h4>

                        <div class="row" v-if="other.questionnaire_id != false">
                            <div class="col-xs-12" style="text-align: center;">
                                <label> {{ other.value2 }} {{ other.value2other }}</label>
                            </div>
                        </div>
                        <div class="row" v-else>
                            <div class="col-xs-6">
                                <input class="form-control" v-model="other.value2" placeholder="จำนวน" type="number">

                            </div>
                            <div class="col-xs-6">
                                <input class="form-control" v-model="other.value2other" placeholder="หน่วย" type="text">

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>`,

mounted: function () {

   
},
methods: {
    edit_other(other) {
        this.$http.put('/api/questionnaire_ans_other/'+other.id,{"other": other}).then(function(myresponse) {
            if(myresponse.data.errors == false){
                swal('แก้ไขสำเร็จ');
                other.questionnaire_id = true;
            }

        }, function(myresponse) {
            // error callback
            console.log(myresponse.data);
        });
    },
    del_method(other){

        swal({
            title: "คุณต้องการลบข้อมูลนี้จริงหรือไม่??",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false
        }, (callback) => {
            console.log(callback);

            this.$http.delete('/api/questionnaire_ans_other/'+other.id).then((myresponse) => {
                // location.reload();
                other.questions_id = 0;
            }, (myresponse) => {
                // error callback
                swal('ไม่สามารถลบได้ เนื่องจากมีข้อมูลอื่นใช้งานข้อมูลนี้อยู่');
                console.log(myresponse.data);
            });

            swal.close();
        });
    },
    edit_method(){
        window.location.href=this.link_edit;
    },
    zoom_img(img){
        var modal = $('#myModalImg');
        var modalImg = $("#img01");
        var captionText = $("#caption");

        modal.css("display", "block");
        modalImg.attr("src", img);
        captionText.innerHTML = this.alt;
    }
},
    watch: {
        // callback for when the selector popup is closed.
        value1 : function() {
            this.$emit('value1', this.value1);
        },
        value2 : function() {
            this.$emit('value2', this.value2);
        },
    },
});