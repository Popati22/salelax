/*
 * Copyright (c) 2561. By Nisachon.
 */

Vue.component('sell_waiting', {
    template:`
<div>
    <div v-for="wait in waites">
        <div style="margin-top:40px;">
            <div class="col-xs-1">
                <img src="/image/sell/icon-list.png" 
                  class="images-thumbnail img-responsive btn-total" style="width:100%; position: relative; z-index: 999;">
            </div>
        
            <div class="col-xs-6">
            <button class="btn-reserv btn-sell-detail padding-10" style="width:100%;">      
                <span class="mg-left-15 h3 tx-weight2" 
                style="position: relative; z-index: 999;">{{ wait.title }}</span>
                <span class="pull-right glyphicon glyphicon-menu-right"></span>
            </button>
            </div>
        
            <div class="col-xs-5">
            <button class="btn-md-blue-w tx-center " style="width:100%; position: relative; z-index: 999;">
                <span class="mg-right-10">ส่งสินค้าแล้วทั้งหมด</span>
            </button>
            </div>
        </div>
        
        
        <div class="col-xs-12 mg-bottom-20 border-blue" style="margin-top:-40px;">
            <sell_waiting_list :orders="wait.order"></sell_waiting_list>
        </div>
    </div>
</div>
    `,

    data: function () {
        return {
            waites: [],

            errors : []
        }
    },
    mounted: function () {

        this.getData();
    },
    methods: {
        getData(){
            this.$http.get('/api/v1/seller/order/waiting').then((myresponse) => {
                this.waites = myresponse.data;
        }, (myresponse) => {
                // error callback
                console.log(myresponse.data);
            });
        },
    }
});