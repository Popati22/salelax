Vue.filter('formatDateThai', function(value) {
    if (value) {
        var year1 = value.substr(0,4);
        var year2 = Number(year1)+543;
        value = value.replace(year1, year2.toString());
        return moment(value).format('lll');//+' '+value.substr(11,6)
    }
})
Vue.filter('money', function(value) {
    if (value) {
        let val = (value/1).toFixed(2)
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
})