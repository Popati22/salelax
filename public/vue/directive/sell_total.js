/*
 * Copyright (c) 2561. By Nisachon.
 */

Vue.component('sell_total', {
    template:`
<div>
    <div v-for="post in posts">
        <div style="margin-top:40px;">
    
            <div class="col-xs-1">
                <img src="/image/sell/icon-list.png" 
                  class="images-thumbnail img-responsive btn-total" style="width:100%; position: relative; z-index: 999;">
            </div>
            
            <div class="col-xs-11">
                <button class="btn-reserv  btn-sell-detail padding-10" style="width:80%;">      
                    <span class="mg-left-15 h3 tx-weight2" 
                     style="position: relative; z-index: 999;" >
                     {{ post.title }}</span>
                    <span class="pull-right glyphicon glyphicon-menu-right"></span>
                </button>
            </div>
            
        </div>
        
        <div class="col-xs-12 mg-bottom-20 border-blue" style="margin-top:-40px;">
        
            <div class="row box-wait-body">
                <div class="col-xs-12 col-xs-12 box-wait " style="margin-bottom:50px;" v-for="order in post.order">
                    <table class="table table-normal mg-top-20">
                    <tbody class="table-body-sell">
                    
                      <tr class="h4 tx-gray" v-for="orderdetail in order.orderdetail">
                        <td class="tx-left" >{{ orderdetail.name }}</td>
                        <td class="tx-right">{{ orderdetail.quantity }} ชิ้น</td>
                      </tr>
                     
                     <tr class="border-bt-gray">
                       <td class="tx-left h4 tx-gray" >รวมเงิน</td>
                       <td class="tx-right h2 tx-black" colspan="2">{{ order.total_amount }} บาท</td>
                     </tr>           
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>
    `,

    data: function () {
        return {
            posts: [],

            errors : []
        }
    },
    mounted: function () {

        this.getData();
    },
    methods: {
        getData(){
            this.$http.get('/api/v1/seller/order/summary').then((myresponse) => {
                this.posts = myresponse.data;
        }, (myresponse) => {
                // error callback
                console.log(myresponse.data);
            });
        },
    }
});