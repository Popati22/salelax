Vue.filter('formatDateThai', function(value) {
    if (value) {
        var year1 = value.substr(0,4);
        var year2 = Number(year1)+543;
        value = value.replace(year1, year2.toString());
        return moment(value).format('lll');//+' '+value.substr(11,6)
    }
})
Vue.filter('money', function(value) {
    if (value) {
        let val = (value/1).toFixed(2)
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
})

var com = Vue.component('data-viewer-col', {
    props: ['source', 'title','linknew', 'linkedit', 'linkdel', 'candel', 'canedit', 'dataset', 'cannew', 'canview','show_key_search','type','submit','columsearch'],
    template : `<div class="dv">
    <div class="dv-header">
        <div class="dv-header-title">
            {{title}}
        </div>

        <div class="dv-header-columns" v-if="show_key_search">
            <span class="dv-header-pre">Search: </span>
            <select class="dv-header-select" v-model="query.search_column">
                <option v-for="column in columns" :value="column">{{column}}</option>
            </select>
        </div>
        <div class="dv-header-operators" v-if="show_key_search">
            <select class="dv-header-select" v-model="query.search_operator">
                <option v-for="(value, key) in operators" :value="key">{{value}}</option>
            </select>
        </div>

        <div class="dv-header-search">
            
            <div class="row col-md-12" style="margin-top: 5px;">
            </div>
        </div>

        <div class="dv-header-submit" v-if="show_key_search">
            <button class="dv-header-btn"@click="fetchIndexData()">Filter</button>
        </div>
        <div class="dv-header-newcompnay" v-if="cannew">
            <button class="btn btn-warning btn-sm btn-block" @click="newdata()" >Add</button>
        </div>
    </div>

    <div class="dv-body">
        <table class="table table-bordered table-hover">

            <thead>
            <tr>
                <th style="width: 120px;">
                    <input class="form-control" v-model="displayname" @keyup="fetchIndexData()">
                </th>
                
                <th>
                    <input class="form-control"  v-model="email" @keyup="fetchIndexData()">
                </th>
                <th>
                    <select class="form-control" v-model="typeuser" @change="fetchIndexData()">
                        <option value="" selected>ทั้งหมด</option>
                        <option value="1">ผู้ดูแลระบบ</option>
                        <option value="2">เจ้าหน้าที่</option>
                        <option value="3">ทำให้ตัวเอง</option>
                    </select>
                </th>
                <th>
                    <select class="form-control" v-model="status" @change="fetchIndexData()">
                        <option value="" selected>ทั้งหมด</option>
                        <option value="1">เปิดใช้งาน</option>
                        <option value="0">ปิดใช้งาน</option>
                    </select>
                </th>
                <th style="width: 163px;">
                    <div class="input-group">
                        <datepicker class="form-control" v-model="created_at"></datepicker>
                        <span class="input-group-addon" style="cursor: pointer;" @click="clear_date">
                            <span class="fa fa-times"></span>
                        </span>
                    </div>
                </th>
                <th style="width: 180px;"></th>

            </tr>
            </thead>
            <tbody>
            <tr v-if="model.data.length > 0 ">
                <td v-for="column in show_col" style="text-align: center; font-weight: 900;">{{ column }}</td>
            </tr>
            <tr v-if="model.data.length == 0 ">
                <td :colspan="columns.length+1" style="text-align: center;padding: 150px;">ไม่พบข้อมูล</td>
            </tr>
            
            
                
            <tr v-for="row in model.data" v-else>
                <td v-for="( value, key) in row" v-if="key != 'edit' && key != 'id' && key != 'updated_at'  ">
                    
                    <div v-if="key == 'created_at'">
                        <span>{{  value | formatDateThai }}</span>
                    </div>
                    <div v-else-if="key == 'type' ">
                        <span v-if="row.edit">
                            <select class="form-control" v-model="row.type">
                                <option value="1">ผู้ดูแลระบบ</option>
                                <option value="2">เจ้าหน้าที่</option>
                                <option value="3">ทำให้ตัวเอง</option>
                            </select>
                        </span>
                        <span v-else>
                            <span v-if="value ==1">ผู้ดูแลระบบ</span>
                            <span v-else-if="value == 2">เจ้าหน้าที่</span>
                            <span v-else-if="value == 3">ทำแบบสอบถามให้ตัวเอง</span>
                        </span>
                    </div>
                    <div v-else-if="key == 'status' " style="width:80px">
                        <div class="switch" style="padding-top: 6px;">
                            <div class="onoffswitch">
                                <input type="checkbox" v-model="row.status" class="onoffswitch-checkbox" :id="row.id">
                                <label class="onoffswitch-label" :for="row.id" @click="change_status(row)">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div v-else>
                        <span>{{value}}</span>
                    </div>
                    
                </td>
                
                <td style="text-align: center;">
                    <div v-if="dataset=='adminview'">
                        <div class="btn-group" v-if="canview" style="margin-bottom: 3px;">
                            <button type="button" v-show="!row.edit" class="btn btn-success btn-outline btn-xs" v-on:click="edit_method(row)">แก้ไข</button>
                            
                            <button style="padding: 1px 10px;" type="button" v-show="!row.edit" class="btn btn-danger btn-outline btn-xs" v-if="candel" v-on:click="del_method(row.id)" >
                                ลบ
                            </button>
 
                            <button type="button" v-show="row.edit" class="btn btn-success btn-outline btn-xs" v-if="canedit" v-on:click="save_method(row)">
                                บันทึก
                            </button>
                            <button type="button" v-show="row.edit" class="btn btn-danger btn-outline btn-xs" v-if="canedit" v-on:click="cancle_method(row)">
                                ยกเลิก
                            </button>
                        </div>
                    </div>
                    

                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="dv-footer">
        <div class="dv-footer-item" style="float: left;">
            <span>Displaying {{model.from}} - {{model.to}} of {{model.total}} rows</span>
        </div>
        <div class="dv-footer-item" style="float: right;">
            <div class="dv-footer-sub">
                <span>Rows per page</span>
                <input type="text" v-model="query.per_page"
                       class="dv-footer-input"
                       @keyup.enter="fetchIndexData()">
            </div>
            <div class="dv-footer-sub">
                <button class="dv-footer-btn" @click="prev()">&laquo;</button>
                <input type="text" v-model="query.page"
                       class="dv-footer-input"
                       @keyup.enter="fetchIndexData()">
                <button class="dv-footer-btn" @click="next()">&raquo;</button>
            </div>
        </div>
    </div>
</div>`,
    watch: {
        submit: function(val) {
            if(val > 0)
                this.fetchIndexData();
        },
        created_at : function (val) {
            this.fetchIndexData();
        }
    },
    mounted: function () {
        moment().locale('thai');

        var d = new Date();
        this.date_input = moment(d).format("DD/MM/YYYY")
    },
    data: function () {
        return {
            model: {},
            columns: {},
            show_col: {},
            status: '',
            displayname: '',
            email: '',
            typeuser: '',
            zone: '',
            created_at: '',
            query: {
                page: 1,
                column: 'id',
                direction: 'desc',
                per_page: 50,
                search_column: 'id',
                search_operator: 'equal',
                search_input: '',
            },
            operators: {
                equal: '=',
                not_equal: '<>',
                less_than: '<',
                greater_than: '>',
                less_than_or_equal_to: '<=',
                greater_than_or_equal_to: '>=',
                in: 'IN',
                like: 'LIKE'
            },
            zones : [],
            hospitals : [],
            list_sp : [],
            submit : false,
            errors : []
        }
    },

    created() {
        this.fetchIndexData();
    },
    filters: {
        moment: function (date) {
            return moment(date).format('DD MMM YYYY, h:mm a');
        }
    },
    methods: {

        moment: function () {
            return moment();
        },
        make(id,type){
            window.location.href='/quest/'+id;
        },
        edit_method(row){
            row.edit = true;
        },
        save_method(val){


        },

        cancle_method(row){
            row.edit = false;
        },
        clear_date(){
            this.created_at = '';
            this.fetchIndexData();
        },
        change_status(row){
            var data={
                "id": row.id
            }

            this.$http.post('/api/change_status',data).then((myresponse) => {
                this.fetchIndexData();
            }, (myresponse) => {
                console.log(myresponse.data);
            });

        },
        del_method(id){

            swal({
                title: "Are you sure remove this data?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false
            }, (callback) => {
                console.log(callback);

            this.$http.post(this.linkdel+''+id).then((myresponse) => {
                if(myresponse.data.status == "success"){
                this.fetchIndexData();
            }else{

                this.fetchIndexData();
            }
        }, (myresponse) => {
// error callback
                swal('ไม่สามารถลบได้ เนื่องจากมีข้อมูลอื่นใช้งานข้อมูลนี้อยู่');
                console.log(myresponse.data);
            });

            swal.close();
        });
        },
        newdata(){
            window.location.href=this.linknew;
        },
        next() {
            if(this.model.next_page_url) {
                this.query.page++
                this.fetchIndexData()
            }
        },
        prev() {
            if(this.model.prev_page_url) {
                this.query.page--
                this.fetchIndexData()
            }
        },
        toggleOrder(column) {
            if(column === this.query.column) {
// only change direction
                if(this.query.direction === 'desc') {
                    this.query.direction = 'asc'
                } else {
                    this.query.direction = 'desc'
                }
            } else {
                this.query.column = column
                this.query.direction = 'asc'
            }
            this.fetchIndexData()
        },
        fetchIndexData() {
            var date_search = "";
            if(this.created_at != ""){
                var year1 = this.created_at.substr(0,4);
                var year2 = Number(year1)-543;
                date_search = this.created_at.replace(year1, year2.toString());

            }

            var vm = this

            axios.get(`${this.source}?column=${this.query.column}&direction=${this.query.direction}&page=${this.query.page}&per_page=${this.query.per_page}&status=${this.status}&type=${this.typeuser}&created_at=${date_search}&displayname=${this.displayname}&email=${this.email}`)
                .then(function (response) {
                    var model = response.data.model;

                    for (i = 0; i < model.data.length; i++) {
                        model.data[i].edit = false;
                    }

                    Vue.set(vm.$data, 'model', model)
                    Vue.set(vm.$data, 'columns', response.data.columns)
                    Vue.set(vm.$data, 'show_col', response.data.show_col)
                })
                .catch(function (response) {
                    console.log(response)
                })
        }
    }
})