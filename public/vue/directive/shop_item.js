Vue.component('shop_item', {
    props: ['data','tags'],
    template:`
    <div class="item">
        <div class="item-hover" style="    margin-bottom: 25px;">
            <div class="seller text-left">
                <div class="row no-margin">
                
                    <div class="col-xs-12" style="padding: 0px;">
                         <a href="/shop/seller"  target="_blank">
                            <img class="mg-top-20" v-if="data.seller_image != null" :src="data.seller_image" >
                            <img class="mg-top-20" src="/image/default/user.png" v-else >
                            
                            <h5 style="display:inline-block; position: absolute; top: 6px;">{{data.seller_name}}</h5>
                         </a>

                    </div>

                </div>
            </div>
            <a style="color:unset;" :href="'/shop/order/'+data.id" target="_blank">
                <div class="goods-image">
                    <div class="buy-count-hover">
                        <div class="buy-count">5</div>
                        <img class="buy-heart" src="/image/app/heart.png">
                    </div>
                    
                    <img class="img-thumbnail" v-if="data.image_cover.length > 0" :src="data.image_cover[0].image_cover">
                    <img v-else src="{{ config('app.url') }}image/shop/im_sell4.png">
    
                    <button class="buy-count-hover buy-button">ซื้อ</button>
                </div>
                <div class="goods-total grey-border-bottom">
                    เหลือจำนวน <span class="orange-font margin-left-15">{{ data.amount-2 }}</span>/{{ data.amount }} 
                </div>
                
                <div class="goods-name">
                    <span>{{ data.name }}</span>
                    <span style="float:right;">{{ data.price }}</span> 
                
                </div>
                <div class="goods-desc text-left">
                    <span>{{ data.description }}</span>
                </div>
            </a>
        </div>
    
    </div>`,

    data: function () {
        return {
            model: {},
            columns: {},
            show_col: {},
            query: {
                page: 1,
                column: 'id',
                direction: 'desc',
                per_page: 15,
                search_column: 'id',
                search_operator: 'equal',
                search_input: '',
            },
            operators: {
                equal: '=',
                not_equal: '<>',
                less_than: '<',
                greater_than: '>',
                less_than_or_equal_to: '<=',
                greater_than_or_equal_to: '>=',
                in: 'IN',
                like: 'LIKE'
            },

            errors : []
        }
    },
    mounted: function () {

        // this.getData();


        //shop hover
        $('.item-hover').mouseover(function(){
            $(this).parent().addClass('buy');
            $(this).find('.buy-count-hover').show();
        });
        $('.item-hover').mouseleave(function(){
            $(this).parent().removeClass('buy');
            $(this).find('.buy-count-hover').hide();
        });

    },
    methods: {
        getData(){
            this.$http.get('/api/v1/product').then((myresponse) => {
                this.model = myresponse.data.data;
            }, (myresponse) => {
                // error callback
                console.log(myresponse.data);
            });
        },
        del_method(){

            swal({
                title: "Are you sure remove this data?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false
            }, (callback) => {
                console.log(callback);

                this.$http.delete(this.link_del).then((myresponse) => {
                    location.reload();
                }, (myresponse) => {
                    // error callback
                    swal('ไม่สามารถลบได้ เนื่องจากมีข้อมูลอื่นใช้งานข้อมูลนี้อยู่');
                    console.log(myresponse.data);
                });
                swal.close();
            });
        },
        edit_method(){
            window.location.href=this.link_edit;
        },
        zoom_img(img){
            var modal = $('#myModalImg');
            var modalImg = $("#img01");
            var captionText = $("#caption");

            modal.css("display", "block");
            modalImg.attr("src", img);
            captionText.innerHTML = this.alt;
        }
    },
    watch: {
        // callback for when the selector popup is closed.
        value1 : function() {
            this.$emit('value1', this.value1);
        },
        value2 : function() {
            this.$emit('value2', this.value2);
        },
    },
});