/*
 * Copyright (c) 2561. By Nisachon.
 */

Vue.component('timepicker', {
    props: ['value'],
    template: '<input type="text" \
            ref="input" \
            v-bind:value="value" \
            v-on:input="$emit(\'input\', $event.target.value)"/>',

    mounted: function () {
        // activate the plugin when the component is mounted.
        $(this.$el).timepicker({
            timeFormat: 'H:mm',
            interval: 30,
            startTime: '12:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true,
            change: this.onChange
        });

    },
    methods: {
        // callback for when the selector popup is closed.
        onChange : function(time) {
            var format = time.toTimeString().substr(0,5);

            this.$emit('input', format);
        }
    },
    watch: {
        // when the value fo the input is changed from the parent,
        // the value prop will update, and we pass that updated value to the plugin.
        value : function(newVal) { $(this.el).timepicker('setDate', newVal); }
    }
});