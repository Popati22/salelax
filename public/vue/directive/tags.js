Vue.component('list_tags', {
    props: ['tags'],
    template:`
            <v-select multiple label="name" :options="options"></v-select>
    `,

    data: function () {
        return {
            options: ['a','b','c'],

            model: {},
            columns: {},
            show_col: {},
            query: {
                page: 1,
                column: 'id',
                direction: 'desc',
                per_page: 15,
                search_column: 'id',
                search_operator: 'equal',
                search_input: '',
            },
            operators: {
                equal: '=',
                not_equal: '<>',
                less_than: '<',
                greater_than: '>',
                less_than_or_equal_to: '<=',
                greater_than_or_equal_to: '>=',
                in: 'IN',
                like: 'LIKE'
            },

            errors : []
        }
    },
    mounted: function () {

        // this.getData();


        //shop hover
        $('.item-hover').mouseover(function(){
            $(this).parent().addClass('buy');
            $(this).find('.buy-count-hover').show();
        });
        $('.item-hover').mouseleave(function(){
            $(this).parent().removeClass('buy');
            $(this).find('.buy-count-hover').hide();
        });

    },
    methods: {
        getData(){
            this.$http.get('/api/v1/product').then((myresponse) => {
                this.options = myresponse.data.data;
            }, (myresponse) => {
                // error callback
                console.log(myresponse.data);
            });
        },
        del_method(){

            swal({
                title: "Are you sure remove this data?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false
            }, (callback) => {
                console.log(callback);

                this.$http.delete(this.link_del).then((myresponse) => {
                    location.reload();
                }, (myresponse) => {
                    // error callback
                    swal('ไม่สามารถลบได้ เนื่องจากมีข้อมูลอื่นใช้งานข้อมูลนี้อยู่');
                    console.log(myresponse.data);
                });
                swal.close();
            });
        },
        edit_method(){
            window.location.href=this.link_edit;
        },
        zoom_img(img){
            var modal = $('#myModalImg');
            var modalImg = $("#img01");
            var captionText = $("#caption");

            modal.css("display", "block");
            modalImg.attr("src", img);
            captionText.innerHTML = this.alt;
        }
    },
    watch: {
        // callback for when the selector popup is closed.
        value1 : function() {
            this.$emit('value1', this.value1);
        },
        value2 : function() {
            this.$emit('value2', this.value2);
        },
    },
});