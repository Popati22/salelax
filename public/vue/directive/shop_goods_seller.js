/*
 * Copyright (c) 2561. By Nisachon.
 */

Vue.component('shop_goods_seller', {
    props: ['seller'],
    template:`
<div>
    <div class="row goods-seller no-margin">
        <div class="col-xs-9 text-left"> 
            ข้อมูลผู้ขาย
        </div>
        <div class="col-xs-3 text-right" role="group"> 
            <span class="icon-zag-ic-more" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer"></span>
                <ul class="dropdown-menu contact-dropdown grey-font contact-shop-order" >
                    <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/email.png"/></span><span class="mg-left-10">{{ seller.email }}</span></div></a></li>
                    <li><a href="javascrip:void(0);"><div><span class="text-left contact-icon"><img src="/image/app/telephone.png" style="width:26px;"/></span><span class="mg-left-20">{{ seller.telephone }}</div></a></li>
                </ul>
        </div>
    </div>
    
    <div class="row no-margin">
        <div class="col-xs-12 text-center" style="    margin-top: 1em;"> 
            <div class="row black-20">
                <div class="col-xs-4 goods-avatar text-left">
                   <a :href="'/shop/profile/'+seller.seller_id" target="_blank"> 
                        <img v-if="seller.seller_image != null" :src="seller.seller_image">
                        <img v-else src="/image/default/user.png">

                   </a>
                </div>
                <div class="col-xs-8 text-right">
                    <div class="row">
                        <span>NAME</span><span class="mg-left-25">: </span><span class="mg-left-25">{{ seller.seller_name }}</span>
                    </div>
                    <div class="row mg-top-15">
                        <img style="width:125px;" src="/image/app/rating.png" ><span class="mg-left-25">{{ seller.saller_rating }}</span>
                    </div>
                </div>
            </div>

        </div>
  
        <div class="col-xs-12 mg-top-30 text-center " > 
            
        </div>
    
    </div>
    
    
</div>    
    `,

    mounted: function () {


    },
    methods: {

    }
});