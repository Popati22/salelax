/*
 * Copyright (c) 2561. By Nisachon.
 */

Vue.component('shop_goods_time', {
    props: ['item_condition','location_type'],
    template:`
<div>
    <div class="row grey-20 mg-top-30">
        <div class="col-xs-6 col-md-4"><p class="text-left"></p></div>
        <!--<div class="col-xs-3 col-md-4"><p class="text-center mg-top-20">เหลือเวลา</p></div>-->
        <div class="col-xs-3 col-md-4"><p class="text-center mg-top-20">สภาพสินค้า</p></div>
    </div>
    <div class="row black-20">
        <div class="col-xs-6 col-md-4">
            <p class="text-center" v-if="location_type==1">ระบุจุดนัดรับสินค้าได้</p>
        </div>
        <!--<div class="col-xs-3 col-md-4"><p class="text-center">{{$time}}</p></div>-->
        <div class="col-xs-3 col-md-4"><p class="text-center">{{ item_condition.name }}</p></div>
    </div>
</div>
    `,

    mounted: function () {


    },
    methods: {

    }
});