var market = Vue.component('market', {
    props: ['data','tags'],
    template:`
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-6 text-center mg-top-15" style="padding-bottom: 15px;">
        <a href="/group"  class="marget-join-group" :id="'market-group-'+data.id">
            <div class="image">
                <img  class="recommend-image-group" :src="data.image_cover">
                <div class="recommend-market-group">
                    <span class="market-group-name">{{ data.name }}</span>
                    <span class="market-group-count-view">10 Members</span>
                    <input type="hidden" name="location" :value="data.distance">
                    <input type="hidden" name="create" :value="moment(data.created_at.date).calendar()">
                    <input type="hidden" name="description" :value="data.description">
                </div>
            </div>
        </a>
    </div>`,

    data: function () {
        return {
            model: {},
            columns: {},
            show_col: {},
            query: {
                page: 1,
                column: 'id',
                direction: 'desc',
                per_page: 15,
                search_column: 'id',
                search_operator: 'equal',
                search_input: '',
            },
            operators: {
                equal: '=',
                not_equal: '<>',
                less_than: '<',
                greater_than: '>',
                less_than_or_equal_to: '<=',
                greater_than_or_equal_to: '>=',
                in: 'IN',
                like: 'LIKE'
            },

            errors : []
        }
    },
    mounted: function () {

        //shop hover
        $('.item-hover').mouseover(function(){
            $(this).parent().addClass('buy');
            $(this).find('.buy-count-hover').show();
        });
        $('.item-hover').mouseleave(function(){
            $(this).parent().removeClass('buy');
            $(this).find('.buy-count-hover').hide();
        });

    },
    methods: {

        del_method(){

            swal({
                title: "Are you sure remove this data?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false
            }, (callback) => {
                console.log(callback);

                this.$http.delete(this.link_del).then((myresponse) => {
                    location.reload();
                }, (myresponse) => {
                    // error callback
                    swal('ไม่สามารถลบได้ เนื่องจากมีข้อมูลอื่นใช้งานข้อมูลนี้อยู่');
                    console.log(myresponse.data);
                });
                swal.close();
            });
        },
        edit_method(){
            window.location.href=this.link_edit;
        },
        zoom_img(img){
            var modal = $('#myModalImg');
            var modalImg = $("#img01");
            var captionText = $("#caption");

            modal.css("display", "block");
            modalImg.attr("src", img);
            captionText.innerHTML = this.alt;
        }
    },
    watch: {
        // callback for when the selector popup is closed.
        value1 : function() {
            this.$emit('value1', this.value1);
        },
        value2 : function() {
            this.$emit('value2', this.value2);
        },
    },
});