Vue.component('preview', {
props: ['name','example','question_id','type_ans','type_unit','value1','ratio','image','unit_select','unit_number','value1','value2','target','note','link_edit','link_del'],
template:`
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 >{{ name }} <small v-if="example">( {{ example}} )</small></h5>
                <span style="position: absolute; right: 35px; margin-top: -6px;">
                    <button class="btn btn-warning btn-xs" @click="edit_method()">แก้ไข</button>
                    <button class="btn btn-danger btn-xs" @click="del_method()">ลบ</button>
                </span>
            </div>
            <div class="ibox-content">
                <form method="get" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <h4 class="blue-color">ความถี่ในการบริโภคอาหาร</h4>
                            <div>
                                <label> 
                                    <input type="radio" checked="" name="freq" value="1" v-model="value1"> ไม่กินเลย
                                </label>
                            </div>
                            <div>
                                <label> 
                                    <input type="radio" checked="" name="freq" value="2" v-model="value1"> น้อยกว่า 1 ครั้ง/เดือน
                                </label>
                            </div>
                            <div>
                                <label> 
                                    <input type="radio" checked="" name="freq" value="3" v-model="value1"> 1-3 ครั้ง/เดือน
                                </label>
                            </div>
                            <div>
                                <label> 
                                    <input type="radio" checked="" name="freq" value="4" v-model="value1"> 1-3 ครั้ง/สัปดาห์
                                </label>
                            </div>
                            <div>
                                <label> 
                                    <input type="radio" checked="" name="freq" value="5" v-model="value1"> 4-6 ครั้ง/สัปดาห์
                                </label>
                            </div>
                            <div>
                                <label v-if="question_id!=55 && question_id!=31"> 
                                    <input type="radio" checked="" name="freq" value="6" v-model="value1"> 1 ครั้ง/วัน
                                </label>
                                <label v-else> 
                                    <input type="radio" checked="" name="freq" value="6" v-model="value1"> ทุกวัน
                                </label>
                            </div>
                            <div v-if="question_id!=55 && question_id!=31">
                                <label> 
                                    <input type="radio" checked="" name="freq" value="7" v-model="value1"> มากกว่า 1 ครั้ง/วัน
                                </label>
                            </div>
                        </div>
                        
                        <div class="col-sm-5">
                            <h4 class="blue-color" style="text-align: center;">สัดส่วนการบริโภค / ครั้ง</h4>
                            <div class="col-sm-12">
                                <span v-if="type_ans == 1">
                                    <select class="form-control" v-if="type_unit == 1" v-model="value2">
                                        <option value="0">น้อยกว่า 1 {{ unit_select }}</option>
                                        <option value="1">1 {{ unit_select }}</option>
                                        <option value="2">2 {{ unit_select }}</option>
                                        <option value="3">3 {{ unit_select }}</option>
                                        <option value="4">4 {{ unit_select }}</option>
                                        <option value="5">5 {{ unit_select }}</option>
                                        <option value="6">6 {{ unit_select }}</option>
                                        <option value="7">7 {{ unit_select }}</option>
                                        <option value="8">8 {{ unit_select }}</option>
                                        <option value="9">9 {{ unit_select }}</option>
                                    </select>
                                    
                                    <select class="form-control" v-if="type_unit == 2" v-model="value2">
										<option value="น้อยกว่า 200 มล." selected>น้อยกว่า 200 มล.</option>
										<option value="น้อยกว่า 200 มล.">200 - 499 มล.</option>
										<option value="น้อยกว่า 200 มล.">500 - 1000 มล.</option>
										<option value="น้อยกว่า 200 มล.">มากกว่า 1000 มล.</option>
									</select>
                                </span>
                                
                                

                                <div v-if="type_ans == 2">
                                    <div class="col-sm-4" style="padding: 0px 2px 0px 0px;">
                                        <label class="choice"> 
                                            <div>
                                                <i class="fa fa-angle-down fa-2x" aria-hidden="true" style="display: block;"></i>น้อยกว่า</div>
                                                <input class="choice-active" type="radio" checked="" value="0" name="ratio" v-model="value2"> 
                                            </div>
                                        </label>
                                    
                                    <div class="col-sm-4" style="padding: 0px 2px;">
                                        <label class="choice"> 
                                            <div>
                                                <img src="/icon/p.png" height="40px" style="display: block;    margin: auto;" v-model="value2">
                                                {{ unit_number }} {{ unit_select }}
                                                </div>
                                                <input class="choice-active" type="radio" checked="" value="1" name="ratio" v-model="value2"> 
                                            </div>
                                        </label>
                                    
                                    <div class="col-sm-4" style="padding: 0px 0px 0px 2px;">
                                        <label class="choice"> 
                                            <div>
                                                <i class="fa fa-angle-up fa-2x" aria-hidden="true" style="display: block;"></i>มากกว่า</div>
                                                <input class="choice-active" type="radio" checked="" value="2" name="ratio" v-model="value2"> 
                                            </div>
                                        </label>
                                    </div>
                                </div>
                        </div>

                        <div class="col-sm-4" style="padding: 15px;">
                            <div v-if="image == '' || image == null" style=" padding: 15px;">
                                <img class="img-thumbnail" src="/img/logo/imgnull.png" width="100%" />
                            </div>
                            <div v-else style=" padding: 15px;">
                                <img v-if="image.substr(0,4) == 'data'" class="img-thumbnail" :src="image" width="100%" @click="zoom_img(image)"/>
                                <img v-else class="img-thumbnail" :src="'/image'+image" width="100%" :id="image" @click="zoom_img('/image'+image)"/>
                            </div>
                        </div>

                        <div class="col-sm-12" v-if="target != '' && target != null">
                            <label><h4 class="blue-color" style="display: -webkit-inline-box;">จุดประสงค์ที่ต้องการทราบ</h4> : {{ target }}</label>
                        </div>
                        
                        <div class="col-sm-12" v-if="note != '' && note != null ">
                            <label><h4 class="blue-color" style="display: -webkit-inline-box;">หมายเหตุ</h4> : {{ note }}</label>
                        </div>
                    </div>
                </form>
            </div>
        </div>`,

mounted: function () {

   
},
methods: {
    del_method(){

        swal({
            title: "Are you sure remove this data?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false
        }, (callback) => {
            console.log(callback);

            this.$http.delete(this.link_del).then((myresponse) => {
                location.reload();
            }, (myresponse) => {
                // error callback
                swal('ไม่สามารถลบได้ เนื่องจากมีข้อมูลอื่นใช้งานข้อมูลนี้อยู่');
                console.log(myresponse.data);
            });

            swal.close();
        });
    },
    edit_method(){
        window.location.href=this.link_edit;
    },
    zoom_img(img){
        var modal = $('#myModalImg');
        var modalImg = $("#img01");
        var captionText = $("#caption");

        modal.css("display", "block");
        modalImg.attr("src", img);
        captionText.innerHTML = this.alt;
    }
},
    watch: {
        // callback for when the selector popup is closed.
        value1 : function() {
            this.$emit('value1', this.value1);
        },
        value2 : function() {
            this.$emit('value2', this.value2);
        },
    },
});