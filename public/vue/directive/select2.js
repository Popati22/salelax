/*
 * Copyright (c) 2561. By Nisachon.
 */

Vue.component('select2', {
    props: ['options', 'value'],
    template: '#select2-template',
    mounted: function () {
        var vm = this
        $(this.$el)
        // init select2
            .select2({
                data: this.options
            })
            .val(this.id)
            .trigger('change')
            // emit event on change.
            .on('change', function () {
                vm.$emit('input', this.value)
            })
    },
    watch: {
        value: function (value) {
            // update value
            $(this.$el).val(value)
        },
        options: function (options) {
            // update options
            $(this.$el).empty().select2({ data: options, templateResult: this.formatState })
        }
    },
    methods: {
        formatState : function(state) {
            if (!state.id) {
                return state.value;
            }

            var $state = $(
                '<span><i class="fa fa-square" aria-hidden="true" style="color:'+state.color+'"></i> ' + state.text + '</span>'
            );
            return $state;
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
})
