Vue.component('preview-ratio', {
props: ['name','example','image','target','note','link_edit','link_del'],
template:`
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 >{{ name }} <small v-if="example">( {{ example}} )</small></h5>
                <span style="position: absolute; right: 35px; margin-top: -6px;">
                    <button class="btn btn-warning btn-xs" @click="edit_method()">แก้ไข</button>
                    <button class="btn btn-danger btn-xs" @click="del_method()">ลบ</button>
                </span>
            </div>
            <div class="ibox-content">
                <form method="get" class="form-horizontal">
                    <div class="form-group">
                        
                        <div class="col-sm-9" style="padding: 25px;">
                            <label class="col-md-7">
                                    1. โดยทั่วไปในแต่ละสัปดาห์ ท่านกินกี่วันต่อสัปดาห์
                            </label> <label class="col-md-5"><input type="number" min="0" class="form-control" style="display: inline-block; width: 100px;">
                                วัน / สัปดาห์
                            </label>
                            <label class="col-md-7">
                                    2. โดยเฉลี่ย ในแต่ละวันท่านกินกี่วันต่อสัปดาห์ 
                            </label> <label class="col-md-5"><input type="number" min="0" class="form-control" style="display: inline-block; width: 100px;">
                                ครั้ง / วัน
                            </label>
                            <label class="col-md-7">
                                    3. ในวันที่กิน ท่านกินกี่ส่วนต่อวัน 
                            </label> <label class="col-md-5"><input type="number" min="0" class="form-control" style="display: inline-block; width: 100px;" placeholder="0.5">
                                ส่วน / วัน
                            </label>
                            
                            <div class="col-sm-12" v-if="target != '' && target != null">
                                <label><h4 class="blue-color" style="display: -webkit-inline-box;">จุดประสงค์ที่ต้องการทราบ</h4> : {{ target }}</label>
                            </div>
                            
                            <div class="col-sm-12" v-if="note != '' && note != null ">
                                <label><h4 class="blue-color" style="display: -webkit-inline-box;">หมายเหตุ</h4> : {{ note }}</label>
                            </div>
                            
                        </div>
                        <div class="col-sm-3" style="padding: 15px;">
                            <div v-if="image == '' || image == null" style=" padding: 15px;">
                                <img class="img-thumbnail" src="/img/logo/imgnull.png" width="100%" />
                            </div>
                            <div v-else style=" padding: 15px;">
                                <img v-if="image.substr(0,4) == 'data'" class="img-thumbnail" :src="image" width="100%" @click="zoom_img(image)"/>
                                <img v-else class="img-thumbnail" :src="'/image'+image" width="100%" :id="image" @click="zoom_img('/image'+image)"/>
                            </div>
                        </div>
                           
                    </div>
                </form>
            </div>
        </div>`,

mounted: function () {

   
},
methods: {
    del_method(){

        swal({
            title: "Are you sure remove this data?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false
        }, (callback) => {
            console.log(callback);

            this.$http.delete(this.link_del).then((myresponse) => {
                location.reload();
            }, (myresponse) => {
                // error callback
                swal('ไม่สามารถลบได้ เนื่องจากมีข้อมูลอื่นใช้งานข้อมูลนี้อยู่');
                console.log(myresponse.data);
            });

            swal.close();
        });
    },
    edit_method(){
        window.location.href=this.link_edit;
    },
    zoom_img(img){
        var modal = $('#myModalImg');
        var modalImg = $("#img01");
        var captionText = $("#caption");

        modal.css("display", "block");
        modalImg.attr("src", img);
        captionText.innerHTML = this.alt;
    }
},
    watch: {
        // callback for when the selector popup is closed.
        value1 : function() {
            this.$emit('value1', this.value1);
        },
        value2 : function() {
            this.$emit('value2', this.value2);
        },
    },
});