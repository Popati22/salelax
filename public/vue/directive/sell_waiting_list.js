/*
 * Copyright (c) 2561. By Nisachon.
 */

Vue.component('sell_waiting_list', {
    props: ['orders'],
    template:`
<div>
    <div v-for="order in orders">
        <div class="row box-wait-body">
            <div class="col-xs-12 box-wait border-bt-gray">
                <div class="col-md-1 col-xs-12 goods-avatar no-padd tx-center" >               
                    <img class="pull-left img-responsive img-i-w" :src="order.buyer_img_profile" >                
                </div>
                <div class="col-md-2 col-xs-12 no-padd ">
                    <h4 class="h4 tx-black tx-center" >{{ order.title }}</h4> 
                </div>
                <div class="col-md-9 col-xs-12">
                    <span class="h4 tx-black pull-right">รวมเงิน {{ order.total_amount }} บาท</span>
                </div>
            </div>
            <div class="col-xs-12 col-xs-12 box-wait border-bt-gray">		
                <label class="h4 tx-gray pull-right">{{ order.orderdetail.length }} รายการ
                    (<span v-for="detail in order.orderdetail">
                        {{ detail.name }} {{ detail.quantity }},
                    </span>)
                </label>
            </div>
            <div class="col-xs-12 col-xs-12 box-wait border-bt-gray">
                <div class="col-md-8">			
                    <span class="h4 tx-blue pull-left">
                        <span class="glyphicon glyphicon-map-marker tx-i-blue"></span>
                        ส่งที่ : ท่ารถตู้ธรรมศาสตร์ - ฟิวเจอร์ปาร์ค...
                    </span>
                </div>
                <div class="col-md-4">
                    <label class="h4 tx-gray pull-right">เวลา 12:00 น.</label>
                </div>
            </div>
            <div class="col-xs-6 col-xs-12 box-wait mg-top-30 mg-b-20">
                <button class="btn-modal-cancel tx-center btn-change-total"
                 style="width:80%;">      
                    <span>เปลี่ยนแปลงการส่ง</span>
                </button>
                
            </div>
            <div class="col-xs-6 col-xs-12 box-wait mg-top-30 mg-b-20">
                <button class="btn-md-blue-w tx-center" style="width:80%;">
                    <span class="mg-right-10">ส่งสินค้าแล้ว</span>
                </button>
            </div>
        </div>
    </div>
</div>    
    `,

    mounted: function () {


    },
    methods: {

    }
});