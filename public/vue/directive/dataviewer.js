Vue.filter('formatDateThai', function(value) {
    if (value) {
        var year1 = value.substr(0,4);
        var year2 = Number(year1)+543;
        value = value.replace(year1, year2.toString());
        return moment(value).format('lll');//+' '+value.substr(11,6)
    }
})
Vue.filter('money', function(value) {
    if (value) {
        let val = (value/1).toFixed(2)
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
})

var com = Vue.component('data-viewer', {
    props: ['source', 'title','titleadd','linknew', 'linkedit', 'linkdel','linkchange','linkview' ,'candel', 'canedit','canchange', 'dataset', 'cannew', 'canview','show_key_search','branchid','submit','columsearch'],
    template : `<div class="dv" >
    <div class="dv-header">
        <div class="col-md-8 col-sm-12" style="color: #00b9cd; font-weight: 600;    font-size: 26px;">
            <span v-if="dataset != 'history' ">{{title}}</span>
            <span v-if="dataset == 'history' && model.data.length > 0">{{title}} {{ model.data[0].profile.office_name }} </span>
            
        </div>

        <div class="col-md-4 col-sm-12" v-if="show_key_search">
            <span class="dv-header-pre">Search: </span>
            <select class="dv-header-select" v-model="query.search_column">
                <option v-for="column in columns" :value="column">{{column}}</option>
            </select>
        </div>
        <div class="col-md-4 col-sm-12" v-if="show_key_search">
            <select class="dv-header-select" v-model="query.search_operator">
                <option v-for="(value, key) in operators" :value="key">{{value}}</option>
            </select>
        </div>

        <div class="col-md-4 col-sm-12" v-if="dataset != 'history'">
                    
             <div class="input-group" style="margin-bottom:2px">
                 <span class="input-group-addon" style=" border: none;">
                 <i class="fa fa-search" aria-hidden="true" style="    color: #d4d4d4;"></i></span> 
                 <input type="text" class="form-control" style="border-radius: 8px;"
                       placeholder="ค้นหา"
                       v-model="query.search_input" @keyup="fetchIndexData()"
                       @keyup.enter="fetchIndexData()">
             </div>
           
        </div>

        <div class="col-md-4 col-sm-12" v-if="show_key_search">
            <button class="dv-header-btn"@click="fetchIndexData()">Filter</button>
        </div>
        <div class="col-md-4 col-sm-12" v-if="cannew && dataset == 'history' && model.data.length > 0"style="padding: 0px">
            
            
            <div class="col-xs-6" style="padding-right:5px;padding-left:5px;" >
                 <button class="btn btn-warning btn-sm btn-block" @click="edit_profile(model.data[0])" style="padding: 2px 0.5em;border-radius: 8px;"><i class="fa fa-pencil-square" aria-hidden="true"></i> แก้ไขโปรไฟล์</button>
            </div>
            <div class="col-xs-6" style="padding-left:5px;padding-right:0px;">
                <button class="btn btn-info btn-sm btn-block" @click="newdata()" style="padding: 2px 0.5em;border-radius: 8px;"><i class="fa fa-plus-square" aria-hidden="true"></i> {{ titleadd }}</button>
            </div>
            
            
        </div>
       
        
    </div>

    <div class="dv-body">
        <table class="table table-bordered table-hover">

            <thead>
                <tr>
                    <th v-for="(value, key) in show_col" @click="toggleOrder(key)">
                         <span>{{value}}</span>
                         <span class="dv-table-column">
                            <span v-if="query.direction === 'desc'"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                            <span v-else><i class="fa fa-angle-up" aria-hidden="true"></i></span>
                         </span>
                    </th>
                    <th style="width: 30%"></th>
                </tr>
            </thead>
            <tbody>
            <tr v-if="model.data.length == 0">
                <td :colspan="columns.length+1" style="text-align: center;padding: 150px;">ไม่พบข้อมูล</td>
            </tr>
            <tr v-for="row in model.data" v-else>
        
                <td v-if="dataset == 'member'">
                    <span> {{row.office_name}} </span>
                </td>
                <td v-if="dataset == 'member'">
                    <span> {{row.created_at | formatDateThai}} </span>
                </td>
                <td v-if="dataset == 'member'">
                    <span v-if="row.questionnaire.length > 0">{{row.questionnaire[0].updated_at | formatDateThai }} </span> 
                </td>
                
                <td v-if="dataset == 'history'">
                    <span>{{ row.user.displayname  }}</span>
                </td>
                <td v-if="dataset == 'history'">
                    <span>{{ row.created_at | formatDateThai }}</span>
                </td>
                <td v-if="dataset == 'history'">
                    <span>{{ row.updated_at | formatDateThai  }}</span>
                </td>
                
                <td v-if="canedit" style="text-align: center;">
                    <div class="" style="margin-bottom: 3px;">
                        <button type="button" v-if="dataset == 'member'" class="btn btn-link btn-sm" v-on:click="ques_method(row)">
                            <img src="/icon/icon-ques.png" style="display:block; margin:auto; height:20px;">
                            <span style="color: #00b9cd;">ทำแบบสอบถาม</span>
                        </button>
                        <button type="button" v-if="dataset == 'member'" class="btn btn-link btn-sm" v-on:click="history_method(row)">
                            <i class="fa fa-history" aria-hidden="true" style="color: rgb(210, 189, 5);display:block; margin:auto; height:20px;"></i>
                            <span style="color: rgb(210, 189, 5);">ประวัติการทำ</span>
                        </button>
                        <button type="button" v-if="canedit" class="btn btn-link btn-sm" v-on:click="export_method(row)">
                            
                            <i v-if="dataset=='history'" class="fa fa-file-word-o" aria-hidden="true" style="display:block; margin:auto; height:20px;"></i>
                            <span v-if="dataset=='history'" style="color: rgb(32, 32, 32);">รายงาน</span>
                        </button>
                            
                        <button type="button" v-if="canedit" class="btn btn-link btn-sm" v-on:click="edit_method(row)">
                            <img v-if="dataset!='history'" src="/icon/icon-edit.png" style="display:block; margin:auto; height:20px;">
                            <span v-if="dataset!='history'" style="color: #231f20;">แก้ไขโปรไฟล์</span>
                            
                            <img v-if="dataset=='history'" src="/icon/icon-ques.png" style="display:block; margin:auto; height:20px;">
                            <span v-if="dataset=='history'" style="color: #00b9cd;">ดูการทำ</span>
                        
                        </button>
                        <button type="button" v-if="candel" class="btn btn-link btn-sm" v-on:click="del_method(row)" >
                            <img src="/icon/icon-del.png" style="display:block; margin:auto; height:20px;">
                            <span style="color: #f09e81;">ลบ</span>
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="dv-footer">
        <div class="col-xs-6">
            <span>แสดงผล {{model.from}} - {{model.to}} of {{model.total}} แถว</span>
        </div>
        <div class="col-xs-6">
            <div class="col-sm-6">
                <span>แถวต่อหน้า</span>
                <input type="text" v-model="query.per_page"
                       class="dv-footer-input"
                       @keyup.enter="fetchIndexData()">
            </div>
            <div class="col-sm-6">
                <button class="dv-footer-btn" @click="prev()"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <input type="text" v-model="query.page"
                       class="dv-footer-input"
                       @keyup.enter="fetchIndexData()">
                <button class="dv-footer-btn" @click="next()"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>`,
    watch: {
        submit: function(val) {
            if(val > 0)
                this.fetchIndexData();
        }
    },
    mounted: function () {
        var d = new Date();
        this.date_input = moment(d).format("DD/MM/YYYY")
    },
    data: function () {
        return {
            model: {},
            columns: {},
            show_col: {},
            date_input: '',
            date_input2: '',
            zone: '',
            query: {
                page: 1,
                column: 'id',
                direction: 'desc',
                per_page: 15,
                search_column: 'id',
                search_operator: 'equal',
                search_input: '',
            },
            operators: {
                equal: '=',
                not_equal: '<>',
                less_than: '<',
                greater_than: '>',
                less_than_or_equal_to: '<=',
                greater_than_or_equal_to: '>=',
                in: 'IN',
                like: 'LIKE'
            },
            zones : [],
            hospitals : [],
            list_sp : [],
            submit : false,
            errors : []
        }
    },

    created() {
        this.fetchIndexData();
    },
    filters: {
        moment: function (date) {
            return moment(date).format('DD MMM YYYY, h:mm a');
        }
    },
    methods: {

        moment: function () {
            return moment();
        },
        ques_method(row){
            window.location.href='/members/'+row.id;
        },
        history_method(row){
            window.location.href='/history/'+row.id;
        },
        export_method(row){
            window.location.href='/word/'+row.profile_id+'/'+row.id;
        },
        edit_profile(row){
            window.location.href='/member/edit/'+$('#profile_id').val();
        },
        edit_method(row){
		    window.location.href=this.linkedit+'/'+row.id;
        },
        del_method(row){
            var title="";
            var text="";

            if(this.dataset == 'history'){
                text = "ลบแบบสอบถาม"
            }else{
                title = row.office_name
                text = "คุณต้องการลบข้อมูลบริษัทใช่หรือไม่?"
            }
            var vm = this

            swal({
                title: title,
                text: text,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "ใช่ ลบเลย",
                cancelButtonText: "ยกเลิก"
            }).then(function () {

                vm.$http.delete(vm.linkdel+'/'+row.id).then(function(myresponse) {
                    vm.fetchIndexData();
                    swal.close();

                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });

            });
        },
        newdata(){
            window.location.href=this.linknew;
        },
        next() {
            if(this.model.next_page_url) {
                this.query.page++
                this.fetchIndexData()
            }
        },
        prev() {
            if(this.model.prev_page_url) {
                this.query.page--
                this.fetchIndexData()
            }
        },
        toggleOrder(column) {
            if(column === this.query.column) {
// only change direction
                if(this.query.direction === 'desc') {
                    this.query.direction = 'asc'
                } else {
                    this.query.direction = 'desc'
                }
            } else {
                this.query.column = column
                this.query.direction = 'asc'
            }
            this.fetchIndexData()
        },
        fetchIndexData() {
            var vm = this
            axios.get(`${this.source}?column=${this.query.column}&direction=${this.query.direction}&page=${this.query.page}&per_page=${this.query.per_page}&search_column=${this.query.search_column}&search_operator=${this.query.search_operator}&search_input=${this.query.search_input}`)
                .then(function (response) {
                    var model = response.data.model;

                    for (i = 0; i < model.data.length; i++) {
                        model.data[i].edit = false;
                    }

                    Vue.set(vm.$data, 'model', model)
                    Vue.set(vm.$data, 'columns', response.data.columns)
                    Vue.set(vm.$data, 'show_col', response.data.show_col)
                })
                .catch(function (response) {
                    console.log(response)
                })

        }
    }
})