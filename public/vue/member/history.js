Vue.config.devtools = true;

new Vue({
    el:'#app',
    data: {

        errors: []
    },
    mounted: function(){
        this.getprofile();

    },
    watch:{

    },
    methods: {
        getprofile : function () {
            if($('#user_type').val() == 3){
                this.$http.get('/api/profile_user_id/'+$('#profile_id').val() ).then(function(myresponse) {
                    this.profile = myresponse.data;
                    if(this.profile.firstname == null){
                        window.location.href = '/member/edit/'+$('#profile_id').val();
                    }
                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });
            }
            else{
                this.$http.get('/api/profile/'+$('#profile_id').val() ).then(function(myresponse) {
                    this.profile = myresponse.data;

                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });
            }

        }

    }
});