Vue.config.devtools = true;
Vue.component('v-select', VueSelect.VueSelect);
Vue.config.devtools = true;
var Validator = SimpleVueValidator.Validator.create({
    templates: {
        greaterThan: 'จำเป็นต้องเลือก อย่างน้อย1ไฟล์',
        required: 'จำเป็นต้องใส่ข้อมูล'
    }
});

Vue.use(SimpleVueValidator);
var this_income='';

new Vue({
    el:'#app',
    data: {
        profile:{
            province: '',
            amphur: '',
            district: '',
            district_name: '',
            address: '',
            tambon: '',
            zipcode: '',
            gender: '',
            age: '',
            idcard: '',
            tel: '',
            mobile: '',
            date_input: '',

            profile_business: {
                policy:'',
                vision:'',
                structure:'',
                character:'',
                innovation:'',

            },

            profile_income : [],
            profile_result :[],

        },
        // office_name: '',
        // province: '',
        // amphur: '',
        // district: '',
        // district_name: '',
        // address: '',
        // tambon: '',
        // zipcode: '',
        // firstname: '',
        // lastname: '',
        // gender: '',
        // age: '',
        // idcard: '',
        // tel: '',
        // mobile: '',
        // date_input: '',
        // height: '',
        // weight: '',
        // weight_cant: '',
        // education: '',
        // education_other: '',
        // career: '',
        // career_other: '',
        // income: '',
        // address_store: '',
        // address_market: '',
        // address_minimart: '',
        // address_mall: '',
        timestart:moment().format('YYYY-MM-DD H:mm:ss'),
        change_province: 0,
        provinces:[],
        amphures:[],
        tambons:[],
        income:[],
        income_last_year:'',
        result_last_year:'',
        result:[],
        income_year : moment().format('YYYY'),
        result_year : moment().format('YYYY'),
        loading_api : true,
        errors: []
    },
    mounted: function(){

        this.getprovince();
        $('html, body').animate({scrollTop: 0}, 'fast');
        if($('#profile_id').val() > 0){
            this.getprofile();
        }

    },
    validators: {
        'profile.owner': function (value) {
            return Validator.value(value).required('จำเป็นต้องเลือก');
        },
        'profile.speaker': function (value) {
            return Validator.value(value).required('จำเป็นต้องใส่ข้อมูล');
        },

    },
    watch:{
        'profile.province' : function (val) {
            this.getamphur();
            this.change_province++;

            if($('#profile_id').val() == 0){
                this.profile.amphur = '';
                this.profile.tambon = '';
                this.profile.zipcode = '';
            }
            if($('#profile_id').val() > 0 && this.change_province > 1){
                this.profile.amphur = '';
                this.profile.tambon = '';
                this.profile.zipcode = '';
            }


        },
        'profile.amphur' : function (val) {
            this.gettambon();
            if($('#profile_id').val() == 0) {
                this.profile.tambon = '';
                this.profile.zipcode = '';
            }
            if($('#profile_id').val() > 0 && this.change_province > 1){
                this.profile.tambon = '';
                this.profile.zipcode = '';
            }
        },
        'profile.tambon' : function (val) {
            this.profile.zipcode = val.zip_code;
        }
    },
    methods: {
        addIncome : function () {
            var this_income = [];
            this_income[0] = { type:0 , amount: this.income[0] };
            this_income[1] = { type:1 , amount: this.income[1] };
            this_income[2] = { type:2 , amount: this.income[2] };

            var obj={};
            obj['year'] = this.income_year;
            obj['value'] = this_income;
            this.profile.profile_income.push(obj);

            this.income_year++;
            this.income[0] = '';
            this.income[1] = '';
            this.income[2] = '';
        },
        addResult : function () {
            var this_result = [];
            this_result[0] = { type:0 , amount: this.result[0] };
            this_result[1] = { type:1 , amount: this.result[1] };
            this_result[2] = { type:2 , amount: this.result[2] };
            this_result[3] = { type:3 , amount: this.result[3] };
            this_result[4] = { type:4 , amount: this.result[4] };
            this_result[5] = { type:5 , amount: this.result[5] };
            this_result[6] = { type:6 , amount: this.result[6] };
            this_result[7] = { type:7 , amount: this.result[7] };

            var obj={};
            obj['year'] = this.result_year;
            obj['value'] = this_result;

            this.profile.profile_result.push( obj );
            this.result_year++;
        },
        removeObject: function (array,index) {
            array.splice(index,1);
        },
        getprovince : function(){
            this.provinces = [];
            this.$http.get('/api/province').then(function(myresponse) {
                this.provinces = myresponse.data;
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
        getamphur : function(){
            this.amphures = [];

            this.$http.get('/api/amphur/'+this.profile.province.id).then(function(myresponse) {
                this.amphures = myresponse.data;
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
        gettambon : function(){
            this.tambons = [];

            this.$http.get('/api/tambon/'+this.profile.amphur.id).then(function(myresponse) {
                this.tambons = myresponse.data;
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
        getprofile : function () {
            if($('#user_type').val() == 3){
                this.$http.get('/api/profile_user_id/'+$('#profile_id').val() ).then(function(myresponse) {
                    this.profile = myresponse.data;

                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });
            }
            else{
                this.$http.get('/api/profile/'+$('#profile_id').val() ).then(function(myresponse) {
                    this.profile = myresponse.data;
                    // this.profile.policy = this.profile.profile_business.policy;
                    // this.profile.vision = this.profile.profile_business.vision;
                    // this.profile.structure = this.profile.profile_business.structure;
                    // this.profile.character = this.profile.profile_business.character;
                    // this.profile.innovation = this.profile.profile_business.innovation;

                    $('#policy').summernote('code',this.profile.profile_business.policy);
                    $('#vision').summernote('code',this.profile.profile_business.vision);
                    $('#character').summernote('code',this.profile.profile_business.character);
                    $('#innovation').summernote('code',this.profile.profile_business.innovation);

                    // this.object_to_graph_pie(this.profile.profile_income[0]);
                    this.loading_api = false;

                    createPie('stat_income_admin',this.profile.profile_income_pie,'amount','type');
                    createBar2('stat_income_admin_group',this.profile.profile_income_group);
                    createMultiLine('stat_result_admin', this.profile.profile_result_group);

                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                    this.profile = myresponse.data;
                });
            }

        },
        object_to_graph_pie: function (array) {
            this.loading_api = false;

            this.income_last_year = array.year;
            var arr = [];
            var value = array.value;
            for(i=0 ; i<value.length ; i++){
                if(value[i].type == 0){
                    arr.push({type:'รายได้ตามสัญญา', amount:value[i].amount});
                }
                else if(value[i].type == 1){
                    arr.push({type:'รายได้จากการขายและการให้บริการ', amount:value[i].amount});
                }
                else if(value[i].type == 2){
                    arr.push({type:'รายได้อื่นๆ', amount:value[i].amount});
                }
            }

            createPie('stat_income_admin',arr,'amount','type');
        },
        submit : function () {
            this.errors = [];
            this.profile.profile_business.policy = $('#policy').summernote('code');
            this.profile.profile_business.vision = $('#vision').summernote('code');
            this.profile.profile_business.character = $('#character').summernote('code');
            this.profile.profile_business.innovation = $('#innovation').summernote('code');

            var formData = new FormData();
            formData.append('structure', $('#structure')[0].files[0]);


            if($('#profile_id').val() == 0){
                this.$http.post('/api/profile',this.profile).then(function(myresponse) {
                    if(myresponse.data.errors == false){
                        swal({
                                title: "บันทึกข้อมูลสำเร็จ",
                                text: "ต้องการกรอกข้อมูลอีกหรือไม่",
                                type: "success",
                                showCancelButton: true,
                                confirmButtonColor: "#0d991f",
                                confirmButtonText: "ใช่",
                                cancelButtonText: "ไม่",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm){
                                if (isConfirm) {
                                    window.location.href = '/member/add';
                                } else {
                                    window.location.href = '/member';

                                }
                            });
                    }else{
                        this.errors = myresponse.data.errors;
                        $('html, body').animate({ scrollTop: 0 }, 'fast');
                    }
                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });
            }
            else{
                this.$http.put('/api/profile/'+$('#profile_id').val() , this.profile).then((myresponse) => {
                    if(myresponse.status == 200)
                    {

                        this.$http.post('/api/updateStructure/'+$('#profile_id').val() , formData).then((myresponse) => {
                            if($('#user_type').val() == 3){
                                window.location.href = "/history/"+$('#user_id').val();
                            }
                            else{
                                window.location.href = "/member";
                            }
                        });



                    }
                    else
                    {
                        this.errors = myresponse.data.errors;
                        $('html, body').animate({scrollTop: 0}, 'fast');
                    }
                },(myresponse) => {
                    // error callback
                    console.log(myresponse.data);
                });
            }

        }

    }
});