Vue.config.devtools = true;

new Vue({
    el:'#app',
    data: {
        displayname: '',
        email: '',
        type : '',
        groups: [],
        errors: []
    },
    methods: {
        submit : function () {
            this.errors = [];
            var profile = {
                "name": this.displayname,
                "email": this.email,
                "type": this.type,
            }

            this.$http.post('/api/users',profile).then(function(myresponse) {
                if(myresponse.data.errors == false){
                    swal({
                            title: "ลงทะเบียนเสร็จสมบูรณ์",
                            text: "ต้องการเพิ่มผู้ใช้อีกหรือไม่",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#0d991f",
                            confirmButtonText: "ใช่",
                            cancelButtonText: "ไม่",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm){
                            if (isConfirm) {
                                window.location.href = '/admin';
                            } else {
                                window.location.href = '/member';
                            }
                        });
                }else{
                    this.errors = myresponse.data.errors;
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                }
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        }
    }
});