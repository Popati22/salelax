Vue.config.devtools = true;

new Vue({
	el:'#app',
	data: {
		lang : 'en',
        news : [],
		errors: []
	},
    mounted: function(){

        this.all_news();
    },
    methods:{
        all_news: function(){
            this.$http.get('/api/newsall').then(function(myresponse) {
                this.news = myresponse.data;
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
    }

});