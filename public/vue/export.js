Vue.config.devtools = true;

new Vue({
    el:'#app',
    data: {
        datestart: '',
        dateend: '',
        groups: [],
        users: [],
        errors: []
    },
    mounted: function(){
        this.getgroups();
        this.getusers();
    },
    watch: {
        datestart : function (val) {
            $('[name="datestart"]').val(this.datestart);
        },
        dateend : function (val) {
            $('[name="dateend"]').val(this.dateend);
        }
    },
    methods: {
        getgroups: function(){
            this.$http.get('/api/question').then(function(myresponse) {
                this.groups = myresponse.data.questions;

                for(i=0 ; i<this.groups[1].length; i++){
                    this.groups[1][i].edit = false;
                }
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
        getusers: function(){
            this.$http.get('/api/users_all').then(function(myresponse) {
                this.users = myresponse.data;

            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        }
    }
});