Vue.config.devtools = true;

new Vue({
	el:'#app',
	data: {
		group: '',
		edit: false,
		edit_value : '',
        groups: [],
        users: [],
		errors: []
	},
    mounted: function(){
        this.getgroups();
        this.getusers();
    },
	watch: {
    	'edit_value' : function (val) {
			this.group = this.edit_value.name;

        }
	},
	methods: {
        savegroup: function(){
        	var data = {
        		"name" : this.group,
                "user_id" : $('#user_id').val()
			}

			if(this.edit == false){
                this.$http.post('/api/group',data).then(function(myresponse) {
                    this.groups = myresponse.data.questions;

                    for(i=0 ; i<this.groups[1].length; i++){
                        this.groups[1][i].edit = false;
                    }
                    this.group = '';

                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });
			}
			else{
                this.$http.put('/api/group/'+this.edit_value.id,data).then(function(myresponse) {
                    this.groups = myresponse.data.questions;

                    for(i=0 ; i<this.groups[1].length; i++){
                        this.groups[1][i].edit = false;
                    }
                    this.group = '';
                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });
			}

        },
        getgroups: function(){
			this.$http.get('/api/group').then(function(myresponse) {
				this.groups = myresponse.data.questions;

			}, function(myresponse) {
				// error callback
				console.log(myresponse.data);
			});
        },
        getusers: function(){
            this.$http.get('/api/users_all').then(function(myresponse) {
                this.users = myresponse.data;

            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
        togglebtn: function(group){
			this.edit = true;
			this.edit_value = group;
		},
        btn_del: function (group) {
            swal({
                title: "Are you sure remove this data?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false
            }, (callback) => {
                console.log(callback);

				this.$http.delete('/api/group/'+group.id).then((myresponse) => {
					if(myresponse.data.status == "success"){
					this.getgroups();
				}else{

					this.getgroups();
				}
			}, (myresponse) => {
					// error callback
					swal('ไม่สามารถลบได้ เนื่องจากมีข้อมูลอื่นใช้งานข้อมูลนี้อยู่');
					console.log(myresponse.data);
				});

				swal.close();
			});
        }
  }
});