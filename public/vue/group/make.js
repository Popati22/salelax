Vue.config.devtools = true;

new Vue({
	el:'#app',
	data: {
		groups : [],
		idActive: 1,
        questionnaire: '',
		status:1,//old status:1
		count:0,
        doned:0,
        count_miss:0,
        units: [],
        noti : 0,
        timestart : '',
        income: '',

        questionnaire_id : '',
        choice1 : [1,2,3,4,5,6,7,8],
        others1: [{
		    id:0,
            name: '',
            value1:0,
            value2:'',
            value2other:''
        }],
        others2: [{
            id:0,
            name: '',
            value1:0,
            value2:'',
            value2other:''
        }],

        other1: {
            name: '',
            value1:0,
            value2:'',
            value2other:''
        },
        other2: {
            name: '',
            value1:0,
            value2:'',
            value2other:''
        },
        topic_miss : [],
        topic_miss_str : '',
		errors: []
	},
	mounted: function(){
        this.timestart = moment().format('YYYY-MM-DD hh:mm:ss');
        this.getgroups();

        // swal({
        //     title: "",
        //     text:'คำถามต่อไปนี้เป็นคำถามเพื่อทราบว่าท่านบริโภคอาหารชนิดใด มากน้อย และบ่อยครั้งเพียงใด ดังนั้นขอให้ตอบในส่วนที่ท่านกินเพียงคนเดียว <u>ในกรณีที่กินร่วมกับคนอื่นขอให้ตอบเฉพาะในส่วนที่ท่านกินเท่านั้น</u>',
        //     html: true
        // });
    },
    ready:function(){
        // code here executes once the component is rendered
        // use this in the child component

    },
    watch: {
        'groups': {
            handler: function(newValue) {
            	this.updateDone(newValue);
            },
            deep: true
            //Scroll to bottom

        },
        // 'status': function (val) {
        //     if($('#questionnaire_id').val() == 0) {
        //
        //         $('#parentVerticalTab').easyResponsiveTabs({
        //             type: 'vertical', //Types: default, vertical, accordion
        //             width: 'auto', //auto or any width like 600px
        //             fit: true, // 100% fit in a container
        //             closed: 'accordion', // Start closed if in accordion view
        //             tabidentify: 'hor_1', // The tab groups identifier
        //             activate: function (event) { // Callback function if tab is switched
        //                 var $tab = $(this);
        //                 var $info = $('#nested-tabInfo2');
        //                 var $name = $('span', $info);
        //                 $name.text($tab.text());
        //                 $info.show();
        //             }
        //         });
        //         $('#parentVerticalTab > div:nth-child(1) > ul > li:nth-child(1)').click();
        //
        //         if (val == 2) {
        //             $('html, body').animate({scrollTop: 0}, 'fast');
        //             $('.resp-vtabs .resp-tabs-container').addClass('fixed_width_100');
        //         }
        //     }
        //     else{
        //         if (val == 2) {//preview
        //             $('html, body').animate({scrollTop: 0}, 'fast');
        //             $('.resp-vtabs .resp-tabs-container').addClass('fixed_width_100');
        //             this.status = 1;
        //         } else {//edit
        //             var delayMillis = 1000; //1 second
        //
        //             setTimeout(function() {
        //                 $('#parentVerticalTab').easyResponsiveTabs({
        //                     type: 'vertical', //Types: default, vertical, accordion
        //                     width: 'auto', //auto or any width like 600px
        //                     fit: true, // 100% fit in a container
        //                     closed: 'accordion', // Start closed if in accordion view
        //                     tabidentify: 'hor_1', // The tab groups identifier
        //                     activate: function (event) { // Callback function if tab is switched
        //                         var $tab = $(this);
        //                         var $info = $('#nested-tabInfo2');
        //                         var $name = $('span', $info);
        //                         $name.text($tab.text());
        //                         $info.show();
        //                     }
        //                 });
        //
        //                 $('#parentVerticalTab').find("[aria-controls=hor_1_tab_item-0]").click()
        //                 $('#parentVerticalTab').find("[aria-labelledby=hor_1_tab_item-0]").slideDown().addClass('resp-tab-content-active');
        //
        //             }, delayMillis);
        //
        //             $('html, body').animate({scrollTop: 0}, 'fast');
        //             $('.resp-vtabs .resp-tabs-container').removeClass('fixed_width_100');
        //
        //         }
        //     }
        // }
    },
	methods: {
        groupupdate: function (group,sub) {
            console.log(group);
            console.log(sub);

             //แหล่งที่มา group.id == 30
            if(( sub.value1 > 1 && sub.value2 > 0 ) || ( sub.value1 > 1 && sub.value2 == 0 && sub.value2other != '' && sub.value2other != null ) || ( sub.value1 == 1 ) || (sub.value1 >= 1 && group.id == 30)){
                if(sub.value1 == 1){
                    sub.value2 = null;
                    sub.value2other = null;
                }

                var data = {
                    "user_id": $('#user_id').val(),
                    "questions_id": sub.questions_id,
                    "questionsubs_id": sub.id,
                    "value1": sub.value1,
                    "value2": sub.value2,
                    "value2other": sub.value2other,
                    "questionnaire_id": this.questionnaire_id
                }
                this.$http.post('/api/questionnaire_ans', data).then((myresponse) => {
                    if(myresponse.data.errors == false){
                        toastr.success('บันทึกข้อมูลแล้ว','ขอบคุณสำหรับคำตอบ',
                            + "\")\n\ntoastr.options = "
                            + JSON.stringify({ timeOut : 2000, extendedTimeOut: 1000 }, null, 2))
                        console.log(myresponse.data);
                        this.updateDone(group);
                    }
                    else{
                        this.errors = myresponse.data.errors;
                        $('html, body').animate({scrollTop: 0}, 'fast');
                    }
                },(myresponse) =>{
                    // error callback
                    console.log(myresponse.data);
                });
            }

        },
        updateDone : function (group) {
            this.doned = 0;
            this.count_miss = 0;
            var lastdone = 0;
            this.topic_miss = [];

            for (y = 0; y < this.groups.length; y++) {
                var groupsublength = this.groups[y].questions.length;
                this.groups[y].allquestion = groupsublength;

                var group_done = 0;

                for (s = 0; s < groupsublength; s++) {
                    var sub = this.groups[y].questions[s];

                    if(sub.value1 != ''){
                        this.doned++;
                        group_done++;
                    }else{
                        this.count_miss++;
                        // if(this.topic_miss[this.topic_miss.length-1] != this.groups[y].name){
                        //     this.topic_miss.push(this.groups[y].name);
                        // }
                    }


                }
                this.groups[y].done = group_done;

                // this.groups[x][y].done = doned;
                // if(x==1) $('#parentVerticalTab > div.resp-tabs-container.hor_1 > h2.resp-accordion.hor_1[aria-controls=hor_1_tab_item-'+y+'] > div > h5 > span:nth-child(1)').text(doned+"/"+groupsublength);
                //
                // if((doned == groupsublength) && (this.groups[x][y].id == group.id)){
                //     console.log(y);
                //     lastdone=y;
                //
                //     //check done next group
                //     var doned_next_group=0;
                //     var index_sub = y+1;
                //     if(index_sub >= this.groups[x].length) index_sub = y;
                //
                //     var nextgroupsublength = this.groups[x][index_sub].questions.length;
                //     for (s = 0; s < nextgroupsublength; s++) {
                //         var sub = this.groups[x][index_sub].questions[s];
                //
                //         if (sub.id < 74 || sub.id > 93  ) {
                //
                //             if(sub.value1 > 1 || sub.value1 == 1){
                //                 doned_next_group++;
                //             }
                //         }
                //     }
                //
                //     if(doned_next_group == 0){
                //         var width_screen = $(window).width();
                //         $('#parentVerticalTab > div:nth-child(1) > ul > li:nth-child('+(lastdone+2)+')').click()
                //
                //         // if(width_screen < 690){
                //         //     var scoll = 200+(y*100);
                //         //     console.log("scoll : "+scoll);
                //         //
                //         //     $('html, body').animate({scrollTop: scoll}, 'fast');
                //         // }
                //     }
                //     else{
                //         $('#parentVerticalTab > div:nth-child(1) > ul > li:nth-child('+(lastdone+2)+')').click()
                //
                //     }
                //     //check done next group
                // }
            }
        },

		toggleActive: function(group){
            // this.idActive = group.id;
            // var width_screen = $(window).width();
            // if(width_screen > 699) {
            //     $('html, body').animate({scrollTop: 370}, 'fast');
            // }
            this.updateDone(group);
		},
        changeStatus: function(val){

			if(val == 2 && this.count_miss == 0){
                this.status = val;
                swal({
                    title: "บันทึกข้อมูลแล้ว",
                    timer: 2000,
                    showConfirmButton: false
                });

                if($('user_type').val() == 3){
                    setTimeout(function(){ window.location.href = "/history/"+$('#user_id').val(); }, 2000);
                }
                else{
                    setTimeout(function(){ window.location.href = "/member"; }, 2000);
                }
            }
            else if(val == 2 && this.count_miss > 0){
			    this.noti = 1;
                swal('คุณยังตอบคำถามไม่ครบ กรุณาตอบข้อมีแถบสีแดง')
            }
            else if(val == 1){
                this.status = val;

            }
		},
        clear_value2 : function (sub) {
            if(sub.value2other != undefined && sub.value2other != ''){
                sub.value2 = 0;
            }
        },
        clear_value2other : function (sub) {
            if(sub.value2 != 0){
                sub.value2other = '';
            }
        },


	    getgroups: function(){

            if($('#questionnaire_id').val() == 0){
                this.$http.get('/api/question').then(function(myresponse) {
                    this.groups = myresponse.data.questions;
                    // this.submit();
                    // this.getother();
                    for(i=0;i<this.groups.length;i++){
                        this.updateDone(this.groups[i]);
                    }

                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });
            }
            else{
                this.questionnaire_id = $('#questionnaire_id').val();
                this.$http.get('/api/questionnaire/'+$('#questionnaire_id').val()).then(function(myresponse) {
                    this.questionnaire = myresponse.data;
                    this.income = myresponse.data.income;
                    this.groups = myresponse.data.questions;
                    // this.getother();
                    for(i=0;i<this.groups.length;i++){
                        this.updateDone(this.groups[i]);
                    }

                    createBarStep(myresponse.data.score_groups);
                    createRadar(myresponse.data.score_groups);
                    createBarColumn(myresponse.data.score_questions);

                    this.status = 2;
                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });
            }




        },
        zoom_img: function(img){
            var modal = $('#myModalImg');
            var modalImg = $("#img01");
            var captionText = $("#caption");

            modal.css("display", "block");
            modalImg.attr("src", img);
            captionText.innerHTML = this.alt;
        },
        add_other_friuts: function (group_id, other) {

                var data = {
                    "questionnaire_id": this.questionnaire_id,
                    "question_id": group_id,
                    "name": other.name,
                    "value1": other.value1,
                    "value2": other.value2,
                    "value2other": other.value2other,
                }
                this.errors = [];

                this.$http.post('/api/questionnaire_ans_other', data).then((myresponse) => {
                    if(myresponse.data.errors == false)
                    {
                        other.name = '';
                        other.value1 = '';
                        other.value2 = '';
                        other.value2other = '';

                        this.getother();
                    }
                    else
                    {
                        this.errors = myresponse.data.errors;
                    }
                },(myresponse) =>{
                    // error callback
                    console.log(myresponse.data);
                });

        },
        edit_other_friuts : function (group_id, other) {
            var data = {
                "questionnaire_id": this.questionnaire_id,
                "question_id": group_id,
                "other": other
            }
            this.errors = [];

            this.$http.put('/api/questionnaire_ans_other/'+other.id, data).then((myresponse) => {
                if(myresponse.data.errors == false)
                {
                    other.name = '';
                    other.value1 = '';
                    other.value2 = '';
                    other.value2other = '';

                    this.getother();
                }
                else
                {
                    this.errors = myresponse.data.errors;
                }
            },(myresponse) =>{
                // error callback
                console.log(myresponse.data);
            });
        },
        remove_other_friuts: function( other) {
            swal({
                title: "ต้องการลบข้อมูลใช่หรือไม่?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "ใช่",
                cancelButtonText: "ยกเลิก",
                closeOnConfirm: false
            }, (callback) => {
                console.log(callback);

                this.$http.delete('/api/questionnaire_ans_other/'+other.id).then((myresponse) => {
                    this.getother();
                },(myresponse) =>{
                    // error callback
                    console.log(myresponse.data);
                });

                swal.close();
            });


        },
        noeatall : function (group) {
            if(group.check == true){
                for(i=0 ; i<group.questions.length ; i++){
                    group.questions[i].value1 = 1;
                    this.groupupdate(group, group.questions[i])

                }
            }
            else{
                for(i=0 ; i<group.questions.length ; i++){
                    group.questions[i].value1 = '';
                    this.groupupdate(group, group.questions[i])

                }
            }

        },

        getother : function () {
            this.$http.get('/api/questionnaire_ans_other/'+this.questionnaire_id).then(function(myresponse) {
                this.others1 = myresponse.data.friut;
                this.others2 = myresponse.data.vetgetable;

            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
        submit : function() {
            if (this.count_miss == 0 ) {
                var param = {'questionnaire': this.groups,
                    'profile_id':$('#profile_id').val() ,
                    'timestart' : this.timestart
                }
                this.errors = [];

                if($('#questionnaire_id').val() == 0){
                    if(this.questionnaire_id == ''){
                        this.$http.post('/api/questionnaire', param).then((myresponse) => {
                            if(myresponse.data.errors == false)
                            {
                                // swal({
                                //     title: "Success",
                                //     text: "(Auto Close)",
                                //     timer: 1000,
                                //     showConfirmButton: false
                                // });
                                this.questionnaire_id = myresponse.data.data.id;
                                window.location.href = "/make/"+$('#profile_id').val()+'/'+this.questionnaire_id ;

                            }
                            else
                            {
                                this.errors = myresponse.data.errors;
                                // $('html, body').animate({scrollTop: 0}, 'fast');
                            }
                        },(myresponse) =>{
                            // error callback
                            console.log(myresponse.data);
                        });
                    }else {
                        if($('user_type').val() == 3){
                            setTimeout(function(){ window.location.href = "/history/"+$('#user_id').val(); }, 2000);
                        }
                        else{
                            setTimeout(function(){ window.location.href = "/member"; }, 2000);
                        }
                    }

                }
                else{
                    this.$http.put('/api/questionnaire/'+$('#questionnaire_id').val() , param).then((myresponse) => {
                        if(myresponse.data.errors == false)
                        {
                            swal({
                                title: "บันทึกข้อมูลแล้ว",
                                text: "(ปิดอัตโนมัติ)",
                                timer: 1000,
                                showConfirmButton: false
                            });

                            if($('user_type').val() == 3){
                                setTimeout(function(){ window.location.href = "/history/"+$('#user_id').val(); }, 2000);
                            }
                            else{
                                setTimeout(function(){ window.location.href = "/member"; }, 2000);
                            }
                        }
                        else
                        {
                            this.errors = myresponse.data.errors;
                            $('html, body').animate({scrollTop: 0}, 'fast');
                        }
                    },(myresponse) => {
                        // error callback
                        console.log(myresponse.data);
                    });
                }


            }
            else{
                this.noti = 1;
                var str = this.topic_miss.join();
                swal({
                    title: "คุณยังตอบคำถามไม่ครบ กรุณาตอบข้อมีแถบสีแดงในหมวด",
                    text: str
                });
            }
        }

	}
});