Vue.config.devtools = true;

new Vue({
	el:'#app',
	data: {
        choice:{
        	name:'',
			level:''
		},
        noti:0,
        question:{
            group_id: $('#group_id').val(),
            name:'',
            question_ans:[],
            note: '',
		},
        groups: [],
        units: [],
		errors: []
	},
    mounted: function(){
        this.getgroups();
        // this.getunit();
    },
	methods: {
        getgroups: function(){
            this.$http.get('/api/group/'+$('#group_id').val()).then(function(myresponse) {

                this.groups = myresponse.data;

                var selector = 'ul .list-choice';

                $(selector).on('click', function(){
                    $(selector).removeClass('active');
                    $(this).addClass('active');
                });

            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
        addChoice: function(choice){
        	if(choice.name != '' && choice.level != ''){
        		var obj = {
        			name: choice.name,
					level: choice.level
				}
                this.question.question_ans.push(obj);
                this.choice.name = '';
                this.choice.level = '';
			}

		},
        delChoice: function(choice, index){
            this.question.question_ans.splice(index,1);
        },

        getunit: function () {
            this.$http.get('/api/get_unit').then(function(myresponse) {
                this.units = myresponse.data;
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
		savequestion: function(){

            this.$http.post('/api/questions',this.question).then(function(myresponse) {
            	if(myresponse.data.errors == false){
            		location.reload();
				}
                // this.groups = myresponse.data;
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
		},

        edit_method: function (question) {
            window.location.href = '/question/edit/'+question.id;
        },
        del_method: function(question){

            this.$http.delete('/api/questions/'+question.id ).then(function(myresponse) {
                location.reload();
                // this.groups = myresponse.data;
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
  }
});