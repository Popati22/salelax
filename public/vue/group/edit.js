Vue.config.devtools = true;

new Vue({
	el:'#app',
	data: {
		id:'',
        choice:{
            name:'',
            level:''
        },

        question:{
            questions_id: $('#group_id').val(),
            name:'',
            question_ans:[],
            note: '',
        },

        groups: [],
        units: [],
		errors: []
	},
    mounted: function(){
        this.getquestions();
        // this.getunit();
    },
	methods: {
        getquestions: function(){
            this.$http.get('/api/questions/'+$('#question_id').val()).then(function(myresponse) {
                this.question = myresponse.data;

            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
        addChoice: function(choice){
            if(choice.name != '' && choice.level != ''){
                var obj = {
                    name: choice.name,
                    level: choice.level
                }
                this.question.question_ans.push(obj);
                this.choice.name = '';
                this.choice.level = '';
            }
        },
        delChoice: function(choice, index){

            this.$http.delete('/api/questionsans/'+choice.id).then(function(myresponse) {
                this.question.question_ans.splice(index,1);

            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
        getunit: function () {
            this.$http.get('/api/get_unit').then(function(myresponse) {
                this.units = myresponse.data;
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
        },
		savequestion: function(){
            this.$http.put('/api/questions/'+this.question.id,this.question).then(function(myresponse) {
                // this.groups = myresponse.data;
				if(myresponse.data.errors == false){
                    window.location.href = '/question/'+myresponse.data.data.group.id;
                }
                else{
					this.errors = myresponse.data.errors;
				}
            }, function(myresponse) {
                // error callback
                console.log(myresponse.data);
            });
		},
	    onFileChange(e) {
	      var files = e.target.files || e.dataTransfer.files;
	      if (!files.length)
	        return;
	      this.createImage(files[0]);
	    },
	    createImage(file) {
	      var image = new Image();
	      var reader = new FileReader();
	      var vm = this;
	
	      reader.onload = (e) => {
	        vm.image = e.target.result;
	      };
	      reader.readAsDataURL(file);
	    },
	    removeImage: function (e) {
	      this.image = '';
	    },
        zoom_img(img){
            var modal = $('#myModalImg');
            var modalImg = $("#img01");
            var captionText = $("#caption");

            modal.css("display", "block");
            modalImg.attr("src", img);
            captionText.innerHTML = this.alt;
        }
  }
});