Vue.config.devtools = true;
Vue.component('v-select', VueSelect.VueSelect);

new Vue({
    el:'#app',
    data: {
        email:'',
        password:'',
        password_confirmation: '',

        errors: []
    },
    mounted: function(){
    },
    methods: {

        submit : function () {
            this.errors = [];
                var profile = {
                    "email": this.email,
                    "password": this.password,
                    "password_confirmation": this.password_confirmation,
                }

                this.$http.post('/api/users',profile).then(function(myresponse) {
                    if(myresponse.data.errors == false){
                        swal({
                            title: "ลงทะเบียนเสร็จสมบูรณ์",
                            text: "ต้องการล็อคอินหรือไม่",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#0d991f",
                            confirmButtonText: "ใช่",
                            cancelButtonText: "ไม่",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm){
                            if (isConfirm) {
                                window.location.href = '/login';
                            } else {
                                window.location.href = '/register';

                            }
                        });
                    }else{
                        this.errors = myresponse.data.errors;
                        $('html, body').animate({ scrollTop: 0 }, 'fast');
                    }
                }, function(myresponse) {
                    // error callback
                    console.log(myresponse.data);
                });


        }

    }
});