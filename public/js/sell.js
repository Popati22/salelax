
// add images sell_images_add
$(function() {
    $('.image-editor').cropit({
      exportZoom: 1.25,
      imageBackground: true,
      imageBackgroundBorderWidth: 20,
      imageState: {
        src: 'http://lorempixel.com/500/400/',
      },
    });

    $('.rotate-cw').click(function() {
      $('.image-editor').cropit('rotateCW');
    });
    $('.rotate-ccw').click(function() {
      $('.image-editor').cropit('rotateCCW');
    });

    $('.export').click(function() {
      var imageData = $('.image-editor').cropit('export');
      window.open(imageData);
    });
});



//====  table add data   =====
 //clon box table
 //    $("#add-data").click(function(){
 //     var f1 = $(".f-name").val();
 //     var f2 = $(".f-price").val();
 //     var f3 = $(".f-amount").val();
 //     var div_time ='<tr class="h4">'+
 //          '<td>'+f1+'</td>'+
 //          '<td class="tx-center">'+f2+'</td>'+
 //          '<td class="tx-center">'+f3+'</td>'+
 //          '<td class="tx-center">'+
 //          '<a href="javascript:void(0);" class="remove-bin">'+
 //          '<img src="../image/sell/icon-bin.png" class="img-responsive icon-bin">'+
 //          '</a>'+
 //          '</td>'+
 //        '</tr>';
 //
 //      if(f1 !='' && f2 != '' && f3 != ''){
 //        $(".table-body-sell").append(div_time);
 //        $(".f-name").val('');
 //        $(".f-price").val('');
 //        $(".f-amount").val('');
 //        remove_row_table();
 //      }
 //  });

remove_row_table();

function remove_row_table(){

  $(".remove-bin").bind( "click", function() {
     //alert('test');
      $(this).parents("tr").remove();
  });
}


//=== sell :set date and time add ===
  //set date
     $(function () {
        // $('.datetimepicker_from').datetimepicker({
        //    format: 'DD/MM/YYYY',
        //    useCurrent: true,
        //    ignoreReadonly: true
        // });
        // $('.datetimepicker_todate').datetimepicker({
        //     format: 'DD/MM/YYYY',
        //     useCurrent: false, //Important! See issue #1075
        //     ignoreReadonly: true
        // });
        // $(".datetimepicker_from").on("dp.change", function (e) {
        //     $('.datetimepicker_todate').data("DateTimePicker").minDate(e.date);
        // });
        // $(".datetimepicker_todate").on("dp.change", function (e) {
        //     $('.datetimepicker_from').data("DateTimePicker").maxDate(e.date);
        // });

        $('.ca-addon1').click(function(){
            $('.datetimepicker_from').data("DateTimePicker").show();
        });
        $('.ca-addon2').click(function(){
            $('.datetimepicker_todate').data("DateTimePicker").show();
        });
    });

     //set times
    $(function () {
        // $('.datetimepicker1').datetimepicker({
        //    format: 'LT',
        //    ignoreReadonly: true
        // });
        //  $('.datetimepicker2').datetimepicker({
        //    format: 'LT',
        //    ignoreReadonly: true
        // });

        $('.add-t1').click(function(){
            $('.datetimepicker1').data("DateTimePicker").show();
        });

        $('.add-t2').click(function(){
            $('.datetimepicker2').data("DateTimePicker").show();
        });
    });

 
    //check dropdown
    var dp0 = 1;
    $("#dp0").find(".dp-down").hide();
    $("#dp0").find(".dp-up").show();
     // document.getElementById("checkbox_place").checked = true;
    $(document).on('click', '#dp0', function() {
        if(dp0 == 1){
          $("#dp0").find(".dp-down").show();
          $("#dp0").find(".dp-up").hide();
          document.getElementById("checkbox_place").checked = false;
          dp0 = 0;     
        }else{
          $("#dp0").find(".dp-down").hide();
          $("#dp0").find(".dp-up").show();          
          dp0 = 1;
          document.getElementById("checkbox_place").checked = true;
        }    
    });

    var dp1 = 1;
    $("#dp1").find(".dp-down").hide();
    $("#dp1").find(".dp-up").show();
    $(document).on('click', '#dp1', function() {
        if(dp1 == 1){
          $("#dp1").find(".dp-down").show();
          $("#dp1").find(".dp-up").hide();
          dp1 = 0;     
        }else{
          $("#dp1").find(".dp-down").hide();
          $("#dp1").find(".dp-up").show();          
          dp1 = 1;
        }    
    });

    var dp2 = 1;
    $("#dp2").find(".dp-down").hide();
    $("#dp2").find(".dp-up").show();
    $(document).on('click', '#dp2', function() {
        if(dp2 == 1){
          $("#dp2").find(".dp-down").show();
          $("#dp2").find(".dp-up").hide();
          dp2 = 0;     
        }else{
          $("#dp2").find(".dp-down").hide();
          $("#dp2").find(".dp-up").show();          
          dp2 = 1;
        }    
    });

//====  ADD PlACE ===
$("#add-place").click(function(){
    var place = $(".input-place").val();
    var meeting = $(".input-meet").val();
    //alert(place+' '+meeting);
    var div_place ='<div class="col-md-12 input-bt-gr mg-top-20 no-padd">'+                              
                    '<div class="input-group">'+
                    '<span class="input-group-btn">'+
                    '<button class="btn bg-none" type="button">'+
                    '<span class="glyphicon glyphicon-map-marker tx-i-blue"></span>'+
                    '</button>'+
                    '</span>'+
                    '<input type="text" class="input-lg input-sty1 form-control no-padd" value="'+place+'">'+
                    '</div>'+                                              
                    '</div>'+
                    '<div class="col-md-12 input-bt-gr mg-top-20 input-sty-gray">'+                              
                    '<input type="text" class="input-lg input-sty1  form-control no-padd"  value="'+meeting+'" />'+                                                
                    '</div>';
    if(place!='' && meeting!=''){
        $(".div-place").append(div_place);
        $(".input-place").val('');
        $(".input-meet").val('');
    }

});

//clon box times
//     $("#add-time").click(function(){
//      var t1 = $(".t1").val();
//      var t2 = $(".t2").val();
//      //alert(t1+' '+t2);
//      var div_time ='<div class="row-time"><div class="col-sm-4  col-xs-12 no-padd input-bt-gr" >'+
//                     '<div class="form-group">'+
//                     '<div class="input-group ">'+
//                     '<input type="text" class="form-control input-sty1 bg-none" value="'+t1+'" readonly="redaonly"/>'+
//                     '</div>'+
//                     '</div>'+
//                     '</div>'+
//                     '<div class="col-sm-2  col-xs-12 tx-center" >'+
//                     '-'+
//                     '</div>'+
//                     '<div class="col-sm-4  col-xs-12 no-padd input-bt-gr" >'+
//                     '<div class="form-group">'+
//                     '<div class="input-group ">'+
//                     '<input type"=text" class="form-control input-sty1 bg-none" value="'+t2+'" readonly="redaonly"/>'+
//                     '</div>'+
//                     '</div>'+
//                     '</div>'+    
//                     '<div class="col-sm-2 col-xs-12 tx-center" >'+
//                     '<a href="javascript:void(0);" class="remove-time">'+
//                     '<b class="badge badge-minus">'+
//                     '<span class="glyphicon glyphicon-minus tx-i-red"></span>'+
//                     '</b>'+
//                     '</a>'+
//                     '</div></div>';
//       if(t1 !='' && t2 != ''){
//         $(".box-times").append(div_time);
//         remove();
//       }
//   });

// remove();

// function remove(){
//   $(".remove-time" ).bind( "click", function() {
//      $(this).parents(".row-time").remove();
//   });
// }



// slide show 
$(document).ready(function () {
    $('.carousel').carousel({
        interval: false
    })
});

// slide show 
// $(document).ready(function () {
//     $('#c-slide2').carousel({
//         interval: 0
//     })
// });


//==== modal sell confirm  =====

$(document).on('click', '.confirm-modal', function() {
   $('#sell_confirmModal').modal('show');
});


$(document).on('click', '.btn-sell-detail', function() {
   $('#sell_detail').modal('show');
});


//==== sell accept list panel ===
$(document).on('click', '.a-sell-panel', function() {
  var check = $(this).hasClass("in");
   if(check){      
      $(this).removeClass('in');
      $(this).find('.glyphicon-menu-down').removeClass('hide');
      $(this).find('.glyphicon-menu-up').addClass('hide');
   }else{
      $(this).addClass('in');
      $(this).find('.glyphicon-menu-down').addClass('hide');
      $(this).find('.glyphicon-menu-up').removeClass('hide');      
   }
});

//==== sell accept detail modal ====
$(document).on('click', '.sell-cancle', function() {
    $('#sell_cancle_modal').modal('show');
});

$(document).on('click', '.sell-detail-modal', function() {
   $('#accept_detail_modal').modal('show');
});


$(document).on('click', '.img_modal', function() {
  var path = $(".path_img").val();
  $('#modal_images').modal('show');
  document.getElementById("tag-img").src = path; 
});


$(document).on('click', '.btn-total', function() {
   $('#detail_total').modal('show');
});



$(document).on('click', '.btn-change-total', function() {
  $('#warnning_deliver_modal').modal('show');
});

$(document).on('click', '.btn-change-total1', function() {
  $('#warnning_deliver_modal').modal('hide');
  $('#change_deliver_modal').modal('show');
});

$(document).on('click', '.btn-change-total2', function() {
  $('#warnning_deliver_modal').modal('hide');
  $('#cancle_deliver_modal').modal('show');
});