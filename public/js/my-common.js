$(document).ready(function() {

    $(function () {
        // $('#datetimepicker1').datetimepicker();
    });

    $(document).on('click','#datetime', function(){
        // $('#datetimepicker1').focus();
       
    });
   
    $(".btn-order-dropdown.dropdown-menu").on('click', 'li a', function(){
         $(this).parent().parent().siblings(".btn:first-child").html('<span class="mg-right-10" style="font-size:20px;">'+$(this).text()+'</span> <span class="caret"></span>');
         $(this).parent().parent().siblings(".btn:first-child").val($(this).text());
     });
     $(".btn-location-point.dropdown-menu").on('click', 'li a', function(){
         $(this).parent().parent().siblings(".btn:first-child").html('<span class="mg-right-10 text-location-point">'+$(this).text()+'</span><span style="font-size:5px;" class="glyphicon glyphicon-menu-down menu-down-location-point grey-font"></span>');
         $(this).parent().parent().siblings(".btn:first-child").val($(this).text());
     });

     /*** image-slider (order page) ***/ 
     $('.venobox').venobox({
        titleattr: 'data-title',
        numeratio: true,
        infinigall: true
    }); 

    //sort 
    $(document).on('click', '.dropdown-menu.price-dropdown li a', function(){
        $('#select_price').html($(this).text());
    });
    $(document).on('click', '.dropdown-menu.category-dropdown li a', function(){
        $('#select_category').html($(this).text());
    });

    //shop hover
    $('.item-hover').mouseover(function(){
        $(this).parent().addClass('buy');
        $(this).find('.buy-count-hover').show();
    });
    $('.item-hover').mouseleave(function(){
        $(this).parent().removeClass('buy');
        $(this).find('.buy-count-hover').hide();
    });
    
    
    /*** HISTORY PAGE ***/
    var ratingId;
    var ratingScore = 0;
    var ratingExp = 0;
    $(document).on('click', '.rating-btn', function(){ 
      
        ratingId = $(this).parent().attr('id');
        $(".rateYo1").rateYo("option", "rating", 0); 
        $('#ratingModal').modal('show');
    });

    $(document).on('click', '.rating-next', function() {
        if(ratingScore != 0 && ratingId != undefined) {
           $('#'+ ratingId).html( `<button class="buyer-star-btn pull-right read-rating-btn" >
                                        <div class="rateYo3" id="rate-` + ratingId + `" data-rateyo-rating="`+ ratingScore + `"></div>
                                    </button>`);
        }

        $(".rateYo3").rateYo({
            "precision": 0,
            "starWidth": "20px",
            "normalFill": "#a1a1a1bf",
            "ratedFill": "#FEBA12",
            onSet: function (rating, rateYoInstance) {
               $("#rate-" + ratingId).attr('data-rateyo-rating', "" + rating);
            }
        }); 

        $('#ratingModal').modal('hide');
        $(".rateYoExp").rateYo("option", "rating", 0); 
        $('#experienceModal').modal('show');
    });

   
    $(document).on('click', '.rating-done', function() {
        $('#'+ ratingId).append('<input type="hidden" value="'+ratingExp+'" id="rateExp-'+ratingId +'">');
        $('#experienceModal').modal('hide');
    });
    /*** START RATING  ***/

    var $rateYoByCustomer = $(".rateYo2").rateYo({
        "precision": 0,
        "starWidth": "20px",
        "normalFill": "#a1a1a1bf",
        "ratedFill": "#FEBA12"
    });
    
    var $rateYo = $(".rateYo1").rateYo({
        "rating": 0,
        "precision": 0,
        "starWidth": "40px",
        "normalFill": "#a1a1a1bf",
        "ratedFill": "#FEBA12",
        onSet: function (rating, rateYoInstance) {
            ratingScore = rating;
        }
    });

    var $rateYoExp = $(".rateYoExp").rateYo({
        "rating": 0,
        "precision": 0,
        "starWidth": "40px",
        "normalFill": "#a1a1a1bf",
        "ratedFill": "#FEBA12",
        onSet: function (rating, rateYoInstance) {
            ratingExp = rating;
        }
    });
   

    $rateYo.rateYo("option", "onChange", function () {
        /* get the rated fill at the current point of time */
        var ratedFill = $rateYo.rateYo("option", "ratedFill");
        // console.log("The color of rating is " + ratedFill);
     });
    
    /* set the option `multiColor` to show Multi Color Rating */
    $rateYo.rateYo("option", "multiColor", false);
    

    /*tab*/
    $(document).on('click', '#history_buy_tab', function() {
        $(this).find('img').attr('src', '/image/app/white_buy_icon.png');
        $('#history_sale_tab').find('img').attr('src', '/image/app/grey_sale_icon.png');

    });

    $(document).on('click', '#history_sale_tab', function() {
        $(this).find('img').attr('src', '/image/app/white_sale_icon.png');
        $('#history_buy_tab').find('img').attr('src', '/image/app/grey_buy_icon.png');

    })
    /*** END HISTORY PAGE ***/
    
    /*** START Market PAGE ***/
    $(document).on('click', '#add-market-btn', function() {
        $('#addModal').modal('show');
     });

     /* Market group */
     $(document).on('click', '.leave-group', function() {
        $('#leaveGroup').modal('show');
     });
     $(document).on('click', '.confirm-leave', function() {
       
        $('#leaveGroup').modal('hide');
        $('#leave-already').modal('show');
     });
     $(document).on('click', '.cancel-red-btn', function() {
       
        $('#leaveGroup').modal('hide');
     });
 
     $(document).on('click', '.invite-friend', function() {
        $('#inviteGroup').modal('show');
     });

     $(document).on('click', '.modal-join-btn.invite-sent', function() {
         $('#inviteGroup').modal('hide');
         $('#invite-already').modal('show');
     });



   

     $(document).on('click', '#administrator', function() {
        $('#admin-modal').modal('show');
    });

    $(document).on('click','#add-admin-btn', function() {   
        $('#admin-modal').modal('hide');
        // alert('test');
        $('#addAdmin').modal('show');              
    })
     /** STOP page */
    $(document).on('click', '.confirm-order-btn', function() {
       $('#order-sent').modal('show');    
    });

     

     /** MARKET join **/
     $(document).on('click', '.market-join', function() {
        
        var market_name = $(this).find('.recommend-market').text();
        var img = $(this).find('.recommend-image').attr('src');
        var view = $(this).find('.recommend-count-view.center-block').text();
        var create = $(this).find('input[name="create"]').val();
        var location = $(this).find('input[name="location"]').val();
        var header =  `<div class="image">
                            <img  class="recommend-image" src="` + img + `">
                            <div class="recommend-market-modal">
                                <span >` + market_name + `</span>
                            </div>
                        </div>`;
        $('#joinModal .modal-title').html(header);
        $('#joinModal #count-view-market').html(view);
        $('#joinModal .modal-create-date').html(create);
        $('#joinModal  .m-location').html(location);
        
       $('#joinModal').modal('show');
    });

    $(document).on('click', '.marget-join-group', function() {
        var market_name = $(this).find('.market-group-name').text();
        var img = $(this).find('.recommend-image-group').attr('src');
        var view = $(this).find('.market-group-count-view').text();
        var create = $(this).find('input[name="create"]').val();
        var location = $(this).find('input[name="location"]').val();
        var description = $(this).find('input[name="description"]').val();
        var header =  `<div class="image">
                            <img  class="recommend-image" src="` + img + `">
                            <div class="recommend-market-modal">
                                <span >` + market_name + `</span>
                            </div>
                        </div>`;

        $('#joinModal .modal-title').html(header);
        $('#joinModal #count-view-market').html(view);
        $('#joinModal .modal-create-date').html(create);
        $('#joinModal  .m-location').html(location);
        $('#joinModal  #market_description').html(description);
        
       $('#joinModal').modal('show');
    });

    $(document).on('click', '.join-market-btn', function(){
        $('#joinModal').modal('hide');
        $('#join-successed').modal('show');

    });
    /*** reserv  ****/
    $(document).on('click', '.re-model', function() {
        $('#reservModal').modal('show');
     });

      /*** profile ****/
     $(document).on('click', '.blue-capsule-feedback', function() {
        $('#feedback').modal('show');
     });
     $(document).on('click', '.send-feedback-btn', function() {
         $('#feedback').modal('hide');
     });

     
     /** add admin on search **/
     $(document).on('click', '.select-admin', function() {
        $(this).find('img').attr('src', '/image/app/grey_circle.png');
        $(this).removeClass('select-admin');
        $(this).addClass('not-select-admin');
        var my_parent = $(this).parent().parent('.grey-capsule');
        var my_id = $(my_parent).attr('id');
        $('#select-' + my_id).remove();

    });

    $(document).on('click', '.not-select-admin', function() {
        event.preventDefault();
        $(this).find('img').attr('src', '/image/app/green_circle.png');
        $(this).removeClass('not-select-admin');
        $(this).addClass('select-admin');

        var my_parent = $(this).parent().parent('.grey-capsule');
        var my_profile = $(my_parent).find('img.user-profile').attr('src');
        var my_name = $(my_parent).find('.my-name').text();
        var my_id = $(my_parent).attr('id');

        $('.current-admin').append(`<div style="background-color:#DEDDDD;" id="select-`+ my_id +`">
                <div class="member-list light-grey-border w-100" >
                    <div class="col-xs-3"><a href="#" class="text-center">
                        <img class="img-responsive mg-top-10" style="height:40px;width:40px;border-radius:50%;" src="` + my_profile + `" /></a></div>
                    <div class="col-xs-6 no-padding blue-font mg-top-15">` + my_name + `</div>  
                    <div class="col-xs-3">
                    </div>                       
                </div>
             </div>`);
    });
    

     /*** market recommend slider */
     // store the slider in a local variable
     


     /** shop order [flexslider click show venobox] */
     $(document).on('click', '.click-venobox', function() {
        $('.modal-venobox').find('.venobox img').trigger('click');
     });
     
    
   });

 
   (function() {
       
        // store the slider in a local variable
        var $window = $(window),
            flexslider = { vars:{} };
       
        // tiny helper function to add breakpoints
        function getGridSizeMemberGroupInside() {
          return (window.innerWidth < 400) ? 5 :
                (window.innerWidth < 600) ? 6 :
                (window.innerWidth < 980) ? 8 :
                (window.innerWidth < 1000) ? 10:
                (window.innerWidth < 1200) ? 13 :
                (window.innerWidth < 1274) ? 15 :
                (window.innerWidth < 1255) ? 16 :
                 (window.innerWidth < 1300) ? 18 : 20;
        }

        function getGridSizeRecommendMarket() {
            return (window.innerWidth < 800) ? 1 : 2;
            
        }

        function getGridSizeSellingProfile() {
            return (window.innerWidth < 400) ? 2 :
                 (window.innerWidth < 600) ? 3 :
                   (window.innerWidth < 900) ? 4 : 5;
          }
      

       
        $window.load(function() {
          $('.flexslider.flex-market-group').flexslider({
            animation: "slide",
            animationLoop: false,
            itemWidth: 50,
            itemMargin: 2,
            minItems: getGridSizeMemberGroupInside(), // use function to pull in initial value
            maxItems: getGridSizeMemberGroupInside() // use function to pull in initial value
          });

          $('.flexslider.recommend-market-slider').flexslider(
            {
                animation: "slide",
                animationLoop: true,
                itemWidth: 50,
                itemMargin: 2,
                minItems: getGridSizeRecommendMarket(), // use function to pull in initial value
                maxItems: getGridSizeRecommendMarket() // use function to pull in initial value
              });

            $('.flexslider.order-image').flexslider({
                animation: "slide",
                minItems:1,
                maxItems:1
            });
            $('.flexslider.profile-selling-slider').flexslider({
                animation: "slide",
                animationLoop: true,
                itemWidth: 210,
                itemMargin: 5,
                minItems: getGridSizeSellingProfile(), // use function to pull in initial value
                maxItems: getGridSizeSellingProfile() // use function to pull in initial value
            });
        });
       
        // check grid size on resize event
        $window.resize(function() {
            var gridSize;
            if( $('.flexslider.recommend-market-slider').length == 1){
                gridSize = getGridSizeRecommendMarket();
            } 
            else if ( $('.flexslider.profile-selling-slider').length == 1) {
                gridSize = getGridSizeSellingProfile();
            }
            else {
                gridSize = getGridSizeMemberGroupInside();
            }
            flexslider.vars.minItems = gridSize;
            flexslider.vars.maxItems = gridSize;
        });
   
      }());
